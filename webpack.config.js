const Encore = require('@symfony/webpack-encore');
const LiveReloadPlugin = require('webpack-livereload-plugin');
const glob = require('glob');
const path = require('path');
const fs = require('fs');
const _ = require('lodash');

// Génère la configuration avec l'aide de webpack-encore
Encore
    .setOutputPath('web/assets/')
    .setPublicPath('/assets')
    .cleanupOutputBeforeBuild()

    .enableLessLoader()
    .enableSourceMaps(!Encore.isProduction())

    .autoProvideVariables({
        '$': 'jquery',
        'jQuery': 'jquery',
        'window.jQuery': 'jquery',
        'moment': 'moment',
        'window.moment': 'moment',
        '_': 'lodash',
        'window._': 'lodash'
    })

    .addEntry('locales/en', ['@IrsteaBdohInternationalisationBundle/locales/en'])
    .addEntry('locales/fr', ['@IrsteaBdohInternationalisationBundle/locales/fr'])

    .createSharedEntry('vendor', ['jquery', 'lodash', 'moment'])

    .addPlugin(new LiveReloadPlugin())
;

const bundleAliases = findBundleAliases();

const bundleEntries = findBundleEntries(bundleAliases, /^@IrsteaBdoh(\w*)Bundle/);
_.each(bundleEntries, (module, entry) => Encore.addEntry(entry, module));

const config = Encore.getWebpackConfig();

const vendor = `${__dirname}/vendor`;
_.assign(
    config.resolve.alias,
    bundleAliases,
    {
        'translator$': `${vendor}/willdurand/js-translation-bundle/Resources/js/translator.js`,
        'sonataadmin': `${vendor}/sonata-project/admin-bundle/Resources/public`,
        'sonatacore': `${vendor}/sonata-project/core-bundle/Resources/public`
    }
);

module.exports = config;

/**
 * @returns {{string: string}}
 */
function findBundleAliases() {
    const aliases = {};

    // Enregistre les dossiers 'Resources/assets' de nos bundles sous la forme '@BundleName'
    for (const assetPath of glob.sync(`${__dirname}/src/**/*Bundle/Resources/assets/`)) {
        const bundlePath = assetPath.substr(0, assetPath.lastIndexOf('Resources/assets'));
        const [bundleClassFile] = glob.sync(`${bundlePath}*Bundle.php`);
        const bundleName = bundleClassFile.substr(bundlePath.length, bundleClassFile.length - bundlePath.length - 4);
        aliases[`@${bundleName}`] = assetPath.substr(0, assetPath.length - 1);
    }

    return aliases;
}

/**
 * @param {{}} aliases
 * @param {RegExp} nameRegex
 * @returns {{string: string}}
 */
function findBundleEntries(aliases, nameRegex) {
    const entries = {};
    _.each(aliases, (assetPath, alias) => {
        const snakeAlias = _.snakeCase(alias.replace(nameRegex, '$1'));

        for (const entryPath of glob.sync(`${assetPath}/entries/**/*.js`)) {
            const modulePath = extractModulePath(entryPath);
            if (!modulePath) {
                continue;
            }
            const moduleName = alias + modulePath.substr(assetPath.length);
            let entryName = snakeAlias + modulePath.substr(assetPath.length + 8);
            if (entryName[0] === '/') {
                entryName = entryName.substr(1);
            }
            entries[entryName] = moduleName;
        }
    });
    return entries;
}

/**
 * @param {string} entryPath
 * @returns {string|boolean}
 */
function extractModulePath(entryPath) {
    if (path.basename(entryPath) === 'index.js') {
        return path.dirname(entryPath);
    }
    if (fs.existsSync(`${path.dirname(entryPath)}/index.js`)) {
        return false;
    }
    return entryPath.substr(0, entryPath.length - 3);
}
