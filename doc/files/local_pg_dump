#!/usr/bin/env bash

DUMPPATH=/var/backups/postgresql

# Execute un commande en tant que postgres
function as_postgres() {
  su - postgres -c "$*"
}

# Effectue la rotation du fichier $FNAME en n'en gardant au maximum $MAX
function rotate_file() {
  local FNAME="$1" MAX=$2 N=$3
  if [[ -e "$FNAME.$N" ]]; then
    if [[ $N < $MAX ]]; then
      N2=$[$N + 1]
      rotate_file "$FNAME" $MAX $N2
      mv "$FNAME.$N" "$FNAME.$N2"
    else
      rm "$FNAME.$N"
    fi
  fi
}

# Dump une base de données
function dump_db() {
  local DBNAME="$1"

  # Nom du fichier de backup
  DUMPFILE="$DUMPPATH/$DBNAME.dump"

  # Dump la base
  if as_postgres "pg_dump --format=custom --compress=9 --dbname=$DBNAME --file=$DUMPFILE.new" >/dev/null; then
    if [[ -e "$DUMPFILE" ]]; then
      rotate_file "$DUMPFILE" 4 1
      mv "$DUMPFILE" "$DUMPFILE.1"
    fi
    mv "$DUMPFILE.new" "$DUMPFILE"
  else
    rm -f "$DUMPFILE.new"
    echo "Error dumping db $DBNAME!" >&2
  fi
}

# Cree le repertoire de backup s'il n'existe pas
[[ -d $DUMPPATH ]] || mkdir -p $DUMPPATH
chown -R postgres.postgres $DUMPPATH

# Pour chaque base de donnee
for DBNAME in `as_postgres "psql -t -l" | grep "|" | cut -f1 -d'|'`; do

  case "$DBNAME" in
    postgres|template0|template1)
      # Ne backup pas les bases "modeles" de postgresql
    ;;
    *)
      # Backup toutes les autres
      dump_db "$DBNAME"
    ;;
  esac

done

