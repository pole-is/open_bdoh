#!/usr/bin/env bash
set -eux

# Définit des variables par défaut
: "${TEST_USER:=devops}"
: "${TEST_HOME:=/home/$TEST_USER}"
: "${BDOH_VERSION:=v4.0.2}"
: "${BDOH_HOST:=localhost}"

# Génère le script à testr
(
  cat <<EOF
#!/usr/bin/env bash
set -eux
export DEBIAN_FRONTEND=noninteractive

EOF
  sed -e '/^```shell$/,/^```$/!d' -e '/^```/d' guide_installation_OpenBDOH.md
) >script.sh

# Crée l'image du container
docker build \
  --file "$(dirname "$0")/Dockerfile" \
  --build-arg "TEST_USER=$TEST_USER" \
  --build-arg "TEST_HOME=$TEST_HOME" \
  --build-arg "BDOH_VERSION=$BDOH_VERSION" \
  ${http_proxy:+--build-arg "http_proxy=$http_proxy"} \
  ${no_proxy:+--build-arg "no_proxy=$no_proxy"} \
  --tag openbdoh-test \
  .

# Démarre le container
CIDFILE="${TMP:-/tmp}/cid.$$"

docker run --rm --detach --privileged \
  --cidfile "$CIDFILE"  \
  --publish "0.0.0.0:8354:80" \
  --hostname openbdoh.example.com \
  openbdoh-test

CID="$(cat "$CIDFILE" && rm -f "$CIDFILE")"

# Nettoie en sortant
finally() {
  # Affiche les logs, s'il y en a
  docker logs "$CID"
  # Arrête le conteneur
  docker stop -t5 "$CID"
}
trap finally EXIT

# Récupère le résultat du premier healthcheck après la période de démarrage
read STATUS < <(docker events -f "container=$CID" -f 'event=health_status' --format='{{.Status}}')

case "$STATUS" in
  *": healthy") ;; # Démarrage opk
  *) # Problème de démarrage
    docker inspect --format '{{json .State.Health.Log}}' "$CID" | jq
    # Affiche les services HS
    docker exec "$CID" systemctl --failed
    exit 1
  ;;
esac

# Lance le script dans le conteneur
if ! docker exec \
      --user "$TEST_USER" \
      --workdir "$TEST_HOME" \
      --env HOME="$TEST_HOME" \
      --tty \
      "$CID" \
      /bin/bash -lc ./script.sh
then
  # En cas d'échec, lance un shell dans le conteneur si on est dans un shell interactif (i.e. pas en CI)
  [[ -t 0 ]] && docker exec -it "$CID" /bin/bash -il
  exit 1
fi

# Exécute une requête pour vérifier le bon fonctionnement
curl -sSf http://${BDOH_HOST}:8354/ | grep 'Base de Données des Observatoires en Hydrologie'

