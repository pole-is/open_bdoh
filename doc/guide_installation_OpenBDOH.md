Guide d'installation d'OpenBDOH
===============================

# Prérequis

## Serveur
Ce guide est rédigé pour une installation sur un serveur ou machine virtuel vérifiant les prérequis suivants :
  * système d'exploitation : [système d'exploitation Debian](https://www.debian.org/), version Buster
  * dimensionnement du serveur :
    * RAM : 4 Go
    * vCPU : 4
    * Disque système : 10 Go
    * Disque base de données Postgresql : 32 Go pour 70 millions de mesures.
  * la locale `fr_FR.UTF-8` et le fuseau horaire sont configurés, selon la localisation du serveur
  * un compte utilisateur non-administrateur a été crée. Il peut utiliser `sudo` pour exécuter des commandes en tant que superutillisateur (`root`)
  * un archive des [sources d'OpenBDOH](https://gitlab.irstea.fr/pole-is/open_bdoh/-/tree/master).

Il est possible d'installer facilement OpenBDOH sur une distribution Linux dérivée de Debian, comme Ubuntu Linux, par exemple. En revanche, l'installation sur une autre distribution, comme RedHad, CentOS..., nécessiterait des adaptations dans les commandes, les noms de paquets, les chemins de fichiers... qui ne seront pas abordées dans ce guide.

## Communications sécurisées

Pour la mise en place de communications sécurisée (protocole HTTPS au lieu de HTTP), il est nécessaire d'obtenir :
  * un nom d'hôte complet inscrit dans le DNS,
  * une clef privée ainsi qu'un certificat SSL correspondant au nom ci-dessous.

## Conventions

Pour la suite de ce guide, les informations suivantes seront utilisées dans les exemples. Veillez à adapter à votre cas.
  * nom du site web : `OPENBDOH.EXAMPLE.COM`
  * compte non-privilégié : `DEVOPS`
  * chemin d'installation : `/VAR/WWW/BDOH`
  * mot de passe de la base de données : `MotDePasseOpenBDOH`
  * chemin de l'archive OpenBDOH : `/home/DEVOPS/bdoh.tar.bz2`

Les commandes à exécuter sont indiquées dans des blocs comme ci-dessous. Il est prévu de les exécuter en tant que `DEVOPS`.
```
echo "une commande"
```

# Procédure d'installation

## Installation des paquets systèmes

Certaines versions des logiciels n'étant pas disponibles dans la version de base de Debian, des dépôts APT tierce-partie sont utilisés :
* le [dépôt APT de PHP par Ondřej Surý](https://deb.sury.org/),
* le [dépôt APT de NodeJS par NodeSource](https://github.com/nodesource/distributions#deb).

* Installer les paquets nécessaires pour utiliser les dépôts tierces.
```shell
sudo apt-get install -yq apt-transport-https ca-certificates wget lsb-release gpg
```
* Configurer APT pour utiliser les dépôts APT supplémentaires :
```shell
sudo wget -O /usr/share/keyrings/php.gpg https://packages.sury.org/php/apt.gpg
echo "deb [signed-by=/usr/share/keyrings/php.gpg] https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php.list
wget -qO - https://deb.nodesource.com/gpgkey/nodesource.gpg.key | gpg --dearmor | sudo tee /usr/share/keyrings/nodesource.gpg >/dev/null
echo "deb [signed-by=/usr/share/keyrings/nodesource.gpg] https://deb.nodesource.com/node_12.x $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/nodesource.list
```
* Installer l'ensemble des packages nécessaires :
```shell
sudo apt-get update -yq
sudo apt-get install -yq acl apache2 cron libpq-dev nodejs php7.1 php7.1-apcu php7.1-apcu-bc php7.1-common php7.1-common php7.1-gd php7.1-intl php7.1-json php7.1-mbstring php7.1-pgsql php7.1-pgsql php7.1-sodium php7.1-xml php7.1-xml php7.1-zip postgis postgresql postgresql-client postgresql-11-postgis-2.5 postgresql-11-postgis-2.5-scripts git-core unzip
```

## Installation d'OpenBDOH
```shell
sudo mkdir -p -m 02755 /VAR/WWW/BDOH
sudo chown DEVOPS:www-data /VAR/WWW/BDOH
tar xfj /home/DEVOPS/bdoh-v4.0.2.tar.bz2 --directory=/VAR/WWW/BDOH --strip-components=1
```

# Paramétrage de PostgreSQL
* Configurer les modes de connexion :
```shell
# Ajoute une ligne 'local	all	DEVOPS peer' après la ligne 'local ... postgres'
# et remplace la ligne 'local all all peer' par 'local all all md5'
sudo sed -i \
  -e '/^local.*postgres/alocal all DEVOPS peer' \
  -e '/^local.*all.*all.*peer/clocal all all md5' \
  /etc/postgresql/11/main/pg_hba.conf
```
* (Ré)démarrer Postgresql :
```shell
sudo systemctl restart postgresql
```
* Créer les utilisateurs DEVOPS et bdoh :
```shell
sudo -u postgres psql <<EOF
CREATE USER DEVOPS SUPERUSER;
CREATE USER bdoh WITH PASSWORD 'MotDePasseOpenBDOH';
EOF
```
* Créer la base de données BDOH :
```shell
psql template1 DEVOPS <<'EOF'
CREATE DATABASE bdoh ENCODING 'UTF8' LC_COLLATE "fr_FR.UTF-8" LC_CTYPE "fr_FR.UTF-8";
ALTER DATABASE bdoh SET search_path = "bdoh", "$user", public;
\c bdoh
CREATE EXTENSION postgis;
CREATE SCHEMA bdoh;
CREATE SCHEMA temp AUTHORIZATION bdoh;
EOF
```
* (optionel) Installer le script de backup automatique de la base de données :
```shell
sudo cp files/local_pg_dump /etc/cron.daily/
sudo chmod 0555 /etc/cron.daily/local_pg_dump
```

# (Optionel) Accès rapide à la base de données pour l'utilisateur DEVOPS
On configure les variables d'environnement PG* dans ~/.profile et on enregistre le mot de passe dans ~/.pgpass.
```shell
echo "export PGDATABASE=bdoh PGUSER=DEVOPS PGHOST=/run/postgresql PGPORT=5432" >>~/.profile
echo "localhost:5432:bdoh:bdoh:MotDePasseOpenBDOH" >~/.pgpass
chmod 0400 ~/.pgpass
```

# Environnement de l'utilisateur DEVOPS
On configure les variables d'environnement SYMFONY_ENV et SYMFONY_DEBUG.
```shell
echo "export SYMFONY_ENV=prod SYMFONY_DEBUG=0" >>~/.profile
. ~/.profile
```

# Rotation des logs
On ajoute un fichier de configuration pour l'outil logrotate :
```shell
sudo tee /etc/logrotate.d/bdoh <<EOF
/VAR/WWW/BDOH/app/logs/*.log {
	weekly
	missingok
	rotate 52
	compress
	delaycompress
	notifempty
	su www-data www-data
}
EOF
```

# Configuration du protocole SSL :
* afin d'offrir aux utilisateurs une connexion HTTPS, permettant de chiffrer les données échangées avec OpenBDOH il s'avère nécessaire de configurer le protocole SSL. Pour ce faire plusieurs étapes sont nécessaires :
	- obtenir les certificats SSL
	- installer les certificats SSL sur le serveur OpenBDOH :
		- /etc/ssl/certs/ pour le certificat public
		- /etc/ssl/private/ pour la clef privée
	- configurer Apache2 afin d'utiliser les certificats nouvellement installés

# Paramétrage de PHP :
* activer les modules PHP nécessaires :
```shell
sudo tee /etc/php/7.1/mods-available/owasp-security.ini <<'EOF'
; priority=99
session.use_trans_sid = 0
session.use_cookies = 1
session.use_only_cookies = 1
session.entropy_file = /dev/urandom
session.entropy_length = 16
display_errors = Off
display_startup_errors = Off
expose_php = Off
EOF
sudo phpenmod -v 7.1 apcu apcu_bc ctype gd iconv intl json mbstring pdo pdo_pgsql pgsql posix readline sodium tokenizer xml xsl zip owasp-security
```

# Installation de Composer et Yarn :
* Installer Composer :
```shell
wget -O - https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer --version=1.10.22
```
* installer Yarn :
```shell
sudo npm install --global --production yarn
```

# Déploiement de l'application
* Se placer dans le répertoire d'installation :
```shell
cd /VAR/WWW/BDOH
```
* Installer les librairies
```shell
composer install --verbose --prefer-dist --no-progress --no-interaction --no-dev --optimize-autoloader --no-suggest
yarn install --pure-lockfile
```
* Configurer l'application. Pour cela, il faut modifier le fichier `/VAR/WWW/BDOH/app/config/parameters.yml` qui a été généré à l'étape précédente pour renseigner les paramètres tels ques :
  - `database_*` : les informations de connexion à la base de données,
  - `mailer_*` : le informations de connexion au serveur STMP sortant,
  - `secret` : un "secret" pour la sécurisation des sessions,
  - `theia_password_secret_key` : un "secret" pour la sécurisation des mots de passe Theia ; ce doit être une valeur de 32 octets en hexadécimal (i.e. 64 caractères),
  - `router.request_context.host` : le nom d'hôte.
  - les paramètres `logo_long_web_link`, `logo_short_web_link` et `mini_logo_sonata` peuvent être modifiés pour afficher des logos ; le chemin est relatif à `/VAR/WWW/BDOH/web`.
```shell
# Exemple :
cat >/VAR/WWW/BDOH/app/config/parameters.yml <<'EOF'
parameters:
    database_driver:   pdo_pgsql
    database_host:     /run/postgresql
    database_port:     5432
    database_name:     bdoh
    database_user:     bdoh
    database_password: MotDePasseOpenBDOH
    database_server_version: 11

    mailer_transport:  smtp
    mailer_host:       smtp
    mailer_user:       ~
    mailer_password:   ~

    secret: f07fb2a3cec442aa23f1dc6a25db0171

    trusted_proxies:    []

    dev_toolbar:       false
    project_guest_path: /var/www
    project_host_path:  .

    theia_password_secret_key: f680e41b1f4f08e5199654803255774b64ef7b9022cf023063e455cc87e4f7e7
    uri_theia_ozcar: https://in-situ.theia-land.fr
    uri_theia_send_data: https://in-situ.theia-land.fr/data/
    uri_theia_ozcar_schema_json: https://in-situ.theia-land.fr/json-schema/pivotSchemaDraft7.json
    logo_long_web_link: images/Logo_INRAE_REPUBLIQUE.png
    logo_short_web_link: images/Logo_INRAE.png
    mini_logo_sonata: images/MiniLogo_INRAE_REPUBLIQUE.png

    router.request_context.host: 'OPENBDOH.EXAMPLE.COM'
EOF
```
* Régénérer le cache de Symfony
```shell
app/console cache:clear --no-interaction
```
* Générer les ressources front-end
```shell
composer install-assets --no-interaction
yarn run encore production
```
* Définir les permissions sur les dossiers :
```shell
mkdir -p app/{cache,logs,imports,exports} web/{uploads,css/themes}
setfacl -R -m u:www-data:rwX -m u:DEVOPS:rwX app/{cache,logs,imports,exports} web/{uploads,css/themes}
setfacl -R -d -m u:www-data:rwX -m u:DEVOPS:rwX app/{cache,logs,imports,exports} web/{uploads,css/themes}
```
* Appliquer les migrations de la base de données :
```shell
app/console doctrine:migrations:migrate --no-interaction --allow-no-migration --write-sql
# La commande a généré un fichier doctrine_migration_AAAAMMJJHHMMSS.sql, on l'exécute :
psql bdoh DEVOPS <doctrine_migration_*.sql
```

# Paramétrage d'Apache2 :
* Créer la configuration du site :
```shell
sudo tee /etc/apache2/sites-available/openbdoh.conf <<'EOF'
ServerName openbdoh.example.org

CustomLog /var/log/apache2/openbdoh-access.log common
ErrorLog /var/log/apache2/openbdoh-error.log

DocumentRoot /VAR/WWW/BDOH/web

ServerTokens Prod
ServerSignature Off
TraceEnable Off
Header set X-Content-Type-Options: "nosniff"
Header set X-XSS-Protection: "1; mode=block"
Header set X-Frame-Options: "sameorigin"

<Directory /VAR/WWW/BDOH/web>
  SetEnv           SYMFONY_ENV prod
  Require          all granted
  AllowOverride    None
  Options          FollowSymLinks
  FallbackResource /app.php
</Directory>

<LocationMatch ^/?(assets|bundles|css|documents|images|uploads)/>
  FallbackResource disabled
</LocationMatch>
EOF
```
* Désactiver tous les sites, modules et fichiers de configuration :
```shell
sudo a2dismod -f '*'
sudo a2disconf -f '*'
sudo a2dissite 000-default
```
* Activer les sites, modules et fichiers de configuration nécessaires :
```shell
sudo a2enmod php7.1 alias authz_core deflate dir env expires headers mime negotiation rewrite setenvif unique_id
sudo a2ensite openbdoh
```
* Tester la configuration et redémarrer le service Apache2 :
```shell
sudo apache2ctl configtest
sudo systemctl restart apache2
```

## Installation de l'éxécuteur de jobs
* Installer le service bdoh-job-worker
```shell
sudo cp /VAR/WWW/BDOH/app/config/bdoh-job-worker.service /etc/systemd/system/
sudo chown root:root /etc/systemd/system/bdoh-job-worker.service
sudo chmod 644 /etc/systemd/system/bdoh-job-worker.service
sudo systemctl daemon-reload
sudo systemctl enable --now bdoh-job-worker.service
```
