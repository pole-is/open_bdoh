{
    "name": "irstea/bdoh",
    "description": "BD Observatoires Hydrologiques",
    "type": "project",
    "license": "AGPL-3.0-or-later",
    "homepage": "https://gitlab.irstea.fr/pole-is/bdoh",
    "readme": "README.md",
    "authors": [
        {
            "name": "Flora Branger",
            "role": "Product Owner"
        },
        {
            "name": "Fabien Thollet",
            "role": "Product Owner"
        },
        {
            "name": "Guillaume Perréal",
            "role": "Developper"
        },
        {
            "name": "Luc Jamet",
            "role": "Developper"
        },
        {
            "name": "Benoit Vila",
            "role": "Developper"
        },
        {
            "name": "Mathieu Poisbeau",
            "role": "Developper"
        },
        {
            "name": "Maxime Crochemore",
            "role": "Developper"
        },
        {
            "name": "Thibault Degiuli",
            "role": "Developper"
        },
        {
            "name": "Daniel Gesche",
            "role": "Developper"
        },
        {
            "name": "Nicolas Raidelet",
            "role": "Developper"
        }
    ],
    "support": {
        "issues": "https://gitlab.irstea.fr/pole-is/bdoh/-/issues"
    },
    "autoload": {
        "psr-0": {
            "Irstea\\": "src/"
        },
        "psr-4": {
            "Irstea\\Bdoh\\Migrations\\": "app/DoctrineMigrations/"
        },
        "classmap": [
            "app/AppKernel.php",
            "app/AppCache.php"
        ]
    },
    "autoload-dev": {
        "psr-0": {
            "Irstea\\BdohBundle\\Tests\\": "src/",
            "Irstea\\BdohDataBundle\\Tests\\": "src/",
            "Irstea\\BdohSecurityBundle\\Tests\\": "src/"
        }
    },
    "config": {
        "sort-packages": true,
        "preferred-install": "dist",
        "platform": {
            "php": "7.1.33"
        }
    },
    "archive": {
        "exclude": [
            ".deploy",
            ".idea",
            "reports",
            "dev",
            ".git*",
            "deploy.php",
            "docker-compose.yml"
        ]
    },
    "prefer-stable": true,
    "require": {
        "php": ">=7.1",
        "ext-ctype": "*",
        "ext-gd": "*",
        "ext-hash": "*",
        "ext-iconv": "*",
        "ext-intl": "*",
        "ext-json": "*",
        "ext-mbstring": "*",
        "ext-pdo": "*",
        "ext-pgsql": "*",
        "ext-posix": "*",
        "ext-sodium": "*",
        "ext-xsl": "*",
        "ext-zip": "*",
        "doctrine/cache": "1.7.0",
        "doctrine/collections": "^1.4",
        "doctrine/common": "^2.7",
        "doctrine/data-fixtures": "^1.2",
        "doctrine/dbal": "^2.5.13",
        "doctrine/doctrine-bundle": "^1.4",
        "doctrine/doctrine-cache-bundle": "^1.3",
        "doctrine/doctrine-fixtures-bundle": "^2.3",
        "doctrine/doctrine-migrations-bundle": "^1.2",
        "doctrine/migrations": "^1.5",
        "doctrine/orm": "^2.4.8",
        "gregwar/captcha-bundle": "v1.0.11",
        "guzzlehttp/guzzle": "6.5.5",
        "incenteev/composer-parameter-handler": "^2.0",
        "jms/aop-bundle": "^1.3",
        "jms/di-extra-bundle": "~1.8.1",
        "jms/job-queue-bundle": "^1.4",
        "jms/security-extra-bundle": "^1.5",
        "jms/serializer": "^1.14",
        "knplabs/knp-menu-bundle": "~2.1.0",
        "ocramius/proxy-manager": "^1.0",
        "opis/json-schema": "^1.1",
        "oyejorge/less.php": "v1.7.0.3",
        "pagerfanta/pagerfanta": "^1.0",
        "psr/log": "^1.1",
        "sensio/distribution-bundle": "^5.0",
        "sensio/framework-extra-bundle": "^3.0.2",
        "sonata-project/admin-bundle": "~3.23.0",
        "sonata-project/block-bundle": "~3.13.0",
        "sonata-project/core-bundle": "~3.5.1",
        "sonata-project/doctrine-orm-admin-bundle": "^3.0",
        "swiftmailer/swiftmailer": "^5.4",
        "symfony/config": "^2.8",
        "symfony/console": "^2.8",
        "symfony/dependency-injection": "^2.8",
        "symfony/doctrine-bridge": "^2.8",
        "symfony/event-dispatcher": "^2.8",
        "symfony/form": "^2.8",
        "symfony/framework-bundle": "^2.8",
        "symfony/http-foundation": "^2.8",
        "symfony/http-kernel": "^2.8",
        "symfony/monolog-bundle": "^2.4",
        "symfony/options-resolver": "^2.8",
        "symfony/property-access": "^2.8",
        "symfony/routing": "^2.8",
        "symfony/security": "^2.8",
        "symfony/security-bundle": "^2.8",
        "symfony/serializer": "^2.8",
        "symfony/swiftmailer-bundle": "^2.3",
        "symfony/translation": "^2.8",
        "symfony/twig-bridge": "^2.8",
        "symfony/twig-bundle": "^2.8",
        "symfony/validator": "^2.8",
        "symfony/yaml": "^2.8",
        "twig/extensions": "^1.5.4",
        "twig/twig": "^1.28",
        "willdurand/js-translation-bundle": "^2.6",
        "willdurand/negotiation": "^2.3"
    },
    "require-dev": {
        "dama/doctrine-test-bundle": "^4.0",
        "deployer/deployer": "^6.0",
        "irstea/composer-require-checker-shim": "^2.0",
        "irstea/phpcpd-shim": "^4.1",
        "irstea/php-cs-fixer-config": "^3.0.2",
        "irstea/phploc-shim": "^4.0",
        "irstea/phpmd-config": "^1.0.0",
        "irstea/plantuml-bundle": "^0.1",
        "mikey179/vfsstream": "^1.6",
        "php-parallel-lint/php-parallel-lint": "^1.2",
        "phpstan/extension-installer": "^1.0.5",
        "phpstan/phpstan": "~0.12.53",
        "phpstan/phpstan-doctrine": "~0.12.22",
        "phpunit/phpunit": "^5.7",
        "roave/security-advisories": "dev-master",
        "sensio/generator-bundle": "^2.3",
        "symfony/browser-kit": "^2.8",
        "symfony/debug-bundle": "^2.8",
        "symfony/web-profiler-bundle": "^2.8"
    },
    "replace": {
        "symfony/polyfill-ctype": "1.99",
        "symfony/polyfill-intl-icu": "1.99",
        "symfony/polyfill-intl-idn": "1.99",
        "symfony/polyfill-intl-normalizer": "1.99",
        "symfony/polyfill-mbstring": "1.99",
        "symfony/polyfill-php54": "1.99",
        "symfony/polyfill-php55": "1.99",
        "symfony/polyfill-php56": "1.99",
        "symfony/polyfill-php70": "1.99",
        "symfony/polyfill-php71": "1.99"
    },
    "extra": {
        "symfony-assets-install": "relative",
        "symfony-app-dir": "app",
        "symfony-web-dir": "web",
        "incenteev-parameters": {
            "file": "app/config/parameters.yml",
            "env-map": {
                "database_host": "POSTGRES_HOST",
                "database_port": "POSTGRES_PORT",
                "database_name": "POSTGRES_DB",
                "database_user": "POSTGRES_USER",
                "database_password": "POSTGRES_PASSWORD",
                "database_server_version": "POSTGRES_VERSION",
                "mailer_host": "SMTP_HOST",
                "project_guest_path": "PROJECT_GUEST_PATH",
                "project_host_path": "PROJECT_HOST_PATH"
            }
        }
    },
    "scripts": {
        "pre-install-cmd": "@force-permissions",
        "pre-update-cmd": "@force-permissions",
        "post-install-cmd": "@symfony-scripts",
        "post-update-cmd": "@symfony-scripts",
        "force-permissions": "`which sudo 2>/dev/null || true` chown -R `id -u`:`id -g` app vendor || true",
        "symfony-scripts": [
            "`which sudo 2>/dev/null || true` rm -rf app/cache/${SYMFONY_ENV:-dev} || true",
            "Sensio\\Bundle\\DistributionBundle\\Composer\\ScriptHandler::buildBootstrap",
            "Incenteev\\ParameterHandler\\ScriptHandler::buildParameters",
            "@php app/console cache:warmup"
        ],
        "install-assets": [
            "@php app/console asset:install -n --symlink --relative",
            "@php app/console bazinga:js-translation:dump -n src/Irstea/BdohInternationalisationBundle/Resources/assets --merge-domains --format=json"
        ],
        "update-database": "@db:migrate",
        "unit-tests": "@test:phpunit",
        "database-tests": "@test:phpunit:database",
        "db:reset": [
            "test \"$SYMFONY_ENV\" != \"prod\" #",
            "@php app/console doctrine:database:drop -n --force --if-exists #",
            "@php app/console doctrine:database:create -n --if-not-exists #"
        ],
        "db:migrate": [
            "@php app/console doctrine:query:sql 'CREATE SCHEMA IF NOT EXISTS bdoh;' #",
            "@php app/console doctrine:migrations:migrate -n --allow-no-migration #"
        ],
        "fix-cs": "@format",
        "format": "@php vendor/bin/php-cs-fixer fix",
        "phploc": "@php vendor/bin/phploc app src --exclude=app/cache",
        "test": [
            "@test:lint",
            "@test:php-cs-fixer",
            "@test:phpstan",
            "@test:phpcpd",
            "@test:phpmd",
            "@test:phpunit",
            "@test:phpunit:database",
            "@test:security-checker",
            "@test:composer-require-checker"
        ],
        "test:lint": [
            "@php app/console lint:yaml app/config",
            "@php app/console lint:yaml app/Resources",
            "@php app/console lint:yaml src",
            "@php app/console lint:twig app/Resources",
            "@php app/console lint:twig src",
            "@php vendor/bin/parallel-lint --exclude app/cache src app"
        ],
        "test:php-cs-fixer": "@format --dry-run",
        "test:phpcpd": "@php vendor/bin/phpcpd app src --exclude=cache --fuzzy",
        "test:phpstan": "@php vendor/bin/phpstan analyse",
        "test:phpmd": "@php vendor/bin/phpmd app,src ansi phpmd-ruleset.xml --exclude 'app/cache/*,app/DoctrineMigrations/*'",
        "test:phpunit": "@php vendor/bin/phpunit --group=unit",
        "test:phpunit:database": [
            "@php app/console doctrine:database:drop -e test -n --force --if-exists #",
            "@php app/console doctrine:database:create -e test -n --if-not-exists #",
            "@php app/console doctrine:query:sql -e test 'CREATE SCHEMA IF NOT EXISTS bdoh;' #",
            "@php app/console doctrine:migrations:migrate -e test -n --allow-no-migration -q #",
            "@php app/console doctrine:fixtures:load -e test --purge-with-truncate -n --fixtures=src/Irstea/BdohDataBundle/DataFixtures/ORM/Test/ --fixtures=src/Irstea/BdohSecurityBundle/DataFixture/ORM/Test #",
            "@php vendor/bin/phpunit --group=database"
        ],
        "test:security-checker": "@composer update --dry-run --quiet roave/security-advisories",
        "test:composer-require-checker": "@php vendor/bin/composer-require-checker --config-file=$PWD/.composer-require-checker.json"
    }
}
