<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohLoggerBundle\Entity;

use Symfony\Component\Translation\TranslatorInterface;

/**
 * HistoriqueAdministrationChronique.
 */
class HistoriqueAdministrationChronique extends HistoriqueAdministration
{
    /**
     * @var \Irstea\BdohDataBundle\Entity\Chronique
     */
    protected $chronique;

    /**
     * Set chronique.
     *
     * @param \Irstea\BdohDataBundle\Entity\Chronique $chronique
     *
     * @return HistoriqueAdministrationChronique
     */
    public function setChronique(\Irstea\BdohDataBundle\Entity\Chronique $chronique = null)
    {
        $this->chronique = $chronique;

        return $this;
    }

    /**
     * Get chronique.
     *
     * @return \Irstea\BdohDataBundle\Entity\Chronique
     */
    public function getChronique()
    {
        return $this->chronique;
    }

    /**
     * @param TranslatorInterface $translator
     */
    public function constructDescription(TranslatorInterface $translator)
    {
        parent::constructDescription($translator);
        $this->setDescription(
            $this->getDescription() . "\n" .
            $translator->trans('historique.chronique') . $translator->trans('deux_points') . ' ' .
            $this->getChronique()
        );
    }
}
