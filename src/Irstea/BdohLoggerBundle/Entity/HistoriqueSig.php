<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohLoggerBundle\Entity;

use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class HistoriqueSig.
 */
class HistoriqueSig extends Historique
{
    /**
     * @var
     */
    protected $typeShape;

    /**
     * @var int
     */
    protected $nombreInserts;

    /**
     * @var int
     */
    protected $nombreMesures;

    /**
     * @return int
     */
    public function getNombreInserts()
    {
        return $this->nombreInserts;
    }

    /**
     * @param int $nombreInserts
     */
    public function setNombreInserts($nombreInserts)
    {
        $this->nombreInserts = $nombreInserts;
    }

    /**
     * @return int
     */
    public function getNombreMesures()
    {
        return $this->nombreMesures;
    }

    /**
     * @param int $nombreMesures
     */
    public function setNombreMesures($nombreMesures)
    {
        $this->nombreMesures = $nombreMesures;
    }

    /**
     * @return mixed
     */
    public function getTypeShape()
    {
        return $this->typeShape;
    }

    /**
     * @param $typeShape
     */
    public function setTypeShape($typeShape)
    {
        $this->typeShape = $typeShape;
    }

    /**
     * @param TranslatorInterface $translator
     */
    public function constructDescription(TranslatorInterface $translator)
    {
        $this->setDescription(
            $translator->trans('historique.action') . $translator->trans('deux_points') . ' ' .
            $translator->trans('historique.actions.ImportGeographique') . "\n" .
            $translator->trans('historique.typeShape') . $translator->trans('deux_points') . ' ' .
            $this->getTypeShape() . "\n" .
            $translator->trans('historique.nbSig') . $translator->trans('deux_points') . ' ' .
            ($this->getNombreInserts() + $this->getNombreMesures())
        );
    }
}
