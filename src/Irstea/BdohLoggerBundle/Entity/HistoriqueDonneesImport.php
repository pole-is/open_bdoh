<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohLoggerBundle\Entity;

use Symfony\Component\Translation\TranslatorInterface;

/**
 * HistoriqueDonneesImport.
 */
class HistoriqueDonneesImport extends HistoriqueDonnees
{
    /**
     * @var string
     */
    protected $formatImport;

    /**
     * @var string
     */
    protected $fuseau;

    /**
     * Set formatImport.
     *
     * @param string $formatImport
     *
     * @return HistoriqueDonneesImport
     */
    public function setFormatImport($formatImport)
    {
        $this->formatImport = $formatImport;

        return $this;
    }

    /**
     * Get formatImport.
     *
     * @return string
     */
    public function getFormatImport()
    {
        return $this->formatImport;
    }

    /**
     * Set fuseau.
     *
     * @param string $fuseau
     *
     * @return HistoriqueDonneesImport
     */
    public function setFuseau($fuseau)
    {
        $this->fuseau = $fuseau;

        return $this;
    }

    /**
     * Get fuseau.
     *
     * @return string
     */
    public function getFuseau()
    {
        return $this->fuseau;
    }

    /**
     * @param TranslatorInterface $translator
     */
    public function constructDescription(TranslatorInterface $translator)
    {
        parent::constructDescription($translator);
        $this->setDescription(
            $translator->trans('historique.action') . $translator->trans('deux_points') . ' ' .
            $translator->trans('historique.actions.MesurePlageImport') . "\n" .
            $this->getDescription() . "\n" .
            $translator->trans('historique.formatImport') . $translator->trans('deux_points') . ' ' .
            $translator->trans('import.' . $this->getFormatImport()) . "\n" .
            $translator->trans('historique.fuseau') . $translator->trans('deux_points') . ' ' . $this->getFuseau()
        );
    }
}
