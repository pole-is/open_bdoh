<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohLoggerBundle\Entity;

use Symfony\Component\Translation\TranslatorInterface;

/**
 * HistoriqueAdministrationFamilleParametres.
 */
class HistoriqueAdministrationFamilleParametres extends HistoriqueAdministration
{
    /**
     * @var \Irstea\BdohDataBundle\Entity\FamilleParametres
     */
    protected $familleParametres;

    /**
     * @var \Irstea\BdohDataBundle\Entity\FamilleParametres
     */
    protected $familleParente;

    /**
     * Set familleParametres.
     *
     * @param \Irstea\BdohDataBundle\Entity\FamilleParametres $familleParametres
     *
     * @return HistoriqueAdministrationFamilleParametres
     */
    public function setFamilleParametres(\Irstea\BdohDataBundle\Entity\FamilleParametres $familleParametres = null)
    {
        $this->familleParametres = $familleParametres;

        return $this;
    }

    /**
     * Get familleParametres.
     *
     * @return \Irstea\BdohDataBundle\Entity\FamilleParametres
     */
    public function getFamilleParametres()
    {
        return $this->familleParametres;
    }

    /**
     * Set familleParente.
     *
     * @param \Irstea\BdohDataBundle\Entity\FamilleParametres $familleParente
     *
     * @return HistoriqueAdministrationFamilleParametres
     */
    public function setFamilleParente(\Irstea\BdohDataBundle\Entity\FamilleParametres $familleParente = null)
    {
        $this->familleParente = $familleParente;

        return $this;
    }

    /**
     * Get familleParente.
     *
     * @return \Irstea\BdohDataBundle\Entity\FamilleParametres
     */
    public function getFamilleParente()
    {
        return $this->familleParente;
    }

    /**
     * @param TranslatorInterface $translator
     */
    public function constructDescription(TranslatorInterface $translator)
    {
        parent::constructDescription($translator);
        $inEnglish = \strpos($translator->getLocale(), 'en') === 0;
        $famille = $inEnglish ? $this->getFamilleParametres()->getNomEn() : $this->getFamilleParametres()->getNom();
        $parente = $this->getFamilleParente() ?
            ($inEnglish ? $this->getFamilleParente()->getNomEn() : $this->getFamilleParente()->getNom()) :
            $translator->trans('historique.noFamilleParente');
        $this->setDescription(
            $this->getDescription() . "\n" .
            $translator->trans('historique.familleParametres') . $translator->trans('deux_points') . ' ' .
            $famille . "\n" .
            $translator->trans('historique.familleParente') . $translator->trans('deux_points') . ' ' .
            $parente
        );
    }
}
