<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohLoggerBundle\Entity;

use Symfony\Component\Translation\TranslatorInterface;

/**
 * HistoriqueAdministrationSiteExperimental.
 */
class HistoriqueAdministrationSiteExperimental extends HistoriqueAdministration
{
    /**
     * @var \Irstea\BdohDataBundle\Entity\SiteExperimental
     */
    protected $siteExperimental;

    /**
     * Set siteExperimental.
     *
     * @param \Irstea\BdohDataBundle\Entity\SiteExperimental $siteExperimental
     *
     * @return HistoriqueAdministrationSiteExperimental
     */
    public function setSiteExperimental(\Irstea\BdohDataBundle\Entity\SiteExperimental $siteExperimental = null)
    {
        $this->siteExperimental = $siteExperimental;

        return $this;
    }

    /**
     * Get siteExperimental.
     *
     * @return \Irstea\BdohDataBundle\Entity\SiteExperimental
     */
    public function getSiteExperimental()
    {
        return $this->siteExperimental;
    }

    /**
     * @param TranslatorInterface $translator
     */
    public function constructDescription(TranslatorInterface $translator)
    {
        parent::constructDescription($translator);
        $this->setDescription(
            $this->getDescription() . "\n" .
            $translator->trans('historique.siteExperimental') . $translator->trans('deux_points') . ' ' .
            $this->getSiteExperimental()
        );
    }
}
