<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohLoggerBundle\Entity;

use Irstea\BdohDataBundle\Entity\TopicCategory;
use Symfony\Component\Translation\TranslatorInterface;

class HistoriqueAdministrationTopicCategory extends HistoriqueAdministration
{
    /**
     * @var TopicCategory
     */
    protected $topicCategory;

    /**
     * @return TopicCategory
     */
    public function getTopicCategory()
    {
        return $this->topicCategory;
    }

    /**
     * @param TopicCategory $topicCategory
     *
     * @return HistoriqueAdministrationTopicCategory
     */
    public function setTopicCategory(TopicCategory $topicCategory)
    {
        $this->topicCategory = $topicCategory;

        return $this;
    }

    /**
     * @param TranslatorInterface $translator
     */
    public function constructDescription(TranslatorInterface $translator)
    {
        parent::constructDescription($translator);
        $this->setDescription(
            $this->getDescription() . "\n" .
            $translator->trans('historique.topicCategory') . $translator->trans('deux_points') . ' ' .
            $this->getTopicCategory()
        );
    }
}
