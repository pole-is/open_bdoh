<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohLoggerBundle\Entity;

use Symfony\Component\Translation\TranslatorInterface;

/**
 * HistoriqueBaremesExport.
 */
class HistoriqueBaremesExport extends HistoriqueBaremes
{
    /**
     * @var \DateTime
     */
    private $dateCreation;

    /**
     * @var int
     */
    private $nValues;

    /**
     * Set dateCreation.
     *
     * @param \DateTime $dateCreation
     *
     * @return HistoriqueBaremesExport
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation.
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set nValues.
     *
     * @param int $nValues
     *
     * @return HistoriqueBaremesExport
     */
    public function setNValues($nValues)
    {
        $this->nValues = $nValues;

        return $this;
    }

    /**
     * Get nValues.
     *
     * @return int
     */
    public function getNValues()
    {
        return $this->nValues;
    }

    /**
     * @param TranslatorInterface $translator
     */
    public function constructDescription(TranslatorInterface $translator)
    {
        $this->setDescription(
            $this->actionLine($translator) .
            $this->nameLine($translator) .
            $translator->trans('bareme.dateCreation') . ' ' .
            $this->getDateCreation()->format($translator->trans('transformation.bareme.dateBareme')) . ' ' .
            $translator->trans('UTC') . "\n" .
            $this->unitsLine($translator) .
            $this->getNValues() . ' ' . $translator->trans('bareme.values') . "\n" .
            $this->minMaxLine($translator)
        );
    }
}
