<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohLoggerBundle\Entity;

use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class HistoriqueTransformations.
 */
class HistoriqueTransformations extends Historique
{
    /**
     * @var string
     */
    private $action;

    /**
     * Set action.
     *
     * @param string $action
     *
     * @return HistoriqueTransformations
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action.
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param TranslatorInterface $translator
     */
    public function constructDescription(TranslatorInterface $translator)
    {
        $this->setDescription(
            $translator->trans('historique.action') . $translator->trans('deux_points') . ' ' .
            $this->getAction()
        );
        //To be overwritten by subclasses.
    }

    /**
     * @param TranslatorInterface $translator
     *
     * @return string
     */
    public function actionLine(TranslatorInterface $translator)
    {
        return $translator->trans('historique.action') . $translator->trans('deux_points') . ' ' .
            $translator->trans($this->getAction()) . "\n";
    }
}
