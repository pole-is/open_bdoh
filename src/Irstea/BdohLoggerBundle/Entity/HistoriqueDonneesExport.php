<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohLoggerBundle\Entity;

use Symfony\Component\Translation\TranslatorInterface;

/**
 * HistoriqueDonneesExport.
 */
class HistoriqueDonneesExport extends HistoriqueDonnees
{
    /**
     * @var string
     */
    protected $formatExport;

    /**
     * @var string
     */
    protected $typeExport;

    /**
     * @var string
     */
    protected $timestep;

    /**
     * @var string
     */
    protected $fuseau;

    /**
     * Set formatExport.
     *
     * @param string $formatExport
     *
     * @return HistoriqueDonneesExport
     */
    public function setFormatExport($formatExport)
    {
        $this->formatExport = $formatExport;

        return $this;
    }

    /**
     * Get formatExport.
     *
     * @return string
     */
    public function getFormatExport()
    {
        return $this->formatExport;
    }

    /**
     * Set typeExport.
     *
     * @param string $typeExport
     *
     * @return HistoriqueDonneesExport
     */
    public function setTypeExport($typeExport)
    {
        $this->typeExport = $typeExport;

        return $this;
    }

    /**
     * Get typeExport.
     *
     * @return string
     */
    public function getTypeExport()
    {
        return $this->typeExport;
    }

    /**
     * Set timestep.
     *
     * @param string $timestep
     *
     * @return HistoriqueDonneesExport
     */
    public function setTimestep($timestep)
    {
        $this->timestep = $timestep;

        return $this;
    }

    /**
     * Get timestep.
     *
     * @return string
     */
    public function getTimestep()
    {
        return $this->timestep;
    }

    /**
     * Set fuseau.
     *
     * @param string $fuseau
     *
     * @return HistoriqueDonneesExport
     */
    public function setFuseau($fuseau)
    {
        $this->fuseau = $fuseau;

        return $this;
    }

    /**
     * Get fuseau.
     *
     * @return string
     */
    public function getFuseau()
    {
        return $this->fuseau;
    }

    /**
     * @param TranslatorInterface $translator
     */
    public function constructDescription(TranslatorInterface $translator)
    {
        parent::constructDescription($translator);

        $timestep = json_decode($this->getTimestep(), true);
        if (\strpos($translator->getLocale(), 'en') === 0) {
            $timestep = $timestep['libelleEn'];
        } else {
            $timestep = $timestep['libelle'];
        }

        $this->setDescription(
            $translator->trans('historique.action') . $translator->trans('deux_points') . ' ' .
            $translator->trans('historique.actions.MesurePlageExport') . "\n" .
            $this->getDescription() . "\n" .
            $translator->trans('historique.formatExport') . $translator->trans('deux_points') . ' ' .
            $translator->trans('export.' . ucfirst($this->getFormatExport())) . "\n" .
            $translator->trans('historique.typeExport') . $translator->trans('deux_points') . ' ' .
            $translator->trans('historique.exportTypes.' . $this->getTypeExport()) . "\n" .
            ($timestep === '' ? '' :
                $translator->trans('historique.timestep') . $translator->trans('deux_points') . ' ' .
                $timestep . "\n") .
            $translator->trans('historique.fuseau') . $translator->trans('deux_points') . ' ' . $this->getFuseau()
        );
    }
}
