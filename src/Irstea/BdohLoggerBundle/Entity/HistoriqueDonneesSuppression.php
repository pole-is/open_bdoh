<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohLoggerBundle\Entity;

use Symfony\Component\Translation\TranslatorInterface;

/**
 * HistoriqueDonneesSuppression.
 */
class HistoriqueDonneesSuppression extends HistoriqueDonnees
{
    /**
     * @var bool
     */
    protected $changedIntoGap;

    /**
     * Set changedIntoGap.
     *
     * @param bool $changedIntoGap
     *
     * @return HistoriqueDonneesSuppression
     */
    public function setChangedIntoGap($changedIntoGap)
    {
        $this->changedIntoGap = $changedIntoGap;

        return $this;
    }

    /**
     * Get changedIntoGap.
     *
     * @return bool
     */
    public function getChangedIntoGap()
    {
        return $this->changedIntoGap;
    }

    /**
     * {@inheritdoc}
     */
    public function constructDescription(TranslatorInterface $translator)
    {
        parent::constructDescription($translator);
        $this->setDescription(
            $translator->trans('historique.action') . $translator->trans('deux_points') . ' ' .
            $translator->trans('historique.actions.MesurePlageSuppression') . "\n" .
            ($this->changedIntoGap ? $translator->trans('historique.changedIntoGap') . "\n" : '') .
            $this->getDescription()
        );
    }
}
