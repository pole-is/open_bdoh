<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohLoggerBundle\Entity;

use Symfony\Component\Translation\TranslatorInterface;

/**
 * HistoriqueAdministrationTypeParametre.
 */
class HistoriqueAdministrationTypeParametre extends HistoriqueAdministration
{
    /**
     * @var \Irstea\BdohDataBundle\Entity\TypeParametre
     */
    protected $typeParametre;

    /**
     * @var \Irstea\BdohDataBundle\Entity\FamilleParametres
     */
    protected $familleParametres;

    /**
     * Set typeParametre.
     *
     * @param \Irstea\BdohDataBundle\Entity\TypeParametre $typeParametre
     *
     * @return HistoriqueAdministrationTypeParametre
     */
    public function setTypeParametre(\Irstea\BdohDataBundle\Entity\TypeParametre $typeParametre = null)
    {
        $this->typeParametre = $typeParametre;

        return $this;
    }

    /**
     * Get typeParametre.
     *
     * @return \Irstea\BdohDataBundle\Entity\TypeParametre
     */
    public function getTypeParametre()
    {
        return $this->typeParametre;
    }

    /**
     * Set familleParametres.
     *
     * @param \Irstea\BdohDataBundle\Entity\FamilleParametres $familleParametres
     *
     * @return HistoriqueAdministrationTypeParametre
     */
    public function setFamilleParametres(\Irstea\BdohDataBundle\Entity\FamilleParametres $familleParametres = null)
    {
        $this->familleParametres = $familleParametres;

        return $this;
    }

    /**
     * Get familleParametres.
     *
     * @return \Irstea\BdohDataBundle\Entity\FamilleParametres
     */
    public function getFamilleParametres()
    {
        return $this->familleParametres;
    }

    /**
     * @param TranslatorInterface $translator
     */
    public function constructDescription(TranslatorInterface $translator)
    {
        parent::constructDescription($translator);
        $inEnglish = \strpos($translator->getLocale(), 'en') === 0;
        $type = $inEnglish ? $this->getTypeParametre()->getNomEn() : $this->getTypeParametre()->getNom();
        $famille = $this->getFamilleParametres() ?
            ($inEnglish ? $this->getFamilleParametres()->getNomEn() : $this->getFamilleParametres()->getNom()) :
            $translator->trans('historique.noFamilleParametres');
        $this->setDescription(
            $this->getDescription() . "\n" .
            $translator->trans('historique.typeParametre') . $translator->trans('deux_points') . ' ' .
            $type . "\n" .
            $translator->trans('historique.familleParametres') . $translator->trans('deux_points') . ' ' .
            $famille
        );
    }
}
