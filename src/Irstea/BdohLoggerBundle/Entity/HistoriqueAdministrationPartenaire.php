<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohLoggerBundle\Entity;

use Symfony\Component\Translation\TranslatorInterface;

/**
 * HistoriqueAdministrationPartenaire.
 */
class HistoriqueAdministrationPartenaire extends HistoriqueAdministration
{
    /**
     * @var \Irstea\BdohDataBundle\Entity\Partenaire
     */
    protected $partenaire;

    /**
     * Set partenaire.
     *
     * @param \Irstea\BdohDataBundle\Entity\Partenaire $partenaire
     *
     * @return HistoriqueAdministrationPartenaire
     */
    public function setPartenaire(\Irstea\BdohDataBundle\Entity\Partenaire $partenaire = null)
    {
        $this->partenaire = $partenaire;

        return $this;
    }

    /**
     * Get partenaire.
     *
     * @return \Irstea\BdohDataBundle\Entity\Partenaire
     */
    public function getPartenaire()
    {
        return $this->partenaire;
    }

    /**
     * @param TranslatorInterface $translator
     */
    public function constructDescription(TranslatorInterface $translator)
    {
        parent::constructDescription($translator);
        $this->setDescription(
            $this->getDescription() . "\n" .
            $translator->trans('historique.partenaire') . $translator->trans('deux_points') . ' ' .
            $this->getPartenaire()
        );
    }
}
