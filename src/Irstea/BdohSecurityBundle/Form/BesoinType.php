<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohSecurityBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BesoinType extends AbstractType
{
    protected $em;

    protected $locale;

    public function __construct(\Doctrine\ORM\EntityManagerInterface $em, $locale = 'fr')
    {
        $this->em = $em;
        $this->locale = $locale;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $propertyObjectif = 'libelle';
        $propertyType = 'libelle';
        $propertyParametre = 'nom';
        $isLocaleEn = \strpos($this->locale, 'en') === 0;
        if ($isLocaleEn) {
            $propertyObjectif .= 'En';
            $propertyType .= 'En';
            $propertyParametre .= 'En';
        }

        $builder
            ->add(
                'stations',
                null,
                [
                    'label'         => 'besoin.stations',
                    'required'      => true,
                    'query_builder' => $this->em->getRepository('IrsteaBdohDataBundle:Station')->createQueryBuilder('st')->orderBy('st.nom', 'ASC'),
                ]
            )
            ->add('duree', 'text', ['label' => 'besoin.duree', 'required' => true])
            ->add(
                'objectif',
                null,
                [
                    'label'         => 'besoin.objectif',
                    'required'      => true,
                    'query_builder' => $this->em->getRepository('IrsteaBdohSecurityBundle:ObjectifRecherche')->createQueryBuilder('ore')->orderBy('ore.' . $propertyObjectif, 'ASC'),
                    'property'      => $propertyObjectif,
                ]
            )
            ->add(
                'type',
                null,
                [
                    'label'         => 'besoin.type',
                    'required'      => true,
                    'query_builder' => $this->em->getRepository('IrsteaBdohSecurityBundle:TypeTravaux')->createQueryBuilder('tt')->orderBy('tt.' . $propertyType, 'ASC'),
                    'property'      => $propertyType,
                ]
            )
            ->add(
                'parametres',
                null,
                [
                    'label'         => 'besoin.parametres',
                    'required'      => true,
                    'query_builder' => $this->em->getRepository('IrsteaBdohDataBundle:Observatoire')->createParametresQueryBuilder($isLocaleEn),
                    'property'      => $propertyParametre,
                ]
            )
            ->add(
                'details',
                'textarea',
                [
                    'label'    => 'besoin.details',
                    'required' => true,
                    'attr'     => ['class' => 'col-md-17', 'style' => 'height: 150px;'],
                ]
            );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'Irstea\BdohSecurityBundle\Entity\Besoin',
            ]
        );
    }

    public function getName()
    {
        return 'irstea_bdohsecuritybundle_besointype';
    }
}
