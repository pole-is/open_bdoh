<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohSecurityBundle\Form;

use Irstea\BdohSecurityBundle\Validator\Constraint\UserPassword;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * {@inheritdoc}
 */
class EditUtilisateurType extends UtilisateurType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            // Old password : virtual field
            ->add(
                'oldPassword',
                PasswordType::class,
                [
                    'required'    => false,
                    'mapped'      => false,
                    'constraints' => new UserPassword(),
                    'label'       => 'oldPassword',
                ]
            )
            // New password
            ->add('password', CheckedPasswordType::class, ['required' => false, 'label' => 'newPassword']);

        // Checks that 'oldPassword' and 'newPassword' are both empty, or both full.
        $builder->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) {
                $form = $event->getForm();
                $oldPassword = $form->get('oldPassword');
                $newPassword = $form->get('password');

                // oldPassword full && newPassword empty => error
                if ($oldPassword->getData()) {
                    if (!$newPassword->getData()) {
                        $newPassword->addError(new FormError('Utilisateur.newPassword.mandatory'));
                    }
                } // oldPassword empty && newPassword full => error
                elseif ($newPassword->getData()) {
                    $oldPassword->addError(new FormError('Utilisateur.oldPassword.mandatory'));
                }
            }
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'irstea_bdohsecuritybundle_editutilisateurtype';
    }
}
