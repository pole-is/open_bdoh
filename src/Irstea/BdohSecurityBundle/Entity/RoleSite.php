<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohSecurityBundle\Entity;

use Irstea\BdohDataBundle\Entity\SiteExperimental;
use Irstea\BdohDataBundle\Entity\SiteRelatedInterface;
use Irstea\BdohDataBundle\Entity\SiteRelatedTrait;

/**
 * Class RoleSite.
 */
class RoleSite extends Role implements SiteRelatedInterface
{
    use SiteRelatedTrait;

    /**
     * @var SiteExperimental
     */
    protected $site;

    /**
     * Set site.
     *
     * @param SiteExperimental $site
     */
    public function setSite(SiteExperimental $site)
    {
        $this->site = $site;
    }

    /**
     * Get site.
     *
     * @return SiteExperimental
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * {@inheritdoc}
     */
    protected function isTarget($target)
    {
        return $target instanceof SiteExperimental && $target->getId() === $this->site->getId();
    }

    /**
     * {@inheritdoc}
     */
    public function getTarget()
    {
        return $this->site;
    }

    /**
     * {@inheritdoc}
     */
    public function setTarget($target)
    {
        $this->setSite($target);
    }

    /**
     * {@inheritdoc}
     */
    public function getSites()
    {
        return [$this->site];
    }
}
