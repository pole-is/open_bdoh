<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohSecurityBundle\Entity;

use Irstea\BdohDataBundle\Entity\Chronique;
use Irstea\BdohDataBundle\Entity\ChroniqueRelatedInterface;
use Irstea\BdohDataBundle\Entity\ChroniqueRelatedTrait;

/**
 * Class RoleChronique.
 */
class RoleChronique extends Role implements ChroniqueRelatedInterface
{
    use ChroniqueRelatedTrait;

    /**
     * @var Chronique
     */
    protected $chronique;

    /**
     * Set chronique.
     *
     * @param Chronique $chronique
     */
    public function setChronique(Chronique $chronique)
    {
        $this->chronique = $chronique;
    }

    /**
     * Get chronique.
     *
     * @return Chronique
     */
    public function getChronique()
    {
        return $this->chronique;
    }

    /**
     * {@inheritdoc}
     */
    protected function isTarget($target)
    {
        return $target instanceof Chronique && $target->getId() === $this->chronique->getId();
    }

    /**
     * {@inheritdoc}
     */
    public function getTarget()
    {
        return $this->chronique;
    }

    /**
     * {@inheritdoc}
     */
    public function setTarget($target)
    {
        $this->setChronique($target);
    }
}
