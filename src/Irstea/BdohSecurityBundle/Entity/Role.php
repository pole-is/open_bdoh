<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohSecurityBundle\Entity;

/**
 * Class Role.
 */
abstract class Role
{
    const GESTIONNAIRE = 'gestionnaire';
    const CONTRIBUTEUR = 'contributeur';
    const UTILISATEUR = 'utilisateur';

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $valeur;

    /**
     * @var Utilisateur
     */
    protected $utilisateur;

    /**
     * Role constructor.
     *
     * @param Utilisateur|null $utilisateur
     * @param string|null      $valeur
     * @param mixed|null       $target
     */
    public function __construct(Utilisateur $utilisateur = null, $valeur = null, $target = null)
    {
        if ($utilisateur !== null) {
            $this->setUtilisateur($utilisateur);
        }
        if ($valeur !== null) {
            $this->setValeur($valeur);
        }
        if ($target !== null) {
            $this->setTarget($target);
        }
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set utilisateur.
     *
     * @param Utilisateur $utilisateur
     */
    public function setUtilisateur(Utilisateur $utilisateur)
    {
        $this->utilisateur = $utilisateur;
    }

    /**
     * Get utilisateur.
     *
     * @return Utilisateur
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Set valeur.
     *
     * @param string $valeur
     */
    public function setValeur($valeur)
    {
        $this->valeur = $valeur;
    }

    /**
     * Get valeur.
     *
     * @return string
     */
    public function getValeur()
    {
        return $this->valeur;
    }

    /**
     * @param mixed  $user
     * @param string $type
     * @param mixed  $target
     *
     * @return bool
     */
    public function matches($user, $type, $target)
    {
        return ($user instanceof Utilisateur && $user->getId() === $this->utilisateur->getId()) &&
            $type === $this->valeur &&
            $this->isTarget($target);
    }

    /**
     * @return bool
     */
    protected function isValid()
    {
        return $this->getTarget() !== null &&
            $this->utilisateur instanceof Utilisateur &&
            in_array($this->valeur, [self::CONTRIBUTEUR, self::GESTIONNAIRE, self::UTILISATEUR], true);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->valeur . '@' . (string) $this->getTarget();
    }

    /**
     * @param mixed $target
     *
     * @return bool
     */
    abstract protected function isTarget($target);

    /**
     * @return mixed
     */
    abstract public function getTarget();

    /**
     * @param mixed $target
     */
    abstract public function setTarget($target);
}
