<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

return [
    /* Common fields */

    /* Besoin */
    'besoin'              => [
        'details'         => 'Additional information',
        'duree'           => 'Desired access duration (in mounths)',
        'objectif'        => 'Theme',
        'parametres'      => 'Parameters',
        'stations'        => 'Stations',
        'type'            => 'Work type',
        'traite'          => 'Processed need',
        'aTraiter'        => 'Need to process ',
        'besoinsTraites'  => 'Processed needs',
        'besoinsATraiter' => 'Needs to process',
        'none'            => 'None',
    ],

    /* Categorie */

    /* ObjectifRecherche */

    /* Role */

    /* RoleChronique */

    /* RoleObservatoire */

    /* RoleSite */

    /* TypeTravaux */

    /* Utilisateur */
    'adresse'           => 'Address',
    'besoins'           => 'Needs',
    'email'             => 'E-mail address',
    'categorie'         => 'Category',
    'finAcces'          => 'End of access',
    'login'             => 'Login',
    'newPassword'       => 'New password',
    'newPasswordRepeat' => 'Repeat the password',
    'oldPassword'       => 'Current password',
    'organisme'         => 'Organism',
    'password'          => 'Password',
    'passwordRepeat'    => 'Repeat the password',
    'prenom'            => 'First name',
    'telephone'         => 'Phone number',
];
