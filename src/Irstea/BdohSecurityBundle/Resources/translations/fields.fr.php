<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

return [
    /* Common fields */

    /* Besoin */
    'besoin'              => [
        'details'         => 'Informations complémentaires',
        'duree'           => "Durée d'accès souhaitée (en mois)",
        'objectif'        => 'Thématique ',
        'parametres'      => 'Paramètres',
        'stations'        => 'Stations',
        'type'            => 'Type de travaux',
        'traite'          => 'Besoin traité',
        'aTraiter'        => 'Besoin à traiter ',
        'besoinsTraites'  => 'Besoins traités',
        'besoinsATraiter' => 'Besoins à traiter',
        'none'            => 'Aucun besoin',
    ],

    /* Categorie */

    /* ObjectifRecherche */

    /* Role */

    /* RoleChronique */

    /* RoleObservatoire */

    /* RoleSite */

    /* TypeTravaux */

    /* Utilisateur */
    'adresse'           => 'Adresse',
    'besoins'           => 'Besoins',
    'email'             => 'Adresse e-mail',
    'categorie'         => 'Catégorie',
    'finAcces'          => "Fin d'accès",
    'login'             => 'Identifiant',
    'newPassword'       => 'Nouveau mot de passe',
    'newPasswordRepeat' => 'Répétez le mot de passe',
    'oldPassword'       => 'Mot de passe actuel',
    'organisme'         => 'Organisme',
    'password'          => 'Mot de passe',
    'passwordRepeat'    => 'Répétez le mot de passe',
    'prenom'            => 'Prénom',
    'telephone'         => 'N° de téléphone',
];
