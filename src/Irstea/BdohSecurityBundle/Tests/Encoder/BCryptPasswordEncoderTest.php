<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohSecurityBundle\Tests\Encoder;

use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;

/**
 * Description of BlowfishPasswordEncoderTest.
 *
 * @group unit
 */
class BCryptPasswordEncoderTest extends \PHPUnit_Framework_TestCase
{
    public function testIsPasswordValid()
    {
        $encoder = new BCryptPasswordEncoder(10);
        $this->assertTrue(
            $encoder->isPasswordValid('$2a$10$012345678901234567890uH/zuqMZaW/R/NuSP.Oii64BEsHGA2a2', 'password', '0123456789012345678901')
        );
    }

    public function testIsPasswordInvalid()
    {
        $encoder = new BCryptPasswordEncoder(10);
        $this->assertFalse(
            $encoder->isPasswordValid('$2a$10$012345678901234567890uH/zuqMZaW/R/NuSP.Oii64BEsHGA2a2', 'Password', '0123456789012345678901')
        );
    }
}
