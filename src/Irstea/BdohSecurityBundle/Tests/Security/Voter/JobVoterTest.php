<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohSecurityBundle\Tests\Security\Voter;

use Irstea\BdohDataBundle\Entity\Observatoire;
use Irstea\BdohSecurityBundle\Entity\Role;
use Irstea\BdohSecurityBundle\Entity\RoleObservatoire;
use Irstea\BdohSecurityBundle\Entity\Utilisateur;
use Irstea\BdohSecurityBundle\Security\Voter\JobVoter;
use JMS\JobQueueBundle\Entity\Job;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;

/**
 * Class JobVoterTest.
 *
 * @group unit
 */
class JobVoterTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var JobVoter
     */
    private $voter;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->voter = new JobVoter();
    }

    /**
     * @dataProvider getSupportTestCases
     * @covers \JobVoter::vote
     *
     * @param mixed      $expected
     * @param mixed      $object
     * @param mixed      $role
     * @param mixed|null $user
     */
    public function testCase($expected, $object, $role, $user = null)
    {
        $token = $user ? new UsernamePasswordToken($user, null, 'provider') : new AnonymousToken(null, 'anon.');

        $this->assertEquals($expected, $this->voter->vote($token, $object, [$role]));
    }

    /**
     * @return array
     */
    public function getSupportTestCases()
    {
        $abs = VoterInterface::ACCESS_ABSTAIN;
        $grt = VoterInterface::ACCESS_GRANTED;
        $den = VoterInterface::ACCESS_DENIED;

        $any = new \stdClass();

        $other = new Utilisateur(1);
        $owner = new Utilisateur(2);
        $admin = new Utilisateur(3);

        $obs = new Observatoire();

        $job = new Job('test');
        $job->addRelatedEntity($obs);
        $job->addRelatedEntity($owner);

        $role = new RoleObservatoire();
        $role->setObservatoire($obs);
        $role->setValeur(Role::GESTIONNAIRE);
        $role->setUtilisateur($admin);
        $admin->addRoleObservatoire($role);

        return [
            [$abs, null, 'ROLE_USER'],
            [$abs, $any, 'ROLE_USER'],
            [$abs, $job, 'ROLE_USER'],

            [$abs, $any, JobVoter::LIST_PERM],
            [$abs, $job, JobVoter::LIST_PERM],
            [$abs, null, JobVoter::LIST_PERM],
            [$den, $obs, JobVoter::LIST_PERM, $other],
            [$den, $obs, JobVoter::LIST_PERM, $owner],
            [$grt, $obs, JobVoter::LIST_PERM, $admin],

            [$abs, null, JobVoter::SHOW_PERM],
            [$abs, $any, JobVoter::SHOW_PERM],
            [$den, $job, JobVoter::SHOW_PERM],
            [$den, $job, JobVoter::SHOW_PERM, $other],
            [$grt, $job, JobVoter::SHOW_PERM, $owner],
            [$grt, $job, JobVoter::SHOW_PERM, $admin],

            [$abs, null, JobVoter::CANCEL_PERM],
            [$abs, $any, JobVoter::CANCEL_PERM],
            [$den, $job, JobVoter::CANCEL_PERM],
            [$den, $job, JobVoter::CANCEL_PERM, $other],
            [$grt, $job, JobVoter::CANCEL_PERM, $owner],
            [$grt, $job, JobVoter::CANCEL_PERM, $admin],

            [$abs, null, JobVoter::RETRY_PERM],
            [$abs, $any, JobVoter::RETRY_PERM],
            [$den, $job, JobVoter::RETRY_PERM],
            [$den, $job, JobVoter::RETRY_PERM, $other],
            [$den, $job, JobVoter::RETRY_PERM, $owner],
            [$grt, $job, JobVoter::RETRY_PERM, $admin],
        ];
    }
}
