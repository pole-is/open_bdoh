<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohSecurityBundle\DataFixtures\ORM\Test;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Irstea\BdohDataBundle\DataFixtures\ORM\AbstractYamlFixture;
use Irstea\BdohDataBundle\DataFixtures\ORM\Test\ChroniquesContinues;
use Irstea\BdohDataBundle\DataFixtures\ORM\Test\Observatoires;
use Irstea\BdohDataBundle\DataFixtures\ORM\Test\SitesExperimentaux;
use Irstea\BdohDataBundle\Entity\ChroniqueContinue;
use Irstea\BdohDataBundle\Entity\Observatoire;
use Irstea\BdohDataBundle\Entity\SiteExperimental;
use Irstea\BdohSecurityBundle\Entity\Role;
use Irstea\BdohSecurityBundle\Entity\RoleChronique;
use Irstea\BdohSecurityBundle\Entity\RoleObservatoire;
use Irstea\BdohSecurityBundle\Entity\RoleSite;
use Irstea\BdohSecurityBundle\Entity\Utilisateur;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\PlaintextPasswordEncoder;

/**
 * Creation of some 'Utilisateur' for credential testing.
 */
class Users extends AbstractYamlFixture implements ContainerAwareInterface, DependentFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [Observatoires::class, SitesExperimentaux::class, ChroniquesContinues::class];
    }

    /**
     * @var PasswordEncoderInterface
     */
    private $encoder;

    /**
     * {@inheritdoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->encoder =
            $container ? $container->get('security.encoder_factory')->getEncoder(Utilisateur::class) : new PlaintextPasswordEncoder();
    }

    /**
     * {@inheritdoc}
     */
    protected function loadEntity(array $data, $username)
    {
        $user = new Utilisateur();
        $user->setEmail($username . '@irstea.fr');
        $user->setNom($data['nom']);
        $user->setPrenom($data['prenom']);
        $user->setPassword($this->encoder->encodePassword('password', $user->getSalt()));

        if (isset($data['roleAdmin'])) {
            $user->setRoleAdmin($data['roleAdmin']);
        }

        if (isset($data['rights'])) {
            foreach ($data['rights'] as $right) {
                $type = constant(Role::class . '::' . $right['type']);

                if (isset($right['observatoire'])) {
                    $target = $this->getReference(Observatoire::class . $right['observatoire']);
                    $role = new RoleObservatoire();
                    $user->addRoleObservatoire($role);
                } elseif (isset($right['site'])) {
                    $target = $this->getReference(SiteExperimental::class . $right['site']);
                    $role = new RoleSite();
                    $user->addRoleSite($role);
                } elseif (isset($right['chronique'])) {
                    $target = $this->getReference(ChroniqueContinue::class . $right['chronique']);
                    $role = new RoleChronique();
                    $user->addRoleChronique($role);
                } else {
                    throw new \RuntimeException('Invalid right');
                }

                $role->setValeur($type);
                $role->setTarget($target);
                $role->setUtilisateur($user);
            }
        }

        return $user;
    }
}
