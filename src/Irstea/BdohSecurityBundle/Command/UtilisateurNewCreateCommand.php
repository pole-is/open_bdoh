<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohSecurityBundle\Command;

use Doctrine\Common\Persistence\ObjectManager;
use Irstea\BdohSecurityBundle\Entity\Utilisateur;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class UtilisateurNewCreateCommand extends Command
{
    const COMMAND_NAME = 'security:users:create';
    const ROLE_ADMIN_FCT = 'FCT';
    const ROLE_ADMIN_TECH = 'TECH';

    /** ®@var ObjectManager $manager**/
    private $manager;

    /**
     * @var EncoderFactoryInterface
     */
    protected $encoderFactory;

    /**
     * UserPasswordValidator constructor.
     *
     * @param EncoderFactoryInterface $encoderFactory
     *
     * @DI\InjectParams({
     *     "encoderFactory" = @DI\Inject("security.encoder_factory"),
     * })
     */
    public function __construct(ObjectManager $manager, EncoderFactoryInterface $encoderFactory)
    {
        $this->manager = $manager;
        $this->encoderFactory = $encoderFactory;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->setDescription('Création Utilisateur Perso : GestionnaireDesObservatoires et AdminTech')
            ->addArgument('firstname', InputArgument::REQUIRED, 'User firstname')
            ->addArgument('lastname', InputArgument::REQUIRED, 'User firstname')
            ->addArgument('email', InputArgument::REQUIRED, 'User email')
            ->addArgument('password', InputArgument::REQUIRED, 'User password')
            ->addArgument('roleAdmin', InputArgument::REQUIRED, 'User roleAdmin');
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '==============================',
            'Welcome you can create a User',
            '===============================',
        ]);
        $questions = [];
        $argumentUser = ['firstname', 'lastname', 'email', 'password', 'roleAdmin'];

        foreach ($argumentUser as $argumentUtilisateur) {
            if (!$input->getArgument($argumentUtilisateur)) {
                $question = new Question('Please enter the ' . $argumentUtilisateur . ':');
                if ($argumentUtilisateur === 'roleAdmin') {
                    $question->setValidator(function ($firstname) {
                        if (empty($firstname)) {
                            throw new \Exception('Argument can not be empty');
                        }
                        $roleAdmin = 'FCT';
                        $roleTech = 'TECH';
                        if ($firstname !== $roleAdmin && $firstname !== $roleTech) {
                            throw new \Exception('This Role does not exist');
                        }

                        return $firstname;
                    });
                } else {
                    $question->setValidator(function ($firstname) {
                        if (empty($firstname)) {
                            throw new \Exception('Argument can not be empty');
                        }

                        return $firstname;
                    });
                }

                $questions[$argumentUtilisateur] = $question;
            }
        }
        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '====================',
            'Command Create User',
            '====================',
        ]);
        $user = new Utilisateur();
        $user->setPrenom($input->getArgument('firstname'));
        $output->writeln('With firstname : ' . $user->getPrenom());
        $user->setNom($input->getArgument('lastname'));
        $output->writeln('With lastname : ' . $user->getNom());
        $user->setEmail($input->getArgument('email'));
        $output->writeln('With email : ' . $user->getEmail());
        $user->setPassword($input->getArgument('password'));
        //encodage du mot de passe
        $encoderGestionnaire = $this->encoderFactory->getEncoder($user);
        $password = $encoderGestionnaire->encodePassword($user->getPassword(), $user->getSalt());
        $user->setPassword($password);
        $user->setRoleAdmin($input->getArgument('roleAdmin'));

        $this->manager->persist($user);
        $this->manager->flush();
        $output->writeln([
            '===============================================================================================',
            'Successful you have created the user : ' . $user->getPrenom() . ' ' . $user->getNom() . ' !!!',
            '===============================================================================================',
        ]);

        return 0;
    }
}
