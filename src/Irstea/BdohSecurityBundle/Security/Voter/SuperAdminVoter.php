<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohSecurityBundle\Security\Voter;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\RoleHierarchyVoter;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;

/**
 * Class SuperAdminVoter.
 *
 * @DI\Service
 * @DI\Tag("security.voter")
 */
class SuperAdminVoter extends RoleHierarchyVoter
{
    /**
     * SuperAdminVoter constructor.
     *
     * @param RoleHierarchyInterface $roleHierarchy
     * @DI\InjectParams({"roleHierarchy" = @DI\Inject("security.role_hierarchy")})
     */
    public function __construct(RoleHierarchyInterface $roleHierarchy)
    {
        parent::__construct($roleHierarchy, '*');
    }

    /**
     * {@inheritdoc}
     */
    public function supportsAttribute($attribute)
    {
        return is_string($attribute);
    }

    /**
     * {@inheritdoc}
     */
    public function vote(TokenInterface $token, $object, array $attributes)
    {
        $vote = parent::vote($token, $object, ['ROLE_SUPER_ADMIN']);

        return $vote === self::ACCESS_GRANTED ? $vote : self::ACCESS_ABSTAIN;
    }
}
