<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohSecurityBundle\Controller;

use Irstea\BdohBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security as CoreSecurity;

/**
 * Class SecurityController.
 */
class SecurityController extends Controller
{
    /**
     * @Route("/login", name="bdoh_security_login")
     * @Method("GET")
     * @Security("is_granted('IS_AUTHENTICATED_ANONYMOUSLY')")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request)
    {
        // si l'utilisateur est déjà identifié on le redirige sur l'accueil de l'observatoire
        if ($this->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('bdoh_home');
        }

        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(CoreSecurity::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(CoreSecurity::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(CoreSecurity::AUTHENTICATION_ERROR);
            $session->remove(CoreSecurity::AUTHENTICATION_ERROR);
        }

        return $this->render(
            'IrsteaBdohSecurityBundle:Security:login.html.twig',
            [
                // last username entered by the user
                'last_username' => $session->get(CoreSecurity::LAST_USERNAME),
                'error'         => $error,
            ]
        );
    }
}
