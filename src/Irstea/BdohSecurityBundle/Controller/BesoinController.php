<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohSecurityBundle\Controller;

use Irstea\BdohBundle\Controller\Controller;
use Irstea\BdohSecurityBundle\Entity\Besoin;
use Irstea\BdohSecurityBundle\Entity\Utilisateur;
use Irstea\BdohSecurityBundle\Form\BesoinType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/besoin")
 */
class BesoinController extends Controller
{
    /**
     * Creates a new 'Besoin' entity.
     *
     * @Route("/new", name="bdoh_security_besoin_new")
     * @Security("is_granted('ROLE_USER')")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        /*
         * @var Utilisateur $user
         */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        // si l'utilisateur a déjà accès à tout on le redirige sur la page de recherche avancée
        if ($user->isATrustedUserOfCurrentObservatory() || $this->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('bdoh_consult_advancedSearch');
        }

        $besoin = new Besoin();
        $besoin->setObservatoire($this->get('irstea_bdoh.manager.observatoire')->getCurrent());
        $besoin->setUtilisateur($user);

        $form = $this->createForm(new BesoinType($this->getEm(), $this->get('session')->get('_locale')), $besoin);

        // Some POST parameters ? Checks the form.
        if (count($request->request) !== 0) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                // Persists the new 'Besoin'
                $em = $this->getEm();
                $em->persist($besoin);
                $em->flush();

                // Send a mail to the managers of the observatory
                $userRepo = $this->getSecurityRepo('Utilisateur');
                $validatorMails = $userRepo->getValidatorMails($this->container->get('irstea_bdoh.manager.observatoire')->getCurrent());
                $message = \Swift_Message::newInstance()
                    ->setSubject($this->get('translator')->trans('security.utilisateur.newNeedMail.title'))
                    ->setFrom('no.reply.BDOH@irstea.fr')
                    ->setTo($validatorMails)
                    ->setBody(
                        $this->renderView(
                            'IrsteaBdohSecurityBundle:Utilisateur:mail.txt.twig',
                            ['user' => $user, 'type' => 'besoin']
                        )
                    );

                $this->get('mailer')->send($message);

                // Redirects to home, with a success message
                $this->getSession()->getFlashBag()->add('success', $this->get('translator')->trans('security.besoin.create.isCompleted'));

                return $this->redirect($this->generateUrl('bdoh_home'));
            }
        }

        return $this->render('IrsteaBdohSecurityBundle:Besoin:new.html.twig', ['form' => $form->createView()]);
    }
}
