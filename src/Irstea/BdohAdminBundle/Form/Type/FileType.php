<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohAdminBundle\Form\Type;

use Irstea\BdohAdminBundle\Form\DataTransformer\FileTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType as BaseFileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FileType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $newFileOptions = $childOptions = [
            'error_bubbling' => true,
            'required'       => false,
        ];
        if (!empty($options['accept'])) {
            $newFileOptions['attr']['accept'] = $options['accept'];
        }

        $builder
            ->add('current', HiddenType::class, $childOptions)
            ->add('new', BaseFileType::class, $newFileOptions)
            ->addModelTransformer(new FileTransformer());
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['accept'] = empty($options['accept']) ? '' : $options['accept'];
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('accept', '')
            ->addAllowedTypes('accept', ['string', 'array'])
            ->setNormalizer(
                'accept',
                function (OptionsResolver $resolver, $value) {
                    if (is_array($value)) {
                        $value = implode(',', $value);
                    }

                    return $value;
                }
            );
    }

    public function getBlockPrefix()
    {
        return 'bdoh_file';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'bdoh_file';
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'form';
    }
}
