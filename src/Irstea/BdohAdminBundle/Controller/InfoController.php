<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohAdminBundle\Controller;

use Irstea\BdohBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class InfoController.
 */
class InfoController extends Controller
{
    /**
     * @return Response
     *
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function generateChroniqueCodeAction()
    {
        $request = $this->getRequest()->query;
        $stationId = $request->get('station');
        $parametreId = $request->get('parametre');

        if (null === $station = $this->getBdohRepo('Station')->find($stationId)) {
            return new Response('');
        }

        if (null === $parametre = $this->getBdohRepo('TypeParametre')->find($parametreId)) {
            return new Response('');
        }

        $count = $this->getBdohRepo('Chronique')->countByStationAndParametre($station, $parametre) + 1;

        return new Response($parametre->getCode() . ($count === 1 ? '' : ("-$count")));
    }

    /**
     * @return Response
     *
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function getFilteredChroniquesAction()
    {
        $request = $this->getRequest()->query;
        $bassinId = $request->get('bassinId');
        $coursEauId = $request->get('coursEauId');

        $chronRepo = $this->getBdohRepo('Chronique');

        // cette methode n'existe pas ...
        // TODO à virer ou à corriger, cette action ne semble pas être utilisée non plus
        $chroniques = $chronRepo->richFindByGeography($bassinId, $coursEauId);

        return new Response($chroniques);
    }
}
