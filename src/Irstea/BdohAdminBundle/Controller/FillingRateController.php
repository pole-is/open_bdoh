<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohAdminBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Irstea\BdohBundle\Controller\Controller;
use Irstea\BdohBundle\Manager\JobManagerInterface;
use Irstea\BdohDataBundle\Entity\ChroniqueContinue;
use Irstea\BdohDataBundle\Entity\Repository\ChroniqueContinueRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class FillingRateController.
 *
 * @Security("is_granted('ROLE_ADMIN') and (user.isAManagerOfCurrentObservatory() or user.isASiteManagerOfCurrentObservatory() or user.isAContributorOfCurrentObservatory())")
 *
 * TODO à virer ? normalement ce controller n'est plus utilisé
 * (les calculs de taux de remplissage sont tout automatique)
 */
class FillingRateController extends Controller
{
    /**
     * @var RegistryInterface
     * @DI\Inject
     */
    public $doctrine;

    /**
     * @var JobManagerInterface
     * @DI\Inject("irstea_bdoh.job_manager")
     */
    public $jobManager;

    /**
     * @Route("/fillingrate", name="bdoh_admin_recalculate")
     * @Method("GET")
     *
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function fillingRateRecalculateDoAction(Request $request)
    {
        $chroniqueId = (int) $request->query->get('id');
        if (!$chroniqueId) {
            throw new BadRequestHttpException();
        }

        /** @var ChroniqueContinueRepository $repo */
        $repo = $this->doctrine->getRepository(ChroniqueContinue::class);

        /** @var ChroniqueContinue $chronique */
        $chronique = $repo->findOneById($chroniqueId);
        if (!$chronique) {
            throw new EntityNotFoundException();
        }

        if (!$this->get('security.authorization_checker')->isGranted('UPLOAD_DATA', $chronique)) {
            throw new AccessDeniedHttpException();
        }

        $this->jobManager->enqueueComputeFillingRates($chronique);

        $url = $this->generateUrl(
            'bdoh_consult_chronique',
            [
                'station'   => $chronique->getStation()->getCode(),
                'chronique' => $chronique->getCode(),
            ]
        );

        return $this->redirect($url);
    }
}
