<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohAdminBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class IrsteaBdohAdminBundle extends Bundle
{
    /**
     * Extracts the first file of a zip archive in a destination directory.
     *
     * @param string $zipPath
     * @param mixed  $destDir
     *
     * @parap string $destDir
     *
     * @return string|null Extracted file path OR null if an error occurred
     */
    public static function unzipFirst($zipPath, $destDir = '/tmp')
    {
        $zip = new \ZipArchive();
        $firstFile = null;

        // Opens the zip archive
        if (true === $zip->open($zipPath)) {
            // Gets the first file name
            if ($firstFile = $zip->getNameIndex(0)) {
                // Extracts this first file
                $firstFile = $zip->extractTo($destDir, $firstFile) ?
                    $destDir . DIRECTORY_SEPARATOR . $firstFile :
                    null;
            } else {
                $firstFile = null;
            }
            $zip->close();
        }

        return $firstFile;
    }

    /**
     * Extracts the file of a zip archive in a destination directory.
     *
     * @param string $zipPath
     * @param mixed  $destDir
     *
     * @parap string $destDir
     *
     * @return string|null Extracted file path OR null if an error occurred
     */
    public static function unzipAll($zipPath, $destDir = '/tmp')
    {
        $zip = new \ZipArchive();

        // Opens the zip archive
        if (true === $zip->open($zipPath)) {
            // Extracts this first file
            $zip->extractTo($destDir);
            $zip->close();

            return true;
        }

        return false;
    }
}
