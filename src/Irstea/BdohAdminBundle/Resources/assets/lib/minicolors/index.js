const $ = require('jquery');

$(() => {
    const inputs = $('input.minicolors');
    if (!inputs.length) {
        return;
    }
    require('@claviska/jquery-minicolors/jquery.minicolors');
    require('@claviska/jquery-minicolors/jquery.minicolors.css');
    require('./style.css');
});
