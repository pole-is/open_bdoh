const $ = require('jquery');

const config = {
    letterCase: 'uppercase',
    theme: 'bootstrap'
};

$(() => {
    const inputs = $('input.colorpicker');
    if (!inputs.length) {
        return;
    }
    require('@claviska/jquery-minicolors');
    require('@claviska/jquery-minicolors/jquery.minicolors.css');
    $(inputs).minicolors(config);
});
