const BaseWidget = require('./base.js');

class ImageWidget extends BaseWidget {

    showNewPreview(file) {
        const reader = new FileReader();
        reader.onloadend = () => this.preview.attr('src', reader.result);
        reader.readAsDataURL(file);
    }

    showCurrentPreview(url) {
        this.preview.attr('src', url);
    }
}

module.exports = ImageWidget;
