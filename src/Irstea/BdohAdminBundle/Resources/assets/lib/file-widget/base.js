const $ = require('jquery');

class BaseWidget {

    constructor(el) {

        this.element = $(el);
        this.basePath = this.element.data('base-path');

        this.fileInput = this.element.find('[name$="[new]"]');
        this.currentInput = this.element.find('[name$="[current]"]');

        this.preview = this.element.find('.preview');
        this.placeholder = this.element.find('.placeholder');
        this.deleteBtn = this.element.find('.delete');
        this.undoBtn = this.element.find('.undo');

        this.initialValue = this.currentInput.val() || '';

        this.undoBtn.on('click.bdoh-file-widget', (ev) => {
            ev.preventDefault();
            this.undo();
        });

        this.deleteBtn.on('click.bdoh-file-widget', (ev) => {
            ev.preventDefault();
            this.delete();
        });

        this.fileInput.on('change.bdoh-file-widget', () => this.update());
    }

    undo() {
        this.fileInput.val('');
        this.currentInput.val(this.initialValue);
        this.update();
    }

    delete() {
        this.fileInput.val('');
        this.currentInput.val('');
        this.update();
    }

    update() {
        const currentValue = this.currentInput.val() || '';
        const newValue = this.fileInput.val() || '';
        const anyValue = newValue || currentValue;
        const hasValue = anyValue !== '';

        this.undoBtn.toggle(anyValue !== this.initialValue && this.initialValue !== '');

        this.deleteBtn.toggle(hasValue);
        this.preview.toggle(hasValue);
        this.placeholder.toggle(!hasValue);

        if (newValue) {
            this.showNewPreview(this.fileInput[0].files[0]);
        } else if (currentValue) {
            this.showCurrentPreview(this.basePath + currentValue);
        }
    }

    showNewPreview() {
        throw new Error('Abstract method AbstractFileWidget.showNewPreview');
    }

    showCurrentPreview() {
        throw new Error('Abstract method AbstractFileWidget.showNewPreview');
    }
}

module.exports = BaseWidget;
