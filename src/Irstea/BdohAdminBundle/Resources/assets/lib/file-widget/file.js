const BaseWidget = require('./base.js');

class FileWidget extends BaseWidget {

    constructor(el) {
        super(el);

        this.link = this.preview.find('a');
        this.linkText = this.link.find('span');
    }

    showNewPreview() {
        this.preview.hide();
    }

    showCurrentPreview(url) {
        const lastSlash = url.lastIndexOf('/');
        const name = lastSlash >= 0 ? url.substr(lastSlash + 1) : url;
        this.link.attr('href', url);
        this.linkText.text(name);
    }
}

module.exports = FileWidget;
