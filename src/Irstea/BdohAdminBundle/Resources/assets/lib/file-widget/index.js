const $ = require('jquery');
const classes = {
    file: require('./file'),
    image: require('./image'),
}

function enableFileWidget(el) {
    const type = $(el).data('file-widget');
        const clazz = classes[type];
        if (clazz) {
            const widget = new clazz(el);
            widget.update();
        }
}

global.enableFileWidget = function(selector) {
  $(() => {
    enableFileWidget($(selector));
  });
};
