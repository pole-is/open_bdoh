const $ = require('jquery');

$(() => {
    const editors = $('textarea.htmleditor');
    if (!editors.length) {
        return;
    }

    require('trumbowyg');
    require('trumbowyg/dist/langs/fr.min');
    require('trumbowyg/dist/ui/trumbowyg.css');

    editors.trumbowyg({
        lang: $('html').attr('lang') || 'fr',
        autogrowOnEnter: true,
        svgPath: require('trumbowyg/dist/ui/icons.svg'),
        btns: [
            ['undo', 'redo'], // Only supported in Blink browsers
            ['formatting'],
            ['strong', 'em', 'del'],
            ['superscript', 'subscript'],
            ['link'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat'],
            ['fullscreen']
        ]
    });
});
