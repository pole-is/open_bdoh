// Inclus dans vendor
global.jQuery = require('jquery');
require('moment');

// Livrés avec Sonata
require('sonataadmin/vendor/jqueryui/ui/minified/jquery-ui.min.js');
require('sonataadmin/vendor/jqueryui/ui/minified/i18n/jquery-ui-i18n.min.js');
require('sonatacore/vendor/bootstrap/dist/js/bootstrap.min.js');
require('sonatacore/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');
require('sonataadmin/vendor/jquery-form/jquery.form.js');
require('sonataadmin/jquery/jquery.confirmExit.js');
require('sonataadmin/vendor/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js');
require('sonatacore/vendor/select2/select2.min.js');
require('sonataadmin/vendor/admin-lte/dist/js/app.min.js');
require('sonataadmin/vendor/iCheck/icheck.min.js');
require('sonataadmin/vendor/slimScroll/jquery.slimscroll.min.js');
require('sonataadmin/vendor/waypoints/lib/jquery.waypoints.min.js');
require('sonataadmin/vendor/waypoints/lib/shortcuts/sticky.min.js');
require('sonataadmin/vendor/readmore-js/readmore.min.js');
require('sonataadmin/vendor/masonry/dist/masonry.pkgd.min.js');
require('script-loader!sonataadmin/Admin.js');
require('sonataadmin/treeview.js');

require('sonatacore/vendor/bootstrap/dist/css/bootstrap.min.css');
require('sonatacore/vendor/components-font-awesome/css/font-awesome.min.css');
require('sonatacore/vendor/ionicons/css/ionicons.min.css');
require('sonataadmin/vendor/admin-lte/dist/css/AdminLTE.min.css');
require('sonataadmin/vendor/admin-lte/dist/css/skins/skin-black.min.css');
require('sonataadmin/vendor/iCheck/skins/square/blue.css');
require('sonatacore/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');
require('sonataadmin/vendor/jqueryui/themes/base/jquery-ui.css');
require('sonatacore/vendor/select2/select2.css');
require('sonatacore/vendor/select2-bootstrap-css/select2-bootstrap.min.css');
require('sonataadmin/vendor/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css');
require('sonataadmin/css/styles.css');
require('sonataadmin/css/layout.css');
require('sonataadmin/css/tree.css');

// Nos widgets et extensions
require('@IrsteaBdohAdminBundle/lib/file-widget');
require('@IrsteaBdohAdminBundle/lib/colorpicker');
require('@IrsteaBdohAdminBundle/lib/htmleditor');

// Style spécifique
require('./style.less');
