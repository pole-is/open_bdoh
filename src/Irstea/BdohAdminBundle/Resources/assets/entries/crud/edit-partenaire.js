const $ = require('jquery');

$(() => {

    console.log('patate');

    const paramEl = $('[data-is-funding]');
    const isFunding = paramEl.data('is-funding');

   const typeFundingsSelector = $('select[id$="_typeFundings"]');
    console.log(typeFundingsSelector)

    const selectIsFunding = $('select[id$="_estFinanceur"] input[type="radio"]');
    console.log(selectIsFunding)

    function getInputIsFundingId() {
        return selectIsFunding.val();
    }

    function updateDirectionSelector() {
        const isFinanceur = isFunding[getInputIsFundingId()] === true;
        typeFundingsSelector
            .attr({disabled: isFinanceur})
            .closest('.form-group')
            .toggle(!isFinanceur);
    }

    updateDirectionSelector();


});
