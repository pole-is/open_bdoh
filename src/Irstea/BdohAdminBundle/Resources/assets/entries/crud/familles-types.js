require('./familles-types.css');
const $ = require('jquery');

$(() => {

    // modificcation du rendu de la liste d'options du select2 pour
    // injecter la class de hierarchie donnée à l'option
    $.fn.select2.defaults.templateResult = function (data, container) {
        if (data.element) {
            $(container).addClass($(data.element).attr('class'));
        }
        return data.text;
    };

});
