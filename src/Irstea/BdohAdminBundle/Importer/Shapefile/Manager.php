<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohAdminBundle\Importer\Shapefile;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Irstea\BdohAdminBundle\Errors\ImportErrors;
use Irstea\BdohDataBundle\Entity\Repository\BassinRepository;
use Irstea\BdohDataBundle\Entity\Repository\CoursEauRepository;
use Irstea\BdohDataBundle\Entity\Repository\StationRepository;
use Irstea\BdohDataBundle\Entity\Station;
use Symfony\Component\Translation\TranslatorInterface;

class Manager
{
    const TMP_TABLE = 'temp.shape';

    protected $em;

    protected $translator;

    protected $tmpTable;

    protected $targettedTable;

    /**
     * Errors manager.
     *
     * @var ImportErrors
     */
    protected $errors;

    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Path of file to import.
     *
     * @var string
     */
    protected $filePath;

    /**
     * Handler on file to import.
     *
     * @var resource
     */
    protected $file;

    public function getTmpTable()
    {
        return $this->tmpTable;
    }

    public function setTmpTable($tmpTable)
    {
        $this->tmpTable = $tmpTable;
    }

    public function getTargettedTable()
    {
        return $this->targettedTable;
    }

    public function setTargettedTable($targettedTable)
    {
        $this->targettedTable = $targettedTable;
    }

    /**
     * Treatments to do when serializing.
     */
    public function __sleep()
    {
        /* Attributes to serialize */
        return [
            'errors',
            'filePath',
            'tmpTable',
            'targettedTable',
        ];
    }

    public function __construct(EntityManager $em, TranslatorInterface $translator, $file, $table, ImportErrors $errors = null)
    {
        $this->em = $em;
        $this->translator = $translator;
        $this->filePath = $file;
        $this->errors = (null === $errors) ? new ImportErrors() : $errors;
        $this->targettedTable = $table;
        $this->tmpTable = static::TMP_TABLE . rand();
    }

    public function getShpToPgsqlCommand()
    {
        $c = $this->em->getConnection();
        $host = $c->getHost();
        $port = $c->getPort();
        $port = $port ?: '5432';
        $database = $c->getDatabase();
        $username = $c->getUsername();
        $password = $c->getPassword();

        $pgpassfile = $this->filePath . '_tmp_psql';
        \file_put_contents($pgpassfile, $host . ':' . $port . ':' . $database . ':' . $username . ':' . $password);

        return 'shp2pgsql -s ' . Station::SRID . ' -ciD ' . $this->filePath .
            ' ' . $this->tmpTable . ' 1> ' . $this->filePath . '_Success.log 2> ' . $this->filePath . '_Errors.log ; ' .
            'chmod 600 ' . $pgpassfile . ' ; env PGPASSFILE=' . $pgpassfile . " psql -h '$host' -d '$database' -U '$username' -f " .
            $this->filePath . '_Success.log 1> ' . $this->filePath . '_Psql.log  2>&1 ; ' .
            'rm ' . $pgpassfile . ' ; ';
    }

    final public function importData($importType, $user)
    {
        try {
            // Can't continue if there are errors
            if ($this->errors->isDirty()) {
                throw new \RuntimeException(
                    $this->translator->trans(
                        'shape.import.thereAreErrors.cantContinue'
                    )
                );
            }
            /** @var BassinRepository|StationRepository|CoursEauRepository $shapeRepo */
            $shapeRepo = $this->getBdohRepo(ucfirst($this->targettedTable));
            if ('keepExisting' === $importType) {
                return $shapeRepo->shapeKeepExistingImport($this->tmpTable, $user);
            } elseif ('overwriteExisting' === $importType) {
                return $shapeRepo->shapeOverwriteExistingImport($this->tmpTable, $user);
            }

            return [0, 0];
        } catch (\Exception $e) {
            $this->errors->addError($e->getMessage());
        }
    }

    /**
     * Cleans the importer.
     */
    final public function clean()
    {
        if ($this->filePath) {
            array_map('unlink', glob($this->filePath . '*'));
            $this->filePath = null;
        }

        // Deletes temporary SQL table
        if ($this->tmpTable) {
            /** @var \Irstea\BdohDataBundle\Entity\Repository\EntityRepository $shapeRepo */
            $shapeRepo = $this->getBdohRepo(ucfirst($this->targettedTable));
            $shapeRepo->dropTemporaryShapeTable($this->tmpTable);
        }
    }

    /**
     * Treatments to do when unserializing.
     * NOTE : run manually this function !
     *
     * @param EntityManagerInterface $em
     * @param TranslatorInterface    $translator
     */
    public function wakeup(EntityManagerInterface $em, TranslatorInterface $translator)
    {
        $this->em = $em;
        $this->translator = $translator;
    }

    /**
     * Shortcut to return a Doctrine repository.
     *
     * @param string $entity
     *
     * @return EntityRepository
     */
    protected function getRepository($entity)
    {
        return $this->em->getRepository($entity);
    }

    /**
     *  Shortcut to return a Doctrine repository of a BDOH entity.
     *
     * @param string $bdohEntity
     *
     * @return EntityRepository
     */
    protected function getBdohRepo($bdohEntity)
    {
        return $this->getRepository('IrsteaBdohDataBundle:' . $bdohEntity);
    }
}
