<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohAdminBundle\Admin\Security;

use Irstea\BdohAdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

/**
 * Class Utilisateur.
 */
class Utilisateur extends Admin
{
    const DEFAULT_PASSWORD = 'password';

    /**
     * Configure the ordered field.
     */
    protected $datagridValues = [
        '_sort_by' => 'nom',  // name of the ordered field
        // (default = the model's id field, if any)
    ];

    /**
     * @var
     */
    protected $encoder;

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('delete');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('email', null, ['route' => ['name' => 'edit']])
            ->add('prenom')
            ->add('nom')
            ->add('organisme')
            ->add('finAcces', 'datetime', ['format' => $this->getContainer()->get('translator')->trans('formatDateTime')]);
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $categoryProperty = 'libelle';
        $translator = $this->getContainer()->get('translator');
        if (\strpos($translator->getLocale(), 'en') === 0) {
            $categoryProperty .= 'En';
        }
        $categoryQueryBuilder = $this->getRepository('IrsteaBdohSecurityBundle:Categorie')
            ->createQueryBuilder('c')->orderBy('c.' . $categoryProperty);

        $formMapper
            ->add('finAcces', 'date', ['required' => false])
            ->add('email', 'email', ['attr' => ['class' => 'col-md-8']])
            ->add('prenom', 'text')
            ->add('nom', 'text')
            ->add('organisme', 'text', ['attr' => ['class' => 'col-md-8']])
            ->add('categorie', null, ['required' => true, 'property' => $categoryProperty, 'query_builder' => $categoryQueryBuilder])
            ->add('telephone', 'text')
            ->add('adresse', 'textarea', ['attr' => ['class' => 'col-md-10', 'style' => 'height: 80px;']]);
    }

    /**
     * @param EncoderFactoryInterface $encoder
     */
    public function setEncoder(EncoderFactoryInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @param $object
     */
    public function postPersist($object)
    {
        $this->logger->createHistoriqueAdministrationUtilisateur(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.UtilisateurCreate',
            $object
        );

        //Send a mail to inform the user of her new account
        $message = \Swift_Message::newInstance()
            ->setSubject($this->getContainer()->get('translator')->trans('utilisateur.newAccountMail.title'))
            ->setFrom('no.reply.BDOH@irstea.fr')
            ->setTo($object->getEmail())
            ->setBody(
                $this->getContainer()->get('templating')->render(
                    'IrsteaBdohAdminBundle:Mail:newAccount.txt.twig',
                    ['user' => $object]
                )
            );

        $this->getContainer()->get('mailer')->send($message);
    }

    /**
     * @param $object
     */
    public function postUpdate($object)
    {
        $this->logger->createHistoriqueAdministrationUtilisateur(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.UtilisateurUpdate',
            $object
        );
    }

    /**
     * @param $object
     */
    public function prePersist($object)
    {
        $object->setPassword(
            $this->encoder->getEncoder($object)->encodePassword(
                self::DEFAULT_PASSWORD,
                $object->getSalt()
            )
        );
    }
}
