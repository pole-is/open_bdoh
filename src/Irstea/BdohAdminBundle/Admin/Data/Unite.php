<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohAdminBundle\Admin\Data;

use Irstea\BdohAdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Class Unite.
 */
class Unite extends Admin
{
    /**
     * Configure the ordered field.
     */
    protected $datagridValues = [
        '_sort_by' => 'libelle',  // name of the ordered field
        // (default = the model's id field, if any)
    ];

    /**
     * Les chroniques qui utilisent cette unité.
     */
    private $timeSeries = null;

    /**
     * @return array|null
     */
    public function getTimeSeries()
    {
        if ($this->timeSeries === null && $this->getSubject() && $this->getSubject()->getId()) {
            $this->timeSeries = $this->getBdohRepo('Chronique')->findByUnite($this->getSubject());
        }

        return $this->timeSeries;
    }

    /**
     * Les bnarèmes qui utilisent cette unité.
     */
    private $scales = null;

    /**
     * @return array|null
     */
    public function getScales()
    {
        if ($this->scales === null && $this->getSubject() && $this->getSubject()->getId()) {
            $this->scales = $this->getBdohRepo('Bareme')->findByUnite($this->getSubject());
        }

        return $this->scales;
    }

    /**
     * Les types de paramètre qui utilisent cette unité.
     */
    private $parameterTypes = null;

    /**
     * @return array|null
     */
    public function getParameterTypes()
    {
        if ($this->parameterTypes === null && $this->getSubject() && $this->getSubject()->getId()) {
            $this->parameterTypes = $this->getBdohRepo('TypeParametre')->findByUnite($this->getSubject());
        }

        return $this->parameterTypes;
    }

    /**
     * {@inheritdoc}
     */
    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'IrsteaBdohAdminBundle:CRUD:edit-unite.html.twig';
            case 'delete':
                return 'IrsteaBdohAdminBundle:CRUD:delete-unite.html.twig';
            default:
                return parent::getTemplate($name);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('libelle', null, ['route' => ['name' => 'edit']]);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('libelle')
            ->add('libelleEn');
    }

    /**
     * {@inheritdoc}
     */
    public function postPersist($object)
    {
        $this->logger->createHistoriqueAdministrationUnite(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.UniteCreate',
            $object
        );
    }

    /**
     * {@inheritdoc}
     */
    public function postUpdate($object)
    {
        $this->logger->createHistoriqueAdministrationUnite(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.UniteUpdate',
            $object
        );
    }

    /**
     * {@inheritdoc}
     */
    public function preRemove($object)
    {
        $this->logger->createHistoriqueAdministrationUnite(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.UniteRemove',
            $object
        );
    }
}
