<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohAdminBundle\Admin\Data;

use Irstea\BdohAdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Class Commune.
 */
class Commune extends Admin
{
    /**
     * Configure the ordered field.
     */
    protected $datagridValues = [
        '_sort_by' => 'nom',  // name of the ordered field
        // (default = the model's id field, if any)
    ];

    /**
     * Les stations qui sont sur cette commune.
     */
    private $stations = null;

    /**
     * @return array|null
     */
    public function getStations()
    {
        if ($this->stations === null && $this->getSubject() && $this->getSubject()->getId()) {
            $this->stations = $this->getBdohRepo('Station')->findByCommune($this->getSubject());
        }

        return $this->stations;
    }

    /**
     * {@inheritdoc}
     */
    public function getTemplate($name)
    {
        switch ($name) {
            case 'delete':
                return 'IrsteaBdohAdminBundle:CRUD:delete-commune.html.twig';
            default:
                return parent::getTemplate($name);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('nom', null, ['route' => ['name' => 'edit']])
            ->add('codePostal')
            ->add('codeInsee');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('nom')
            ->add('codePostal')
            ->add('codeInsee');
    }

    /**
     * {@inheritdoc}
     */
    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nom')
            ->add('codePostal');
    }

    /**
     * {@inheritdoc}
     */
    public function postPersist($object)
    {
        $this->logger->createHistoriqueAdministrationCommune(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.CommuneCreate',
            $object
        );
    }

    /**
     * {@inheritdoc}
     */
    public function postUpdate($object)
    {
        $this->logger->createHistoriqueAdministrationCommune(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.CommuneUpdate',
            $object
        );
    }

    /**
     * {@inheritdoc}
     */
    public function preRemove($object)
    {
        $this->logger->createHistoriqueAdministrationCommune(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.CommuneRemove',
            $object
        );
    }
}
