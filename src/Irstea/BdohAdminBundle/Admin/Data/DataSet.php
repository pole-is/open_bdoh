<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohAdminBundle\Admin\Data;

use Irstea\BdohAdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class DataSet extends Admin
{
    /**
     * Configure the ordered field.
     */
    protected $datagridValues = [
        '_sort_by' => 'titre',  // name of the ordered field
        // (default = the model's id field, if any)
        //
    ];

    /**
     * the 'Chronique' admin uses its own editing template.
     *
     * {@inheritdoc}
     */
    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'IrsteaBdohAdminBundle:CRUD:edit-dataset.html.twig';
            default:
                return parent::getTemplate($name);
        }
    }

    /**
     * Sets 'observatoire' attribute to the current 'observatoire'.
     *
     * {@inheritdoc}
     */
    public function prePersist($dataset)
    {
        $dataset->setObservatoire(
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent()
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('titre')
            ->add('principalInvestigators')
            ->add('portailSearchCriteriaClimat')
            ->add('portailSearchCriteriaGeology');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('titre', null, ['route' => ['name' => 'edit']])
            ->add('principalInvestigators')
            ->add('observatoire')
            ->add('portailSearchCriteriaClimat')
            ->add('portailSearchCriteriaGeology');
    }

    public function validate(ErrorElement $errorElement, $object)
    {
        /**
         * Les contraintes s'active uniquement si le code Theia a été renseigné.
         */
        $observatoireCourant = $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent();
        $theiaCode = $observatoireCourant->getTheiaCode();
        if ($theiaCode !== null) {
            $errorElement
                ->with('principalInvestigators')
                ->assertCount(
                    ['min'           => 1,
                        'minMessage' => 'Il faut au moins un Principal Investigator (Contraintes Theia/Ozcar)',
                    ]
                )
                ->end()
                ->with('portailSearchCriteriaGeology')
                ->assertCount(
                    ['min'           => 1,
                        'minMessage' => 'Il faut au moins un critère (Contraintes Theia/Ozcar)',
                    ]
                )
                ->end()
                ->with('portailSearchCriteriaClimat')
                ->assertCount(
                    ['min'           => 1,
                        'minMessage' => 'Il faut au moins un critère (Contraintes Theia/Ozcar)',
                    ]
                )
                ->end()
                ->with('topicCategories')
                ->assertCount(
                    ['min'           => 1,
                        'minMessage' => 'Il faut au moins une catégorie (Contraintes Theia/Ozcar)',
                    ]
                )
                ->end();
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $translator = $this->getContainer()->get('translator');
        $repoChronique = $this->getBdohRepo('Chronique');
        $repoPersonneTheia = $this->getBdohRepo('PersonneTheia');

        $dataset = $this->getSubject();

        $formMapper
            ->add('titre', TextType::class, [
                'required' => true,
            ])
            ->add('titreEn', TextType::class, [
                'required' => true,
            ])
            ->add('uuid', HiddenType::class)
            ->add('description', TextareaType::class, [
                'required' => true,
            ])
            ->add('descriptionEn', TextareaType::class, [
                'required' => true,
            ])
            ->add('genealogie', TextareaType::class, [], [
                'required' => true,
            ])
            ->add('genealogieEn', TextareaType::class, [], [
                'required' => true,
            ])
            ->add('dataConstraint', null, [
                'required' => true,
            ])
            ->add(
                'doi',
                'sonata_type_model_list',
                [
                    'btn_add'  => false,
                    'required' => false,
                ],
                [
                ]
            )
            ->end()
            ->with('Contact', ['class' => 'col-md-6'])
            ->add('principalInvestigators', 'sonata_type_model', [
                'query'    => $repoPersonneTheia->createQueryBuilder('PersonneTheia'),
                'required' => true,
                'multiple' => true,
                'btn_add'  => $translator->trans('button.add'),
            ])
            ->add('dataManagers', 'sonata_type_model', [
                'query'    => $repoPersonneTheia->createQueryBuilder('PersonneTheia'),
                'required' => false,
                'multiple' => true,
                'btn_add'  => $translator->trans('button.add'),
            ])
            ->add('dataCollectors', 'sonata_type_model', [
                'query'    => $repoPersonneTheia->createQueryBuilder('PersonneTheia'),
                'required' => false,
                'multiple' => true,
                'btn_add'  => $translator->trans('button.add'),
            ])
            ->add('projectMembers', 'sonata_type_model', [
                'query'    => $repoPersonneTheia->createQueryBuilder('PersonneTheia'),
                'required' => false,
                'multiple' => true,
                'btn_add'  => $translator->trans('button.add'),
            ])
            ->end()
            ->with('Mots clés', ['class' => 'col-md-6'])
            ->add('topicCategories', null, [
                'required' => true,
            ])
            ->add('inspireTheme')
            ->add('portailSearchCriteriaClimat', null, [
                'required' => true,
                'expanded' => true,
            ])
            ->add('portailSearchCriteriaGeology', null, [
                'required' => true,
                'expanded' => true,
            ])
            ->end()
            ->with('Chroniques', ['class' => 'col-md-12'])
            ->add('chroniques', 'sonata_type_model', [
                'query'        => $repoChronique->createQueryBuilderFilter('Chronique', null, $dataset),
                'required'     => false,
                'btn_add'      => false,
                'multiple'     => true,
                'by_reference' => false,
                'property'     => 'nomLong', ])
            ->end()
            ->setHelps(
                [
                    'chroniques'             => $translator->trans('admin.help.astuce'),
                    'inspireTheme'           => $translator->trans('admin.help.astuce'),
                    'topicCategories'        => $translator->trans('admin.help.astuce'),
                    'projectMembers'         => $translator->trans('admin.help.astuce'),
                    'dataCollectors'         => $translator->trans('admin.help.astuce'),
                    'dataManagers'           => $translator->trans('admin.help.astuce'),
                    'principalInvestigators' => $translator->trans('admin.help.astuce'),
                    'description'            => $translator->trans('admin.help.dataset.description'),
                    'genealogie'             => $translator->trans('admin.help.dataset.genealogy'),
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function postPersist($object)
    {
        $this->logger->createHistoriqueAdministrationDataSet(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.DatasetCreate',
            $object
        );
    }

    /**
     * {@inheritdoc}
     */
    public function postUpdate($object)
    {
        $this->logger->createHistoriqueAdministrationDataSet(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.DatasetUpdate',
            $object
        );
    }

    /**
     * {@inheritdoc}
     */
    public function preRemove($object)
    {
        $this->logger->createHistoriqueAdministrationDataSet(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.DatasetRemove',
            $object
        );
    }
}
