<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohAdminBundle\Admin\Data;

use Doctrine\ORM\EntityRepository;
use Irstea\BdohDataBundle\Entity\ChroniqueContinue as ChroniqueContinueEntity;
use Irstea\BdohDataBundle\Entity\OptionEchantillonnageSortie;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Class ChroniqueContinue.
 */
class ChroniqueContinue extends Chronique
{
    /**
     * @var array
     */
    private $samplings = null;

    /**
     * @var array
     */
    private $outputTimeSteps = null;

    /**
     * @var null
     */
    private $hadToSetExportOptions = null;

    /**
     * @return array
     */
    public function getSamplings()
    {
        if ($this->samplings === null) {
            $this->samplings = [];
            foreach ($this->getBdohRepo('Echantillonnage')->findAll() as $sampling) {
                /* @var \Irstea\BdohDataBundle\Entity\Echantillonnage $sampling */
                $this->samplings[$sampling->getCode()] = $sampling;
            }
        }

        return $this->samplings;
    }

    /**
     * @return array
     */
    public function getCumulativeSamplings()
    {
        return $this->filterMapSamplings(
            null,
            function (\Irstea\BdohDataBundle\Entity\Echantillonnage $s) {
                return $s->getCode() === 'cumulative';
            }
        );
    }

    /**
     * @return array
     */
    public function getOrientedSamplings()
    {
        return $this->filterMapSamplings(
            null,
            function (\Irstea\BdohDataBundle\Entity\Echantillonnage $s) {
                return $s->getHasDirection();
            }
        );
    }

    /**
     * @return array
     */
    public function getSamplingOrders()
    {
        return $this->filterMapSamplings(
            function (\Irstea\BdohDataBundle\Entity\Echantillonnage $s) {
                return $s->getOrdreSortie();
            }
        );
    }

    /**
     * @param callable|null $map
     * @param callable|null $filter
     *
     * @return array
     */
    private function filterMapSamplings(callable $map = null, callable $filter = null)
    {
        $samplings = $this->getSamplings();
        $output = [];
        foreach ($samplings as $sampling) {
            /** @var \Irstea\BdohDataBundle\Entity\Echantillonnage $sampling */
            if ($filter === null || call_user_func($filter, $sampling)) {
                $output[$sampling->getId()] = $map ? call_user_func($map, $sampling) : true;
            }
        }

        return $output;
    }

    /**
     * @return array|null
     */
    public function getOutputTimeSteps()
    {
        if ($this->outputTimeSteps === null) {
            $ots = [];
            foreach ($this->getBdohRepo('OptionEchantillonnageSortie')->findAll() as $item) {
                /* @var OptionEchantillonnageSortie $item */
                $ots[$item->getId()] = $item->getEchantillonnageEntree()->getId();
            }
            $this->outputTimeSteps = $ots;
        }

        return $this->outputTimeSteps;
    }

    /**
     * @param bool $excludeNew
     *
     * @return bool
     */
    public function getHadToSetDefaultExportOptions($excludeNew = false)
    {
        $subject = $this->getSubject();
        if ($this->hadToSetExportOptions === null) {
            $this->hadToSetExportOptions = $subject && $subject->getMustEditExportOptions();
        }

        return $this->hadToSetExportOptions && (!$excludeNew || ($subject && $subject->getId() !== null));
    }

    /**
     * {@inheritdoc}
     */
    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'IrsteaBdohAdminBundle:CRUD:edit-chronique-continue.html.twig';
            default:
                return parent::getTemplate($name);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        /* @var $chronique ChroniqueContinueEntity */
        $chronique = $this->getSubject();

        // If export options were not set for this time series, then set default values
        if ($this->getHadToSetDefaultExportOptions()) {
            // If no sampling was defined for this time series, set 'instantaneous'
            $defaultSamplingCode = 'instantaneous';

            $allSamplings = $this->getSamplings();

            // Time series' sampling & its code
            $sampling = $chronique->getEchantillonnage();
            if (!$sampling) {
                $sampling = $allSamplings[$defaultSamplingCode];
                $chronique->setEchantillonnage($sampling);
            }
            $samplingCode = $sampling->getCode();

            // Default export options
            $dae = $this->getContainer()->getParameter('default_allowed_exports');
            $daeForSampling = $dae[$samplingCode];

            // Set default export samplings
            foreach ($daeForSampling['samplings'] as $code) {
                $chronique->addEchantillonnagesSortieLicite($allSamplings[$code]);
            }

            // Set default regular time steps
            foreach ($daeForSampling['steps'] as $valueAndUnit) {
                $qb = $this->getBdohRepo('OptionEchantillonnageSortie')->createQueryBuilder('oes')
                    ->leftJoin('oes.pasEchantillonnage', 'pe')
                    ->where('oes.echantillonnageEntree = :e')
                    ->andWhere('pe.valeur = :v')
                    ->andWhere('pe.unite = :u')
                    ->setParameters(
                        [
                            'e' => $sampling,
                            'v' => $valueAndUnit[0],
                            'u' => $valueAndUnit[1],
                        ]
                    );
                $oes = $qb->getQuery()->getSingleResult();
                $chronique->addOptionsEchantillonnageSortie($oes);
            }
        }

        if (!$chronique->getUniteCumul() && $chronique->getUnite()) {
            $chronique->setUniteCumul($chronique->getUnite());
        }

        parent::configureFormFields($formMapper);

        $translator = $this->getContainer()->get('translator');
        $inEnglish = \strpos($translator->getLocale(), 'en') === 0;
        $i18nSuffix = $inEnglish ? 'en' : '';

        $formMapper
            ->add(
                'echantillonnage',
                null,
                [
                    'required'                  => true,
                    'choice_label'              => 'code',
                    'choice_translation_domain' => 'echantillonnage',
                ]
            )
            ->add(
                'directionMesure',
                'choice',
                [ // Field for "measure direction"
                    'choices'  => [
                        'ahead'    => $translator->trans('period.ahead'),
                        'backward' => $translator->trans('period.backward'),
                    ],
                    'multiple' => false,
                    'expanded' => true,
                ]
            )
            ->add(
                'echantillonnagesSortieLicites',
                null,
                [ // Field for allowed regular-step sampling types
                    'multiple'     => true,
                    'expanded'     => true,
                    'choice_label' => 'nom' . $i18nSuffix,
                ]
            )
            ->add('uniteCumul')
            ->add(
                'optionsEchantillonnageSortie',
                null,
                [ // Field for allowed regular time steps
                    'multiple'      => true,
                    'expanded'      => true,
                    'attr'          => ['class' => 'inputs-list-inline'],
                    'choice_label'  => 'libelle' . $i18nSuffix,
                    'query_builder' => function (EntityRepository $repo) {
                        return $repo->createQueryBuilder('o')
                            ->innerJoin('o.pasEchantillonnage', 'p')
                            ->orderBy('p.approxSecondes', 'DESC');
                    },
                ]
            )
            ->setHelps(
                [
                    'unite' => $translator->trans('admin.help.chronique.update-unite-cumul'),
                ]
            );
    }
}
