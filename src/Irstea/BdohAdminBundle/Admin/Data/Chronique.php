<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohAdminBundle\Admin\Data;

use DateTime;
use Doctrine\DBAL\Types\TextType;
use Doctrine\ORM\EntityRepository;
use Irstea\BdohAdminBundle\Admin\Admin;
use Irstea\BdohDataBundle\Entity\ChroniqueContinue;
use Irstea\BdohDataBundle\Entity\Qualite;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\DoctrineORMAdminBundle\Filter\DateFilter;

/**
 * Class Chronique.
 */
class Chronique extends Admin
{
    /**
     * Configure the ordered field.
     */
    protected $datagridValues = [
        '_sort_order' => 'DESC',
        '_sort_by'    => 'dataset.titre',  // name of the ordered field
        // (default = the model's id field, if any)
    ];

    /**
     * Les points de contrôle de la chronique.
     */
    private $checkpoints = null;

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->add('list');
    }

    /**
     * @return array|null
     */
    public function getCheckpoints()
    {
        if ($this->checkpoints === null && $this->getSubject() && $this->getSubject()->getId()) {
            $this->checkpoints = $this->getBdohRepo('PointControle')->findByChronique($this->getSubject());
        }

        return $this->checkpoints;
    }

    /**
     * Les chroniques filles.
     */
    private $childrenTimeSeries = null;

    /**
     * @return array|null
     */
    public function getChildrenTimeSeries()
    {
        if ($this->childrenTimeSeries === null && $this->getSubject() && $this->getSubject()->getId()) {
            $this->childrenTimeSeries = $this->getBdohRepo('Chronique')->findChildren($this->getSubject());
        }

        return $this->childrenTimeSeries;
    }

    /**
     * @var null
     */
    private $mustDefineMilieu = null;

    /**
     * @return bool
     */
    public function getMustDefineMilieu()
    {
        $subject = $this->getSubject();
        if ($this->mustDefineMilieu === null) {
            $this->mustDefineMilieu = $subject && $subject->getId() && $subject->getMilieu() === null;
        }

        return $this->mustDefineMilieu;
    }

    /**
     * In order to automaticaly generate a Chronique.code from TypeParametre.code,
     * the 'Chronique' admin uses its own editing template.
     *
     * {@inheritdoc}
     */
    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'IrsteaBdohAdminBundle:CRUD:edit-chronique.html.twig';
            case 'delete':
                return 'IrsteaBdohAdminBundle:CRUD:delete-chronique.html.twig';
            default:
                return parent::getTemplate($name);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
          ->add('code')
          ->add('unite')
          ->add('station')
          ->add('station.sites', null, [], null, ['expanded' => false, 'multiple' => true])
            ->add('producteur', null, [], null, ['expanded' => false, 'multiple' => true])
            ->add('parametre', null, [], null, ['expanded' => false, 'multiple' => true])
            ->add('parametre.familleParametres', null, [], null, ['expanded' => false, 'multiple' => true])
            ->add('dateDebutMesures', DateFilter::class, [
            ], null, ['widget' => 'single_text'])
            ->add('dateFinMesures', DateFilter::class, [
            ], null, ['widget' => 'single_text'])
            ->add('dataset', null, [], null, ['expanded' => false, 'multiple' => true]);
    }

    /**
     * @param ErrorElement $errorElement
     * @param mixed        $object
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        /**
         * Les contraintes s'active uniquement si le code Theia a été renseigné et si la chronique est reliée à un jeu de donnée.
         */
        $observatoireCourant = $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent();
        $theiaCode = $observatoireCourant->getTheiaCode();
        $chronique = $this->getSubject();
        $dataset = $chronique->getDataSet();

        if ($theiaCode !== null && $dataset !== null) {
            $errorElement
                ->with('libelleEn')
                ->assertNotNull(
                    [
                        'message' => 'Il faut au moins un libellé en anglais (Contraintes Theia/Ozcar)',
                    ]
                )
                ->end();
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $isLocaleEn = \strpos($this->getContainer()->get('translator')->getLocale(), 'en') === 0;
        $label = $isLocaleEn ? 'nomEn' : 'nom';
        $listMapper
            ->addIdentifier('code', null, ['route' => ['name' => 'edit']])
            ->add('estVisible')
            ->add('unite')
            ->add('parametre', null, ['associated_property' => $label])
            ->add('parametre.familleParametres')
            ->add('producteur')
            ->add('dataSet')
            ->add('station')
            ->add('station.sites')
            ->add('dateDebutMesures', TextType::class, [])
            ->add('dateFinMesures', TextType::class, [])
        ->add('_action', 'actions', [
        'actions' => [
            'edit'   => [],
            'delete' => [],
        ], ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $repoPartenaire = $this->getBdohRepo('Partenaire');
        $repoStation = $this->getBdohRepo('Station');
        $repoParam = $this->getBdohRepo('TypeParametre');
        $translator = $this->getContainer()->get('translator');
        $dateDebutMesure = $this->getSubject()->getDateDebutMesures();
        $dateFinMesure = $this->getSubject()->getDateFinMesures();

        $propertyParameter = 'nom';
        if (($isLocaleEn = \strpos($translator->getLocale(), 'en') === 0)) {
            $propertyParameter .= 'En';
        }

        $formMapper
            ->add('station', null, ['required' => true, 'query_builder' => $repoStation->createSecuredQueryBuilderForMesure($this->getUser())])
            ->add(
                'parametre',
                null,
                [
                    'required'      => true,
                    'property'      => $propertyParameter,
                    'query_builder' => $repoParam->createQueryBuilder('p')->orderBy('p.' . $propertyParameter, 'ASC'),
                ]
            )
            ->add('code')
            ->add('libelle')
            ->add(
                'libelleEn',
                null,
                [
                    'required' => true,
                ]
            )
            ->add('genealogie', 'textarea', ['required' => false])
            ->add('genealogieEn', 'textarea', ['required' => false]);

        if ($dateDebutMesure !== null && $dateFinMesure !== null) {
            $formMapper->add('dataset');
        }

        $formMapper
            ->add('milieu', null, ['required' => true])
            ->add('estVisible', 'checkbox', ['required' => false])
            ->add('minimumValide', 'number', ['required' => false, 'precision' => 3])
            ->add('maximumValide', 'number', ['required' => false, 'precision' => 3])
            ->add('unite', null, ['required' => true])
            ->add('producteur', 'sonata_type_model', ['btn_add' => $translator->trans('button.add'),
                'required'                                      => false, 'query' => $repoPartenaire->createQueryBuilder(), ])

            ->setHelps(
                [
                    'code'                                  => $translator->trans('admin.help.chronique.code'),
                    'libelle'                               => $translator->trans('admin.help.chronique.libelle'),
                    'estVisible'                            => $translator->trans('admin.help.chronique.estVisible'),
                    'dateDebutMesures'                      => $translator->trans('admin.help.theia.theia-message'),
                    'dateFinMesures'                        => $translator->trans('admin.help.theia.theia-message'),
                    'libelleEn'                             => $translator->trans('admin.help.theia.theia-message'),
                    'unite'                                 => $translator->trans('admin.help.theia.theia-message'),
                ]
            );

        // Check whether the current "chronique"'s measures contain at least one
        // "value limit" (quality order = 600 or 700)
        $chronique = $this->getSubject();
        $hasLimiteValeur = $chronique->getId() !== null;
        if ($hasLimiteValeur) {
            $repoName = ($chronique instanceof ChroniqueContinue) ? 'Mesure' : 'Plage';
            /* @var $mesureOuPlageRepo EntityRepository */
            $mesureOuPlageRepo = $this->getBdohRepo($repoName);
            $hasLimiteValeur = (bool)
                $mesureOuPlageRepo->createQueryBuilder('m')->select('m')
                    ->where('m.chronique = :chronique')
                    ->leftJoin('m.qualite', 'q')->andWhere('q.ordre IN (:ordres)')
                    ->setParameters(['chronique' => $chronique, 'ordres' => Qualite::$ordresLimites])
                    ->setMaxResults(1)->getQuery()->getOneOrNullResult();
        }

        $isConvertie = $chronique->isConvertie();
        $valueLimits = (bool) $chronique->getAllowValueLimits();

        if ($hasLimiteValeur) {
            $formMapper->add(
                $allowValueLimitsField = 'allowValueLimits_dummy',
                'checkbox',
                [
                    'required' => true,
                    'disabled' => true,
                    'data'     => true,
                    'mapped'   => false,
                    'label'    => 'allowValueLimits',
                ]
            );
            // s'il s'agit d'une chronique convertie, le champs est purrement informatif, il ne peut pas être modifié
            if (!$isConvertie) {
                $formMapper->add(
                    'allowValueLimits',
                    'hidden',
                    [
                        'required' => true,
                        'data'     => true,
                    ]
                );
            }
            $formMapper->setHelps(
                [
                    $allowValueLimitsField => $isConvertie ? 'chronique.lq_ld.legacy_and_already_present' : 'chronique.lq_ld.forced_allow_already_present',
                ]
            );
        } else {
            if ($this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent()->getJeu()->hasValueLimits()) {
                if ($isConvertie) {
                    $formMapper
                        ->add(
                            $allowValueLimitsField = 'allowValueLimits_dummy',
                            'checkbox',
                            [
                                'required' => false,
                                'disabled' => true,
                                'data'     => $valueLimits,
                                'mapped'   => false,
                                'label'    => 'allowValueLimits',
                            ]
                        )
                        // s'il s'agit d'une chronique convertie, le champs est purrement informatif, il ne peut pas être modifié
                        ->setHelps(
                            [
                                $allowValueLimitsField => 'chronique.lq_ld.legacy_parent_time_series',
                            ]
                        );
                } else {
                    $formMapper->add('allowValueLimits', 'checkbox', ['required' => false]);
                }
            }
            // je ne sais pas ce qui est le mieux entre le laisser à NULL ou le forcer à FALSE quand ce n'est pas autorisé
            /*else {
                $formMapper->add(
                    'allowValueLimits',
                    'hidden',
                    [
                        'required' => false,
                        'data'     => false,
                    ]
                );
            }*/
        }
    }

    /**
     * {@inheritdoc}
     */
    public function postPersist($object)
    {
        $em = $this->getModelManager()->getEntityManager($object);
        $object->setEchantillonageSet(true);
        $em->persist($object);
        $em->flush();

        $this->logger->createHistoriqueAdministrationChronique(
            $this->getUser(),
            new DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.ChroniqueCreate',
            $object
        );
    }

    /**
     * {@inheritdoc}
     */
    public function postUpdate($object)
    {
        $em = $this->getModelManager()->getEntityManager($object);
        $object->setEchantillonageSet(true);
        $em->persist($object);
        $em->flush();

        $this->logger->createHistoriqueAdministrationChronique(
            $this->getUser(),
            new DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.ChroniqueUpdate',
            $object
        );
    }

    /**
     * {@inheritdoc}
     */
    public function preRemove($object)
    {
        $this->logger->createHistoriqueAdministrationChronique(
            $this->getUser(),
            new DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.ChroniqueRemove',
            $object
        );
    }
}
