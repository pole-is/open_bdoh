<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohAdminBundle\Admin\Data;

use Irstea\BdohAdminBundle\Admin\Admin;
use Irstea\BdohDataBundle\Entity\Repository\FamilleParametresRepository;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Class FamilleParametres.
 */
class FamilleParametres extends Admin
{
    /**
     * Configure the ordered field.
     */
    protected $datagridValues = [
        '_sort_by' => 'nom',  // name of the ordered field
        // (default = the model's id field, if any)
    ];

    /**
     * Les familles qui utilisent cette famille comme famille mère.
     */
    private $childrenFamilies = null;

    /**
     * @return array|null
     */
    public function getChildrenFamilies()
    {
        if ($this->childrenFamilies === null && $this->getSubject() && $this->getSubject()->getId()) {
            $this->childrenFamilies = $this->getBdohRepo('FamilleParametres')->findByFamilleParente($this->getSubject());
        }

        return $this->childrenFamilies;
    }

    /**
     * Les types de paramètres qui sont dans cette famille de paramètres.
     */
    private $parameterTypes = null;

    /**
     * @return array|null
     */
    public function getParameterTypes()
    {
        if ($this->parameterTypes === null && $this->getSubject() && $this->getSubject()->getId()) {
            $this->parameterTypes = $this->getBdohRepo('TypeParametre')->findByFamilleParametres($this->getSubject());
        }

        return $this->parameterTypes;
    }

    /**
     * {@inheritdoc}
     */
    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'IrsteaBdohAdminBundle:CRUD:edit-famille-parametres.html.twig';
            default:
                return parent::getTemplate($name);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $isLocaleEn = \strpos($this->getContainer()->get('translator')->getLocale(), 'en') === 0;
        $label = $isLocaleEn ? 'nomEn' : 'nom';
        $listMapper
            ->addIdentifier('nom', null, ['route' => ['name' => 'edit']])
            ->addIdentifier('nomEn', null, ['route' => ['name' => 'edit']])
            ->addIdentifier('prefix')
            ->add('familleParente', null, ['associated_property' => $label]);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $translator = $this->getContainer()->get('translator');
        $isLocaleEn = \strpos($translator->getLocale(), 'en') === 0;
        /** @var FamilleParametresRepository $familleRepo */
        $familleRepo = $this->getBdohRepo('FamilleParametres');
        $choices = $familleRepo->findAllHierachy($this->getSubject() ?: null, $isLocaleEn);
        $label = $isLocaleEn ? 'nomEn' : 'nom';
        $formMapper
            ->add('nom')
            ->add('nomEn')
            ->add('prefix')
            ->add(
                'familleParente',
                ChoiceType::class,
                [
                    'choices'           => $choices,
                    'choice_label'      => $label,
                    'choice_attr'       => function ($family) {
                        /* @var \Irstea\BdohDataBundle\Entity\FamilleParametres $family */
                        return ['class' => 'hierarchy-level-' . $family->depth];
                    },
                    'choices_as_values' => true,
                    'required'          => false,
                ]
            )
            ->setHelps(
                [
                    'prefix'         => $translator->trans('admin.help.familleParametres.help-prefix'),
                    'familleParente' => $translator->trans('admin.help.familleParametres.help-familleParente'),
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('familleParente');
    }

    /**
     * {@inheritdoc}
     */
    public function postPersist($object)
    {
        $this->logger->createHistoriqueAdministrationFamilleParametres(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.FamilleParametresCreate',
            $object
        );
    }

    /**
     * {@inheritdoc}
     */
    public function postUpdate($object)
    {
        $this->logger->createHistoriqueAdministrationFamilleParametres(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.FamilleParametresUpdate',
            $object
        );
    }

    /**
     * {@inheritdoc}
     */
    public function preRemove($object)
    {
        $this->logger->createHistoriqueAdministrationFamilleParametres(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.FamilleParametresRemove',
            $object
        );
    }
}
