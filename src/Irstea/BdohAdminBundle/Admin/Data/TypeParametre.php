<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohAdminBundle\Admin\Data;

use Irstea\BdohAdminBundle\Admin\Admin;
use Irstea\BdohDataBundle\Entity\Repository\FamilleParametresRepository;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Class TypeParametre.
 */
class TypeParametre extends Admin
{
    /**
     * Configure the ordered field.
     */
    protected $datagridValues = [
        '_sort_by' => 'nom',  // name of the ordered field
        // (default = the model's id field, if any)
    ];

    /**
     * Les chroniques qui utilisent ce type de paramètre.
     */
    private $timeSeries = null;

    /**
     * @return array|null
     */
    public function getTimeSeries()
    {
        if ($this->timeSeries === null && $this->getSubject() && $this->getSubject()->getId()) {
            $this->timeSeries = $this->getBdohRepo('Chronique')->findByParametre($this->getSubject());
        }

        return $this->timeSeries;
    }

    /**
     * @var null
     */
    private $mustDefineParameterFamily = null;

    /**
     * @return bool
     */
    public function getMustDefineParameterFamily()
    {
        $subject = $this->getSubject();
        if ($this->mustDefineParameterFamily === null) {
            $this->mustDefineParameterFamily = $subject && $subject->getId() && $subject->getFamilleParametres() === null;
        }

        return $this->mustDefineParameterFamily;
    }

    /**
     * {@inheritdoc}
     */
    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'IrsteaBdohAdminBundle:CRUD:edit-type-parametre.html.twig';
            default:
                return parent::getTemplate($name);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $isLocaleEn = \strpos($this->getContainer()->get('translator')->getLocale(), 'en') === 0;
        $label = $isLocaleEn ? 'nomEn' : 'nom';
        $listMapper
            ->addIdentifier('nom', null, ['route' => ['name' => 'edit']])
            ->addIdentifier('nomEn', null, ['route' => ['name' => 'edit']])
            ->add('code')
            ->add('familleParametres', null, ['associated_property' => $label]);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $isLocaleEn = \strpos($this->getContainer()->get('translator')->getLocale(), 'en') === 0;
        /** @var FamilleParametresRepository $familleRepo */
        $familleRepo = $this->getBdohRepo('FamilleParametres');
        $choices = $familleRepo->findAllHierachy(null, $isLocaleEn);
        $label = $isLocaleEn ? 'nomEn' : 'nom';
        $formMapper
            ->add('nom')
            ->add('nomEn')
            ->add('code')
            ->add(
                'familleParametres',
                ChoiceType::class,
                [
                    'choices'           => $choices,
                    'choice_label'      => $label,
                    'choice_attr'       => function ($family) {
                        /* @var \Irstea\BdohDataBundle\Entity\FamilleParametres $family */
                        return ['class' => 'hierarchy-level-' . $family->depth];
                    },
                    'choices_as_values' => true,
                    'required'          => true,
                ]
            )
            ->add(
                'unites',
                'sonata_type_model',
                [
                    'expanded' => true,
                    'multiple' => true,
                    'required' => false,
                    'query'    => $this->getBdohRepo('Unite')->createOrderedQueryBuilder(),
                    'attr'     => ['class' => 'inputs-list-inline'],
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('familleParametres');
    }

    /**
     * {@inheritdoc}
     */
    public function postPersist($object)
    {
        $this->logger->createHistoriqueAdministrationTypeParametre(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.TypeParametreCreate',
            $object
        );
    }

    /**
     * {@inheritdoc}
     */
    public function postUpdate($object)
    {
        $this->logger->createHistoriqueAdministrationTypeParametre(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.TypeParametreUpdate',
            $object
        );
    }

    /**
     * {@inheritdoc}
     */
    public function preRemove($object)
    {
        $this->logger->createHistoriqueAdministrationTypeParametre(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.TypeParametreRemove',
            $object
        );
    }
}
