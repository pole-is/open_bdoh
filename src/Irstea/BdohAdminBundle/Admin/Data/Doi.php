<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohAdminBundle\Admin\Data;

use Irstea\BdohAdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class Doi.
 */
class Doi extends Admin
{
    /**
     * Configure the ordered field.
     */
    protected $datagridValues = [
        '_sort_by' => 'identifiant',  // name of the ordered field
        // (default = the model's id field, if any)
    ];

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('identifiant')
            ->add('description');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('identifiant', null, ['route' => ['name' => 'edit']])
            ->add('description');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('identifiant', TextType::class, ['attr' => ['maxlength' => '255']])
            ->add('description', TextType::class, ['attr' => ['maxlength' => '750']])
            ->add('descriptionEn', TextType::class, ['attr' => ['maxlength' => '750']])
            ->setHelps(
                [
                    'identifiant' => 'admin.help.doi.identifiant',
                    'description' => 'admin.help.doi.description',
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist($doi)
    {
        /* @var \Irstea\BdohDataBundle\Entity\Doi $doi */
        $doi->setObservatoire(
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function postPersist($object)
    {
        $this->logger->createHistoriqueAdministrationDoi(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.DoiCreate',
            $object
        );
    }

    /**
     * {@inheritdoc}
     */
    public function postUpdate($object)
    {
        $this->logger->createHistoriqueAdministrationDoi(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.DoiUpdate',
            $object
        );
    }

    /**
     * {@inheritdoc}
     */
    public function preRemove($object)
    {
        $this->logger->createHistoriqueAdministrationDoi(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.DoiRemove',
            $object
        );
    }
}
