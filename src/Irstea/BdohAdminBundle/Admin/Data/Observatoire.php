<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohAdminBundle\Admin\Data;

use Irstea\BdohAdminBundle\Admin\Admin;
use Irstea\BdohAdminBundle\Form\Type\FileType;
use Irstea\BdohAdminBundle\Form\Type\WysiwygType;
use Irstea\BdohDataBundle\Entity\JeuQualite;
use Irstea\BdohDataBundle\Entity\Repository\JeuQualiteRepository;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class Observatoire.
 */
class Observatoire extends Admin
{
    /**
     * @var \Irstea\BdohDataBundle\Entity\Observatoire|null
     */
    protected $previousObs = null;

    /**
     * @var bool|null
     */
    protected $isCurrentObs = null;

    /**
     * @return bool|null
     */
    public function isCurrentObs()
    {
        if ($this->isCurrentObs === null && $this->getSubject() && $this->getSubject()->getId()) {
            $this->isCurrentObs =
                $this->getSubject() === $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent();
        }

        return $this->isCurrentObs;
    }

    /**
     * Configure the ordered field.
     */
    protected $datagridValues = [
        '_sort_by' => 'nom',  // name of the ordered field
        // (default = the model's id field, if any)
    ];

    /**
     * List of sites in the observatory.
     */
    private $sites = null;

    /**
     * @return array|null
     */
    public function getSites()
    {
        if ($this->sites === null && $this->getSubject() && $this->getSubject()->getId()) {
            $this->sites = $this->getSubject()->getSites();
        }

        return $this->sites;
    }

    /**
     * Number of scales linked to the observatory.
     */
    private $scales = null;

    /**
     * @return array|null
     */
    public function getScales()
    {
        if ($this->scales === null && $this->getSubject() && $this->getSubject()->getId()) {
            $this->scales = $this->getBdohRepo('Bareme')->findByObservatoire($this->getSubject());
        }

        return $this->scales;
    }

    /**
     * Number of basins linked to the observatory.
     */
    private $basins = null;

    /**
     * @return array|null
     */
    public function getBasins()
    {
        if ($this->basins === null && $this->getSubject() && $this->getSubject()->getId()) {
            $this->basins = $this->getBdohRepo('Bassin')->findByObservatoire($this->getSubject());
        }

        return $this->basins;
    }

    /**
     * Number of river linked to the observatory.
     */
    private $rivers = null;

    /**
     * @return array|null
     */
    public function getRivers()
    {
        if ($this->rivers === null && $this->getSubject() && $this->getSubject()->getId()) {
            $this->rivers = $this->getBdohRepo('CoursEau')->findByObservatoire($this->getSubject());
        }

        return $this->rivers;
    }

    /**
     * Number of DOIs linked to the observatory.
     */
    private $dois = null;

    /**
     * @return array|null
     */
    public function getDois()
    {
        if ($this->dois === null && $this->getSubject() && $this->getSubject()->getId()) {
            $this->dois = $this->getBdohRepo('Doi')->findByObservatoire($this->getSubject());
        }

        return $this->dois;
    }

    private $partenaires = null;

    /**
     * @return array|null
     */
    public function getPartenaires()
    {
        if ($this->partenaires === null && $this->getSubject() && $this->getSubject()->getId()) {
            $this->partenaires = $this->getBdohRepo('Partenaires')->findByObservatoire($this->getSubject());
        }

        return $this->partenaires;
    }

    /**
     * @var null
     */
    private $mustDefineUpdate = null;

    /**
     * @return bool
     */
    public function getMustDefineUpdate()
    {
        $subject = $this->getSubject();
        if ($this->mustDefineUpdate === null) {
            $this->mustDefineUpdate = $subject && $subject->getId() && $subject->getTheiaCode() === null;
        }

        return $this->mustDefineUpdate;
    }

    /**
     * Pre-update :
     *  => Moves uploaded image files to webdir.
     *
     * {@inheritdoc}
     */
    public function preUpdate($object)
    {
        /*
         * Before update, Doctrine has already persited in cache the modification
         * That's why if we want to find the slug before pre update it doesn't exist
         * Now we know there is a modification of slug if the result of the query returns null
         */

        $oldObs = $this->getBdohRepo('Observatoire')->findOneBySlug($object->getSlug());
        $this->previousObs = $oldObs;
    }

    /**
     * {@inheritdoc}
     */
    public function postPersist($object)
    {
        $this->logger->createHistoriqueAdministrationObservatoire(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.ObservatoireCreate',
            $object
        );
    }

    /**
     * Post-update :
     *  => Removes 'observatoire' css file. It will be recreated on the next request.
     *  => Redirect to home.
     *
     * {@inheritdoc}
     */
    public function postUpdate($object)
    {
        $this->logger->createHistoriqueAdministrationObservatoire(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.ObservatoireUpdate',
            $object
        );

        /* on ne redirifge sur le nouveau slug que si c'est l'observatoire courant qui a été modifié */
        if (!$this->previousObs && $this->isCurrentObs()) {
            $redirect = new RedirectResponse(
                $this->getContainer()->get('router')->generate(
                    'bdoh_admin_observatoire_edit',
                    ['id' => $object->getId(), '_observatoire' => $object->getSlug()]
                )
            );
            $redirect->send();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function preRemove($object)
    {
        /* construction / enregistrement de l'information isCurrentObs avant la suppression */
        $this->isCurrentObs();

        $this->logger->createHistoriqueAdministrationObservatoire(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.ObservatoireRemove',
            $object
        );
    }

    /**
     * {@inheritdoc}
     */
    public function postRemove($object)
    {
        /* redirection sur la page d'accueil de tous les observatoires si on supprime l'observatoire courant */
        if ($this->isCurrentObs()) {
            $redirect = new RedirectResponse(
                $this->getContainer()->get('router')->generate('bdoh_all_home')
            );
            $redirect->send();
        }
    }

    /**
     * Since it uses "colopicker", "image" and "wysiwyg html editor" widgets,
     * the 'Observatoire' admin uses its own editing template.
     *
     * {@inheritdoc}
     */
    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'IrsteaBdohAdminBundle:CRUD:edit-observatoire.html.twig';
            case 'delete':
                return 'IrsteaBdohAdminBundle:CRUD:delete-observatoire.html.twig';
            default:
                return parent::getTemplate($name);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nom')
            ->add('theiaCode')
            ->add('projectLeader')
            ->add('dataManagers')
            ->add('titre')
            ->add('partenaires')
            ->add('dataSets');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('nom', null, ['route' => ['name' => 'edit']])
            ->add('theiaCode')
            ->add('projectLeader')
            ->add('dataManagers')
            ->add('titre')
            ->add('partenaires')
            ->add('dataSets');
    }

    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
            ->with('theiaCode')
            ->assertLength([
                'min' => 4,
                'max' => 4, ])
            ->assertRegex([
                'pattern' => '/[a-z0-9]/',
                'match'   => false,
                'message' => 'Un Identifiant Theia doit faire 4 caractères et être en MAJUSCULE !!!',
            ])
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $translator = $this->getContainer()->get('translator');
        $repoPersonneTheia = $this->getBdohRepo('PersonneTheia');

        $formMapper
            ->add(
                'nom',
                TextType::class,
                [
                    'required' => true,
                ]
            )
            ->add(
                'titre',
                TextType::class,
                [
                    'required' => true,
                ]
            )
            ->add(
                'titreEn',
                TextType::class,
                [
                    'required' => true,
                ]
            )
            ->add('description', WysiwygType::class)
            ->add('descriptionEn', WysiwygType::class)
            ->add('email', EmailType::class, [
                'required' => false,
            ])
            ->add('lien', UrlType::class, ['required' => false])
            ->add(
                'jeu',
                EntityType::class,
                [
                    'required'      => true,
                    'expanded'      => true,
                    'multiple'      => false,
                    'class'         => JeuQualite::class,
                    'query_builder' => function (JeuQualiteRepository $repo) {
                        return $repo->createQueryBuilder('jq')->where('jq.estAffectable = TRUE');
                    },
                ]
            )
            ->add(
                'doiPrincipal',
                'sonata_type_model_list',
                [
                    'btn_add'  => false,
                    'required' => false,
                ],
                ['placeholder' => 'Pas de DOI',
                ]
            )
            ->add(
                'theiaCode',
                TextType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'theiaPassword',
                PasswordType::class,
                [
                    'required' => false,
                ]
            )
            ->end()
            ->with('Contact', ['class' => 'col-md-12'])
            ->add('projectLeader', 'sonata_type_model', [
                'query'    => $repoPersonneTheia->createQueryBuilder('PersonneTheia'),
                'required' => true,
                'multiple' => false,
                'btn_add'  => $translator->trans('button.add'),
            ])
            ->add('dataManagers', 'sonata_type_model', [
                'query'    => $repoPersonneTheia->createQueryBuilder('PersonneTheia'),
                'required' => false,
                'multiple' => true,
                'btn_add'  => $translator->trans('button.add'),
            ])
            ->end()
            ->with('fieldset.graphic', ['collapsed' => true])
            // colorpicker 1st widget
            ->add('couleurPrimaire', TextType::class, ['required' => true], ['block_name' => 'couleur'])
            // colorpicker 2nd widget
            ->add('couleurSecondaire', TextType::class, ['required' => true], ['block_name' => 'couleur'])
            // image widget
            ->add(
                'pathLogo',
                FileType::class,
                ['required' => false, 'accept' => ['image/*', '.png', '.jpg', '.gif'], 'label' => 'fileLogo']
            )
            // image widget
            ->add(
                'pathPhotoPrincipale',
                FileType::class,
                ['required' => false, 'accept' => ['image/*', '.png', '.jpg', '.gif'], 'label' => 'filePhotoPrincipale']
            )
            // image widget
            ->add(
                'pathPhotoSecondaire',
                FileType::class,
                ['required' => false, 'accept' => ['image/*', '.png', '.jpg', '.gif'], 'label' => 'filePhotoSecondaire']
            )
            ->end()
            ->setHelps(
                [
                    'jeu'          => $translator->trans('admin.help.observatoire.jeu'),
                    'titre'        => $translator->trans('admin.help.observatoire.titre-info'),
                    'email'        => $translator->trans('admin.help.observatoire.email-info'),
                    'theiaCode'    => $translator->trans('admin.help.observatoire.theiaCode-message'),
                    'doiPrincipal' => $translator->trans('admin.help.observatoire.doi-principal-message'),
                    'dataManagers' => $translator->trans('admin.help.astuce'),
                ]
            );

        // Met en place une gestion spéciale pour le mot de passe
        $formMapper->get('theiaPassword')
            ->addEventSubscriber($this->getContainer()->get('irstea_bdoh_theia_ozcar.password_event_subscriber'));
    }
}
