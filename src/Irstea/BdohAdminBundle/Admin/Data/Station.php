<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohAdminBundle\Admin\Data;

use Irstea\BdohAdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Class Station.
 */
class Station extends Admin
{
    /**
     * Configure the ordered field.
     */
    protected $datagridValues = [
        '_sort_by' => 'nom',  // name of the ordered field
        // (default = the model's id field, if any)
    ];

    /**
     * List of time series in the station.
     */
    private $timeSeries = null;

    /**
     * @return array|null
     */
    public function getTimeSeries()
    {
        if ($this->timeSeries === null && $this->getSubject() && $this->getSubject()->getId()) {
            $this->timeSeries = $this->getSubject()->getChroniques();
        }

        return $this->timeSeries;
    }

    /**
     * Les bassins qui utilisent cette station en tant que station exutoire.
     */
    private $basins = null;

    /**
     * @return array|null
     */
    public function getBasins()
    {
        if ($this->basins === null && $this->getSubject() && $this->getSubject()->getId()) {
            $this->basins = $this->getBdohRepo('Bassin')->findByStationExutoire($this->getSubject());
        }

        return $this->basins;
    }

    /**
     * In order to delete the "add and delete" tools for Station.commune field,
     * the 'Station' admin uses its own editing template.
     *
     * {@inheritdoc}
     */
    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'IrsteaBdohAdminBundle:CRUD:edit-station.html.twig';
            case 'delete':
                return 'IrsteaBdohAdminBundle:CRUD:delete-station.html.twig';
            default:
                return parent::getTemplate($name);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('nom', null, ['route' => ['name' => 'edit']])
            ->add('code')
            ->add('codeAlternatif')
            ->add('commune')
            ->add('altitude')
            ->add('estActive');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $translator = $this->getContainer()->get('translator');
        $repoSite = $this->getBdohRepo('SiteExperimental');
        $repoCommune = $this->getBdohRepo('Commune');

        $formMapper
            ->add('nom')
            ->add('code')
            ->add(
                'commune',
                'sonata_type_model_autocomplete',
                ['property'  => ['nom', 'codePostal'], 'required'  => false]
            )
            ->add('sites', null, ['required' => true, 'query_builder' => $repoSite->createSecuredQueryBuilder($this->getUser())])
            ->add('codeAlternatif')
            ->add('altitude', 'number', ['required' => false, 'precision' => 3])
            ->add('latitude', 'number', ['required' => false, 'precision' => 3])
            ->add('longitude', 'number', ['required' => false, 'precision' => 3])
            ->add('commentaire', 'textarea', ['required' => false])
            ->add('commentaireEn', 'textarea', ['required' => false])
            ->add('estActive', 'checkbox', ['required' => false])
            ->setHelps(
                [
                    'code'           => $translator->trans('admin.help.station.code'),
                    'codeAlternatif' => $translator->trans('admin.help.station.codeAlternatif'),
                    'latitude'       => $translator->trans('admin.help.station.latlong'),
                    'longitude'      => $translator->trans('admin.help.station.latlong'),
                    'commune'        => $translator->trans('admin.help.station.town-name-is-case-sensitive'),
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function postPersist($object)
    {
        $this->logger->createHistoriqueAdministrationStation(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.StationCreate',
            $object
        );
    }

    /**
     * {@inheritdoc}
     */
    public function postUpdate($object)
    {
        $stationRepo = $this->getBdohRepo('Station');
        $stationRepo->updatePosition($object);

        $this->logger->createHistoriqueAdministrationStation(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.StationUpdate',
            $object
        );
    }

    /**
     * {@inheritdoc}
     */
    public function preRemove($object)
    {
        $this->logger->createHistoriqueAdministrationStation(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.StationRemove',
            $object
        );
    }
}
