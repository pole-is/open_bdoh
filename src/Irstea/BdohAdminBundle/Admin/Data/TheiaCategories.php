<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohAdminBundle\Admin\Data;

use Irstea\BdohAdminBundle\Admin\Admin;
use Irstea\BdohAdminBundle\Form\Type\TheiaThesaurusType;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class TheiaCategories extends Admin
{
    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('nom', null, ['route' => ['name' => 'edit']])
            ->add('familleParametre')
            ->add('milieu');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('milieu')
            ->add('familleParametre');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $translator = $this->getContainer()->get('translator');

        $formMapper
            ->add('milieu', null, ['required' => true])
            ->add('nom', HiddenType::class)
            ->add('familleParametre', null, ['required' => true])
            ->add(
                'uriOzcarTheia',
                TheiaThesaurusType::class,
                [
                    'multiple' => true,
                    'required' => true,
                ]
            )
        ->setHelps(['uriOzcarTheia' => $translator->trans('admin.help.theia.uriOzcarTheia')]);
    }

    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
            ->with('uriOzcarTheia')
            ->assertCount(
                [
                    'min'        => 1,
                    'minMessage' => 'Veuillez sélectionner au moins une catégorie',
                ]
            )
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    public function postPersist($object)
    {
        $this->logger->createHistoriqueAdministrationTheiaCategories(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.TheiaCategoriesCreate',
            $object
        );
    }

    /**
     * {@inheritdoc}
     */
    public function postUpdate($object)
    {
        $this->logger->createHistoriqueAdministrationTheiaCategories(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.TheiaCategoriesUpdate',
            $object
        );
    }

    /**
     * {@inheritdoc}
     */
    public function preRemove($object)
    {
        $this->logger->createHistoriqueAdministrationTheiaCategories(
            $this->getUser(),
            new \DateTime(),
            $this->getContainer()->get('irstea_bdoh.manager.observatoire')->getCurrent(),
            'historique.actions.TheiaCategoriesRemove',
            $object
        );
    }
}
