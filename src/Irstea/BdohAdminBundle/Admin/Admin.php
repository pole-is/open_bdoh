<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohAdminBundle\Admin;

use Irstea\BdohDataBundle\Entity\Repository\EntityRepository;
use Irstea\BdohLoggerBundle\Logger\BdohLogger;
use Irstea\BdohSecurityBundle\Entity\Utilisateur;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Translator\NoopLabelTranslatorStrategy;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Defines some parameters & treatments for all BDOH admin classes.
 */
abstract class Admin extends AbstractAdmin
{
    /**
     * @var BdohLogger
     */
    protected $logger;

    /**
     * @param BdohLogger $logger
     */
    public function setLogger(BdohLogger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function setRequest(Request $request = null)
    {
        if ($request !== null) {
            parent::setRequest($request);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
            case 'list':
            case 'delete':
                return 'IrsteaBdohAdminBundle:CRUD:' . $name . '.html.twig';
            default:
                return parent::getTemplate($name);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setFormGroups(array $formGroups)
    {
        foreach ($formGroups as &$formGroup) {
            if ($formGroup['label'] === $this->getLabel()) {
                $formGroup['label'] = $this->getContainer()->get('translator')->trans($this->getClassnameLabel(), [], 'entitiesSingulars');
            }
        }
        parent::setFormGroups($formGroups);
    }

    public function configure()
    {
        /* Label name */
        $this->setLabel($this->getContainer()->get('translator')->trans($this->getClassnameLabel(), [], 'entitiesPlurals'));

        /* Labels translator strategy */
        $transStrategy = new NoopLabelTranslatorStrategy();
        $this->setLabelTranslatorStrategy($transStrategy);

        /* Route name */
        $this->baseRouteName = 'bdoh_admin_' . $this->urlize($this->getClassnameLabel());

        /* Route pattern */
        $this->baseRoutePattern = $this->urlize($this->getClassnameLabel(), '-');
    }

    /**
     * Direct access to the service container.
     *
     * @return ContainerInterface
     */
    protected function getContainer()
    {
        return $this->getConfigurationPool()->getContainer();
    }

    /**
     * Direct access to the current User.
     *
     * @return Utilisateur|null
     */
    protected function getUser()
    {
        /** @var TokenInterface $token */
        $token = $this->getContainer()->get('security.token_storage')->getToken();

        return $token ? $token->getUser() : null;
    }

    /**
     * Shortcut to return a Doctrine repository.
     *
     * @param string $entity
     *
     * @return
     */
    public function getRepository($entity)
    {
        return $this->getModelManager()->getEntityManager($entity)->getRepository($entity);
    }

    /**
     *  Shortcut to return a Doctrine repository of a BDOH entity.
     *
     * @param string $bdohEntity
     *
     * @return EntityRepository
     */
    public function getBdohRepo($bdohEntity)
    {
        return $this->getRepository('IrsteaBdohDataBundle:' . $bdohEntity);
    }

    /**
     * No export formats.
     */
    public function getExportFormats()
    {
        return [];
    }

    /**
     * No batch actions.
     */
    public function getBatchActions()
    {
        return [];
    }
}
