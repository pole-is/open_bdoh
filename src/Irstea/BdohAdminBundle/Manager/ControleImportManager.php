<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohAdminBundle\Manager;

use Symfony\Component\HttpFoundation\ParameterBag;

class ControleImportManager
{
    /**
     * @var string
     */
    protected $appAbsolutePath;

    /**
     * @var string
     */
    protected $directory;

    /**
     * @var array
     */
    protected $mimeTypes;

    /**
     * @var Symfony\Component\HttpFoundation\ParameterBag
     */
    public $importers;

    /**
     * Constructor.
     *
     * @param string $appAbsolutePath
     * @param array  $controleImportParams See src/Irstea//BdohAdminBundle/Resources/config/services.yml
     */
    public function __construct($appAbsolutePath, array $controleImportParams)
    {
        $this->appAbsolutePath = $appAbsolutePath;
        $this->directory = $controleImportParams['directory'];
        $this->mimeTypes = $controleImportParams['mimeTypes'];
        $this->importers = new ParameterBag($controleImportParams['importers']);
    }

    /**
     * Checks that given mime type is valid for given category.
     * If no category given, checks that it is valid for ALL categories.
     *
     * @param string $mime
     * @param string $category
     *
     * @return bool
     */
    public function isValidMimeType($mime, $category = '')
    {
        if ($category && array_key_exists($category, $this->mimeTypes)) {
            return in_array($mime, $this->mimeTypes[$category]);
        }
        foreach ($this->mimeTypes as $mimeTypes) {
            if (in_array($mime, $mimeTypes)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns directory containing measures files to import.
     *
     * @return string Absolute path
     */
    public function getDir()
    {
        return $this->appAbsolutePath . DIRECTORY_SEPARATOR . $this->directory;
    }

    /**
     * Returns sorted array of measure files to import.
     *
     * @return array
     */
    public function getFiles()
    {
        $files = [];
        $all = scandir($dirname = $this->getDir());

        foreach ($all as $e) {
            $e = $dirname . $e;
            if (is_file($e)) {
                $files[] = $e;
            }
        }

        return $files;
    }
}
