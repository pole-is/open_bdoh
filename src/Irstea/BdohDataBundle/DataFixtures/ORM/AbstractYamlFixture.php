<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Yaml\Yaml;

/**
 * Class AbstractYamlFixture.
 */
abstract class AbstractYamlFixture extends AbstractFixture
{
    /**
     * Entry point of data loading.
     */
    public function load(ObjectManager $manager)
    {
        $data = $this->readYaml();
        $this->loadEntities($data, $manager);
        $manager->flush();
    }

    /**
     * @return array
     */
    protected function readYaml()
    {
        $path = $this->getFilePath();
        $content = file_get_contents($path);

        return Yaml::parse($content);
    }

    /**
     * @return string
     */
    protected function getFilePath()
    {
        return $this->getBaseDir() . '/files/' . $this->getFilename();
    }

    /**
     * @return string
     */
    protected function getFilename()
    {
        $parts = explode('\\', get_class($this));

        return end($parts) . 'Data.yml';
    }

    /**
     * @return string
     */
    protected function getBaseDir()
    {
        $reflThis = new \ReflectionObject($this);

        return dirname($reflThis->getFileName());
    }

    /**
     * @param array         $data
     * @param ObjectManager $manager
     *
     * @return mixed
     */
    protected function loadEntities(array $data, ObjectManager $manager)
    {
        foreach ($data as $key => $entityData) {
            $entity = $this->loadEntity($entityData, $key);
            $manager->persist($entity);
            $this->setReference(get_class($entity) . $key, $entity);
        }
    }

    /**
     * @param array  $data
     * @param string $key
     *
     * @return mixed
     */
    abstract protected function loadEntity(array $data, $key);
}
