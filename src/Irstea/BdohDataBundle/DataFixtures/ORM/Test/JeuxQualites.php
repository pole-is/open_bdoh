<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\DataFixtures\ORM\Test;

use Irstea\BdohDataBundle\DataFixtures\ORM\AbstractYamlFixture;
use Irstea\BdohDataBundle\Entity\JeuQualite;
use Irstea\BdohDataBundle\Entity\Qualite;

/**
 * {@inheritdoc}
 */
class JeuxQualites extends AbstractYamlFixture
{
    /**
     * {@inheritdoc}
     */
    protected function loadEntity(array $jeuData, $nomJeu)
    {
        $jeu = new JeuQualite();
        $jeu->setNom($nomJeu);
        $jeu->setEstAffectable($jeuData['estAffectable'] ?? false);

        /* Loads 'Qualites' of current 'JeuQualite' */
        foreach ($jeuData['qualites'] as $code => $qualiteData) {
            $qualite = new Qualite();
            $qualite->setJeu($jeu);
            $qualite->setCode($code);
            $qualite->setLibelle($qualiteData[0]);
            $qualite->setLibelleEn($qualiteData[1]);
            $qualite->setOrdre(count($qualiteData) > 2 ? $qualiteData[2] : null);
            if ($nomJeu === 'Draix' || $nomJeu === 'VALID') {
                if ($qualite->getOrdre() < 2) {
                    $qualite->setType('gap');
                }
            }

            $jeu->addQualite($qualite);
        }

        return $jeu;
    }
}
