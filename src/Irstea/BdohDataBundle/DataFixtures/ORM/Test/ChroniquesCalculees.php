<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\DataFixtures\ORM\Test;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Irstea\BdohDataBundle\DataFixtures\ORM\AbstractYamlFixture;
use Irstea\BdohDataBundle\Entity\ChroniqueCalculee;
use Irstea\BdohDataBundle\Entity\Echantillonnage;
use Irstea\BdohDataBundle\Entity\Station;
use Irstea\BdohDataBundle\Entity\TypeParametre;
use Irstea\BdohDataBundle\Entity\Unite;

/**
 * {@inheritdoc}
 */
class ChroniquesCalculees extends AbstractYamlFixture implements DependentFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [TypeParametres::class, Stations::class, Echantillonnages::class, Unites::class];
    }

    /**
     * {@inheritdoc}
     */
    protected function loadEntity(array $chroniqueData, $nomChronique)
    {
        /** @var Echantillonnage $echantillonnage */
        $echantillonnage = $this->getReference(Echantillonnage::class . 'instantaneous');

        /** @var TypeParametre $param */
        $param = $this->getReference(TypeParametre::class . $chroniqueData['typeParametre']);

        /** @var Station $station */
        $station = $this->getReference(Station::class . $chroniqueData['station']);

        /** @var Unite $unite */
        $unite = $this->getReference(Unite::class . $chroniqueData['unite']);

        $chronique = new ChroniqueCalculee();
        $chronique->setLibelle($nomChronique);
        $chronique->setCode($chroniqueData['code']);
        $chronique->setUnite($unite);
        $chronique->setGenealogie($chroniqueData['genealogie']);
        $chronique->setEstVisible($chroniqueData['estVisible']);
        $chronique->setFacteurMultiplicatif($chroniqueData['coefficient']);
        $chronique->setParametre($param);
        $chronique->setStation($station);
        $chronique->setEchantillonnage($echantillonnage);

        return $chronique;
    }
}
