<?php
declare(strict_types=1);

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\DataFixtures\ORM\Test;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Irstea\BdohDataBundle\DataFixtures\ORM\AbstractYamlFixture;
use Irstea\BdohDataBundle\Entity\SiteExperimental;
use Irstea\BdohDataBundle\Entity\Station;

/**
 * Class Stations.
 */
class Stations extends AbstractYamlFixture implements DependentFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    protected function getFilename()
    {
        return 'OSSData.yml';
    }

    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [SitesExperimentaux::class];
    }

    /**
     * {@inheritdoc}
     */
    protected function readYaml()
    {
        $data = parent::readYaml();

        $res = [];
        foreach ($data as $obs) {
            foreach ($obs['Sites'] as $nomSite => $stations) {
                foreach ($stations as $nomStation => $station) {
                    $station['site'] = $nomSite;
                    $res[$nomStation] = $station;
                }
            }
        }

        return $res;
    }

    /**
     * {@inheritdoc}
     */
    protected function loadEntity(array $stationData, $nomStation)
    {
        /** @var SiteExperimental $siteExperimental */
        $siteExperimental = $this->getReference(SiteExperimental::class . $stationData['site']);
        unset($stationData['site']);

        $station = new Station();
        $station->setNom($nomStation);
        $station->addSite($siteExperimental);
        $station->setCode($nomStation);
        $this->setReference(Station::class . $nomStation, $station);

        foreach ($stationData as $field => $value) {
            $setter = 'set' . ucfirst($field);
            $station->$setter($value);
        }

        return $station;
    }
}
