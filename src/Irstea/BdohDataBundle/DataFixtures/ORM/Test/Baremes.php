<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\DataFixtures\ORM\Test;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Irstea\BdohDataBundle\DataFixtures\ORM\AbstractYamlFixture;
use Irstea\BdohDataBundle\Entity\Bareme;
use Irstea\BdohDataBundle\Entity\Observatoire;
use Irstea\BdohDataBundle\Entity\Unite;

/**
 * {@inheritdoc}
 */
class Baremes extends AbstractYamlFixture implements DependentFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [Observatoires::class, Unites::class];
    }

    /**
     * {@inheritdoc}
     */
    protected function loadEntity(array $baremeData, $nomBareme)
    {
        $observatoire = $this->getReference(Observatoire::class . $baremeData['observatoire']);

        $bareme = new Bareme();
        $bareme->setNom($nomBareme);
        $bareme->setCommentaire($baremeData['commentaire']);
        $bareme->setObservatoire($observatoire);

        if (array_key_exists('uniteentree', $baremeData)) {
            $unite = $this->getReference(Unite::class . $baremeData['uniteentree']);
            $bareme->setUniteEntree($unite);
        }

        if (array_key_exists('unitesortie', $baremeData)) {
            $unite = $this->getReference(Unite::class . $baremeData['unitesortie']);
            $bareme->setUniteSortie($unite);
        }

        $bareme->setValeurs($baremeData['valeurs']);
        $bareme->setDateCreation(new \DateTime());

        return $bareme;
    }
}
