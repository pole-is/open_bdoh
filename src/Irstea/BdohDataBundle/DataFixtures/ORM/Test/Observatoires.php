<?php
declare(strict_types=1);

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\DataFixtures\ORM\Test;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Irstea\BdohDataBundle\DataFixtures\ORM\AbstractYamlFixture;
use Irstea\BdohDataBundle\Entity\JeuQualite;
use Irstea\BdohDataBundle\Entity\Observatoire;

/**
 * Observatoires.
 */
class Observatoires extends AbstractYamlFixture implements DependentFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    protected function getFilename()
    {
        return 'OSSData.yml';
    }

    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [JeuxQualites::class];
    }

    /**
     * {@inheritdoc}
     */
    protected function loadEntity(array $observatoireData, $nomObs)
    {
        $observatoire = new Observatoire();
        $observatoire->setNom($nomObs);

        /* Defines 'JeuQualite' */
        /** @var JeuQualite $jeuQualite */
        $jeuQualite = $this->getReference(JeuQualite::class . ($observatoireData['JeuQualite'] ?? 'VALID'));
        $observatoire->setJeu($jeuQualite);

        return $observatoire;
    }
}
