<?php
declare(strict_types=1);

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\DataFixtures\ORM\Test;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Irstea\BdohDataBundle\DataFixtures\ORM\AbstractYamlFixture;
use Irstea\BdohDataBundle\Entity\TypeParametre;
use Irstea\BdohDataBundle\Entity\Unite;

/**
 * Class TypeParametres.
 */
class TypeParametres extends AbstractYamlFixture implements DependentFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [Unites::class];
    }

    /**
     * {@inheritdoc}
     */
    protected function loadEntity(array $parametreData, $nomParam)
    {
        $typeParametre = new TypeParametre();
        $typeParametre->setNom($nomParam);
        $typeParametre->setNomEn($parametreData['en']);
        $typeParametre->setCode($parametreData['code']);

        foreach ($parametreData['unites'] as $libelleUnite) {
            /** @var Unite $unite */
            $unite = $this->getReference(Unite::class . $libelleUnite);
            $typeParametre->addUnite($unite);
        }

        return $typeParametre;
    }
}
