<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\EventListener;

use Irstea\BdohDataBundle\Entity\Repository\ChroniqueRepository;
use Irstea\BdohDataBundle\Events as BdohEvents;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class MeasuresMetadataListener.
 *
 * @DI\Service()
 */
class MeasuresMetadataListener
{
    /**
     * @var RegistryInterface
     *
     * @DI\Inject("doctrine")
     */
    public $doctrine;

    /**
     * @param MeasuresUpdateEvent $event
     *
     * @DI\Observe(BdohEvents::MEASURES_UPDATE)
     */
    public function onMeasuresUpdate(MeasuresUpdateEvent $event)
    {
        $chronique = $event->getChronique();

        /** @var ChroniqueRepository $repo */
        $repo = $this->doctrine
            ->getEntityManagerForClass(get_class($chronique))
            ->getRepository(get_class($chronique));

        $repo->updateMeasuresMetadata($chronique);
    }
}
