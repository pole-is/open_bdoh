<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\EventListener;

use Irstea\BdohBundle\Manager\JobManagerInterface;
use Irstea\BdohDataBundle\Events as BdohEvents;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class ChronicleListener.
 *
 * @DI\Service()
 */
class ChronicleListener
{
    /**
     * @var JobManagerInterface
     */
    private $jobManager;

    /**
     * @var RegistryInterface
     */
    private $doctrine;

    /**
     * ChronicleListener constructor.
     *
     * @param JobManagerInterface $jobManager
     * @param RegistryInterface   $doctrine
     * @DI\InjectParams({
     *  "jobManager"=@DI\Inject("irstea_bdoh.job_manager"),
     *  "doctrine"=@DI\Inject("doctrine")
     * })
     */
    public function __construct(JobManagerInterface $jobManager, RegistryInterface $doctrine)
    {
        $this->jobManager = $jobManager;
        $this->doctrine = $doctrine;
    }

    /**
     * @param MeasuresUpdateEvent $event
     * @DI\Observe(BdohEvents::MEASURES_UPDATE)
     */
    public function onMeasuresUpdate(MeasuresUpdateEvent $event)
    {
        if (!$event->doesPropagate()) {
            return;
        }

        $chronique = $event->getChronique();

        // gestion des chroniques filles ayant 2 fois la même mère -> ne lance le calcul qu'une fois
        $sorties = [];
        foreach ($chronique->getTransformations() as $transfo) {
            if (!in_array($transfo->getSortie(), $sorties)) {
                $sorties[] = $transfo->getSortie();
                if ($transfo->getSortie()->isCalculee()) {
                    $this->jobManager->enqueueChronicleComputation($transfo->getSortie(), $event->getUser(), true);
                }
                if ($transfo->getSortie()->isConvertie()) {
                    $this->jobManager->enqueueChronicleConversion($transfo->getSortie(), $event->getUser(), true);
                }
            }
        }
    }
}
