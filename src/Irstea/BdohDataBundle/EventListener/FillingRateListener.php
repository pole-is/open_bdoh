<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\EventListener;

use Irstea\BdohBundle\Manager\JobManagerInterface;
use Irstea\BdohDataBundle\Events as BdohEvents;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class FillingRateListener.
 *
 * @DI\Service()
 */
class FillingRateListener
{
    /**
     * @var JobManagerInterface
     *
     * @DI\Inject("irstea_bdoh.job_manager")
     */
    public $jobManager;

    /**
     * @param MeasuresUpdateEvent $event
     *
     * @DI\Observe(BdohEvents::MEASURES_UPDATE)
     */
    public function onMeasuresUpdate(MeasuresUpdateEvent $event)
    {
        $chronique = $event->getChronique();
        if ($chronique->isContinue()) {
            $this->jobManager->enqueueComputeFillingRates(
                $chronique,
                $event->getDebut(),
                $event->getFin()
            );
        }
    }
}
