<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\EventListener;

use Irstea\BdohDataBundle\Entity\Chronique;
use Irstea\BdohSecurityBundle\Entity\Utilisateur;
use Symfony\Component\EventDispatcher\Event;

/**
 * {@inheritdoc}
 */
class MeasuresUpdateEvent extends Event
{
    /**
     * @var Chronique
     */
    private $chronique;

    /**
     * @var Utilisateur
     */
    private $user;

    /**
     * @var \DateTime|null
     */
    private $debut;

    /**
     * @var \DateTime|null
     */
    private $fin;

    /**
     * @var bool
     */
    private $propagate;

    /**
     * MeasuresUpdateEvent constructor.
     *
     * @param Chronique   $chronique
     * @param Utilisateur $user
     * @param \DateTime   $debut
     * @param \DateTime   $fin
     * @param bool        $propagate
     */
    public function __construct(Chronique $chronique, Utilisateur $user, \DateTime $debut = null, \DateTime $fin = null, $propagate = false)
    {
        $this->chronique = $chronique;
        $this->user = $user;
        $this->debut = $debut;
        $this->fin = $fin;
        $this->propagate = $propagate;
    }

    /** Get chronique.
     * @return Chronique
     */
    public function getChronique()
    {
        return $this->chronique;
    }

    /** Get user.
     * @return Utilisateur
     */
    public function getUser()
    {
        return $this->user;
    }

    /** Get debut.
     * @return \DateTime
     */
    public function getDebut()
    {
        return $this->debut;
    }

    /** Get fin.
     * @return \DateTime
     */
    public function getFin()
    {
        return $this->fin;
    }

    /** Get propagate.
     * @return bool
     */
    public function doesPropagate()
    {
        return $this->propagate;
    }
}
