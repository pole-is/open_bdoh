<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Util;

class MathUtil
{
    /**
     * Performs a linear interpolation of a value as a function of time.
     *
     * @param array  $dateValue1 Array containing the 1st date as a 1st element, and the associated value as a 2nd element
     * @param array  $dateValue2 Same as $dateValue1 but for the 2nd date
     * @param string $dateInterp Date at which the value must be interpolated
     *
     * @return float Interpolated value
     */
    public static function interpolate($dateValue1, $dateValue2, $dateInterp)
    {
        //abs(strtotime($date2) - strtotime($date1));
        $x = ((float) (strtotime($dateInterp) - strtotime($dateValue1[0])))
            / (strtotime($dateValue2[0]) - strtotime($dateValue1[0]));

        return $x * ((float) $dateValue2[1]) + (1.0 - $x) * ((float) $dateValue1[1]);
    }
}
