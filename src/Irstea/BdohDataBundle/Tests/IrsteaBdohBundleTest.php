<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Tests;

use Irstea\BdohDataBundle\IrsteaBdohDataBundle;
use org\bovigo\vfs\vfsStream;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class ObservatoireTest.
 *
 * @group unit
 */
class IrsteaBdohBundleTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider slugifyProvider
     *
     * @param mixed $slug
     * @param mixed $text
     */
    public function testSlugifyWorking($slug, $text)
    {
        $this->assertEquals($slug, IrsteaBdohDataBundle::slugify($text));
    }

    public function slugifyProvider()
    {
        return [
            ['n-a', ' '],
            ['EFOOE', 'éFooè'],
            ['EFOOE', 'efooe'],
            ['OEAE-CUI-OAE', "œæ çûî/\ôÂÊ"],
            ['BAC-A-SABLE', 'Bac à sable'],
            // TODO: add other tests
        ];
    }

    public function testRemoveFile()
    {
        $root = vfsStream::setup('/tmp');

        // Creation of a deletable file in /tmp directory
        $filePath = 'foo.txt';
        $file = vfsStream::newFile($filePath, 0644);
        $root->addChild($file);

        IrsteaBdohDataBundle::removeFile('vfs://tmp/', $filePath);
        $this->assertFileNotExists('vfs://tmp/foo.txt');    // deleted
        $this->assertEmpty($filePath);                      // should be empty
    }

    public function testUploadFile()
    {
        // File structure : /tmp/uploaded_file.txt and /tmp/uploads/old_file.txt
        $root = vfsStream::setUp(
            '/tmp',
            0755,
            [
                'uploaded_file.txt' => "I'm a fucking uploaded file !",
                'uploads'           => ['old_file.txt' => "I'm the old old file. My name is Luc."],
            ]
        );

        $uploadedFile = new UploadedFile('vfs://tmp/uploaded_file.txt', 'uploaded_file.txt', 'txt', 0, 0, true);
        $path = 'uploads/old_file.txt';
        IrsteaBdohDataBundle::uploadFile('vfs://tmp/', 'uploads/', $uploadedFile, $path);

        $this->assertFileNotExists('vfs://tmp/uploads/old_file.txt');
        $this->assertFileNotExists('vfs://tmp/uploaded_file.txt');
        $this->assertFileExists('vfs://tmp/' . $path);
        $this->assertEquals($root->getChild($path)->getContent(), "I'm a fucking uploaded file !");
        $this->assertNull($uploadedFile);
    }
}
