<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Tests\Entity;

use Irstea\BdohDataBundle\Entity\Observatoire;

/**
 * @backupStaticAttributes disabled
 * @group unit
 */
class ObservatoireTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @covers \Observatoire::defineAsCurrent
     * @covers \Observatoire::isCurrent
     */
    public function testIsCurrent()
    {
        $currObs = new Observatoire();
        $currObs->defineAsCurrent();
        $this->assertTrue($currObs->isCurrent());

        $otherObs = new Observatoire();
        $this->assertFalse($otherObs->isCurrent());
    }

    /**
     * @covers \Observatoire::defineAsCurrent
     * @covers \Observatoire::getCurrent
     */
    public function testIntegrityOfCurrent()
    {
        $obs = new Observatoire();
        $obs->defineAsCurrent();
        $obs->setNom('Foo bar');
        $this->assertSame($obs, Observatoire::getCurrent());
    }

    /**
     * @covers \Observatoire::defineAsCurrent
     * @covers \Observatoire::isCurrent
     */
    public function testUnicityOfCurrent()
    {
        $oldObs = new Observatoire();
        $oldObs->defineAsCurrent();

        $newObs = new Observatoire();
        $newObs->defineAsCurrent();

        $this->assertFalse($oldObs->isCurrent());
        $this->assertTrue($newObs->isCurrent());
    }

    /**
     * @covers \Observatoire::resetCurrent
     * @covers \Observatoire::getCurrent
     */
    public function testNoCurrent()
    {
        Observatoire::resetCurrent();
        $this->assertNull(Observatoire::getCurrent());
    }
}
