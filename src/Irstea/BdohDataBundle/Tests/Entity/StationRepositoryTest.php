<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Tests\Entity;

use Irstea\BdohBundle\Tests\ORMTestCase;
use Irstea\BdohDataBundle\Entity\Observatoire;
use Irstea\BdohDataBundle\Entity\Repository\StationRepository;

/**
 * Class StationRepositoryTest.
 *
 * @group database
 */
class StationRepositoryTest extends ORMTestCase
{
    /**
     * @var StationRepository
     */
    public $repo;

    public function setUp()
    {
        $this->repo = self::$entityManager->getRepository('IrsteaBdohDataBundle:Station');
        Observatoire::resetCurrent();
    }

    public function testCreatequerybuilderWithoutObservatoireDoesNotModifyBuilder()
    {
        $qb = $this->repo->createQueryBuilder('alias');

        $this->assertNull($qb->getParameter('observatoire'));
    }

    public function testCreatequerybuilderWithObservatoireRestrictQuery()
    {
        $obs = new Observatoire();
        $obs->defineAsCurrent();

        $qb = $this->repo->createQueryBuilder('alias');
        $this->assertSame($obs, $qb->getParameter('observatoire')->getValue());
    }

    public function testFindallReturnsOnlyStationsOfCurrentObservatoire()
    {
        return $this->markTestSkipped('Nécessite des fixtures.');

        $obs = self::$entityManager
            ->getRepository('IrsteaBdohDataBundle:Observatoire')
            ->findOneByNom("Lônes de l'Ain");
        $obs->defineAsCurrent();

        $this->assertCount(15, $this->repo->findAll());
    }

    public function testFindallReturnsAllStationsIfNoCurrentObservatoire()
    {
        return $this->markTestSkipped('Nécessite des fixtures.');
        $this->assertCount(199, $this->repo->findAll());
    }
}
