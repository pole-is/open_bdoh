<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Controller;

use Irstea\BdohBundle\Controller\Controller;
use Irstea\BdohBundle\Form\Type\UTCDateTimeType;
use Irstea\BdohDataBundle\Entity\Repository\ChroniqueRepository;
use Irstea\BdohDataBundle\Entity\Repository\QualiteRepository;
use Irstea\BdohSecurityBundle\Exception\ForbiddenException;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * This controller has actions returning data sets,
 * for chart visualization, displaying of measures table...
 *
 * Usually, these actions return JSON data, for Javascript processing.
 */
class DataProvidingController extends Controller
{
    /**
     * Returns all measures :
     *      -> in json format ;
     *      -> for a given 'chronique'
     *      -> and beetwen 2 dates.
     *
     * @param Request $request
     * @param $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function getChroniqueMeasuresAction(Request $request, $id)
    {
        $request->setRequestFormat('json');
        $params = $this->parseQuery($request);
        $data = $this->fetchViewerData(
            (int) $id,
            $params['beginDate'],
            $params['endDate']
        );

        return $this->renderJson($data);
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    private function parseQuery(Request $request)
    {
        $form = $this->createQueryForm();
        $form->handleRequest($request);
        if (!$form->isValid()) {
            throw new BadRequestHttpException($this->createErrorMessage($form));
        }

        return $form->getData();
    }

    /**
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createQueryForm()
    {
        return $this->container->get('form.factory')
            ->createNamedBuilder(null, FormType::class, null, ['csrf_protection' => false])
            ->setMethod('GET')
            ->add('beginDate', UTCDateTimeType::class, ['constraints' => [new NotBlank()]])
            ->add('endDate', UTCDateTimeType::class, ['constraints' => [new NotBlank()]])
            ->getForm();
    }

    /**
     * @param FormInterface $form
     *
     * @return Response
     */
    private function createErrorMessage($form)
    {
        $errors = [];
        foreach ($form->getErrors(true) as $error) {
            $property = $error->getOrigin()->getName();
            $value = json_encode($error->getOrigin()->getViewData());
            $message = $error->getMessage();
            $errors[] = "$property: $message ($value)";
        }

        return implode("\n", $errors);
    }

    /**
     * @param $ids
     * @param \DateTime $beginDate
     * @param \DateTime $endDate
     *
     * @throws \Exception
     *
     * @return mixed
     */
    private function fetchViewerData($ids, \DateTime $beginDate, \DateTime $endDate)
    {
        $authChecker = $this->get('security.authorization_checker');

        /** @var ChroniqueRepository $repository */
        $repository = $this->getBdohRepo('Chronique');

        $chroniques = $repository->findById(\explode('_', $ids));
        if (empty($chroniques)) {
            throw new NotFoundHttpException();
        }

        $chroniquesAndRights = [];
        foreach ($chroniques as $chronique) {
            if (!$this->isGranted('CONSULT_TIMESERIES', $chronique)) {
                throw new ForbiddenException();
            }
            $chroniquesAndRights[] = [$chronique, $authChecker->isGranted('CONSULT_CHECKPOINTS', $chronique)];
        }

        $maxPoints = $this->container->getParameter('irstea_bdoh_data.viewers.max_valid_display_points');

        $data = $repository->getMultipleChroniquesViewerData(
            $chroniquesAndRights,
            $beginDate,
            $endDate,
            $maxPoints
        );

        // construction des données des qualités (style, label, showLabel)
        /** @var QualiteRepository $qualiteRepo */
        $qualiteRepo = $this->getBdohRepo('Qualite');
        $isEnglish = \strpos($this->getTranslator()->getLocale(), 'en') === 0;
        $qualites = [];
        foreach ($data['chronicles'] as &$chroniquesData) {
            foreach ($chroniquesData['series'] as &$serie) {
                if (isset($serie['label'])) {
                    $serie['label'] = $this->getTranslator()->trans($serie['label']);
                } elseif (isset($serie['qualiteId'])) {
                    $qid = $serie['qualiteId'];
                    if (!isset($qualites[$qid])) {
                        $qualites[$qid] = $qualiteRepo->findOneById($qid);
                    }
                    $serie['style'] = $qualites[$qid]->getStyle();
                    if (isset($chroniquesData['discontinuousStyle'])) {
                        $serie['style'] = \array_merge($serie['style'], $chroniquesData['discontinuousStyle']);
                    }
                    $serie['label'] = $qualites[$qid]->getLibelle($isEnglish ? 'en' : 'fr');
                    $serie['showLabel'] = true;
                }
            }
        }

        return $data;
    }
}
