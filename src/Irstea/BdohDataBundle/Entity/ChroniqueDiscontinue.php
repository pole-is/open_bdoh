<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Class ChroniqueDiscontinue.
 */
class ChroniqueDiscontinue extends Chronique
{
    /**
     * @var Collection
     */
    protected $plages;

    /**
     * @var Collection|Conversion[]
     */
    private $conversions;

    /**
     * ChroniqueDiscontinue constructor.
     */
    public function __construct()
    {
        $this->plages = new ArrayCollection();
        $this->conversions = new ArrayCollection();
    }

    /**
     * Add plages.
     *
     * @param Plage $plages
     */
    public function addPlage(Plage $plages)
    {
        $this->plages[] = $plages;
    }

    /**
     * Set plages.
     *
     * @param Collection $plages
     */
    public function setPlages(Collection $plages)
    {
        $this->plages = $plages;
    }

    /**
     * Get plages.
     *
     * @return Collection
     */
    public function getPlages()
    {
        return $this->plages;
    }

    /*
     * Whether this time series should be plotted with reverse Y axis
     *
     * @return bool
     */
    public function getReverseAxis()
    {
        return false;
    }

    /** Get transformations.
     * @return Collection|Conversion[]
     */
    public function getTransformations()
    {
        return $this->getConversions();
    }

    /** Get conversions.
     * @return Collection|Conversion[]
     */
    public function getConversions()
    {
        return $this->conversions;
    }

    /** Has children.
     * @return bool
     */
    public function hasChildren()
    {
        return !($this->conversions->isEmpty());
    }

    /**
     * {@inheritdoc}
     */
    public function isDiscontinue()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function dtype()
    {
        return 'discontinue';
    }
}
