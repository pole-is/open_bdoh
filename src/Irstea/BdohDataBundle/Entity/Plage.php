<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Entity;

/**
 * Class Plage.
 */
class Plage
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $debut;

    /**
     * @var string
     */
    protected $fin;

    /**
     * @var float
     */
    protected $valeur;

    /**
     * @var float
     */
    protected $minimum;

    /**
     * @var float
     */
    protected $maximum;

    /**
     * @var Qualite
     */
    protected $qualite;

    /**
     * @var ChroniqueDiscontinue
     */
    protected $chronique;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set debut.
     *
     * @param string $debut
     */
    public function setDebut($debut)
    {
        $this->debut = $debut;
    }

    /**
     * Get debut.
     *
     * @return string
     */
    public function getDebut()
    {
        return $this->debut;
    }

    /**
     * Set fin.
     *
     * @param string $fin
     */
    public function setFin($fin)
    {
        $this->fin = $fin;
    }

    /**
     * Get fin.
     *
     * @return string
     */
    public function getFin()
    {
        return $this->fin;
    }

    /**
     * Set valeur.
     *
     * @param float $valeur
     */
    public function setValeur($valeur)
    {
        $this->valeur = $valeur;
    }

    /**
     * Get valeur.
     *
     * @return float
     */
    public function getValeur()
    {
        return $this->valeur;
    }

    /**
     * Set minimum.
     *
     * @param float $minimum
     */
    public function setMinimum($minimum)
    {
        $this->minimum = $minimum;
    }

    /**
     * Get minimum.
     *
     * @return float
     */
    public function getMinimum()
    {
        return $this->minimum;
    }

    /**
     * Set maximum.
     *
     * @param float $maximum
     */
    public function setMaximum($maximum)
    {
        $this->maximum = $maximum;
    }

    /**
     * Get maximum.
     *
     * @return float
     */
    public function getMaximum()
    {
        return $this->maximum;
    }

    /**
     * Set qualite.
     *
     * @param Qualite $qualite
     */
    public function setQualite(Qualite $qualite)
    {
        $this->qualite = $qualite;
    }

    /**
     * Get qualite.
     *
     * @return Qualite
     */
    public function getQualite()
    {
        return $this->qualite;
    }

    /**
     * Set chronique.
     *
     * @param ChroniqueDiscontinue $chronique
     */
    public function setChronique(ChroniqueDiscontinue $chronique)
    {
        $this->chronique = $chronique;
    }

    /**
     * Get chronique.
     *
     * @return ChroniqueDiscontinue
     */
    public function getChronique()
    {
        return $this->chronique;
    }
}
