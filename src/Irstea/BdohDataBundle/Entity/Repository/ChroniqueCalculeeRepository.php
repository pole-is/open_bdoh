<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Entity\Repository;

use Doctrine\ORM\Query;

/**
 * Class ChroniqueCalculeeRepository.
 */
class ChroniqueCalculeeRepository extends ChroniqueContinueRepository
{
    /**
     * @param string $chroniqueId
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @return int
     */
    public function computeChronique($chroniqueId)
    {
        $sqlCommand = "SELECT bdoh_compute_chronique($chroniqueId) as code";
        $dcs = $this->_em->getConnection()->prepare($sqlCommand);
        $dcs->execute();
        list($code) = $dcs->fetch(Query::HYDRATE_SCALAR);

        if ($code === 0) {
            $this->updateDatesMesuresByChronique($chroniqueId);
        }

        return $code;
    }
}
