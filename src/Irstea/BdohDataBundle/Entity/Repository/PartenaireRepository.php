<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Entity\Repository;

use Doctrine\ORM\QueryBuilder;

/**
 * Class PartenaireRepository.
 */
class PartenaireRepository extends EntityRepository
{
    /**
     * @param string $alias
     * @param null   $indexBy
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function createQueryBuilder($alias = 'p', $indexBy = null)
    {
        if (!$currentObs = $this->getCurrentObservatoire()) {
            return parent::createQueryBuilder($alias, $indexBy);
        }

        return parent::createQueryBuilder($alias = 'p', $indexBy)
            ->leftJoin('p.observatoire', 'o')
            ->where('o = :observatoire')
            ->setParameter('observatoire', $currentObs);
    }

    /**
     * Selects only entities linked to the current 'observatoire'.
     *
     * @param \Irstea\BdohDataBundle\Entity\Observatoire $observatoire
     *
     * @return QueryBuilder
     */
    public function findByObservatoire($observatoire)
    {
        return parent::createQueryBuilder('p')
            ->leftJoin('p.observatoire', 'o')
            ->where('o = :observatoire')
            ->andWhere('p.estFinanceur = true')
            ->setParameter('observatoire', $observatoire->getId());
    }

    /**
     * @return array
     */
    public function findAll()
    {
        return $this->createQueryBuilder()->getQuery()->getResult();
    }
}
