<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Entity\Repository;

/**
 * Class BaremeRepository.
 */
class BaremeRepository extends EntityRepository
{
    /**
     * Selects only entities linked to the current 'observatoire' for the list in Sonata Admin.
     *
     * @param mixed      $alias
     * @param mixed|null $indexBy
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function createQueryBuilder($alias = 'b', $indexBy = null)
    {
        return parent::createQueryBuilder($alias, $indexBy)
            ->where($alias . '.observatoire = :observatoire')
            ->setParameter('observatoire', $this->getCurrentObservatoire());
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function createOrderedQueryBuilder()
    {
        return parent::createQueryBuilder('b')->orderBy('b.dateCreation', 'DESC');
    }

    /**
     * @return array
     */
    public function findAll()
    {
        return $this->createOrderedQueryBuilder()->getQuery()->getResult();
    }

    /**
     * @param bool $includeTechnical
     *
     * @return array
     */
    public function securedFindAll($includeTechnical = true)
    {
        $where = 'b.observatoire = :observatoire';
        if ($includeTechnical) {
            $where .= ' OR b.observatoire IS NULL';
        }

        return $this->createOrderedQueryBuilder()
            ->andWhere($where)
            ->setParameter('observatoire', $this->getCurrentObservatoire())
            ->getQuery()->getResult();
    }

    /**
     * Returns all 'scales' entities that use the given unit.
     *
     * @param \Irstea\BdohDataBundle\Entity\Unite $unite
     *
     * @return array
     */
    public function findByUnite($unite)
    {
        return parent::createQueryBuilder('b')
            ->leftJoin('b.uniteEntree', 'ue')
            ->leftJoin('b.uniteSortie', 'us')
            ->where('ue = :unite or us = :unite')
            ->setParameter('unite', $unite)
            ->getQuery()
            ->getResult();
    }
}
