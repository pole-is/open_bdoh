<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Entity;

use Irstea\BdohBundle\Model\LabelledEntityInterface;

/**
 * Class OptionEchantillonnageSortie.
 */
class OptionEchantillonnageSortie implements LabelledEntityInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var Echantillonnage
     */
    private $echantillonnageEntree;

    /**
     * @var PasEchantillonnage
     */
    private $pasEchantillonnage;

    /**
     * Set echantillonnageEntree.
     *
     * @param Echantillonnage $echantillonnageEntree
     *
     * @return OptionEchantillonnageSortie
     */
    public function setEchantillonnageEntree(Echantillonnage $echantillonnageEntree = null)
    {
        $this->echantillonnageEntree = $echantillonnageEntree;

        return $this;
    }

    /**
     * Get echantillonnageEntree.
     *
     * @return Echantillonnage
     */
    public function getEchantillonnageEntree()
    {
        return $this->echantillonnageEntree;
    }

    /**
     * Set pasEchantillonnage.
     *
     * @param PasEchantillonnage $pasEchantillonnage
     *
     * @return OptionEchantillonnageSortie
     */
    public function setPasEchantillonnage(PasEchantillonnage $pasEchantillonnage = null)
    {
        $this->pasEchantillonnage = $pasEchantillonnage;

        return $this;
    }

    /**
     * Get pasEchantillonnage.
     *
     * @return PasEchantillonnage
     */
    public function getPasEchantillonnage()
    {
        return $this->pasEchantillonnage;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getLibelle();
    }

    /**
     * @param string $lang
     *
     * @return string
     */
    public function getLibelle(string $lang = 'fr'): string
    {
        return $this->pasEchantillonnage ? $this->pasEchantillonnage->getLibelle($lang) : '';
    }

    /**
     * @return string
     */
    public function getLibelleEn(): string
    {
        return $this->getLibelle('en');
    }
}
