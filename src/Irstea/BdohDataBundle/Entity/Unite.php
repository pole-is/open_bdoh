<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Unite.
 *
 * @UniqueEntity(fields="libelle")
 */
class Unite
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $libelle;

    /**
     * @var string
     */
    protected $libelleEn;

    /**
     * @var Collection
     */
    protected $parametres;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->parametres = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle.
     *
     * @param string $libelle
     *
     * @return Unite
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle.
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @return string
     */
    public function getLibelleEn()
    {
        return $this->libelleEn;
    }

    /**
     * @param string $libelleEn
     *
     * @return Unite
     */
    public function setLibelleEn($libelleEn)
    {
        $this->libelleEn = $libelleEn;

        return $this;
    }

    /**
     * Add parametres.
     *
     * @param TypeParametre $parametres
     *
     * @return Unite
     */
    public function addParametre(TypeParametre $parametres)
    {
        $this->parametres[] = $parametres;

        return $this;
    }

    /**
     * Remove parametres.
     *
     * @param TypeParametre $parametres
     */
    public function removeParametre(TypeParametre $parametres)
    {
        $this->parametres->removeElement($parametres);
    }

    /**
     * Set parametres.
     *
     * @param Collection $parametres
     *
     * @return Unite
     */
    public function setParametres(Collection $parametres)
    {
        $this->parametres = $parametres;

        return $this;
    }

    /**
     * Get parametres.
     *
     * @return Collection
     */
    public function getParametres()
    {
        return $this->parametres;
    }

    /**
     * To String.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getLibelle() ?: '';
    }
}
