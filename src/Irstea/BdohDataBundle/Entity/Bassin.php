<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Entity;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class Bassin.
 *
 * @UniqueEntity(fields={"nom", "observatoire"}, message="Bassin.nom.alreadyExists")
 */
class Bassin implements ObservatoireRelatedInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $nom;

    /**
     * @var float
     */
    protected $aire;

    /**
     * @var geometry
     */
    protected $perimetre;

    /**
     * @var Observatoire
     */
    protected $observatoire;

    /**
     * @var Station
     */
    protected $stationExutoire;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return Bassin
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set aire.
     *
     * @param float $aire
     *
     * @return Bassin
     */
    public function setAire($aire)
    {
        $this->aire = $aire;

        return $this;
    }

    /**
     * Get aire.
     *
     * @return float
     */
    public function getAire()
    {
        return $this->aire;
    }

    /**
     * Set perimetre.
     *
     * @param geometry $perimetre
     *
     * @return Bassin
     */
    public function setPerimetre($perimetre)
    {
        $this->perimetre = $perimetre;

        return $this;
    }

    /**
     * Get perimetre.
     *
     * @return geometry
     */
    public function getPerimetre()
    {
        return $this->perimetre;
    }

    /**
     * Set observatoire.
     *
     * @param Observatoire $observatoire
     *
     * @return Bassin
     */
    public function setObservatoire(Observatoire $observatoire = null)
    {
        $this->observatoire = $observatoire;

        return $this;
    }

    /**
     * Get observatoire.
     *
     * @return Observatoire
     */
    public function getObservatoire()
    {
        return $this->observatoire;
    }

    /**
     * Set stationExutoire.
     *
     * @param Station $stationExutoire
     *
     * @return Bassin
     */
    public function setStationExutoire(Station $stationExutoire = null)
    {
        $this->stationExutoire = $stationExutoire;

        return $this;
    }

    /**
     * Get stationExutoire.
     *
     * @return Station
     */
    public function getStationExutoire()
    {
        return $this->stationExutoire;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->nom;
    }
}
