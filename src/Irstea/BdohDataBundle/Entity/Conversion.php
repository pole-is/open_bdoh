<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Class Conversion.
 */
class Conversion
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var JeuConversion
     */
    protected $jeuConversionActuel;

    /**
     * @var Collection|JeuConversion[]
     */
    protected $jeuConversionsHistoriques;

    /**
     * @var ChroniqueDiscontinue
     */
    private $entree;

    /**
     * @var ChroniqueConvertie
     */
    private $sortie;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->jeuConversionsHistoriques = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set jeuConversionActuel.
     *
     * @param JeuConversion $jeuConversion
     *
     * @return Conversion
     */
    public function setJeuConversionActuel(JeuConversion $jeuConversion)
    {
        $this->jeuConversionActuel = $jeuConversion;

        return $this;
    }

    /**
     * Get jeuConversionActuel.
     *
     * @return JeuConversion
     */
    public function getJeuConversionActuel()
    {
        return $this->jeuConversionActuel;
    }

    /**
     * Add jeuConversionsHistoriques.
     *
     * @param JeuConversion $jeuConversion
     *
     * @return Conversion
     */
    public function addJeuConversionsHistoriques(JeuConversion $jeuConversion)
    {
        $this->jeuConversionsHistoriques[] = $jeuConversion;

        return $this;
    }

    /**
     * Get jeuConversionsHistoriques.
     *
     * @return Collection|JeuConversion[]
     */
    public function getJeuConversionsHistoriques()
    {
        return $this->jeuConversionsHistoriques;
    }

    /**
     * Set entree.
     *
     * @param ChroniqueDiscontinue $entree
     *
     * @return Conversion
     */
    public function setEntree(ChroniqueDiscontinue $entree)
    {
        $this->entree = $entree;

        return $this;
    }

    /**
     * Get entree.
     *
     * @return ChroniqueDiscontinue
     */
    public function getEntree()
    {
        return $this->entree;
    }

    /**
     * Set sortie.
     *
     * @param ChroniqueConvertie $sortie
     *
     * @return Conversion
     */
    public function setSortie(ChroniqueConvertie $sortie)
    {
        $this->sortie = $sortie;

        return $this;
    }

    /**
     * Get sortie.
     *
     * @return ChroniqueConvertie
     */
    public function getSortie()
    {
        return $this->sortie;
    }
}
