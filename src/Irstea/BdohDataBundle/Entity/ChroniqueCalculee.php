<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Entity;

/**
 * Class ChroniqueCalculee.
 */
class ChroniqueCalculee extends ChroniqueContinue
{
    /**
     * @var float
     */
    protected $facteurMultiplicatif = 1.0;

    /**
     * @var \DateTime
     */
    protected $miseAJour;

    /**
     * @var Transformation|null
     */
    protected $premiereEntree;

    /**
     * @var Transformation|null
     */
    protected $secondeEntree;

    /**
     * Set facteurMultiplicatif.
     *
     * @param float $facteurMultiplicatif
     */
    public function setFacteurMultiplicatif($facteurMultiplicatif)
    {
        $this->facteurMultiplicatif = $facteurMultiplicatif;
    }

    /**
     * Get facteurMultiplicatif.
     *
     * @return float
     */
    public function getFacteurMultiplicatif()
    {
        return $this->facteurMultiplicatif;
    }

    /**
     * @return \DateTime
     */
    public function getMiseAJour()
    {
        return $this->miseAJour;
    }

    /**
     * @param \DateTime $miseAJour
     *
     * @return ChroniqueCalculee
     */
    public function setMiseAJour($miseAJour)
    {
        $this->miseAJour = $miseAJour;

        return $this;
    }

    /**
     * Get premiereEntree.
     *
     * @return Transformation|null
     */
    public function getPremiereEntree()
    {
        return $this->premiereEntree;
    }

    /**
     * Set premiereEntree.
     *
     * @param Transformation|null $transformation
     */
    public function setPremiereEntree(Transformation $transformation = null)
    {
        $this->premiereEntree = $transformation;
        if ($transformation) {
            $transformation->setSortie($this);
        }
    }

    /**
     * Get secondeEntree.
     *
     * @return Transformation|null
     */
    public function getSecondeEntree()
    {
        return $this->secondeEntree;
    }

    /**
     * Set secondeEntree.
     *
     * @param Transformation|null $transformation
     */
    public function setSecondeEntree(Transformation $transformation = null)
    {
        $this->secondeEntree = $transformation;
        if ($transformation) {
            $transformation->setSortie($this);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isCalculee()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function dtype()
    {
        return 'calculee';
    }
}
