<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Entity;

/**
 * Class QualiteJeuQualite.
 */
class QualiteJeuQualite
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var Qualite
     */
    protected $traduction;

    /**
     * @var JeuQualite
     */
    protected $jeu;

    /**
     * @var Qualite
     */
    protected $qualite;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set traduction.
     *
     * @param Qualite $traduction
     */
    public function setTraduction(Qualite $traduction)
    {
        $this->traduction = $traduction;
    }

    /**
     * Get traduction.
     *
     * @return Qualite
     */
    public function getTraduction()
    {
        return $this->traduction;
    }

    /**
     * Set jeu.
     *
     * @param JeuQualite $jeu
     */
    public function setJeu(JeuQualite $jeu)
    {
        $this->jeu = $jeu;
    }

    /**
     * Get jeu.
     *
     * @return JeuQualite
     */
    public function getJeu()
    {
        return $this->jeu;
    }

    /**
     * Set qualite.
     *
     * @param Qualite $qualite
     */
    public function setQualite(Qualite $qualite)
    {
        $this->qualite = $qualite;
    }

    /**
     * Get qualite.
     *
     * @return Qualite
     */
    public function getQualite()
    {
        return $this->qualite;
    }
}
