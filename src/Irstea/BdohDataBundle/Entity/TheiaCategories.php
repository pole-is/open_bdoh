<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Entity;

class TheiaCategories
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $nom;

    /**
     * @var FamilleParametres
     */
    protected $familleParametre;

    /**
     * @var Milieu
     */
    protected $milieu;

    /**
     * @var string[]
     */
    protected $uriOzcarTheia = [];

    //-----------------getter et settter--------------------------------------------------------------

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getNom(): ?string
    {
        return $this->nom = $this->getMilieu() . ' / ' . $this->getFamilleParametre();
    }

    /**
     * @param string $nom
     */
    public function setNom(string $nom)
    {
        $this->nom = $this->getMilieu() . ' / ' . $this->getFamilleParametre();
    }

    /**
     * @return FamilleParametres|null
     */
    public function getFamilleParametre(): ?FamilleParametres
    {
        return $this->familleParametre;
    }

    /**
     * @param FamilleParametres $familleParametre
     */
    public function setFamilleParametre(FamilleParametres $familleParametre): void
    {
        $this->familleParametre = $familleParametre;
    }

    /**
     * @return Milieu|null
     */
    public function getMilieu(): ?Milieu
    {
        return $this->milieu;
    }

    /**
     * @param Milieu $milieu
     */
    public function setMilieu(Milieu $milieu): void
    {
        $this->milieu = $milieu;
    }

    /**
     * @return string[]
     */
    public function getUriOzcarTheia(): array
    {
        return $this->uriOzcarTheia;
    }

    /**
     * @param string[] $uriOzcarTheia
     */
    public function setUriOzcarTheia(array $uriOzcarTheia): void
    {
        $this->uriOzcarTheia = $uriOzcarTheia;
    }

    /**
     * To String.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getNom() ?: '';
    }
}
