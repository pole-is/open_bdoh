<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Entity;

/**
 * JeuConversion.
 */
class JeuConversion
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Conversion
     */
    private $conversion;

    /**
     * @var \DateTime
     */
    private $dateCreation;

    /**
     * @var int
     */
    private $paramConversion;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set conversion.
     *
     * @param Conversion $conversion
     *
     * @return JeuConversion
     */
    public function setConversion(Conversion $conversion = null)
    {
        $this->conversion = $conversion;

        return $this;
    }

    /**
     * Get conversion.
     *
     * @return Conversion
     */
    public function getConversion()
    {
        return $this->conversion;
    }

    /**
     * Set dateCreation.
     *
     * @param \DateTime $dateCreation
     *
     * @return JeuConversion
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation.
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * equals.
     *
     * @param JeuConversion $jeuConversion
     *
     * @return bool
     */
    public function equals($jeuConversion)
    {
        return $jeuConversion && ($this->getParamConversion() === $jeuConversion->getParamConversion());
    }

    /**
     * Set paramConversion.
     *
     * @param int $paramConversion
     *
     * @return JeuConversion
     */
    public function setParamConversion($paramConversion)
    {
        $this->paramConversion = $paramConversion;

        return $this;
    }

    /**
     * Get paramConversion.
     *
     * @return int
     */
    public function getParamConversion()
    {
        return $this->paramConversion;
    }
}
