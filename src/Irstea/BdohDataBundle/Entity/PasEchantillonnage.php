<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Entity;

use Irstea\BdohBundle\Model\LabelledEntityInterface;
use Irstea\BdohBundle\Model\LabelledEntityTrait;

/**
 * Class PasEchantillonnage.
 */
class PasEchantillonnage implements LabelledEntityInterface
{
    use LabelledEntityTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    protected $approxSecondes;

    /**
     * @var string
     */
    private $unite;

    /**
     * @var int
     */
    private $valeur;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set unite.
     *
     * @param string $unite
     *
     * @return self
     */
    public function setUnite(string $unite): self
    {
        $this->unite = $unite;

        return $this;
    }

    /**
     * Get unite.
     *
     * @return string
     */
    public function getUnite(): string
    {
        return $this->unite;
    }

    /**
     * Set valeur.
     *
     * @param int $valeur
     *
     * @return self
     */
    public function setValeur(int $valeur): self
    {
        $this->valeur = $valeur;

        return $this;
    }

    /**
     * Get valeur.
     *
     * @return int
     */
    public function getValeur(): int
    {
        return $this->valeur;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->valeur . ' ' . $this->unite;
    }

    /**
     * Set approxSecondes.
     *
     * @param int $approxSecondes
     *
     * @return self
     */
    public function setApproxSecondes(int $approxSecondes): self
    {
        $this->approxSecondes = $approxSecondes;

        return $this;
    }

    /**
     * Get approxSecondes.
     *
     * @return int
     */
    public function getApproxSecondes(): int
    {
        return $this->approxSecondes;
    }
}
