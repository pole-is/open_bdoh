<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Exporter\Measure\Archiver;

use Irstea\BdohDataBundle\Exporter\Measure\ArchiverInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ConsoleArchiver implements ArchiverInterface
{
    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * ConsoleArchiver constructor.
     *
     * @param OutputInterface $output
     */
    public function __construct(OutputInterface $output)
    {
        $this->output = $output;
    }

    /**
     * {@inheritdoc}
     */
    public function addFile($path, $name = null)
    {
        if ($name === null) {
            $name = basename($path);
        }
        if (substr($name, strlen($name) - 4) === '.pdf') {
            $this->output->writeln("\n<info># Ignoring $name</info>\n");

            return;
        }
        $this->output->writeln("\n<info># Printing $name from $path</info>\n");
        $handle = fopen($path, 'rb');
        try {
            while ($line = fgets($handle)) {
                $this->output->write($line);
            }
        } finally {
            fclose($handle);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function close()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getArchivePath()
    {
        return '/dev/null';
    }
}
