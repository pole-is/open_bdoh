<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Exporter\Measure\Archiver;

use Irstea\BdohDataBundle\Exporter\Measure\ArchiverInterface;
use RuntimeException;

class ZipArchiver implements ArchiverInterface
{
    /**
     * @var string
     */
    private $filePath;

    /**
     * @var \ZipArchive
     */
    private $archive;

    /**
     * ZipArchiver constructor.
     *
     * @param string $filePath
     */
    public function __construct($filePath = null)
    {
        $this->filePath = $filePath ?: tempnam(sys_get_temp_dir(), 'bdoh-archive-');
        @unlink($this->filePath);

        $this->archive = new \ZipArchive();

        $error = $this->archive->open($this->filePath, \ZipArchive::CREATE | \ZipArchive::EXCL);
        if (true !== $error) {
            throw new RuntimeException(sprintf('%s: %d', $this->filePath, $error));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function addFile($path, $name = null)
    {
        if ($name === null) {
            $name = basename($path);
        }
        if (!$this->archive->addFile($path, $name)) {
            throw new RuntimeException(
                sprintf('Adding %s (%s) to %s: %s', $path, $name, $this->filePath, $this->archive->getStatusString())
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function close()
    {
        $this->archive->close();
    }

    /**
     * {@inheritdoc}
     */
    public function getArchivePath()
    {
        return $this->filePath;
    }
}
