<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Exporter\Measure;

use Irstea\BdohDataBundle\Entity\Chronique;
use Irstea\BdohDataBundle\Util\JeuToJeu;

/**
 * Interface FormatterInterface.
 */
interface FormatterInterface
{
    /**
     * @param Chronique $chronique
     * @param resource  $resource
     * @param array     $extractorReport
     */
    public function start(Chronique $chronique, $resource, array $extractorReport);

    /**
     * @param string      $debut
     * @param string|null $fin
     * @param float       $valeur
     * @param string      $qualite
     * @param float null  $minimum
     * @param float       $maximum
     */
    public function writeMeasure($debut, $fin, $valeur, $qualite, $minimum = null, $maximum = null);

    /**
     * @return array
     */
    public function done();

    /**
     * @param mixed $codeJeu
     *
     * @return string
     */
    public function resolveJeuQualite($codeJeu);

    /**
     * @param JeuToJeu $jeuToJeu
     */
    public function setJeuToJeu(JeuToJeu $jeuToJeu);

    /**
     * @return bool
     */
    public function supportsRange();
}
