<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Exporter\Measure;

use Irstea\BdohDataBundle\Entity\JeuQualite;
use Irstea\BdohDataBundle\Entity\Observatoire;
use Irstea\BdohDataBundle\Exporter\Measure\Extractor\Factory as ExtractorFactory;
use Irstea\BdohDataBundle\Exporter\Measure\Formatter\Factory as FormatterFactory;
use Irstea\BdohDataBundle\Exporter\Measure\Reporter\LoggerDecorator;
use Irstea\BdohDataBundle\Exporter\Measure\Reporter\Twig;
use Irstea\BdohDataBundle\Util\JeuToJeu;
use Irstea\BdohLoggerBundle\Logger\BdohLogger;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bridge\Twig\TwigEngine;

/**
 * Class ExporterBuilder.
 *
 * @DI\Service("irstea_bdoh.export.builder")
 */
class ExporterBuilder
{
    /**
     * @var FormatterFactory
     *
     * @DI\Inject("irstea_bdoh.export.formatter.factory")
     */
    public $formatterFactory;

    /**
     * @var ExtractorFactory
     *
     * @DI\Inject("irstea_bdoh.export.extractor.factory")
     */
    public $extractorFactory;

    /**
     * @var TwigEngine
     *
     * @DI\Inject("templating")
     */
    public $twigEngine;

    /**
     * @var BdohLogger
     * @DI\Inject("irstea_bdoh_logger.logger")
     */
    public $bdohLogger;

    /**
     * @var RegistryInterface
     *
     * @DI\Inject
     */
    public $doctrine;

    /**
     * @var string
     * @DI\Inject("%irstea_bdoh_data.measure_export.document_base_path%")
     */
    public $documentBasePath;

    /**
     * @var string
     * @DI\Inject("%irstea_bdoh_data.measure_export.formats_export_file%")
     */
    public $formatDocPath;

    /**
     * @var string
     * @DI\Inject("%irstea_bdoh_data.measure_export.default_termofuses_file%")
     */
    public $defaultTosPath;

    /**
     * @param array $parameters
     *
     * @return ExporterInterface
     */
    public function getExporter(array $parameters, ArchiverInterface $archiver)
    {
        $formatter = $this->createFormatter($parameters['format'], $parameters['observatoire']);
        $extractor = $this->createExtractor($parameters['extractor'], $formatter, $parameters);
        $reporter = $this->createReporter($parameters);

        return $this->createExporter($extractor, $reporter, $archiver);
    }

    /**
     * @param string       $format
     * @param Observatoire $observatoire
     *
     * @return FormatterInterface
     */
    private function createFormatter($format, Observatoire $observatoire)
    {
        $formatter = $this->formatterFactory->create($format);
        $formatter->setJeuToJeu($this->createJeuToJeu($observatoire, $formatter));

        return $formatter;
    }

    /**
     * @param FormatterInterface $formatter
     *
     * @return JeuToJeu
     */
    private function createJeuToJeu(Observatoire $observatoire, FormatterInterface $formatter)
    {
        $manager = $this->doctrine->getEntityManagerForClass(JeuQualite::class);

        $jeuObservatoire = $observatoire->getJeu()->getNom();
        $jeuFormatter = $formatter->resolveJeuQualite($jeuObservatoire);

        return new JeuToJeu($manager, $jeuObservatoire, $jeuFormatter);
    }

    /**
     * @param string             $type
     * @param FormatterInterface $formatter
     * @param array              $parameters
     *
     * @return ExtractorInterface
     */
    private function createExtractor($type, FormatterInterface $formatter, array $parameters)
    {
        return $this->extractorFactory->create($type, $formatter, $parameters);
    }

    /**
     * @param array $parameters
     *
     * @return ReporterInterface
     */
    private function createReporter(array $parameters)
    {
        $reporter = new Twig($this->twigEngine);
        if (isset($parameters['user'])) {
            $reporter = new LoggerDecorator($this->bdohLogger, $reporter);
        }
        $reporter->addParameters($parameters);

        return $reporter;
    }

    /**
     * @param ExtractorInterface $extractor
     * @param ReporterInterface  $reporter
     * @param ArchiverInterface  $archiver
     *
     * @return ExporterInterface
     */
    private function createExporter(
        ExtractorInterface $extractor,
        ReporterInterface $reporter,
        ArchiverInterface $archiver
    ) {
        $exporter = new Exporter();

        return $exporter
            ->setExtractor($extractor)
            ->setReporter($reporter)
            ->setArchiver($archiver)
            ->setFormatDocPath($this->formatDocPath)
            ->setDocumentBasePath($this->documentBasePath)
            ->setDefaultTosPath($this->defaultTosPath);
    }
}
