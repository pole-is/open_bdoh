<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Exporter\Measure\Extractor;

use Irstea\BdohDataBundle\Exporter\Exception\UnknownExtractorException;
use Irstea\BdohDataBundle\Exporter\Measure\ExtractorInterface;
use Irstea\BdohDataBundle\Exporter\Measure\FormatterInterface;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class Factory.
 *
 * @DI\Service("irstea_bdoh.export.extractor.factory")
 */
class Factory
{
    /**
     * @var RegistryInterface
     *
     * @DI\Inject("doctrine")
     */
    public $doctrine;

    /**
     * @var string[]
     */
    private static $classes = [
        'identical'     => Identical::class,
        'instantaneous' => Instantaneous::class,
        'mean'          => Mean::class,
        'cumulative'    => Cumulative::class,
    ];

    /**
     * @param $extraction
     * @param FormatterInterface $formatter
     * @param array              $parameters
     *
     * @throws UnknownExtractorException
     *
     * @return ExtractorInterface
     */
    public function create($extraction, FormatterInterface $formatter, array $parameters)
    {
        $class = self::$classes[$extraction];
        if (!$class) {
            throw new UnknownExtractorException($extraction);
        }

        return new $class($this->doctrine, $formatter, $parameters);
    }
}
