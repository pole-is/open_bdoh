<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Exporter\Measure\Extractor;

use Irstea\BdohDataBundle\Entity\Chronique;
use Irstea\BdohDataBundle\Entity\ChroniqueContinue;
use Irstea\BdohDataBundle\Entity\PasEchantillonnage;
use Irstea\BdohDataBundle\Entity\Repository\ChroniqueContinueRepository;
use Irstea\BdohDataBundle\Entity\Repository\ChroniqueRepository;
use Irstea\BdohDataBundle\Exporter\Measure\FormatterInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

class Mean extends AbstractExtractor
{
    /**
     * @var PasEchantillonnage
     */
    protected $timestep;

    /**
     * {@inheritdoc}
     */
    public function __construct(RegistryInterface $doctrine, FormatterInterface $formatter, array $parameters)
    {
        parent::__construct($doctrine, $formatter, $parameters);
        $this->timestep = $parameters['timestep'];
    }

    /**
     * {@inheritdoc}
     */
    public function supportsChronique(Chronique $chronique)
    {
        return $chronique->isContinue();
    }

    /**
     * {@inheritdoc}
     */
    public function supportsFormatter(FormatterInterface $formatter)
    {
        return $formatter->supportsRange();
    }

    /**
     * {@inheritdoc}
     */
    protected function extractMeasures(ChroniqueRepository $repo, Chronique $chronique)
    {
        assert($repo instanceof ChroniqueContinueRepository);
        assert($chronique instanceof ChroniqueContinue);

        $result = $this->doExtractMeasure($repo, $chronique);
        $result[1]['isRange'] = true;

        return $result;
    }

    /**
     * @param ChroniqueContinueRepository $repo
     * @param ChroniqueContinue           $chronique
     *
     * @return array
     */
    protected function doExtractMeasure(ChroniqueContinueRepository $repo, ChroniqueContinue $chronique)
    {
        /* @var ChroniqueContinueRepository $repo */
        return $repo->getDataChangeStepMean(
            $chronique,
            $this->begin,
            $this->end,
            $this->timestep,
            $this->timezone
        );
    }
}
