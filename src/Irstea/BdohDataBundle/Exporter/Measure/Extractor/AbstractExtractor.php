<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Exporter\Measure\Extractor;

use Irstea\BdohDataBundle\Entity\Chronique;
use Irstea\BdohDataBundle\Entity\Repository\ChroniqueRepository;
use Irstea\BdohDataBundle\Exporter\Measure\ExtractorInterface;
use Irstea\BdohDataBundle\Exporter\Measure\FormatterInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

abstract class AbstractExtractor implements ExtractorInterface
{
    /**
     * @var RegistryInterface
     */
    private $doctrine;

    /**
     * @var FormatterInterface
     */
    protected $formatter;

    /**
     * @var \DateTime
     */
    protected $begin;

    /**
     * @var \DateTime
     */
    protected $end;

    /**
     * @var \DateTimeZone
     */
    protected $timezone;

    /**
     * @var array
     */
    protected $report;

    /**
     * @var int
     */
    protected $precision;

    /**
     * AbstractExtractor constructor.
     *
     * @param RegistryInterface  $doctrine
     * @param FormatterInterface $formatter
     * @param array              $parameters
     */
    public function __construct(RegistryInterface $doctrine, FormatterInterface $formatter, array $parameters)
    {
        $this->doctrine = $doctrine;

        assert($this->supportsFormatter($formatter));
        $this->formatter = $formatter;

        $this->begin = $parameters['begin'];
        $this->end = $parameters['end'];
        $this->timezone = $parameters['timezone'];
        $this->precision = $parameters['precision'] ?? 3;
    }

    /**
     * {@inheritdoc}
     */
    public function extract(Chronique $chronique, $output)
    {
        assert($this->supportsChronique($chronique));

        /** @var ChroniqueRepository $repo */
        $repo = $this->doctrine->getRepository(get_class($chronique));

        $this->report = [
            'isRange'    => !$chronique->isContinue(),
            'isContinue' => $chronique->isContinue(),
            'isComputed' => $chronique->isCalculee(),
            'timezone'   => $this->timezone,
            'unite'      => $this->getUnite($chronique),
        ];

        list($measures, $report) = $this->extractMeasures($repo, $chronique);

        $this->report = array_merge($this->report, $report);

        $this->formatter->start($chronique, $output, $this->report);

        foreach ($measures as $measure) {
            $this->writeMeasure($measure);
        }

        $formatterReport = $this->formatter->done();

        return array_merge($this->report, $formatterReport);
    }

    /**
     * @param Chronique $chronique
     *
     * @return string
     */
    protected function getUnite(Chronique $chronique)
    {
        return $chronique->getUnite();
    }

    /**
     * @param ChroniqueRepository $repo
     * @param Chronique           $chronique
     *
     * @return array
     */
    abstract protected function extractMeasures(ChroniqueRepository $repo, Chronique $chronique);

    /**
     * {@inheritdoc}
     */
    protected function writeMeasure(array $measure)
    {
        $this->formatter->writeMeasure(
            $measure['debut'] ?? $measure['date'],
            $measure['fin'] ?? null,
            $this->formatNumber($measure['valeur']),
            $measure['qualite'],
            isset($measure['minimum']) ? $this->formatNumber($measure['minimum']) : null,
            isset($measure['maximum']) ? $this->formatNumber($measure['maximum']) : null
        );
    }

    /**
     * @param mixed $value
     *
     * @return string
     */
    private function formatNumber($value)
    {
        return is_numeric($value) ? number_format($value, $this->precision, '.', '') : null;
    }
}
