<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Exporter\Measure\Formatter;

use Irstea\BdohDataBundle\Entity\Chronique;
use Irstea\BdohDataBundle\Exporter\Measure\FormatterInterface;
use Irstea\BdohDataBundle\Util\JeuToJeu;

abstract class AbstractFormatter implements FormatterInterface
{
    /**
     * @var
     */
    private $file;

    /**
     * @var JeuToJeu
     */
    protected $jeuToJeu;

    /**
     * @var Chronique
     */
    protected $chronique;

    /**
     * @var array
     */
    protected $extractorReport;

    /**
     * @var int
     */
    protected $lineCount;

    /**
     * @var int
     */
    protected $measureCount;

    /**
     * @var int
     */
    protected $gapCount;

    /**
     * {@inheritdoc}
     */
    public function start(Chronique $chronique, $resource, array $extractorReport)
    {
        $this->chronique = $chronique;
        $this->file = $resource;
        $this->extractorReport = $extractorReport;
        $this->lineCount = 0;
        $this->gapCount = 0;
        $this->measureCount = 0;

        $this->writeHeader();
    }

    protected function writeHeader()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function writeMeasure($debut, $fin, $valeur, $qualite, $minimum = null, $maximum = null)
    {
        if ($valeur === null || $this->jeuToJeu->isGapType($qualite)) {
            $this->writeGap($debut, $fin);
            ++$this->gapCount;

            return;
        }

        $qualite = $this->jeuToJeu->codeToCode($qualite);
        $this->doWriteMeasure($debut, $fin, $valeur, $qualite, $minimum, $maximum);
        ++$this->measureCount;
    }

    /**
     * @param string      $debut
     * @param string|null $fin
     * @param float       $valeur
     * @param string      $qualite
     * @param float null  $minimum
     * @param float       $maximum
     */
    abstract protected function doWriteMeasure($debut, $fin, $valeur, $qualite, $minimum = null, $maximum = null);

    /**
     * @param string      $debut
     * @param string|null $fin
     */
    abstract protected function writeGap($debut, $fin);

    /**
     * {@inheritdoc}
     */
    public function done()
    {
        $this->writeFooter();

        return [
            'count' => [
                'lines'    => $this->lineCount,
                'gaps'     => $this->gapCount,
                'measures' => $this->measureCount,
            ],
        ];
    }

    protected function writeFooter()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function setJeuToJeu(JeuToJeu $jeuToJeu)
    {
        $this->jeuToJeu = $jeuToJeu;
    }

    /**
     * {@inheritdoc}
     */
    public function resolveJeuQualite($default)
    {
        return $default;
    }

    /**
     * @param string[] $values
     *
     * @return self
     */
    protected function writeCsv(...$values)
    {
        fwrite($this->file, implode(';', $values) . "\n");
        ++$this->lineCount;

        return $this;
    }
}
