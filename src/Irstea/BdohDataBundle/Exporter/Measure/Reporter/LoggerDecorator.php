<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Exporter\Measure\Reporter;

use Irstea\BdohDataBundle\Entity\Chronique;
use Irstea\BdohDataBundle\Entity\PasEchantillonnage;
use Irstea\BdohDataBundle\Exporter\Measure\ReporterInterface;
use Irstea\BdohLoggerBundle\Logger\BdohLogger;

/**
 * Class LoggerDecorator.
 */
class LoggerDecorator implements ReporterInterface
{
    /**
     * @var BdohLogger
     */
    private $logger;

    /**
     * @var ReporterInterface
     */
    private $inner;

    /**
     * @var array
     */
    private $parameters = [];

    /**
     * LoggerDecorator constructor.
     *
     * @param BdohLogger        $logger
     * @param ReporterInterface $inner
     */
    public function __construct(BdohLogger $logger, ReporterInterface $inner)
    {
        $this->logger = $logger;
        $this->inner = $inner;
    }

    /**
     * {@inheritdoc}
     */
    public function addChronique(Chronique $chronique, array $report)
    {
        $this->inner->addChronique($chronique, $report);

        if (empty($report['number'])) {
            return;
        }

        $timestep = ['libelle' => '', 'libelleEn' => ''];
        if (isset($this->parameters['timestep'])) {
            if (is_integer($this->parameters['timestep'])) {
                $minutes = ' minute';
                if ($this->parameters['timestep'] > 1) {
                    $minutes = ' minutes';
                }
                $timestep['libelle'] = $this->parameters['timestep'] . $minutes;
                $timestep['libelleEn'] = $this->parameters['timestep'] . $minutes;
            } elseif ($this->parameters['timestep'] instanceof PasEchantillonnage) {
                $timestep['libelle'] = $this->parameters['timestep']->getLibelle();
                $timestep['libelleEn'] = $this->parameters['timestep']->getLibelleEn();
            }
        }

        $this->logger->createHistoriqueDonneesExport(
            $this->parameters['user'],
            $this->parameters['date'],
            $chronique->getObservatoire(),
            $chronique,
            $this->parameters['begin']->format(\DateTime::ATOM),
            $this->parameters['end']->format(\DateTime::ATOM),
            $report['number'],
            $this->parameters['format'],
            $this->parameters['extractor'],
            \json_encode($timestep),
            $this->parameters['timezone']->getName()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function addParameters(array $parameters)
    {
        $this->parameters = array_merge($this->parameters, $parameters);
        $this->inner->addParameters($parameters);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function writeTo($resource)
    {
        $this->inner->writeTo($resource);
    }
}
