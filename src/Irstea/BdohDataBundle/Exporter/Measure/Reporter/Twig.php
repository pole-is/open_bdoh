<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Exporter\Measure\Reporter;

use Irstea\BdohDataBundle\Entity\Chronique;
use Irstea\BdohDataBundle\Exporter\Measure\ReporterInterface;
use Symfony\Bridge\Twig\TwigEngine;

/**
 * Class Twig.
 */
class Twig implements ReporterInterface
{
    /**
     * @var TwigEngine
     */
    private $twig;

    /**
     * @var array
     */
    private $parameters = ['chroniques' => []];

    /**
     * Twig constructor.
     *
     * @param TwigEngine $twig
     */
    public function __construct(TwigEngine $twig)
    {
        $this->twig = $twig;
    }

    /**
     * {@inheritdoc}
     */
    public function addChronique(Chronique $chronique, array $report)
    {
        $this->parameters['chroniques'][] = ['chronique' => $chronique, 'report' => $report];
    }

    /**
     * {@inheritdoc}
     */
    public function addParameters(array $parameters)
    {
        $this->parameters = array_replace($this->parameters, $parameters);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function writeTo($resource)
    {
        $lines = explode("\n", $this->twig->render('@IrsteaBdohData/Exporter/report.text.twig', $this->parameters));
        $lines = array_map('rtrim', $lines);
        fwrite($resource, implode("\n", $lines));
    }
}
