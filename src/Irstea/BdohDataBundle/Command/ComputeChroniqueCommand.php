<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Irstea\BdohDataBundle\Entity\ChroniqueCalculee;
use Irstea\BdohDataBundle\Entity\Repository\ChroniqueCalculeeRepository;
use Irstea\BdohDataBundle\EventListener\MeasuresUpdateEvent;
use Irstea\BdohDataBundle\Events;
use Irstea\BdohSecurityBundle\Entity\Repository\UtilisateurRepository;
use Irstea\BdohSecurityBundle\Entity\Utilisateur;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * {@inheritdoc}
 */
class ComputeChroniqueCommand extends AbstractJobCommand
{
    const COMMAND_NAME = 'bdoh:compute:chronique';

    /**
     * {@inheritdoc}
     *
     * @throws InvalidArgumentException
     */
    protected function configure()
    {
        parent::configure();
        $this
            ->setName(self::COMMAND_NAME)
            ->setDescription('Calcule les mesures d\'une chronique calculée')
            ->addArgument('chronique_id', InputArgument::REQUIRED, 'Identifiant de la chronique')
            ->addArgument('user_id', InputArgument::REQUIRED, 'Initiateur de la comande')
            ->addOption('propagate', 'p', InputOption::VALUE_NONE, 'Propager aux chroniques filles');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws EntityNotFoundException
     * @throws InvalidArgumentException
     *
     * @return int Exit code
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $chroniqueId = $input->getArgument('chronique_id');
        $userId = $input->getArgument('user_id');
        $propagate = $input->getOption('propagate');

        return $this->getContainer()
            ->get('doctrine')
            ->getManagerForClass(ChroniqueCalculee::class)
            ->transactional(
                function (EntityManagerInterface $manager) use ($chroniqueId, $userId, $propagate) {
                    /** @var ChroniqueCalculeeRepository $chroRepo */
                    $chroRepo = $manager->getRepository(ChroniqueCalculee::class);

                    /** @var ChroniqueCalculee $chronique */
                    $chronique = $chroRepo->findOneById($chroniqueId);

                    if (!$chronique) {
                        throw new EntityNotFoundException(sprintf('%s:%d', ChroniqueCalculee::class, $chroniqueId));
                    }

                    /** @var UtilisateurRepository $userRepo */
                    $userRepo = $manager->getRepository(Utilisateur::class);

                    /** @var Utilisateur $user */
                    $user = $userRepo->find($userId);

                    if (!$user) {
                        throw new EntityNotFoundException(sprintf('%s:%d', Utilisateur::class, $userId));
                    }

                    $result = $chroRepo->computeChronique($chronique->getId());

                    if ($result === 0) {
                        $manager->refresh($chronique);

                        // met à jour la date de calcul de la chronique (date de mise à jour)
                        $date = new \DateTime('now', new \DateTimeZone('UTC'));
                        $chronique->setMiseAJour($date);
                        $manager->persist($chronique);
                        $manager->flush();

                        // historique de calcul
                        $this->getContainer()->get('irstea_bdoh_logger.logger')->createHistoriqueCalculChronique(
                            $user,
                            $date,
                            $chronique->getObservatoire(),
                            $chronique
                        );

                        // MeasuresUpdateEvent pour les taux de remplissage et la propagation
                        $event = new MeasuresUpdateEvent(
                            $chronique,
                            $user,
                            new \DateTime($chronique->getDateDebutMesures()),
                            new \DateTime($chronique->getDateFinMesures()),
                            $propagate
                        );
                        $this->getContainer()->get('event_dispatcher')->dispatch(Events::MEASURES_UPDATE, $event);
                    }

                    return $result;
                }
            );
    }
}
