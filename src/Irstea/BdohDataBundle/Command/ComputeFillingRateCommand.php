<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Command;

use Doctrine\ORM\EntityNotFoundException;
use Irstea\BdohDataBundle\Entity\Chronique;
use Irstea\BdohDataBundle\Entity\ChroniqueContinue;
use Irstea\BdohDataBundle\Entity\Observatoire;
use Irstea\BdohDataBundle\Entity\Repository\ChroniqueRepository;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * {@inheritdoc}
 */
class ComputeFillingRateCommand extends AbstractJobCommand
{
    const COMMAND_NAME = 'bdoh:compute:filling_rates';

    /**
     * {@inheritdoc}
     *
     * @throws InvalidArgumentException
     */
    protected function configure()
    {
        parent::configure();
        $this
            ->setName(self::COMMAND_NAME)
            ->setDescription('Met à jour les lacunes d\'une chronique')
            ->addArgument('chronique_id', InputArgument::REQUIRED, 'Identifiant de la chronique')
            ->addArgument('date_debut', InputArgument::OPTIONAL, 'Début de la période à recalculer')
            ->addArgument('date_fin', InputArgument::OPTIONAL, 'Fin de la période à recalculer');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws EntityNotFoundException
     * @throws InvalidArgumentException
     *
     * @return int Exit code
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $chroniqueId = $input->getArgument('chronique_id');
        $dateDebutStr = $input->getArgument('date_debut');
        $dateFinStr = $input->getArgument('date_fin');

        $manager = $this->getContainer()->get('doctrine')->getManagerForClass(ChroniqueContinue::class);

        /** @var ChroniqueRepository $repo */
        $repo = $manager->getRepository(Chronique::class);

        /** @var Chronique $chronique */
        $chronique = $repo->findOneById($chroniqueId);

        if (!$chronique) {
            throw new EntityNotFoundException(sprintf('%s:%d', Chronique::class, $chroniqueId));
        }

        $utc = new \DateTimeZone('UTC');
        $dateDebut = $dateDebutStr ? new \DateTime($dateDebutStr, $utc) : null;
        $dateFin = $dateFinStr ? new \DateTime($dateFinStr, $utc) : null;

        /** @var Observatoire $obs */
        $obs = $chronique->getStation()->getSites()->first()->getObservatoire();
        $obs->defineAsCurrent();

        $repo->updateFillingRates($chronique, $dateDebut, $dateFin);

        return 0;
    }
}
