<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Command;

use Irstea\BdohDataBundle\Entity\Chronique;
use Irstea\BdohDataBundle\Exporter\Measure\Archiver\ConsoleArchiver;
use Irstea\BdohDataBundle\Exporter\Measure\Archiver\ZipArchiver;
use Irstea\BdohDataBundle\Exporter\Measure\ArchiverInterface;
use Irstea\BdohDataBundle\Exporter\Measure\ExporterBuilder;
use Irstea\BdohSecurityBundle\Entity\Utilisateur;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class AbstractExportCommand.
 */
abstract class AbstractExportCommand extends Command
{
    const COMMAND_NAME_PREFIX = 'bdoh:export:';
    //const EXTRACTOR_TYPE = 'abstract';
    const DATETIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssX";

    /**
     * @var ExporterBuilder
     * @DI\Inject("irstea_bdoh.export.builder")
     */
    public $exporterBuilder;

    /**
     * @var TranslatorInterface
     * @DI\Inject
     */
    public $translator;

    /**
     * @var TwigEngine
     * @DI\Inject
     */
    public $templating;

    /**
     * @var \Swift_Mailer
     * @DI\Inject
     */
    public $mailer;

    /**
     * @var RegistryInterface
     * @DI\Inject
     */
    public $doctrine;

    /**
     * {@inheritdoc}
     *
     * @throws InvalidArgumentException
     */
    protected function configure()
    {
        $this
            ->setName(self::COMMAND_NAME_PREFIX . static::EXTRACTOR_TYPE)
            ->setDescription('Export de chroniques')
            ->addOption('jms-job-id', null, InputOption::VALUE_REQUIRED, 'Identifiant du job')
            ->addOption('locale', 'l', InputOption::VALUE_REQUIRED, 'Localisation', 'fr')
            ->addOption('send-to', 'm', InputOption::VALUE_REQUIRED, "E-mail d'un utilisateur à qui envoyer l'archive")
            ->addOption('write-to', 'o', InputOption::VALUE_REQUIRED, 'Chemin du fichier de sortie')
            ->addOption('timezone', 't', InputOption::VALUE_REQUIRED, 'Fuseau horaire cible', 'UTC')
            ->addOption('format', 'f', InputOption::VALUE_REQUIRED, "Format d'export", 'bdoh')
            ->addArgument('begin', InputArgument::REQUIRED, 'Date de début')
            ->addArgument('end', InputArgument::REQUIRED, 'Date de fin')
            ->addArgument(
                'chroniques',
                InputArgument::REQUIRED | InputArgument::IS_ARRAY,
                'Identifiants des chroniques à exporter'
            );
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Doctrine\ORM\EntityNotFoundException
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $arguments = array_merge($input->getArguments(), $input->getOptions());
        $parameters = $this->validateParameters($arguments);

        $parameters['date'] = new \DateTime('now', new \DateTimeZone('UTC'));

        $this->setLocale($parameters['locale']);
        unset($parameters['locale']);

        $user = $parameters['user'] = $parameters['send-to'] ?? null;

        $archiver = $this->createArchiver($parameters, $output);
        unset($parameters['send-to'], $parameters['write-to']);

        $chroniques = $parameters['chroniques'];
        unset($parameters['chroniques']);

        $parameters['observatoire']->defineAsCurrent();

        $this->exporterBuilder
            ->getExporter($parameters, $archiver)
            ->export($chroniques);

        if ($user !== null) {
            return 1 - $this->sendArchive($archiver->getArchivePath(), $user, $chroniques);
        }

        return 0;
    }

    /**
     * @param array $arguments
     *
     * @return array
     */
    private function validateParameters(array $arguments)
    {
        $resolver = new OptionsResolver();
        $this->configureParameterResolver($resolver);

        $values = array_intersect_key($arguments, array_fill_keys($resolver->getDefinedOptions(), true));
        $parameters = $resolver->resolve($values);

        unset($parameters['command']);

        return $parameters;
    }

    /**
     * @param array           $parameters
     * @param OutputInterface $output
     *
     * @return ArchiverInterface
     */
    private function createArchiver(array $parameters, OutputInterface $output)
    {
        if (isset($parameters['send-to'])) {
            $archiver = new ZipArchiver();
            $filePath = $archiver->getArchivePath();
            register_shutdown_function(
                function () use ($filePath) {
                    @unlink($filePath);
                }
            );

            return $archiver;
        }

        if (isset($parameters['write-to'])) {
            return new ZipArchiver($parameters['write-to']);
        }

        return new ConsoleArchiver($output);
    }

    /**
     * @param OptionsResolverInterface $resolver
     *
     * @return mixed
     */
    protected function configureParameterResolver(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'send-to'      => null,
                'write-to'     => null,
                'extractor'    => static::EXTRACTOR_TYPE,
                'observatoire' => null,
                'locale'       => 'fr',
            ]
        );

        $resolver->setRequired(
            [
                'command',
                'format',
                'chroniques',
                'timezone',
                'begin',
                'end',
            ]
        );

        $resolver->addAllowedTypes(
            [
                'send-to'    => ['string', 'null'],
                'write-to'   => ['string', 'null'],
                'chroniques' => ['array'],
                'format'     => ['string'],
                'timezone'   => ['string'],
                'begin'      => ['string'],
                'end'        => ['string'],
            ]
        );

        $resolver->setNormalizers(
            [
                'send-to'      => function (Options $options, $value) {
                    if (!$value) {
                        return null;
                    }

                    return $this->doctrine
                        ->getRepository(Utilisateur::class)
                        ->findOneByEmail($value);
                },
                'chroniques'   => function (Options $options, $value) {
                    if (!$value) {
                        return [];
                    }

                    return array_map(
                        function ($id) {
                            return $this->doctrine
                                ->getRepository(Chronique::class)
                                ->findOneById($id);
                        },
                        $value
                    );
                },
                'timezone'     => function (Options $options, $value) {
                    return new \DateTimeZone(str_replace(':', '', trim($value, "'")));
                },
                'begin'        => function (Options $options, $value) {
                    return new \DateTime($value, $options['timezone']);
                },
                'end'          => function (Options $options, $value) {
                    return new \DateTime($value, $options['timezone']);
                },
                'observatoire' => function (Options $options, $value) {
                    $chroniques = $options['chroniques'];

                    return count($chroniques) > 0 ? $chroniques[0]->getObservatoire() : null;
                },
            ]
        );
    }

    /**
     * @param string $locale
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    private function setLocale($locale)
    {
        \Locale::setDefault($locale);
        $this->translator->setLocale($locale);
    }

    /**
     * @param string $archivePath
     *
     * @throws \Twig_Error
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     *
     * @return int
     */
    private function sendArchive($archivePath, Utilisateur $user, array $chroniques)
    {
        $attachment = \Swift_Attachment::fromPath($archivePath, 'application/zip');
        $attachment->setFilename('export.zip');

        $message = \Swift_Message::newInstance()
            ->setSubject(
                $this->translator->transChoice(
                    'export.exportMail.title(%chronique%)',
                    count($chroniques),
                    ['%chronique%' => implode(', ', $chroniques)]
                )
            )
            ->setFrom('no.reply.BDOH@irstea.fr')
            ->setTo([$user->getEmail() => (string) $user])
            ->attach($attachment);

        $message->setBody(
            $this->templating->render(
                'IrsteaBdohConsultBundle:Mail:export.txt.twig',
                ['user' => (string) $user]
            )
        );

        return $this->mailer->send($message);
    }
}
