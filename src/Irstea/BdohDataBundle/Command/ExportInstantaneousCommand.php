<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Command;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class ExportInstantaneousCommand.
 *
 * @DI\Service()
 * @DI\Tag("console.command")
 */
class ExportInstantaneousCommand extends AbstractExportCommand
{
    const EXTRACTOR_TYPE = 'instantaneous';

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->addArgument('timestep', InputArgument::REQUIRED, 'Pas de temps');
        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureParameterResolver(OptionsResolverInterface $resolver)
    {
        parent::configureParameterResolver($resolver);

        $resolver->setRequired(['timestep']);

        $resolver->setAllowedTypes(
            [
                'timestep' => ['string'],
            ]
        );

        $resolver->setNormalizers(
            [
                'timestep' => function (Options $options, $value) {
                    return (int) $value;
                },
            ]
        );
    }
}
