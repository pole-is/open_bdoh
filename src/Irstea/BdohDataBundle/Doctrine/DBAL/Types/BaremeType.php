<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Doctrine\DBAL\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class BaremeType extends Type
{
    const BAREME = 'bareme';

    public function getName()
    {
        return self::BAREME;
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return '_float8';
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (!is_array($value) || sizeof($value) < 2) {
            return null;
        }

        $valueToReturn = [];
        foreach ($value as $line) {
            foreach ($line as $index => $x) {
                if (!is_numeric($x)) {
                    $line[$index] = 'null';
                }
            }
            while (sizeof($line) < 5) {
                $line[] = 'null';
            }

            $valueToReturn[] = '{' . implode(',', $line) . '}';
        }

        return '{' . implode(',', $valueToReturn) . '}';
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        $value = str_replace('},{', ';', $value);
        $value = str_replace('{', '', $value);
        $value = str_replace('}', '', $value);

        $values = explode(';', $value);

        $toReturn = [];

        foreach ($values as $line) {
            $temp = explode(',', $line);
            $toReturn[] = $temp;
        }

        return $toReturn;
    }
}
