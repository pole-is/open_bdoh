<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Doctrine\DBAL\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

abstract class SQLArrayType extends Type
{
    /**
     * @return Type
     */
    abstract protected function getInnerType();

    /**
     * @return Type
     */
    private function doGetInnerType()
    {
        static $type = null;
        if ($type === null) {
            return $type = $this->getInnerType();
        }

        return $type;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->doGetInnerType()->getName() . 'array';
    }

    /**
     * {@inheritdoc}
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return '_' . $this->doGetInnerType()->getSQLDeclaration($fieldDeclaration, $platform);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (!is_array($value)) {
            return null;
        }

        return $this->convertToDatabaseValueRecursive($value, $platform, $this->doGetInnerType());
    }

    /**
     * @param array            $values
     * @param AbstractPlatform $platform
     * @param Type             $type
     *
     * @return string
     */
    protected function convertToDatabaseValueRecursive(array $values, AbstractPlatform $platform, Type $type)
    {
        $result = [];
        foreach ($values as $value) {
            if (is_array($value)) {
                $result[] = $this->convertToDatabaseValueRecursive($value, $platform, $type);
            } else {
                $result[] = $type->convertToDatabaseValue($value, $platform);
            }
        }

        return '{' . implode(',', $result) . '}';
    }

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        return $this->convertToPHPValueRecursive($value, $platform, $this->doGetInnerType());
    }

    /**
     * @param                  $values
     * @param AbstractPlatform $platform
     * @param Type             $type
     */
    protected function convertToPHPValueRecursive($values, AbstractPlatform $platform, Type $type)
    {
        throw new \RuntimeException(__METHOD__ . ' not yet implemented');
    }
}
