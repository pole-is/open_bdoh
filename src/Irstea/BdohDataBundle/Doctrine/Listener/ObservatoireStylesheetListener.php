<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohDataBundle\Doctrine\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Irstea\BdohBundle\Manager\CssManager;
use Irstea\BdohDataBundle\Entity\Observatoire;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Crée/Met à jour/supprime la feuille de style d'un observatoire quand il est créé/modifié/supprimé.
 *
 * @DI\Service
 * @DI\Tag("doctrine.orm.entity_listener", attributes={"lazy"=true})
 */
class ObservatoireStylesheetListener
{
    /**
     * @var CssManager
     *
     * @DI\Inject("irstea_bdoh.manager.css")
     */
    public $cssManager;

    /**
     * @param Observatoire       $obs
     * @param LifecycleEventArgs $event
     *
     * @throws \Less_Exception_Parser
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function postPersist(Observatoire $obs, LifecycleEventArgs $unused)
    {
        $this->cssManager->removeObservatoireCssFile($obs);
    }

    /**
     * @param Observatoire       $obs
     * @param LifecycleEventArgs $event
     *
     * @throws \Less_Exception_Parser
     */
    public function postUpdate(Observatoire $obs, LifecycleEventArgs $event)
    {
        $changes = $event->getEntityManager()->getUnitOfWork()->getEntityChangeSet($obs);

        if (isset($changes['couleurPrimaire']) || isset($changes['couleurSecondaire'])) {
            $this->cssManager->removeObservatoireCssFile($obs);
        }
    }

    /**
     * @param Observatoire       $obs
     * @param LifecycleEventArgs $event
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function postRemove(Observatoire $obs, LifecycleEventArgs $unused)
    {
        $this->cssManager->removeObservatoireCssFile($obs);
    }
}
