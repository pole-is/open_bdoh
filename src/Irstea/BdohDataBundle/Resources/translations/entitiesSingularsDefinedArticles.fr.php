<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

return [
    'Observatoire'                                          => "de l'",
    'Doi'                                                   => 'du ',
    'Partenaire'                                            => 'du ',
    'SiteExperimental'                                      => 'du ',
    'Station'                                               => 'de la ',
    'Chronique'                                             => 'de la ',
    'ChroniqueDiscontinue'                                  => 'de la ',
    'ChroniqueContinue'                                     => 'de la ',
    'ChroniqueCalculee'                                     => 'de la ',
    'ChroniqueConvertie'                                    => 'de la ',
    'Commune'                                               => 'de la ',
    'FamilleParametres'                                     => 'de la ',
    'TypeParametre'                                         => 'du ',
    'Unite'                                                 => "de l'",
    'Mesure'                                                => 'de la ',
    'PointControle'                                         => 'du ',
    'Echantillonnage'                                       => "de l'",
    'PasEchantillonnage'                                    => 'du ',
    'LigneCalcul'                                           => 'de la ',
    'Bareme'                                                => 'du ',
    'PeriodeValidite'                                       => 'de la ',
    'Plage'                                                 => 'de la ',
    'Qualite'                                               => 'de la ',
    'JeuQualite'                                            => 'du ',
    'Bassin'                                                => 'du ',
    'CoursEau'                                              => 'du ',
    'TauxRemplissage'                                       => 'du ',
    'DataSet'                                               => 'du ',
    'Milieu'                                                => 'du ',
    'DataConstraint'                                        => 'du ',
    'CriteriaGeology'                                       => 'du ',
    'CriteriaClimat'                                        => 'du ',
    'InspireTheme'                                          => 'du ',
    'TopicCategory'                                         => 'de la ',
    'TypeFunding'                                           => 'du ',
    'ContactOrganisationRole'                               => 'du ',
    'RolePersonneTheia'                                     => 'du ',
    'PersonneTheia'                                         => 'de la ',
    'TheiaCategories'                                       => 'de la ',
];
