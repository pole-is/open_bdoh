<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

return [
    'Observatoire'                                          => 'un',
    'Doi'                                                   => 'un',
    'Partenaire'                                            => 'un',
    'SiteExperimental'                                      => 'un',
    'Station'                                               => 'une',
    'Chronique'                                             => 'une',
    'ChroniqueDiscontinue'                                  => 'une',
    'ChroniqueContinue'                                     => 'une',
    'ChroniqueCalculee'                                     => 'une',
    'ChroniqueConvertie'                                    => 'une',
    'Commune'                                               => 'une',
    'FamilleParametres'                                     => 'une',
    'TypeParametre'                                         => 'un',
    'Unite'                                                 => 'une',
    'Mesure'                                                => 'une',
    'PointControle'                                         => 'un',
    'Echantillonnage'                                       => 'un',
    'PasEchantillonnage'                                    => 'un',
    'LigneCalcul'                                           => 'une',
    'Bareme'                                                => 'un',
    'PeriodeValidite'                                       => 'une',
    'Plage'                                                 => 'une',
    'Qualite'                                               => 'une',
    'JeuQualite'                                            => 'un',
    'Bassin'                                                => 'un',
    'CoursEau'                                              => 'un',
    'TauxRemplissage'                                       => 'un',
    'DataSet'                                               => 'un',
    'Milieu'                                                => 'un',
    'DataConstraint'                                        => 'une',
    'CriteriaGeology'                                       => 'un',
    'CriteriaClimat'                                        => 'un',
    'InspireTheme'                                          => 'un',
    'TopicCategory'                                         => 'une',
    'TypeFunding'                                           => 'un',
    'ContactOrganisationRole'                               => 'un',
    'RolePersonneTheia'                                     => 'un',
    'PersonneTheia'                                         => 'un',
    'TheiaCategories'                                       => 'une',
];
