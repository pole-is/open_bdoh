<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

return [
    'Observatoire'                                          => 'an',
    'Doi'                                                   => 'a',
    'Partenaire'                                            => 'a',
    'SiteExperimental'                                      => 'an',
    'Station'                                               => 'a',
    'Chronique'                                             => 'a',
    'ChroniqueDiscontinue'                                  => 'a',
    'ChroniqueContinue'                                     => 'a',
    'ChroniqueCalculee'                                     => 'a',
    'ChroniqueConvertie'                                    => 'a',
    'Commune'                                               => 'a',
    'FamilleParametres'                                     => 'a',
    'TypeParametre'                                         => 'a',
    'Unite'                                                 => 'a',
    'Mesure'                                                => 'a',
    'PointControle'                                         => 'a',
    'Echantillonnage'                                       => 'a',
    'PasEchantillonnage'                                    => 'a',
    'LigneCalcul'                                           => 'a',
    'Bareme'                                                => 'a',
    'PeriodeValidite'                                       => 'a',
    'Plage'                                                 => 'a',
    'Qualite'                                               => 'a',
    'JeuQualite'                                            => 'a',
    'Bassin'                                                => 'a',
    'CoursEau'                                              => 'a',
    'TauxRemplissage'                                       => 'a',
    'DataSet'                                               => 'a',
    'Milieu'                                                => 'an',
    'DataConstraint'                                        => 'a',
    'CriteriaGeology'                                       => 'a',
    'CriteriaClimat'                                        => 'a',
    'InspireTheme'                                          => 'an',
    'TopicCategory'                                         => 'a',
    'TypeFunding'                                           => 'a',
    'ContactOrganisationRole'                               => 'a',
    'RolePersonneTheia'                                     => 'a',
    'PersonneTheia'                                         => 'a',
    'TheiaCategories'                                       => 'a',
];
