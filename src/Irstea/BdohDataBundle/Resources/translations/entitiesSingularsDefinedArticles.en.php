<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

return [
    'Observatoire'                                          => 'of the ',
    'Doi'                                                   => 'of the ',
    'Partenaire'                                            => 'of the ',
    'SiteExperimental'                                      => 'of the ',
    'Station'                                               => 'of the ',
    'Chronique'                                             => 'of the ',
    'ChroniqueDiscontinue'                                  => 'of the ',
    'ChroniqueContinue'                                     => 'of the ',
    'ChroniqueCalculee'                                     => 'of the ',
    'ChroniqueConvertie'                                    => 'of the ',
    'Commune'                                               => 'of the ',
    'FamilleParametres'                                     => 'of the ',
    'TypeParametre'                                         => 'of the ',
    'Unite'                                                 => 'of the ',
    'Mesure'                                                => 'of the ',
    'PointControle'                                         => 'of the ',
    'Echantillonnage'                                       => 'of the ',
    'PasEchantillonnage'                                    => 'of the ',
    'LigneCalcul'                                           => 'of the ',
    'Bareme'                                                => 'of the ',
    'PeriodeValidite'                                       => 'of the ',
    'Plage'                                                 => 'of the ',
    'Qualite'                                               => 'of the ',
    'JeuQualite'                                            => 'of the ',
    'Bassin'                                                => 'of the ',
    'CoursEau'                                              => 'of the ',
    'TauxRemplissage'                                       => 'of the ',
    'DataSet'                                               => 'of the ',
    'Milieu'                                                => 'of the ',
    'DataConstraint'                                        => 'of the ',
    'CriteriaGeology'                                       => 'of the ',
    'CriteriaClimat'                                        => 'of the ',
    'InspireTheme'                                          => 'of the ',
    'TopicCategory'                                         => 'of the ',
    'TypeFunding'                                           => 'of the ',
    'ContactOrganisationRole'                               => 'of the ',
    'RolePersonneTheia'                                     => 'of the ',
    'PersonneTheia'                                         => 'of the ',
    'TheiaCategories'                                       => 'of the ',
];
