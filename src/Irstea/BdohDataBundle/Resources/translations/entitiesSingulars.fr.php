<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

return [
    'Observatoire'                                          => 'Observatoire',
    'Doi'                                                   => 'DOI',
    'Partenaire'                                            => 'Partenaire',
    'SiteExperimental'                                      => 'Site expérimental',
    'Station'                                               => 'Station',
    'Chronique'                                             => 'Chronique',
    'ChroniqueDiscontinue'                                  => 'Chronique discontinue',
    'ChroniqueContinue'                                     => 'Chronique continue',
    'ChroniqueCalculee'                                     => 'Chronique calculée',
    'ChroniqueConvertie'                                    => 'Chronique convertie',
    'Commune'                                               => 'Commune',
    'FamilleParametres'                                     => 'Famille de paramètres',
    'TypeParametre'                                         => 'Type de paramètre',
    'Unite'                                                 => 'Unité',
    'Mesure'                                                => 'Mesure',
    'PointControle'                                         => 'Point de contrôle',
    'Echantillonnage'                                       => 'Échantillonnage',
    'PasEchantillonnage'                                    => 'Pas de temps',
    'LigneCalcul'                                           => 'Ligne de calculs',
    'Bareme'                                                => 'Barème',
    'PeriodeValidite'                                       => 'Période de validité',
    'Plage'                                                 => 'Plage',
    'Qualite'                                               => 'Qualité',
    'JeuQualite'                                            => 'Jeu de qualités',
    'Bassin'                                                => 'Bassin',
    'CoursEau'                                              => "Cours d'eau",
    'TauxRemplissage'                                       => 'Taux de remplissage',
    'DataSet'                                               => 'Jeu de données',
    'DataConstraint'                                        => 'Contrainte données',
    'CriteriaGeology'                                       => 'Critère géologique',
    'CriteriaClimat'                                        => 'Critère climatique',
    'InspireTheme'                                          => 'Thème INSPIRE',
    'TopicCategory'                                         => 'Catégorie de Thématiques INSPIRE',
    'TypeFunding'                                           => 'Type de financeurs',
    'ContactOrganisationRole'                               => 'Role des Organisations',
    'RolePersonneTheia'                                     => 'Role des Personnes',
    'PersonneTheia'                                         => 'Personne pour Theia/OZCAR',
    'Milieu'                                                => 'Milieu',
    'TheiaCategories'                                       => 'Connection TheiaCategories',
];
