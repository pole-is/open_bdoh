<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

return [
    'Observatoire'                                          => 'Observatory',
    'Doi'                                                   => 'DOI',
    'Partenaire'                                            => 'Partner',
    'SiteExperimental'                                      => 'Experimental site',
    'Station'                                               => 'Station',
    'Chronique'                                             => 'Time series',
    'ChroniqueDiscontinue'                                  => 'Discontinuous time series',
    'ChroniqueContinue'                                     => 'Continuous time series',
    'ChroniqueCalculee'                                     => 'Calculated time series',
    'ChroniqueConvertie'                                    => 'Converted time series',
    'Commune'                                               => 'Town',
    'FamilleParametres'                                     => 'Parameter category',
    'TypeParametre'                                         => 'Parameter type',
    'Unite'                                                 => 'Unit',
    'Mesure'                                                => 'Data point',
    'PointControle'                                         => 'Checkpoint',
    'Echantillonnage'                                       => 'Sampling',
    'PasEchantillonnage'                                    => 'Time step',
    'LigneCalcul'                                           => 'Calculation line',
    'Bareme'                                                => 'Scale',
    'PeriodeValidite'                                       => 'Validity period',
    'Plage'                                                 => 'Range',
    'Qualite'                                               => 'Quality',
    'JeuQualite'                                            => 'Set of qualities',
    'Bassin'                                                => 'Basin',
    'CoursEau'                                              => 'River',
    'TauxRemplissage'                                       => 'Data completeness',
    'DataSet'                                               => 'Dataset',
    'Milieu'                                                => 'Environment',
    'DataConstraint'                                        => 'Data Constraint',
    'CriteriaGeology'                                       => 'Criteria Geology',
    'CriteriaClimat'                                        => 'Criteria Climat',
    'InspireTheme'                                          => 'INSPIRE Theme',
    'TopicCategory'                                         => 'INSPIRE Topic Category',
    'TypeFunding'                                           => 'Type Funding',
    'ContactOrganisationRole'                               => 'Organisations Role',
    'RolePersonneTheia'                                     => 'User Theia/OZCAR Role',
    'PersonneTheia'                                         => 'Theia User',
    'TheiaCategories'                                       => 'Correspondance TheiaCategories',
];
