<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

return [
    'Observatoire.slug.alreadyExists'                                                        => "Le nom choisi est identique ou trop proche de celui d'un observatoire existant.",
    'SiteExperimental.slug.alreadyExists'                                                    => "Le nom choisi est identique ou trop proche de celui d'un site existant.",
    'Observatoire.theiaCode.alreadyExists'                                                   => "Le nom choisi est identique ou trop proche de celui d'un Code Theia d'un autre observatoire.",
    'Station.nom.alreadyExists'                                                              => 'Ce nom de station existe déjà.',
    'Station.code.alreadyExists'                                                             => 'Ce code de station existe déjà.',
    'Chronique.code.alreadyExists'                                                           => 'Ce code existe déjà pour cette station.',
    'Bassin.nom.alreadyExists'                                                               => 'Ce bassin existe déjà pour cet observatoire.',
    'CoursEau.nom.alreadyExists'                                                             => "Ce cours d'eau existe déjà pour cet observatoire.",
    'FamilleParametres.nom.alreadyExists'                                                    => 'Cette famille de paramètres existe déjà.',
    'This file is not a valid image'                                                         => "Le fichier envoyé n'était pas une image valide.",
    'The file is too large. Allowed maximum size is {{ limit }}'                             => 'Le fichier envoyé était trop volumineux (taille supérieure à {{ limit }})',
    'The mime type of the file is invalid ({{ type }}). Allowed mime types are {{ types }}'  => 'Le fichier envoyé est de type incorect ({{ type }}). Les fichiers autorisés sont : {{ types }}',
];
