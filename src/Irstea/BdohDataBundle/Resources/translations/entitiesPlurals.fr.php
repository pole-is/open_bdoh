<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

return [
    'Observatoire'                                          => 'Observatoires',
    'Doi'                                                   => 'DOI',
    'Partenaire'                                            => 'Partenaires',
    'SiteExperimental'                                      => 'Sites expérimentaux',
    'Station'                                               => 'Stations',
    'Chronique'                                             => 'Ensembles des chroniques',
    'ChroniqueDiscontinue'                                  => 'Chroniques discontinues',
    'ChroniqueContinue'                                     => 'Chroniques continues',
    'ChroniqueCalculee'                                     => 'Chroniques calculées',
    'ChroniqueConvertie'                                    => 'Chroniques converties',
    'Commune'                                               => 'Communes',
    'FamilleParametres'                                     => 'Familles de paramètres',
    'TypeParametre'                                         => 'Types de paramètre',
    'Unite'                                                 => 'Unités',
    'Mesure'                                                => 'Mesures',
    'PointControle'                                         => 'Points de contrôle',
    'Echantillonnage'                                       => 'Échantillonnages',
    'PasEchantillonnage'                                    => 'Pas de temps',
    'LigneCalcul'                                           => 'Lignes de calculs',
    'Bareme'                                                => 'Barèmes',
    'PeriodeValidite'                                       => 'Périodes de validité',
    'Plage'                                                 => 'Plages',
    'Qualite'                                               => 'Qualités',
    'JeuQualite'                                            => 'Jeux de qualités',
    'Bassin'                                                => 'Bassins',
    'CoursEau'                                              => "Cours d'eau",
    'TauxRemplissage'                                       => 'Taux de remplissage',
    'DataSet'                                               => 'Jeux de données',
    'DataConstraint'                                        => 'Contraintes données',
    'CriteriaGeology'                                       => 'Critères géologiques',
    'CriteriaClimat'                                        => 'Critères climatiques',
    'InspireTheme'                                          => 'Thèmes INSPIRE',
    'TopicCategory'                                         => 'Catégories Thématiques INSPIRE',
    'TypeFunding'                                           => 'Types de financeurs',
    'TheiaCategories'                                       => 'Correspondances TheiaCategories',
    'ContactOrganisationRole'                               => 'Roles des Organisations',
    'RolePersonneTheia'                                     => 'Roles des Personnes',
    'PersonneTheia'                                         => 'Personnes pour Theia',
    'Milieu'                                                => 'Milieux',
];
