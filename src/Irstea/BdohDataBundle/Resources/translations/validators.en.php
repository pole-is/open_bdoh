<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

return [
    'Observatoire.slug.alreadyExists'                                                       => 'The chosen name is identical or too close to the one of an existing observatory.',
    'SiteExperimental.slug.alreadyExists'                                                   => 'The chosen name is identical or too close to the one of an existing site.',
    'Observatoire.theiaCode.alreadyExists'                                                  => "The chosen name is identical or too close to theia's code of an existing observatory.",
    'Station.nom.alreadyExists'                                                             => 'The station name already exists.',
    'Station.code.alreadyExists'                                                            => 'The station code already exists.',
    'Chronique.code.alreadyExists'                                                          => 'This code already exists for this station.',
    'Bassin.nom.alreadyExists'                                                              => 'This basin already exists for this observatory.',
    'CoursEau.nom.alreadyExists'                                                            => 'This river already exists for this observatory.',
    'FamilleParametres.nom.alreadyExists'                                                   => 'This parameter category already exists.',
    'This file is not a valid image'                                                        => 'This file is not a valid image.',
    'The file is too large. Allowed maximum size is {{ limit }}'                            => 'The file is too large (Allowed maximum size is {{ limit }})',
    'The mime type of the file is invalid ({{ type }}). Allowed mime types are {{ types }}' => 'The mime type of the file is invalid ({{ type }}). Allowed mime types are: {{ types }}',
];
