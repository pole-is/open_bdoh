<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

return [
    'Observatoire'                                          => 'Observatories',
    'Doi'                                                   => 'DOI',
    'Partenaire'                                            => 'Partners',
    'SiteExperimental'                                      => 'Experimental sites',
    'Station'                                               => 'Stations',
    'Chronique'                                             => 'All Time series',
    'ChroniqueDiscontinue'                                  => 'Discontinuous time series',
    'ChroniqueContinue'                                     => 'Continuous time series',
    'ChroniqueCalculee'                                     => 'Calculated time series',
    'ChroniqueConvertie'                                    => 'Converted time series',
    'Commune'                                               => 'Towns',
    'FamilleParametres'                                     => 'Parameter categories',
    'TypeParametre'                                         => 'Parameter types',
    'Unite'                                                 => 'Units',
    'Mesure'                                                => 'Data points',
    'PointControle'                                         => 'Checkpoints',
    'Echantillonnage'                                       => 'Sampling',
    'PasEchantillonnage'                                    => 'Time steps',
    'LigneCalcul'                                           => 'Calculation lines',
    'Bareme'                                                => 'Scales',
    'PeriodeValidite'                                       => 'Validity periods',
    'Plage'                                                 => 'Ranges',
    'Qualite'                                               => 'Qualities',
    'JeuQualite'                                            => 'Sets of qualities',
    'Bassin'                                                => 'Basins',
    'CoursEau'                                              => 'Rivers',
    'TauxRemplissage'                                       => 'Data completeness',
    'DataSet'                                               => 'Datasets',
    'Milieu'                                                => 'Environments',
    'DataConstraint'                                        => 'Data Constraints',
    'CriteriaGeology'                                       => 'Criteria Geologies',
    'CriteriaClimat'                                        => 'Criteria Climates',
    'InspireTheme'                                          => 'INSPIRE Themes',
    'TopicCategory'                                         => 'INSPIRE Topic Categories',
    'TypeFunding'                                           => 'Type Fundings',
    'ContactOrganisationRole'                               => 'Organisations Roles',
    'RolePersonneTheia'                                     => 'Users Roles',
    'PersonneTheia'                                         => 'Theia Users',
    'TheiaCategories'                                       => 'Connections TheiaCategories',
];
