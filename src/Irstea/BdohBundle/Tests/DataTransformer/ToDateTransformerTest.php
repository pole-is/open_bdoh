<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohBundle\Tests\DataTransformer;

use Irstea\BdohBundle\DataTransformer\ToDateTransformer as ToDate;

/**
 * Class ToDateTransformerTest.
 *
 * @group unit
 */
class ToDateTransformerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @covers \Irstea\BdohBundle\DataTransformer\ToDateTransformer::checkSplitDate
     */
    public function testValidDatesForChecksplitdateFunctionReturnTrue()
    {
        $this->assertTrue(ToDate::checkSplitDate('2004', '10', '15', '-', '-'));       // default delimiter
        $this->assertTrue(ToDate::checkSplitDate('2004', '10', '15', '/', '/', '/'));  // customized delimiter
    }

    /**
     * @covers \Irstea\BdohBundle\DataTransformer\ToDateTransformer::checkSplitDate
     */
    public function testInvalidDatesForChecksplitdateFunctionReturnFalse()
    {
        $this->assertFalse(ToDate::checkSplitDate('2oo4', '10', '15', '-', '-'));      // year not numeric
        $this->assertFalse(ToDate::checkSplitDate('2004', 'io', '15', '-', '-'));      // month not numeric
        $this->assertFalse(ToDate::checkSplitDate('2004', '10', 'iS', '-', '-'));      // day not numeric
        $this->assertFalse(ToDate::checkSplitDate('2004', '10', '15', '/', '-'));      // bad year delimiter
        $this->assertFalse(ToDate::checkSplitDate('2004', '10', '15', '-', '/'));      // bad month delimiter
        $this->assertFalse(ToDate::checkSplitDate('2004', '10', '15', '-', '-', '/')); // bad both delimiter
        $this->assertFalse(ToDate::checkSplitDate('-200', '10', '15', '-', '-'));      // bad Gregorian year
        $this->assertFalse(ToDate::checkSplitDate('2004', '99', '15', '-', '-'));      // bad Gregorian month
        $this->assertFalse(ToDate::checkSplitDate('2004', '10', '99', '-', '-'));      // bad Gregorian day
    }

    /**
     * @covers \Irstea\BdohBundle\DataTransformer\ToDateTransformer::date
     */
    public function testValidDatesAreTransformed()
    {
        $this->assertEquals('2004-10-04', ToDate::date('2004-10-04'));
    }

    /**
     * @dataProvider invalidDatesProvider
     * @covers       \Irstea\BdohBundle\DataTransformer\ToDateTransformer::date
     *
     * @param mixed $invalidDate
     */
    public function testInvalidDatesReturnFalse($invalidDate)
    {
        $this->assertFalse(ToDate::date($invalidDate));
    }

    public function invalidDatesProvider()
    {
        return [
            ['2005-10-15 05:08:00.777'],
            ['04-10-15'],               // bad string lengths
            ['2oo4-10-15'],
            ['2004-io-15'],
            ['2004-10-iS'],     // year, month, day not numeric
            ['2004/10/15'],
            ['2004-10/15'],
            ['2004/10-15'],     // bad delimiters
            ['  2004-10-15  '],                                           // spaces surrounds date
            ['2004 -10-15'],
            ['2004- 10-15'],
            ['2004 - 10-15'], // spaces beetwen day and month
        ];
    }

    /**
     * @covers \Irstea\BdohBundle\DataTransformer\ToDateTransformer::frenchDate
     */
    public function testValidFrenchDatesAreTransformed()
    {
        $this->assertEquals('2004-10-05', ToDate::frenchDate('5/10/2004'));  // Format D/MM/YYYY
        $this->assertEquals('2004-10-05', ToDate::frenchDate('05/10/2004')); // Format DD/MM/YYYY
    }

    /**
     * @dataProvider invalidFrenchDatesProvider
     * @covers       \Irstea\BdohBundle\DataTransformer\ToDateTransformer::frenchDate
     *
     * @param mixed $invalidFrenchDate
     */
    public function testInvalidFrenchDatesReturnFalse($invalidFrenchDate)
    {
        $this->assertFalse(ToDate::frenchDate($invalidFrenchDate));
    }

    public function invalidFrenchDatesProvider()
    {
        return [
            ['15/10/2004 05:08:00.777'],
            ['15/10/04'],               // bad string lengths
            ['15/10/204'],
            ['5/1000/204'],                           // bad dates
            ['iS/10/2004'],
            ['15/io/2004'],
            ['15/10/2oo4'],     // year, month, day not numeric
            ['15-10-2004'],
            ['15-10/2004'],
            ['15/10-2004'],     // bad delimiters
            ['2004/10/15'],                                               // year and day swapped
            ['  15/10/2004  '],                                           // spaces surrounds date
            ['15 /10/2004'],
            ['15/ 10/2004'],
            ['15 / 10/2004'], // spaces beetwen day and month
        ];
    }

    /**
     * @covers \Irstea\BdohBundle\DataTransformer\ToDateTransformer::shortFrenchDate
     */
    public function testValidShortFrenchDatesAreTransformed()
    {
        $this->assertEquals('2004-10-05', ToDate::shortFrenchDate('5/10/04'));
        $this->assertEquals('1961-10-05', ToDate::shortFrenchDate('5/10/61'));
        $this->assertEquals('2004-10-05', ToDate::shortFrenchDate('05/10/04'));
        $this->assertEquals('1961-10-05', ToDate::shortFrenchDate('05/10/61'));
    }

    /**
     * @dataProvider invalidShortFrenchDatesProvider
     * @covers       \Irstea\BdohBundle\DataTransformer\ToDateTransformer::frenchDate
     *
     * @param mixed $invalidShortFrenchDate
     */
    public function testInvalidShortFrenchDatesReturnFalse($invalidShortFrenchDate)
    {
        $this->assertFalse(ToDate::shortFrenchDate($invalidShortFrenchDate));
    }

    public function invalidShortFrenchDatesProvider()
    {
        return [
            ['5/10/4'],
            ['15/10/2004'],                        // bad string lengths
            ['15/10/4'],
            ['5/100/4'],                          // bad dates
            ['iS/10/04'],
            ['15/io/04'],
            ['15/10/o4'],     // year, month, day not numeric
            ['15-10-04'],
            ['15-10/04'],
            ['15/10-04'],     // bad delimiters
            ['  15/10/04  '],                                       // spaces surrounds date
            ['15 /10/04'],
            ['15/ 10/04'],
            ['15 / 10/04'], // spaces beetwen day and month
        ];
    }
}
