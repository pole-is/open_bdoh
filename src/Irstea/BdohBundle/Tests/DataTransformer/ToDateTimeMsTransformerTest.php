<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohBundle\Tests\DataTransformer;

use Irstea\BdohBundle\DataTransformer\ToDateTimeMsTransformer as ToDateTimeMs;

/**
 * Class ToDateTimeMsTransformerTest.
 *
 * @group unit
 */
class ToDateTimeMsTransformerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider validDatetimemsProvider
     * @covers       \Irstea\BdohBundle\DataTransformer\ToDateTimeMsTransformer::varDateTimeMs
     *
     * @param mixed $validDateTimeMs
     * @param mixed $dateTimeMs
     */
    public function testValidDatetimemsAreTransformed($validDateTimeMs, $dateTimeMs)
    {
        $this->assertEquals($dateTimeMs, ToDateTimeMs::varDateTimeMs($validDateTimeMs));
    }

    public function validDatetimemsProvider()
    {
        return [
            ['2004-10-15', '2004-10-15 00:00:00.000'], // a date
            ['2004-10-15 1', '2004-10-15 01:00:00.000'], // a datetime with just hours (once digit)
            ['2004-10-15 12', '2004-10-15 12:00:00.000'], // a datetime with just hours
            ['2004-10-15 12:37', '2004-10-15 12:37:00.000'], // a datetime without seconds
            ['2004-10-15 12:37:42', '2004-10-15 12:37:42.000'], // a datetime without milliseconds
            ['2004-10-15 12:37:42.543', '2004-10-15 12:37:42.543'], // a datetime with milliseconds
            ['  2004-10-15 12:37:42.543', '2004-10-15 12:37:42.543'], // left spaces
            ['2004-10-15 12:37:42.543  ', '2004-10-15 12:37:42.543'], // right spaces
            ['2004-10-15      12:37:42.543', '2004-10-15 12:37:42.543'], // middle spaces
            ['  2004-10-15    12:37:42.543  ', '2004-10-15 12:37:42.543'], // spaces on all sides
        ];
    }

    /**
     * @dataProvider invalidDatetimemsProvider
     * @covers       \Irstea\BdohBundle\DataTransformer\ToDateTimeMsTransformer::varDateTimeMs
     *
     * @param mixed $invalidDateTimeMs
     */
    public function testInvalidDatetimemsReturnFalse($invalidDateTimeMs)
    {
        $this->assertFalse(ToDateTimeMs::varDateTimeMs($invalidDateTimeMs), "$invalidDateTimeMs is invalid");
    }

    public function invalidDatetimemsProvider()
    {
        return [
            ['2004/10/15 12:37:42.543'],   // bad date
            ['2004-10-15 25'],             // bad time with just hours
            ['2004-10-15 12:61'],          // bad time without seconds
            ['2004-10-15 12:37:ii'],       // bad time without milliseconds
            ['2004-10-15 12:37:42.aei'],   // bad time with milliseconds
            ['2004-10-15...12:37:42.543'], // bad delimiter beetwen date and time
            ['04-10-15 12:37:42.543'],     // bad date length
            ['2004-10-15 12:37:42.54'],    // bad milliseconds length
        ];
    }

    /**
     * @dataProvider validFrenchDatetimemsProvider
     * @covers       \Irstea\BdohBundle\DataTransformer\ToDateTimeMsTransformer::varFrenchDateTimeMs
     *
     * @param mixed $validFrenchDateTimeMs
     * @param mixed $dateTimeMs
     */
    public function testValidFrenchDatetimemsAreTransformed($validFrenchDateTimeMs, $dateTimeMs)
    {
        $this->assertEquals($dateTimeMs, ToDateTimeMs::varFrenchDateTimeMs($validFrenchDateTimeMs));
    }

    public function validFrenchDatetimemsProvider()
    {
        return [
            ['15/10/2004', '2004-10-15 00:00:00.000'], // a date
            ['15/10/2004 1', '2004-10-15 01:00:00.000'], // a datetime with just hours (once digit)
            ['15/10/2004 12', '2004-10-15 12:00:00.000'], // a datetime with just hours
            ['15/10/2004 12:37', '2004-10-15 12:37:00.000'], // a datetime without seconds
            ['15/10/2004 12:37:42', '2004-10-15 12:37:42.000'], // a datetime without milliseconds
            ['15/10/2004 12:37:42.543', '2004-10-15 12:37:42.543'], // a complete datetime
            ['5/10/2004 12:37:42.543', '2004-10-05 12:37:42.543'], // a complete datetime with date format = d/MM/YYYY
            ['  15/10/2004 12:37:42.543', '2004-10-15 12:37:42.543'], // left spaces
            ['15/10/2004 12:37:42.543  ', '2004-10-15 12:37:42.543'], // right spaces
            ['15/10/2004      12:37:42.543', '2004-10-15 12:37:42.543'], // middle spaces
            ['  15/10/2004    12:37:42.543  ', '2004-10-15 12:37:42.543'], // spaces on all sides
        ];
    }

    /**
     * @dataProvider invalidFrenchDatetimemsProvider
     * @covers       \Irstea\BdohBundle\DataTransformer\ToDateTimeMsTransformer::varFrenchDateTimeMs
     *
     * @param mixed $invalidFrenchDateTimeMs
     */
    public function testInvalidFrenchDatetimemsReturnFalse($invalidFrenchDateTimeMs)
    {
        $this->assertFalse(ToDateTimeMs::varFrenchDateTimeMs($invalidFrenchDateTimeMs));
    }

    public function invalidFrenchDatetimemsProvider()
    {
        return [
            ['15/10/04 12:37:42.543'],     // bad date length
            ['15/10/2004 25'],             // bad time with just hours
            ['15/10/2004 12:61'],          // bad time without seconds
            ['15/10/2004 12:37:ii'],       // bad time without milliseconds
            ['15/10/2004 12:37:42.aei'],   // bad time with milliseconds
            ['15/10/2004...12:37:42.543'], // bad delimiter beetwen date and time
            ['15/10/2004 12:37:42.54'],    // bad milliseconds length
        ];
    }

    /**
     * @dataProvider validShortFrenchDatetimemsProvider
     * @covers       \Irstea\BdohBundle\DataTransformer\ToDateTimeMsTransformer::varShortFrenchDateTimeMs
     *
     * @param mixed $validShortFrenchDateTimeMs
     * @param mixed $dateTimeMs
     */
    public function testValidShortFrenchDatetimemsAreTransformed($validShortFrenchDateTimeMs, $dateTimeMs)
    {
        $this->assertEquals($dateTimeMs, ToDateTimeMs::varShortFrenchDateTimeMs($validShortFrenchDateTimeMs));
    }

    public function validShortFrenchDatetimemsProvider()
    {
        return [
            ['15/10/04', '2004-10-15 00:00:00.000'], // a date
            ['15/10/61', '1961-10-15 00:00:00.000'], // a date
            ['15/10/04 1', '2004-10-15 01:00:00.000'], // a datetime with just hours (once digit)
            ['15/10/04 12', '2004-10-15 12:00:00.000'], // a datetime with just hours
            ['15/10/04 12:37', '2004-10-15 12:37:00.000'], // a datetime without seconds
            ['15/10/04 12:37:42', '2004-10-15 12:37:42.000'], // a datetime without milliseconds
            ['15/10/04 12:37:42.543', '2004-10-15 12:37:42.543'], // a complete datetime
            ['5/10/04 12:37:42.543', '2004-10-05 12:37:42.543'], // a complete datetime with date format = d/MM/YYYY
            ['  15/10/04 12:37:42.543', '2004-10-15 12:37:42.543'], // left spaces
            ['15/10/04 12:37:42.543  ', '2004-10-15 12:37:42.543'], // right spaces
            ['15/10/04      12:37:42.543', '2004-10-15 12:37:42.543'], // middle spaces
            ['  15/10/04    12:37:42.543  ', '2004-10-15 12:37:42.543'], // spaces on all sides
        ];
    }

    /**
     * @dataProvider invalidShortFrenchDatetimemsProvider
     * @covers       \Irstea\BdohBundle\DataTransformer\ToDateTimeMsTransformer::varShortFrenchDateTimeMs
     *
     * @param mixed $invalidShortFrenchDateTimeMs
     */
    public function testInvalidShortFrenchDatetimemsReturnFalse($invalidShortFrenchDateTimeMs)
    {
        $this->assertFalse(
            ToDateTimeMs::varShortFrenchDateTimeMs($invalidShortFrenchDateTimeMs),
            "$invalidShortFrenchDateTimeMs is invalid"
        );
    }

    public function invalidShortFrenchDatetimemsProvider()
    {
        return [
            ['15/10/2004 12:37:42.543'], // bad date length
            ['15/10/04 25'],             // bad time with just hours
            ['15/10/04 12:61'],          // bad time without seconds
            ['15/10/04 12:37:ii'],       // bad time without milliseconds
            ['15/10/04 12:37:42.aei'],   // bad time with milliseconds
            ['15/10/04...12:37:42.543'], // bad delimiter beetwen date and time
            ['15/10/04 12:37:42.54'],    // bad milliseconds length
        ];
    }
}
