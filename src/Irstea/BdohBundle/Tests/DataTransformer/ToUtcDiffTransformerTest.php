<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohBundle\Tests\DataTransformer;

use Irstea\BdohBundle\DataTransformer\ToUtcDiffTransformer;

/**
 * Class ToUtcDiffTransformerTest.
 *
 * @group unit
 */
class ToUtcDiffTransformerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider validUtcDiffTimezonesProvider
     * @covers       \Irstea\BdohBundle\DataTransformer\ToUtcDiffTransformer::utcDiff
     *
     * @param mixed $validUtcDiffTz
     * @param mixed $utcDiffTimezone
     */
    public function testValidUtcDiffTimezonesAreTransformed($validUtcDiffTz, $utcDiffTimezone)
    {
        $this->assertEquals($utcDiffTimezone, ToUtcDiffTransformer::utcDiff($validUtcDiffTz));
    }

    public function validUtcDiffTimezonesProvider()
    {
        return [
            ['+00:00', '+00:00'], // Plus
            ['-12:45', '-12:45'], // Minus
            ['  +00:00  ', '+00:00'], // Spaces on left and right sides
            ['-12:13', '-12:13'], // Semi-consistent timezone
        ];
    }

    /**
     * @dataProvider invalidUtcDiffTimezonesProvider
     * @covers       \Irstea\BdohBundle\DataTransformer\ToUtcDiffTransformer::utcDiff
     *
     * @param mixed $invalidUtcDiffTz
     */
    public function testInvalidUtcDiffTimezonesReturnFalse($invalidUtcDiffTz)
    {
        $this->assertFalse(ToUtcDiffTransformer::utcDiff($invalidUtcDiffTz));
    }

    public function invalidUtcDiffTimezonesProvider()
    {
        return [
            ['00:00'],
            ['+0000'],
            ['0000'],                      // bad string lengths
            ['~00:00'],
            ['+00/00'],                                   // bad sign or delimiter
            ['+20:00'],
            ['+15:00'],
            ['+00:60'],                  // inconsistent hours or minutes
            ['+a0:00'],
            ['+2o:00'],
            ['+20:o0'],
            ['+20:0o'], // not digit
        ];
    }
}
