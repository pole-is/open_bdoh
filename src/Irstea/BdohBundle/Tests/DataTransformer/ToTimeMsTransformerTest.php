<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohBundle\Tests\DataTransformer;

use Irstea\BdohBundle\DataTransformer\ToTimeMsTransformer as ToTimeMs;

/**
 * Class ToTimeMsTransformerTest.
 *
 * @group unit
 */
class ToTimeMsTransformerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @covers \Irstea\BdohBundle\DataTransformer\ToTimeMsTransformer::checkTimeJustHours
     */
    public function testChecktimejusthoursFunctionWorksFine()
    {
        $this->assertTrue(ToTimeMs::checkTimeJustHours('04'));
        $this->assertTrue(ToTimeMs::checkTimeJustHours('14'));
        $this->assertTrue(ToTimeMs::checkTimeJustHours('23'));
        $this->assertTrue(ToTimeMs::checkTimeJustHours('24'));

        $this->assertFalse(ToTimeMs::checkTimeJustHours('a1'));
        $this->assertFalse(ToTimeMs::checkTimeJustHours('0a'));
        $this->assertFalse(ToTimeMs::checkTimeJustHours('2c'));
        $this->assertFalse(ToTimeMs::checkTimeJustHours('29'));
    }

    /**
     * @covers \Irstea\BdohBundle\DataTransformer\ToTimeMsTransformer::checkTimeWithoutSeconds
     */
    public function testChecktimewithoutsecondsFunctionWorksFine()
    {
        $this->assertTrue(ToTimeMs::checkTimeWithoutSeconds('15:00'));
        $this->assertTrue(ToTimeMs::checkTimeWithoutSeconds('15:59'));
        $this->assertTrue(ToTimeMs::checkTimeWithoutSeconds('24:00'));

        $this->assertFalse(ToTimeMs::checkTimeWithoutSeconds('15/10'));
        $this->assertFalse(ToTimeMs::checkTimeWithoutSeconds('15:60'));
        $this->assertFalse(ToTimeMs::checkTimeWithoutSeconds('15:99'));
        $this->assertFalse(ToTimeMs::checkTimeWithoutSeconds('15:a5'));
        $this->assertFalse(ToTimeMs::checkTimeWithoutSeconds('15:5a'));
    }

    /**
     * @covers \Irstea\BdohBundle\DataTransformer\ToTimeMsTransformer::checkTime
     */
    public function testChecktimeFunctionWorksFine()
    {
        $this->assertTrue(ToTimeMs::checkTime('15:00:00'));
        $this->assertTrue(ToTimeMs::checkTime('15:59:59'));
        $this->assertTrue(ToTimeMs::checkTime('24:00:00'));

        $this->assertFalse(ToTimeMs::checkTime('15:00/10'));
        $this->assertFalse(ToTimeMs::checkTime('15:00:60'));
        $this->assertFalse(ToTimeMs::checkTime('15:00:99'));
        $this->assertFalse(ToTimeMs::checkTime('15:00:a5'));
        $this->assertFalse(ToTimeMs::checkTime('15:00:5a'));
        $this->assertFalse(ToTimeMs::checkTime('15:60:00')); // bad minutes
    }

    /**
     * @covers \Irstea\BdohBundle\DataTransformer\ToTimeMsTransformer::checkTimeMs
     */
    public function testChecktimemsFunctionWorksFine()
    {
        $this->assertTrue(ToTimeMs::checkTimeMs('12:45:37.123'));
        $this->assertTrue(ToTimeMs::checkTimeMs('24:45:37.124'));

        $this->assertFalse(ToTimeMs::checkTimeMs('12:45:37:123'));   // bad delimiter
        $this->assertFalse(ToTimeMs::checkTimeMs('12:45:37.aei'));   // milliseconds not numeric
        $this->assertFalse(ToTimeMs::checkTimeMs('12:45:60.123'));   // bad seconds
        $this->assertFalse(ToTimeMs::checkTimeMs('12:60:37.123'));   // bad minutes
    }

    /**
     * @dataProvider validTimemsProvider
     * @covers       \Irstea\BdohBundle\DataTransformer\ToTimeMsTransformer::varTimeMs
     *
     * @param mixed $validTimeMs
     * @param mixed $timeMs
     */
    public function testValidTimemsAreTransformed($validTimeMs, $timeMs)
    {
        $this->assertEquals($timeMs, ToTimeMs::varTimeMs($validTimeMs));
    }

    public function validTimemsProvider()
    {
        return [
            ['15', '15:00:00.000'], // a TimeMs with just hours
            ['15:10', '15:10:00.000'], // a TimeMs without seconds
            ['15:10:04', '15:10:04.000'], // a TimeMs without milliseconds
            ['15:10:04.666', '15:10:04.666'], // a complete TimeMs
            ['  15:10:04.666  ', '15:10:04.666'], // spaces surrounds time
            ['5', '05:00:00.000'], // once digit for hours
            ['5:10', '05:10:00.000'], // once digit for hours
            ['5:10:04', '05:10:04.000'], // once digit for hours
            ['5:10:04.666', '05:10:04.666'], // once digit for hours
        ];
    }

    /**
     * @dataProvider invalidTimemsProvider
     * @covers       \Irstea\BdohBundle\DataTransformer\ToTimeMsTransformer::varTimeMs
     *
     * @param mixed $invalidTimeMs
     */
    public function testInvalidTimemsReturnFalse($invalidTimeMs)
    {
        $this->assertFalse(ToTimeMs::varTimeMs($invalidTimeMs), "$invalidTimeMs is invalid");
    }

    public function invalidTimemsProvider()
    {
        return [
            ['15:10:04.6665'],
            ['15:10:04.'],
            ['15:10:'],
            ['15:'], // bad length
            ['15:60'],        // bad minutes
            ['15:10:60'],     // bad seconds
            ['15:10:04.aaa'], // milliseconds not numeric
        ];
    }
}
