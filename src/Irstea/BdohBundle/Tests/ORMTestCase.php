<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohBundle\Tests;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\DBAL\Schema\SchemaException;
use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Bridge\Doctrine\DataFixtures\ContainerAwareLoader;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @group database
 */
class ORMTestCase extends WebTestCase
{
    /**
     * @var ContainerInterface
     */
    protected static $container;

    /**
     * Le gestionnaire d'entité.
     *
     * @var \Doctrine\ORM\EntityManager
     */
    protected static $entityManager;

    /**
     * Les fixtures à charger;
     *data.
     *
     * @var array
     */
    protected static $ormFixtures;

    /**
     * Charge le kernel, l'entityManager et prépare le chargement des fixtures.
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        self::$kernel = static::createKernel();
        self::$kernel->boot();
        self::$container = self::$kernel->getContainer();
        self::$entityManager = self::$container->get('doctrine.orm.entity_manager');
        // static::generateSchema();
        self::$ormFixtures = static::prepareFixtures();
    }

    public function getContainer()
    {
        return self::$container;
    }

    public function get($id)
    {
        return self::$container->get($id);
    }

    protected static function prepareFixtures()
    {
        $paths = [];
        foreach (self::$kernel->getBundles() as $bundle) {
            if (is_dir($bundle->getPath() . '/DataFixtures/ORM')) {
                $paths[] = $bundle->getPath() . '/DataFixtures/ORM';
            }
        }

        $loader = new ContainerAwareLoader(self::$container);
        foreach ($paths as $path) {
            $loader->loadFromDirectory($path);
        }

        return $loader->getFixtures();
    }

    public function setUp()
    {
        parent::setUp();
        $this->loadFixtures(self::$ormFixtures);
    }

    protected function loadFixtures($fixtures)
    {
        $purger = new ORMPurger(self::$entityManager);
        $purger->setPurgeMode(ORMPurger::PURGE_MODE_TRUNCATE);
        $executor = new ORMExecutor(self::$entityManager, $purger);
        $executor->execute($fixtures);
    }

    /**
     * Génére le schéma.
     *
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    protected static function generateSchema()
    {
        // Get the metadatas of the application to create the schema.
        $metadatas = static::getMetadatas();

        if (!empty($metadatas)) {
            // Create SchemaTool
            $tool = new SchemaTool(static::$entityManager);
            $tool->createSchema($metadatas);
        } else {
            throw new SchemaException('No Metadata Classes to process.');
        }
    }

    /**
     * Overwrite this method to get specific metadatas.
     *
     * @return array
     */
    protected static function getMetadatas()
    {
        return self::$entityManager->getMetadataFactory()->getAllMetadata();
    }
}
