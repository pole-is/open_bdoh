<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohBundle\DataTransformer;

/**
 * Transforms datetime (without validating), from any format to DateTimeMs format
 * (see : Irstea\BdohDataBundle\Doctrine\DBAL\Types\DateTimeMs).
 *
 * Each function returns :
 *  => the transformed datetime (format "YYYY-MM-DD hh:mm:ss.uuu") ;
 *  => or FALSE if any error occured.
 */
class ToDateTimeMsFastTransformer
{
    /**
     * Format : strictly "YYYY-MM-DD hh:mm:ss".
     *
     * @param mixed $datetime
     */
    public static function dateTime($datetime)
    {
        return trim($datetime);
    }

    /**
     * Format : strictly "DD/MM/YYYY hh:mm:ss".
     *
     * @param mixed $datetime
     */
    public static function frenchDateTime($datetime)
    {
        $datetime = trim($datetime);
        $day = strtok($datetime, '/');
        $month = strtok('/');
        $year = strtok(' ');
        $time = strtok('');

        return "$year-$month-$day $time";
    }

    /**
     * Format : strictly "DD/MM/YY hh:mm:ss".
     *
     * @param mixed $datetime
     */
    public static function shortFrenchDateTimeMs($datetime)
    {
        $datetime = trim($datetime);
        $day = strtok($datetime, '/');
        $month = strtok('/');
        $year = strtok(' ');
        $time = strtok('');
        $year = (((int) $year > 45) ? '19' : '20') . $year;

        return "$year-$month-$day $time";
    }
}
