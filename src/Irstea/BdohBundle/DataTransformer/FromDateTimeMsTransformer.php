<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohBundle\DataTransformer;

/**
 * Transforms datetime (without validating), from DateTimeMs format ("YYYY-MM-DD hh:mm:ss.uuu") to any format
 * (see : Irstea\BdohDataBundle\Doctrine\DBAL\Types\DateTimeMs).
 *
 * Each function returns :
 *  => the transformed datetime ;
 *  => or FALSE if any error occured.
 */
class FromDateTimeMsTransformer
{
    /**
     * Return format : "DD/MM/YYYY hh:mm:ss" or "DD/MM/YYYY hh:mm:ss.uuu".
     *
     * @param mixed $dt
     */
    public static function toFrenchDateTimeMs($dt)
    {
        return substr($dt, 8, 2) . '/' .
            substr($dt, 5, 2) . '/' .
            substr($dt, 0, 4) . ' ' .
            substr($dt, 11);
    }
}
