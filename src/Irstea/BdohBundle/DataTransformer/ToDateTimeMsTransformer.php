<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohBundle\DataTransformer;

/**
 * Validates and transforms datetime, from any format to DateTimeMs format
 * (see : Irstea\BdohDataBundle\Doctrine\DBAL\Types\DateTimeMs).
 *
 * Each function returns :
 *  => the transformed datetime (format "YYYY-MM-DD hh:mm:ss.uuu") ;
 *  => or FALSE if any error occured.
 */
class ToDateTimeMsTransformer
{
    /**
     * Format : "YYYY-MM-DD" + a possible variable time.
     * See    : ToTimeMsTransformer::varTimeMs().
     *
     * @param mixed $datetime
     */
    public static function varDateTimeMs($datetime)
    {
        // Splits $datetime into its "date" and "time" parts
        $datetime = trim($datetime);
        $date = strtok($datetime, ' ');
        $time = strtok(' ');

        if (false === $date = ToDateTransformer::date($date)) {
            return false;
        }

        if (false !== $time) {
            return ($time = ToTimeMsTransformer::varTimeMs($time)) ?
                ($date . ' ' . $time) : false;
        }

        return $date . ' 00:00:00.000';
    }

    /**
     * Format : "D/MM/YYYY" or "DD/MM/YYYY" + a possible variable time
     * See    : ToTimeMsTransformer::varTimeMs().
     *
     * @param mixed $datetime
     */
    public static function varFrenchDateTimeMs($datetime)
    {
        // Splits $datetime into its "date" and "time" parts
        $datetime = trim($datetime);
        $date = strtok($datetime, ' ');
        $time = strtok(' ');

        if (false === $date = ToDateTransformer::frenchDate($date)) {
            return false;
        }

        if (false !== $time) {
            return ($time = ToTimeMsTransformer::varTimeMs($time)) ?
                ($date . ' ' . $time) : false;
        }

        return $date . ' 00:00:00.000';
    }

    /**
     * Format : "D/MM/YY" or "DD/MM/YY" + a possible variable time
     * See    : ToTimeMsTransformer::varTimeMs().
     *
     * @param mixed $datetime
     */
    public static function varShortFrenchDateTimeMs($datetime)
    {
        // Splits $datetime into its "date" and "time" parts
        $datetime = trim($datetime);
        $date = strtok($datetime, ' ');
        $time = strtok(' ');

        if (false === $date = ToDateTransformer::shortFrenchDate($date)) {
            return false;
        }

        if (false !== $time) {
            return ($time = ToTimeMsTransformer::varTimeMs($time)) ?
                ($date . ' ' . $time) : false;
        }

        return $date . ' 00:00:00.000';
    }
}
