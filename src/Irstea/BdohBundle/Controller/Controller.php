<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Irstea\BdohDataBundle\Entity\Observatoire;
use Irstea\BdohSecurityBundle\Entity\Utilisateur;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Translation\TranslatorInterface;

class Controller extends BaseController
{
    /**
     * Shortcut to return the session manager.
     *
     * @return SessionInterface
     */
    public function getSession(): SessionInterface
    {
        return $this->get('session');
    }

    /**
     *  Shortcut to return the translator.
     *
     * @return TranslatorInterface
     */
    public function getTranslator(): TranslatorInterface
    {
        return $this->get('translator');
    }

    /**
     * Shortcut to return the default Doctrine Entity Manager.
     *
     * @return EntityManagerInterface
     */
    public function getEm(): EntityManagerInterface
    {
        /* @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->getDoctrine()->getManager();
    }

    /**
     * Shortcut to return a Doctrine repository.
     *
     * @param string $entity
     *
     * @return EntityRepository
     */
    public function getRepository(string $entity): EntityRepository
    {
        return $this->getDoctrine()->getRepository($entity);
    }

    /**
     * Shortcut to return the Doctrine repository suitable for an entity class.
     *
     * @param object $entity
     *
     * @return EntityRepository
     */
    public function getClassRepo($entity): EntityRepository
    {
        return $this->getRepository(get_class($entity));
    }

    /**
     *  Shortcut to return a Doctrine repository of a BDOH entity.
     *
     * @param string $bdohEntity
     *
     * @return EntityRepository
     */
    public function getBdohRepo(string $bdohEntity): EntityRepository
    {
        return $this->getRepository('IrsteaBdohDataBundle:' . $bdohEntity);
    }

    /**
     * Shortcut to return a Doctrine repository of a "BDOH Security" entity.
     *
     * @param string $securityEntity
     *
     * @return EntityRepository
     */
    public function getSecurityRepo(string $securityEntity): EntityRepository
    {
        return $this->getRepository('IrsteaBdohSecurityBundle:' . $securityEntity);
    }

    /**
     * @param mixed $data
     * @param int   $status
     * @param array $headers
     *
     * @return Response with json encoded data
     */
    public function renderJson($data, int $status = 200, array $headers = [])
    {
        return new JsonResponse($data, $status, $headers);
    }

    /**
     * @return bool true if the request is done by an ajax query
     */
    public function isXmlHttpRequest(): bool
    {
        $request = $this->get('request_stack')->getMasterRequest();

        return $request ? $request->isXmlHttpRequest() : false;
    }

    /**
     * @return RedirectResponse to HTTP_REFERER
     */
    public function redirectToReferer(): RedirectResponse
    {
        $referer = $this->get('request_stack')->getMasterRequest()->headers->get('referer');

        return new RedirectResponse($referer);
    }

    /**
     * @return bool
     */
    protected function isLocaleEn(): bool
    {
        $locale = $this->get('session')->get('_locale');

        return \strtolower(\substr($locale, 0, 2)) === 'en';
    }

    /**
     * @return Observatoire|null
     */
    protected function getCurrentObservatoire()
    {
        return $this->get('irstea_bdoh.manager.observatoire')->getCurrent();
    }

    /**
     * @return Utilisateur|null
     */
    protected function getUtilisateur()
    {
        $token = $this->get('security.token_storage')->getToken();
        $user = $token ? $token->getUser() : null;

        return $user instanceof Utilisateur ? $user : null;
    }
}
