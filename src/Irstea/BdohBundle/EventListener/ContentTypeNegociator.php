<?php declare(strict_types=1);

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohBundle\EventListener;

use JMS\DiExtraBundle\Annotation as DI;
use Negotiation\Accept;
use Negotiation\Negotiator;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class ContentTypeNegociator.
 *
 * @DI\Service
 * @DI\Tag("kernel.event_subscriber")
 */
class ContentTypeNegociator implements EventSubscriberInterface
{
    /**
     * @var Negotiator
     */
    private $negotiator;

    /**
     * @var string[]
     */
    private $priorities;

    /**
     * ContentTypeNegociator constructor.
     *
     * @param Negotiator $negotiator
     */
    public function __construct(array $priorities = null, Negotiator $negotiator = null)
    {
        $this->negotiator = $negotiator ?? new Negotiator();
        $this->priorities = $priorities ?? ['text/html', 'application/json'];
    }

    /**
     * @param GetResponseEvent $ev
     */
    public function onRequest(GetResponseEvent $ev)
    {
        $request = $ev->getRequest();
        $accept = $request->headers->get('Accept');

        /** @var Accept|null $match */
        $match = $this->negotiator->getBest($accept, $this->priorities);
        if (!$match) {
            $ev->setResponse(new Response(null, Response::HTTP_NOT_ACCEPTABLE));
            $ev->stopPropagation();

            return;
        }

        $format = $request->getFormat($match->getNormalizedValue());
        if ($format) {
            $request->setRequestFormat($format);
        }
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [KernelEvents::REQUEST => 'onRequest'];
    }
}
