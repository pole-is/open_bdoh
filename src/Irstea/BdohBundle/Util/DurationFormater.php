<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohBundle\Util;

use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class DurationFormater.
 */
class DurationFormater
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * DurationFormater constructor.
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param int $duration
     *
     * @return string
     */
    public function format(int $duration): string
    {
        $times = [
            'jour'    => 86400,
            'heure'   => 3600,
            'minute'  => 60,
            'seconde' => 1,
        ];

        $text = '';

        foreach ($times as $label => $time) {
            if ($text !== '') {
                $text .= ' ';
            }
            if ($duration >= $time) {
                $nb = intdiv($duration, $time);
                $duration %= $time;
                $text .= $nb . ' ' . $this->translator->transChoice($label, $nb);
            }
        }

        return trim($text);
    }
}
