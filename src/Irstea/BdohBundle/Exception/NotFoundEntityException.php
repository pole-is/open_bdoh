<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohBundle\Exception;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Thrown when the searching for an entity (from certain criteria)
 * returns nothing.
 */
class NotFoundEntityException extends NotFoundHttpException implements Exception
{
    private $criteria;

    private $entityClass;

    /**
     * @param string $entityClass
     * @param array  $criteria    Keys are criteria and values are values for these criteria
     */
    public function __construct($entityClass, $criteria = [])
    {
        $this->entityClass = $entityClass;
        $this->criteria = $criteria;

        $txt = "A '%s' entity was not found";
        if ($criteria) {
            $txt .= ' from the following criteria : %s';
        }

        parent::__construct(sprintf($txt, $entityClass, $this->criteriaToString()));
    }

    public function getCriteria()
    {
        return $this->criteria;
    }

    public function getEntityClass()
    {
        return $this->entityClass;
    }

    public function criteriaToString()
    {
        $txt = '';
        foreach ($this->criteria as $key => $value) {
            $txt .= $key . $value . ', ';
        }

        return substr($txt, 0, -2);
    }
}
