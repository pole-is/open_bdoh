<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohBundle\Twig;

use Doctrine\ORM\EntityManagerInterface;
use Irstea\BdohBundle\Manager\CssManager;
use Irstea\BdohBundle\Manager\ObservatoireManagerInterface;
use Irstea\BdohBundle\Util\DurationFormater;
use Irstea\BdohDataBundle\Entity\Observatoire;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * Twig extension for Bdoh.
 */
class BdohExtension extends AbstractExtension
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var ObservatoireManagerInterface
     */
    protected $manager;

    /**
     * @var CssManager
     */
    protected $cssManager;

    /**
     * @var DurationFormater
     */
    protected $durationFormater;

    /**
     * @param EntityManagerInterface       $em
     * @param ObservatoireManagerInterface $manager
     * @param CssManager                   $cssManager
     * @param DurationFormater             $durationFormater
     */
    public function __construct(
        EntityManagerInterface $em,
        ObservatoireManagerInterface $manager,
        CssManager $cssManager,
        DurationFormater $durationFormater
    ) {
        $this->em = $em;
        $this->manager = $manager;
        $this->cssManager = $cssManager;
        $this->durationFormater = $durationFormater;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'BdohExtension';
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            'round'               => new TwigFunction('round', 'round'),
            'themeCssFile'        => new TwigFunction('themeCssFile', [$this, 'getThemeCssFile']),
            'currentObservatoire' => new TwigFunction('currentObservatoire', [$this->manager, 'getCurrent']),
            'allObservatoires'    => new TwigFunction('allObservatoires', [$this, 'getAllObservatoires']),
        ];
    }

    /**
     * @return TwigFilter[]
     */
    public function getFilters()
    {
        return [
            'durationFormat' => new TwigFilter('durationFormat', [$this->durationFormater, 'format']),
        ];
    }

    /**
     * @return Observatoire[]
     */
    public function getAllObservatoires()
    {
        try {
            return $this->em->getRepository(Observatoire::class)->findAll();
        } catch (\Exception $ex) {
            return [];
        }
    }

    /**
     * @return string
     */
    public function getThemeCssFile()
    {
        try {
            $observatoire = $this->manager->getCurrent();

            return $this->cssManager->getObservatoireCssFile($observatoire);
        } catch (\Exception $ex) {
            return '';
        }
    }
}
