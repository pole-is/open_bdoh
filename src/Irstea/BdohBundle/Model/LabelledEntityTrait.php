<?php declare(strict_types=1);

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohBundle\Model;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @UniqueEntity(fields="libelle")
 * @UniqueEntity(fields="libelleEn")
 */
trait LabelledEntityTrait
{
    /**
     * @var string
     */
    protected $libelle;

    /**
     * @var string
     */
    protected $libelleEn;

    /**
     * Set libelle.
     *
     * @param string|null $libelle
     * @param string      $lang
     *
     * @return $this
     */
    public function setLibelle(string $libelle, string $lang = 'fr')
    {
        if (\strpos($lang, 'fr') === 0) {
            $this->libelle = $libelle;
        } else {
            $this->libelleEn = $libelle;
        }

        return $this;
    }

    /**
     * Get libelle.
     *
     * @param string $lang
     *
     * @return string
     */
    public function getLibelle(string $lang = 'fr'): string
    {
        if (\strpos($lang, 'fr') === 0) {
            return $this->libelle ?? $this->libelleEn ?? '';
        }

        return $this->libelleEn ?? $this->libelle;
    }

    /**
     * Set libelleEn.
     *
     * @param string|null $libelleEn
     */
    public function setLibelleEn(string $libelleEn = null)
    {
        $this->libelleEn = $libelleEn;
    }

    /**
     * Get libelleEn.
     *
     * @return string|null
     */
    public function getLibelleEn()
    {
        return $this->libelleEn;
    }
}
