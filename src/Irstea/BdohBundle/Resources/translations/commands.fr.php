<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

$selection = [
    'selection' => [
        'empty.key' => 'Rien',
        'empty.msg' => 'Ne rien sélectionner',
        'free.key'  => 'Libre',
        'free.msg'  => 'Sélection libre',
        'stop.key'  => 'Stop',
        'stop.msg'  => 'Stopper la sélection',

        'ask.key'     => 'Quel est votre choix ?',
        'ask.element' => 'Votre sélection libre : ',

        'error.empty'          => 'Veuillez sélectionner un élément de la liste.',
        'error.invalid(%key%)' => "«\u{a0}%key%\u{a0}» est invalide ! Veuillez sélectionner un élément de la liste.",
    ],
];

/*************************************************
 * ERRORS
 ************************************************/

$errors = [
];

/*************************************************
 * VARIOUS MESSAGES
 ************************************************/

$various = [
    'file_path'   => 'Chemin du fichier',
    'file_format' => 'Format du fichier',
];

/*************************************************
 * RETURNED MESSAGES
 ************************************************/

return array_merge($selection, $errors, $various);
