<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

$selection = [
    'selection' => [
        'empty.key' => 'Nothing',
        'empty.msg' => 'Select none',
        'free.key'  => 'Free',
        'free.msg'  => 'Free selection',
        'stop.key'  => 'Stop',
        'stop.msg'  => 'Stop selection',

        'ask.key'     => 'What is your choice?',
        'ask.element' => 'Your free selection: ',

        'error.empty'          => 'Please select an item from the list.',
        'error.invalid(%key%)' => "“\u{a0}%key%\u{a0}” is invalid! Please select an item from the list.",
    ],
];

/*************************************************
 * ERRORS
 ************************************************/

$errors = [
];

/*************************************************
 * VARIOUS MESSAGES
 ************************************************/

$various = [
    'file_path'   => 'File path',
    'file_format' => 'File format',
];

/*************************************************
 * RETURNED MESSAGES
 ************************************************/

return array_merge($selection, $errors, $various);
