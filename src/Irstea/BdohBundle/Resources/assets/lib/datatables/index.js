/*
 * © 2016-2017 IRSTEA
 * Guillaume Perréal <guillaume.perreal@irstea.fr>
 * Tous droits réservés.
 */
require('./theme');
require('./row-grouping');
require('./column-filters');

module.exports = require('datatables.net');
