<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BdohPreInstallDatabaseCommand extends Command
{
    protected $conn;

    protected function configure()
    {
        $this->setName('bdoh:preinstall:database');
        $this->setDescription('Installs PostGis and schemas temp, bdoh');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->conn = $this->getContainer()->get('doctrine')->getEntityManager()->getConnection();

        $output->write(sprintf('<info>Create language : plpgsql</info>...'));
        try {
            $createLanguage = 'create language plpgsql;';
            $this->conn->exec($createLanguage);
            $output->writeln('Ok');
        } catch (\Exception $e) {
            $output->writeln('Error!');
            //$output->writeln($e->getMessage());
        }

        $output->writeln('');
        $output->writeln(sprintf('<info>Installation PostGis</info>'));

        $this->importFile(
            $output,
            [
                '/usr/share/postgresql/8.4/contrib/postgis-1.5/postgis.sql',
                '/usr/share/postgresql/8.4/contrib/postgis-1.5/spatial_ref_sys.sql',
            ]
        );

        $output->writeln('');
        $output->write(sprintf('<info>Create schema : bdoh</info>...'));
        try {
            $createSchemaBdoh = 'CREATE SCHEMA bdoh;';
            $this->conn->exec($createSchemaBdoh);
            $output->writeln('Ok');
        } catch (\Exception $e) {
            $output->writeln('Error!');
            //$output->writeln($e->getMessage());
        }

        $output->writeln('');
        $output->write(sprintf('<info>Create schema : temp</info>...'));
        try {
            $createSchemaTemp = 'CREATE SCHEMA temp;';
            $this->conn->exec($createSchemaTemp);
            $output->writeln('Ok');
        } catch (\Exception $e) {
            $output->writeln('Error!');
            //$output->writeln($e->getMessage());
        }
    }
}
