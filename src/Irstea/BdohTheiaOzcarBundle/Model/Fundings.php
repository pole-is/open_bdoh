<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohTheiaOzcarBundle\Model;

final class Fundings
{
    /** @var string */
    private $type = '';

    /** @var string */
    private $iso3166 = '';

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $idScanR;

    //------------------------------getter et setter-----------------------------------

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getIso3166(): string
    {
        return $this->iso3166;
    }

    /**
     * @param string $iso3166
     */
    public function setIso3166(string $iso3166): void
    {
        $this->iso3166 = $iso3166;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getIdScanR(): ?string
    {
        return $this->idScanR;
    }

    /**
     * @param string|null $idScanR
     */
    public function setIdScanR(?string $idScanR): void
    {
        $this->idScanR = $idScanR;
    }
}
