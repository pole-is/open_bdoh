<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohTheiaOzcarBundle\Model;

final class FormatPivot
{
    /** @var string */
    public $version = '1.1';

    /** @var Producer */
    public $producer;

    /** @var Datasets */
    public $datasets;

    public function __construct()
    {
        $this->producer = new Producer();
    }

    /**
     * @return Datasets[]|Datasets
     */
    public function getDatasets()
    {
        return $this->datasets;
    }

    /**
     * @param Datasets $datasets
     */
    public function setDatasets(Datasets $datasets): void
    {
        $this->datasets[] = $datasets;
    }

    /**
     * @return Producer
     */
    public function getProducer(): Producer
    {
        return $this->producer;
    }

    /**
     * @param Producer $producer
     */
    public function setProducer(Producer $producer): void
    {
        $this->producer = $producer;
    }
}
