<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohTheiaOzcarBundle\Model;

class PortalSearchCriteria
{
    /**
     * @var string[]
     */
    private $climates = [];

    /**
     * @var string[]
     */
    private $geologies = [];

    //--------------------getter et setter--------

    /**
     * @return string[]
     */
    public function getClimates(): array
    {
        return $this->climates;
    }

    /**
     * @param string $climate
     */
    public function addClimate(string $climate): void
    {
        $this->climates[] = $climate;
    }

    /**
     * @return string[]
     */
    public function getGeologies(): array
    {
        return $this->geologies;
    }

    /**
     * @param string $geology
     */
    public function addGeology(string $geology): void
    {
        $this->geologies[] = $geology;
    }
}
