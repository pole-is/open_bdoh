<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohTheiaOzcarBundle\Model;

final class OnlineResource
{
    /**
     * @var string
     */
    private $urlDownload;

    /**
     * @var string
     */
    private $urlInfo;

    /**
     * @var string
     */
    private $doi;

    /**
     * @return string|null
     */
    public function getUrlDownload(): ?string
    {
        return $this->urlDownload;
    }

    /**
     * @param string $urlDownload
     */
    public function setUrlDownload(string $urlDownload): void
    {
        $this->urlDownload = $urlDownload;
    }

    /**
     * @return string
     */
    public function getUrlInfo(): string
    {
        return $this->urlInfo;
    }

    /**
     * @param string $urlInfo
     */
    public function setUrlInfo(string $urlInfo): void
    {
        $this->urlInfo = $urlInfo;
    }

    /**
     * @return string|null
     */
    public function getDoi(): ?string
    {
        return $this->doi;
    }

    /**
     * @param string|null $doi
     */
    public function setDoi(?string $doi)
    {
        $this->doi = $doi;
    }
}
