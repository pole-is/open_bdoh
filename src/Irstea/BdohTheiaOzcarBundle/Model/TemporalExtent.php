<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohTheiaOzcarBundle\Model;

class TemporalExtent
{
    /**
     * @var string|null
     */
    private $dateBeg = '';

    /**
     * @var string|null
     */
    private $dateEnd = '';

    //@todo actuellement mis en string ou null mais en réalité il est obligatoire

    /**
     * @return string|null
     */
    public function getDateBeg(): ?string
    {
        return $this->dateBeg;
    }

    /**
     * @param string|null $dateBeg
     */
    public function setDateBeg(?string $dateBeg): void
    {
        $dateBeg = date('Y-m-d\TH:i:s.00', $dateBeg) . 'Z';
        $this->dateBeg = $dateBeg;
    }

    /**
     * @return string|null
     */
    public function getDateEnd(): ?string
    {
        return $this->dateEnd;
    }

    /**
     * @param string|null $dateEnd
     */
    public function setDateEnd(?string $dateEnd): void
    {
        $dateEnd = date('Y-m-d\TH:i:s.00', $dateEnd) . 'Z';
        $this->dateEnd = $dateEnd;
    }
}
