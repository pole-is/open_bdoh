<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohTheiaOzcarBundle\Model;

class SpatialExtent
{
    /**
     * @var Geometry
     */
    public $geometry;

    /**
     * @var \stdClass
     */
    public $properties;

    /**
     * @var string
     */
    public $type;

    //------------------------------ctor------------------
    public function __construct()
    {
        $this->geometry = new Geometry();
    }

    //------------------------getter et setter------------

    /**
     * @return \stdClass
     */
    public function getProperties(): \stdClass
    {
        return $this->properties;
    }

    /**
     * @param \stdClass $properties
     */
    public function setProperties(\stdClass $properties): void
    {
        $this->properties = $properties;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Feature';
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }
}
