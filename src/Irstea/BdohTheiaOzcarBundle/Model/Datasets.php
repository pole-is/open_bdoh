<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohTheiaOzcarBundle\Model;

final class Datasets
{
    /** @var string */
    public $datasetId = '';

    /** @var Metadata */
    public $metadata;

    /** @var Observations[] */
    public $observations = [];

    public function __construct()
    {
        $this->metadata = new Metadata();
    }

    /**
     * @return string
     */
    public function getDatasetId(): string
    {
        return $this->datasetId;
    }

    /**
     * @param string $datasetId
     */
    public function setDatasetId(string $datasetId): void
    {
        $this->datasetId = $datasetId;
    }

    /**
     * @return Observations|Observations[]
     */
    public function getObservations()
    {
        return $this->observations;
    }

    /**
     * @param Observations|null $observation
     */
    public function addObservations(?Observations $observation): void
    {
        $this->observations[] = $observation;
    }
}
