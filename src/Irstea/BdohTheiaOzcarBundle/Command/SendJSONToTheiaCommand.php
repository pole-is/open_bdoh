<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohTheiaOzcarBundle\Command;

use Irstea\BdohDataBundle\Command\AbstractJobCommand;
use Irstea\BdohTheiaOzcarBundle\Service\EnvoiJSON;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class SendJSONToTheiaCommand extends AbstractJobCommand
{
    const COMMAND_NAME = 'theia:send:json:data';

    /** @var EnvoiJSON */
    private $envoieJSON;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)
        ->setDescription('Commande pour envoyer les données à Theia/OZCAR par Observatoire')
        ->addArgument('codeTheia', InputArgument::REQUIRED, 'Theia/OZCAR Identifier (4 letters)')
        ->addOption('jms-job-id', null, InputOption::VALUE_REQUIRED, 'Identifiant du job');

        // NOOP
    }

    /**
     * SendJSONToTheiaCommand constructor.
     */
    public function __construct(
        EnvoiJSON $envoieJSON
    ) {
        parent::__construct('theia:send:json:data');
        $this->envoieJSON = $envoieJSON;
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '==================================================================================',
            'Welcome you can send your datas to Theia/OZCAR with your Theia/OZCAR Identifier ',
            '===================================================================================',
        ]);
        $questions = [];
        $argumentUser = ['codeTheia'];

        foreach ($argumentUser as $argumentUtilisateur) {
            if (!$input->getArgument($argumentUtilisateur)) {
                $question = new Question('Please enter the ' . $argumentUtilisateur . ':');
                $question->setValidator(function ($firstname) {
                    if (empty($firstname)) {
                        throw new \Exception('Argument can not be empty');
                    }

                    return $firstname;
                });

                $questions[$argumentUtilisateur] = $question;
            }
        }
        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     *
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $theiaCode = $input->getArgument('codeTheia');

        $envoi = $this->envoieJSON->sendJsonTheia($theiaCode);
        [$unix, $key, $message, $responseBody, $result, $seri] = $envoi;

        // message d'erreur à faire apparaitre dans les résumés des jobs
        if ($unix === 0) {
            $output->writeln('Envoi réussi');
        } elseif ($key !== null) {
            //si le validateur json ne fonctionne pas
            $output->writeln([
                '==================================== ERROR JSON SCHEMA==================================================== ',
            ]);
            foreach ($result->getErrors() as $erreurs) {
                $key = $erreurs->keyword();
                $dataPoitner = $erreurs->dataPointer();
                $dataPointerString = implode('.', $dataPoitner);
                $dataErr = $erreurs->data();
                $dataString = json_encode($dataErr);
                $output->writeln([
                        'Keywords : ' . $key,
                        'DataPointer : ' . $dataPointerString,
                        'Data : ' . $dataString,
                    ]);
            }
            $output->writeln([
                '==================================== JSON ==================================================== ',
                $seri,
            ]);
        } elseif ($message !== null) {
            //si l'envoie ne fonctionne pas
            $output->writeln([
                '==================================== GuzzleException ==================================================== ',
                'Message : ' . $message,
                'Body response : ' . $responseBody,
                '==================================== JSON ==================================================== ',
                $seri,
            ]);
        }

        return $unix;
    }
}
