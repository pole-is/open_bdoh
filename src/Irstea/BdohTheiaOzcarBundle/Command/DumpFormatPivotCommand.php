<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohTheiaOzcarBundle\Command;

use Doctrine\ORM\EntityRepository;
use Irstea\BdohDataBundle\Entity\Observatoire;
use Irstea\BdohTheiaOzcarBundle\Mapper\MapperInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\SerializerInterface;

class DumpFormatPivotCommand extends Command
{
    /** @var RegistryInterface */
    private $registry;

    /** @var SerializerInterface */
    private $serializer;

    /** @var MapperInterface */
    private $mapper;

    public function __construct(
        RegistryInterface $registry,
        SerializerInterface $serializer,
        MapperInterface $mapper
    ) {
        parent::__construct('theia:dump:pivot');

        $this->registry = $registry;
        $this->serializer = $serializer;
        $this->mapper = $mapper;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('theia:dump:pivot');
        // NOOP
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //format pivot
        /** @var EntityRepository $observatoireRepository */
        $observatoireRepository = $this->registry->getRepository(Observatoire::class);

        $observatoires = $observatoireRepository->findAll();
        $output->writeln('Dumping <info>' . count($observatoires) . '</info> format(s) pivot(s):');

        foreach ($observatoires as $observatoire) {
            $dto = $this->mapper->mapFormatPivotInterface($observatoire);
            $json = $this->serializer->serialize($dto, 'json');
            $output->writeln($json);
        }

        return 0;
    }
}
