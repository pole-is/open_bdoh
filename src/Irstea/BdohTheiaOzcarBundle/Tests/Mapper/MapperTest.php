<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohTheiaOzcarBundle\Tests\Mapper;

use Irstea\BdohDataBundle\Entity\ChroniqueDiscontinue;
use Irstea\BdohDataBundle\Entity\DataConstraint;
use Irstea\BdohDataBundle\Entity\DataSet;
use Irstea\BdohDataBundle\Entity\Observatoire;
use Irstea\BdohDataBundle\Entity\Partenaire;
use Irstea\BdohDataBundle\Entity\PersonneTheia;
use Irstea\BdohDataBundle\Entity\Unite;
use Irstea\BdohTheiaOzcarBundle\Mapper\Mapper;
use PHPUnit\Framework\TestCase;

/**
 * @group unit
 */
class MapperTest extends TestCase
{
    public function testMapMetadata(): void
    {
        // Arrange
       /* $mapper = new Mapper();
        $dataSetEntity = new DataSet();
        $dataSetEntity->setId('13');
        $dataSetEntity->setTitre('foo');
        $dataSetEntity->setDataConstraints(new DataConstraint());
        $dataSetEntity->setDescription('popo');
        $dataSetEntity->setGenealogie('truc');
        $dataSetEntity->setThemeInpire('Environnemental monitoring');

        // Act
        $dataSetDTO = $mapper->map($dataSetEntity);

        // Assert
        //@todo quand l'uuid sera reglé
        self::assertEquals('_DAT_13', $dataSetDTO->getDatasetId());
        self::assertEquals('foo', $dataSetDTO->metadata->getTitle());
        self::assertEquals('popo', $dataSetDTO->metadata->getDescription());
        self::assertEquals('', $dataSetDTO->metadata->getDataConstraint()->getAccessUseConstraint());
        self::assertEquals('truc', $dataSetDTO->metadata->getDatasetLineage());
        self::assertEquals('Environnemental monitoring', $dataSetDTO->metadata->getInspireTheme());**/
    }

    public function testMapProducer(): void
    {
        //Arrange
        /*
        $mapper = new Mapper($this->registry);
        $observatorieEntity = new Observatoire();

        $personneTheiaEntity = new PersonneTheia();
        $personneTheiaEntity->setNom('TESS');
        $personneTheiaEntity->setPrenom('Adri');
        $personneTheiaEntity->setPartenaires(new Partenaire());
        $personneTheiaEntity->setEmail('adrien.tessanne@outlook.fr');

        $partenaireEntity = $personneTheiaEntity->getPartenaires();
        $partenaireEntity->setNom('Enterprise');
        $partenaireEntity->setIso('FR');
        $partenaireEntity->setScanR('200017466P');

        $observatorieEntity->setTheiaCode('_OBS_15');
        $observatorieEntity->setTitre('Pommerainette');
        $observatorieEntity->setNom('Babar');
        $observatorieEntity->setDescriptionEn('description');
        $observatorieEntity->setEmail('adrien.tessae@outlook.fr');
        $observatorieEntity->addDataManager($personneTheiaEntity);
        $observatorieEntity->setProjectLeader($personneTheiaEntity);

        //Act
        $producerDTO = $mapper->mapProducerInterface($observatorieEntity);
        $contactDTO = $mapper->mapContactInterface($personneTheiaEntity);
        $organisationDTO = $mapper->mapOrganisationInterface($partenaireEntity);

        //Assert
        self::assertEquals('_OBS_15', $producerDTO->getProducerId());
        self::assertEquals('Pommerainette', $producerDTO->getTitle());
        self::assertEquals('Babar', $producerDTO->getName());
        self::assertEquals('description', $producerDTO->getDescription());
        self::assertEquals('adrien.tessae@outlook.fr', $producerDTO->getEmail());

        self::assertEquals('TESS', $contactDTO->getLastName());
        self::assertEquals('Adri', $contactDTO->getFirstName());
        self::assertEquals('adrien.tessanne@outlook.fr', $contactDTO->getEmail());

        self::assertEquals('Enterprise', $organisationDTO->getName());
        self::assertEquals('FR', $organisationDTO->getIso3166());
        self::assertEquals('200017466P', $organisationDTO->getIdScanR());
        self::assertEquals('Research group', $organisationDTO->getRole());*/
    }

    //@todo faire maperTest de l'observation
   /* public function testMapObservation(): void{

        //Arrange
        $mapper = new Mapper();
        $observationEntity = new ChroniqueDiscontinue();

        $observationEntity->setDateDebutMesures('2021-02-22T14:41:10.00Z');
        $observationEntity->setDateFinMesures('2021-02-22T14:41:10.00Z');
        $observationEntity->setLibelle('papa');
        $observationEntity->setUnite( new Unite());

        //Act
        $observationDTO = $mapper->mapObs($observationEntity);

        //Assert
        self::assertEquals('2021-02-22T14:41:10.00Z', $observationDTO->getTemporalExtends()->getDateBeg());
        self::assertEquals('2021-02-22T14:41:10.00Z', $observationDTO->getTemporalExtends()->getDateEnd());
        self::assertEquals('papa', $observationDTO->getObservedProperty()->getName());
        self::assertEquals('', $observationDTO->getObservedProperty()->getUnit());



    }**/
}
