<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohTheiaOzcarBundle\Tests\Service;

use Irstea\BdohTheiaOzcarBundle\Exception\PasswordException;
use Irstea\BdohTheiaOzcarBundle\Service\PasswordEncryptionService;
use PHPUnit\Framework\TestCase;

class PasswordEncryptionServiceTest extends TestCase
{
    public function testInvalidSecretKey(): void
    {
        $this->expectException(PasswordException::class);
        $dummy = new PasswordEncryptionService('foo');

        $this->assertNull($dummy);
    }

    public function testShortSecretKey(): void
    {
        $this->expectException(PasswordException::class);
        $dummy = new PasswordEncryptionService('deadbeef');

        $this->assertNull($dummy);
    }

    public function testCreate(): void
    {
        $secretKey = bin2hex(random_bytes(SODIUM_CRYPTO_SECRETBOX_KEYBYTES));

        $service = new PasswordEncryptionService($secretKey);

        $this->assertInstanceOf(PasswordEncryptionService::class, $service);
    }

    public function testEncrypt(): void
    {
        $secretKey = bin2hex(random_bytes(SODIUM_CRYPTO_SECRETBOX_KEYBYTES));

        $service = new PasswordEncryptionService($secretKey);

        $clear = 'my password !';
        $crypted1 = $service->encrypt($clear);
        $crypted2 = $service->encrypt($clear);

        $this->assertNotEquals($clear, $crypted1);
        $this->assertNotEquals($crypted2, $crypted1);
    }

    public function testSymmetry(): void
    {
        $secretKey = bin2hex(random_bytes(SODIUM_CRYPTO_SECRETBOX_KEYBYTES));

        $service = new PasswordEncryptionService($secretKey);

        $clear = 'my password !';
        $crypted = $service->encrypt($clear);
        $decrypted = $service->decrypt($crypted);

        $this->assertEquals($clear, $decrypted);
    }
}
