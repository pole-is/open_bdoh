<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohTheiaOzcarBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Irstea\BdohDataBundle\Entity\Observatoire;
use Irstea\BdohDataBundle\Entity\Repository\ObservatoireRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Routing\RouterInterface;

class EnvoiJSON
{
    /** @var string */
    private $uriTheiaSend;

    /** @var PasswordEncryptionService */
    private $passwordEncryptionService;

    /** @var RouterInterface */
    private $router;

    /** @var SerializerJSON */
    private $serializerJson;

    /** @var SchemaJSON */
    private $schemaJSON;

    /** @var ZipJSON */
    private $zipJSON;

    /**
     * @var RegistryInterface
     * @DI\Inject
     */
    public $doctrine;

    /**
     * SendJSONToTheiaCommand constructor.
     *
     * @param string $uriTheiaSend
     */
    public function __construct(
        string $uriTheiaSend,
        PasswordEncryptionService $passwordEncryptionService,
        RouterInterface $router,
        SerializerJSON $serializerJson,
        SchemaJSON $schemaJSON,
        ZipJSON $zipJSON,
        RegistryInterface $doctrine
    ) {
        $this->uriTheiaSend = $uriTheiaSend;
        $this->passwordEncryptionService = $passwordEncryptionService;
        $this->router = $router;
        $this->serializerJson = $serializerJson;
        $this->schemaJSON = $schemaJSON;
        $this->zipJSON = $zipJSON;
        $this->doctrine = $doctrine;
    }

    public function sendJsonTheia(string $theiaCode)
    {
        $envoi = $this->doctrine
            ->getManagerForClass(Observatoire::class)
            ->transactional(
                function (EntityManagerInterface $manager) use ($theiaCode) {
                    //pour les messages d'erreurs
                    $key = null;
                    $message = null;
                    $responseBody = null;
                    $nameZip = 'BDOH.zip';

                    /** @var ObservatoireRepository $observatoireRepo */
                    $observatoireRepo = $manager->getRepository(Observatoire::class);

                    //récupérer l'bservatoire via son codeTheia
                    $observatoire = $observatoireRepo->findOneByTheiaCode($theiaCode);
                    $username = $observatoire->getTheiaCode();
                    $password = $observatoire->getTheiaPassword();

                    //décrypter le mot de passe
                    $passwordDecrypt = $this->passwordEncryptionService->decrypt($password);

                    //ajout du _observatoire pour les routes
                    $slug = $observatoire->getSlug();
                    $url = $this->router->getContext()->setParameter('_observatoire', $slug);

                    //serialization du json avec le mapper theia
                    $seri = $this->serializerJson->serialize($observatoire);
                    $data = json_decode($seri);

                    //vérification du schema de valisation du json
                    $result = $this->schemaJSON->validateSchemaTheia($data);

                    if ($result->isValid()) {
                        // Compression dans un zip du fichier json
                        $filename = tempnam('/tmp', 'theia');

                        $this->zipJSON->zipArchive($seri, $filename, $username);

                        $content = file_get_contents($filename);
                        $uriTheia = $this->uriTheiaSend . $username . '/new/' . $nameZip;
                        //Envoie du zip à Theia
                        $client = new Client();

                        try {
                            $response = $client->request(
                                'PUT',
                                $uriTheia,
                                [
                                    'headers' => [
                                        'Content-Type' => 'application/zip',
                                    ],
                                    'body'         => $content,
                                    'auth'         => [$username, $passwordDecrypt],
                                ]
                            );
                            $responseBody = $response->getBody()->getContents();
                            $unix = 0;
                        } catch (GuzzleException $e) {
                            // si l'envoie ne fonctionne pas
                            $message = $e->getMessage();
                            $response = $e->getResponse();
                            $responseBody = $response->getBody()->getContents();
                            // passage de la mise à jour en false
                            $observatoire->setMiseajourAutomatique(false);

                            $unix = 1;
                        }
                    } else {
                        // si le json n'est pas valid affichage des erreurs dans la console

                        foreach ($result->getErrors() as $erreurs) {
                            $key = $erreurs->keyword();
                        }
                        // passage de la mise à jour en false

                        $observatoire->setMiseajourAutomatique(false);

                        $unix = 1;
                    }//end result is valid

                    return [$unix, $key, $message, $responseBody, $result, $seri];
                }
            );

        return $envoi;
    }
}
