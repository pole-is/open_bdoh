<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohTheiaOzcarBundle\Form;

use Irstea\BdohTheiaOzcarBundle\Service\PasswordEncryptionServiceInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class PasswordEventSubscriber implements EventSubscriberInterface
{
    /** @var PasswordEncryptionServiceInterface */
    private $passwordEncryptionService;

    public function __construct(PasswordEncryptionServiceInterface $passwordEncryptionService)
    {
        $this->passwordEncryptionService = $passwordEncryptionService;
    }

    public function onPreSubmit(FormEvent $event): void
    {
        $clearPassword = trim($event->getData());
        if ($clearPassword) {
            // Encrypte le mot de passe
            $event->setData($this->passwordEncryptionService->encrypt($clearPassword));
        } else {
            // Ignore un champ vide (=n'efface pas le mot de passe existant)
            $form = $event->getForm();
            if ($form->getParent()) {
                $form->getParent()->remove($form->getName());
            }
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [FormEvents::PRE_SUBMIT => 'onPreSubmit'];
    }
}
