<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohConsultBundle\Service\JsonSchema;

use Doctrine\Common\Cache\ApcuCache;
use Doctrine\Common\Cache\ArrayCache;
use Opis\JsonSchema\ISchemaLoader;

final class CachingLoader implements ISchemaLoader
{
    /** @var ISchemaLoader */
    private $decorated;

    /** @var int */
    private $lifeTime;

    /** @var ApcuCache */
    private $apcuCache;

    public function __construct(ISchemaLoader $decorated, ApcuCache $apcuCache = null, int $lifeTime = 0)
    {
        $this->decorated = $decorated;
        $this->apcuCache = $apcuCache ?: new ArrayCache();
        $this->lifeTime = $lifeTime;
        // $this->apcuCache = $apcuCache;
    }

    /**
     * {@inheritdoc}
     */
    public function loadSchema(string $uri)
    {
        if ($this->apcuCache->contains($uri)) {
            return $this->apcuCache->fetch($uri);
        }

        $schema = $this->decorated->loadSchema($uri);
        if ($schema !== null) {
            $this->apcuCache->save($uri, $schema, $this->lifeTime);
        }

        return $schema;
    }
}
