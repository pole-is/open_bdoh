<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\BdohConsultBundle\Service\JsonSchema;

use Irstea\BdohConsultBundle\Exception\InvalidJsonSchemaURIException;
use Opis\JsonSchema\ISchemaLoader;
use Opis\JsonSchema\Schema;

final class HttpLoader implements ISchemaLoader
{
    /**
     * {@inheritdoc}
     */
    public function loadSchema(string $uri)
    {
        $scheme = parse_url($uri, PHP_URL_SCHEME);
        if ($scheme !== 'http' && $scheme !== 'https') {
            throw new InvalidJsonSchemaURIException('Expected a http(s) URL, not ' . $scheme);
        }

        $content = file_get_contents($uri);
        if ($content === false) {
            throw new \RuntimeException('could not fetch ' . $uri);
        }

        return Schema::fromJsonString($content);
    }
}
