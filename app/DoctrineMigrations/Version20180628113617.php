<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180628113617 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE transformation DROP CONSTRAINT FK_3FD6625DAF7BD910');
        $this->addSql('ALTER TABLE transformation DROP CONSTRAINT FK_3FD6625DCC72D953');
        $this->addSql('ALTER TABLE transformation ADD CONSTRAINT FK_3FD6625DAF7BD910 FOREIGN KEY (entree_id) REFERENCES Chronique (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE transformation ADD CONSTRAINT FK_3FD6625DCC72D953 FOREIGN KEY (sortie_id) REFERENCES Chronique (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE Transformation DROP CONSTRAINT fk_3fd6625daf7bd910');
        $this->addSql('ALTER TABLE Transformation DROP CONSTRAINT fk_3fd6625dcc72d953');
        $this->addSql('ALTER TABLE Transformation ADD CONSTRAINT fk_3fd6625daf7bd910 FOREIGN KEY (entree_id) REFERENCES chronique (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Transformation ADD CONSTRAINT fk_3fd6625dcc72d953 FOREIGN KEY (sortie_id) REFERENCES chronique (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
