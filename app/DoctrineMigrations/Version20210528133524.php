<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210528133524 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
        $this->addSql("INSERT INTO
    objectifrecherche (id , libelle, libelleen)
VALUES
       (nextval('objectifrecherche_id_seq'), 'Agrosystème', 'Agrosystem'),
       (nextval('objectifrecherche_id_seq'), 'Aménagement du territoire', 'Land use planning'),
       (nextval('objectifrecherche_id_seq'), 'Biodiversité', 'Biodiversity'),
       (nextval('objectifrecherche_id_seq'), 'Biogéochimie', 'Biogeochemistry'),
       (nextval('objectifrecherche_id_seq'), 'Changement climatique', 'Climate change' ),
       (nextval('objectifrecherche_id_seq'), 'Écologie', 'Ecology'),
       (nextval('objectifrecherche_id_seq'),'Écologie du paysage', 'Landscape ecology' ),
       (nextval('objectifrecherche_id_seq'),'Géochimie', 'Geochemistry' ),
       (nextval('objectifrecherche_id_seq'),'Géologie', 'Geology' ),
       (nextval('objectifrecherche_id_seq'),'Géophysique', 'Geophysics' ),
       (nextval('objectifrecherche_id_seq'),'Gestion de l''eau', 'Water resource management' ),
       (nextval('objectifrecherche_id_seq'),'Hydraulique', 'Hydraulics'  ),
       (nextval('objectifrecherche_id_seq'),'Hydrogéologie', 'Hydrogeology' ),
       (nextval('objectifrecherche_id_seq'),'Hydrologie', 'Hydrology'  ),
       (nextval('objectifrecherche_id_seq'),'Météorologie', 'Meteorology'   ),
       (nextval('objectifrecherche_id_seq'),'Microbiologie', 'Microbiology' ),
       (nextval('objectifrecherche_id_seq'), 'Micropolluants', 'Micropollutants' ),
       (nextval('objectifrecherche_id_seq'), 'Pédologie', 'Pedology' ),
       (nextval('objectifrecherche_id_seq'), 'Sciences humaines', 'Humanities' ),
       (nextval('objectifrecherche_id_seq'), 'Télédétection', 'Remote sensing' )

ON CONFLICT (libelle)
DO UPDATE SET libelleen = EXCLUDED.libelleen;
");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('DELETE FROM objectifrecherche');
    }
}
