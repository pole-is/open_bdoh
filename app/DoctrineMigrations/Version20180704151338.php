<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180704151338 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE IF EXISTS periodevalidite DROP CONSTRAINT fk_e3e6450b5a438e76');
        $this->addSql('ALTER TABLE IF EXISTS chroniques_pas DROP CONSTRAINT fk_bd0c31ffe38e1cf');
        $this->addSql('DROP SEQUENCE IF EXISTS bdoh.lignecalcul_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE IF EXISTS bdoh.pastemps_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE IF EXISTS bdoh.periodevalidite_id_seq CASCADE');
        $this->addSql('DROP TABLE IF EXISTS chroniques_pas');
        $this->addSql('DROP TABLE IF EXISTS lignecalcul');
        $this->addSql('DROP TABLE IF EXISTS pastemps');
        $this->addSql('DROP TABLE IF EXISTS periodevalidite');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE bdoh.lignecalcul_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE bdoh.pastemps_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE bdoh.periodevalidite_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chroniques_pas (chronique_id INT NOT NULL, pas_id INT NOT NULL, PRIMARY KEY(chronique_id, pas_id))');
        $this->addSql('CREATE INDEX idx_bd0c31ffe38e1cf ON chroniques_pas (pas_id)');
        $this->addSql('CREATE INDEX idx_bd0c31f93054d ON chroniques_pas (chronique_id)');
        $this->addSql('CREATE TABLE lignecalcul (id INT NOT NULL, entree_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_be7993e5af7bd910 ON lignecalcul (entree_id)');
        $this->addSql('CREATE TABLE pastemps (id INT NOT NULL, nom VARCHAR(255) NOT NULL, nomen VARCHAR(255) NOT NULL, nombresecondes INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX uniq_f7247ba8e46e7608 ON pastemps (nomen)');
        $this->addSql('CREATE UNIQUE INDEX uniq_f7247ba86c6e55b5 ON pastemps (nom)');
        $this->addSql('CREATE TABLE periodevalidite (id INT NOT NULL, ligne_id INT DEFAULT NULL, bareme_id INT DEFAULT NULL, debut TIMESTAMP(3) WITHOUT TIME ZONE NOT NULL, fin TIMESTAMP(3) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_e3e6450b5f49eaad ON periodevalidite (bareme_id)');
        $this->addSql('CREATE INDEX idx_e3e6450b5a438e76 ON periodevalidite (ligne_id)');
        $this->addSql('COMMENT ON COLUMN periodevalidite.debut IS \'(DC2Type:datetimems)\'');
        $this->addSql('COMMENT ON COLUMN periodevalidite.fin IS \'(DC2Type:datetimems)\'');
        $this->addSql('ALTER TABLE chroniques_pas ADD CONSTRAINT fk_bd0c31f93054d FOREIGN KEY (chronique_id) REFERENCES chronique (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chroniques_pas ADD CONSTRAINT fk_bd0c31ffe38e1cf FOREIGN KEY (pas_id) REFERENCES pastemps (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lignecalcul ADD CONSTRAINT fk_be7993e5af7bd910 FOREIGN KEY (entree_id) REFERENCES chronique (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE periodevalidite ADD CONSTRAINT fk_e3e6450b5a438e76 FOREIGN KEY (ligne_id) REFERENCES lignecalcul (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE periodevalidite ADD CONSTRAINT fk_e3e6450b5f49eaad FOREIGN KEY (bareme_id) REFERENCES bareme (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
