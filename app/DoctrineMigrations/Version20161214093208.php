<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Schema\Schema;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 * @SuppressWarnings(PHPMD.ShortMethodName)
 */
class Version20161214093208 extends AbstractScriptMigration
{
    private static $scripts = [
        'chronique_calcul_dates_mesures.sql',
        'compute_chronique.sql',
        'copyFromCsv.sql',
        'insert_mesures_one_transformation.sql',
        'insert_mesures_two_transformations.sql',
        'interp_between_dates.sql',
        'interp_for_bareme.sql',
        'purge_mesures_child_chronique.sql',
        'transformed_mesures.sql',
        'trunc_timestamp_function_create.sql',
        'bdoh_subsampled_instantaneous.sql',
        'bdoh_subsampled_mean.sql',
        'bdoh_cross_quality_orders.sql',
        'bdoh_integrate_instant_interval.sql',
        'bdoh_integrate_mean_interval.sql',
        'bdoh_interp_cumul_to_cumul.sql',
        'bdoh_interp_instant_to_instant.sql',
        'bdoh_interp_instant_to_mean_or_cumul.sql',
        'bdoh_interp_to_mean_or_cumul.sql',
        'bdoh_jeu_qualite_id.sql',
        'bdoh_select_values_for_time_step.sql',
        'bdoh_interp_to_cumulative.sql',
    ];

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        foreach (self::$scripts as $script) {
            $this->addStatements($this->readFile($script));
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        foreach (self::$scripts as $script) {
            $this->removeFunctions($this->readFile($script));
        }
    }
}
