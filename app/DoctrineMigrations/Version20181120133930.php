<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181120133930 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE bdoh.pointjaugeage_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE Conversion_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE JeuConversion_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE Conversion (id INT NOT NULL, entree_id INT DEFAULT NULL, sortie_id INT DEFAULT NULL, jeuConversionActuel_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F2CC24949F6D100F ON Conversion (jeuConversionActuel_id)');
        $this->addSql('CREATE INDEX IDX_F2CC2494AF7BD910 ON Conversion (entree_id)');
        $this->addSql('CREATE INDEX IDX_F2CC2494CC72D953 ON Conversion (sortie_id)');
        $this->addSql('CREATE UNIQUE INDEX unique_conversion ON Conversion (entree_id, sortie_id)');
        $this->addSql('CREATE TABLE JeuConversion (id INT NOT NULL, conversion_id INT DEFAULT NULL, dateCreation TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, paramConversion INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A66418444C1FF126 ON JeuConversion (conversion_id)');
        $this->addSql('ALTER TABLE Conversion ADD CONSTRAINT FK_F2CC24949F6D100F FOREIGN KEY (jeuConversionActuel_id) REFERENCES JeuConversion (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Conversion ADD CONSTRAINT FK_F2CC2494AF7BD910 FOREIGN KEY (entree_id) REFERENCES Chronique (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Conversion ADD CONSTRAINT FK_F2CC2494CC72D953 FOREIGN KEY (sortie_id) REFERENCES Chronique (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE JeuConversion ADD CONSTRAINT FK_A66418444C1FF126 FOREIGN KEY (conversion_id) REFERENCES Conversion (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE pointjaugeage');
        $this->addSql('ALTER TABLE chronique ADD conversion_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chronique ADD CONSTRAINT FK_DAC287494C1FF126 FOREIGN KEY (conversion_id) REFERENCES Conversion (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_DAC287494C1FF126 ON chronique (conversion_id)');
        $this->addSql('ALTER TABLE historique ADD chroniqueconvertie_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE historique ADD chroniquemere_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE historique ADD paramConversion INT DEFAULT NULL');
        $this->addSql('ALTER TABLE historique ADD CONSTRAINT FK_A2E2D63C408C274E FOREIGN KEY (chroniqueconvertie_id) REFERENCES Chronique (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE historique ADD CONSTRAINT FK_A2E2D63C26FB49FF FOREIGN KEY (chroniquemere_id) REFERENCES Chronique (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_A2E2D63C408C274E ON historique (chroniqueconvertie_id)');
        $this->addSql('CREATE INDEX IDX_A2E2D63C26FB49FF ON historique (chroniquemere_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE Chronique DROP CONSTRAINT FK_DAC287494C1FF126');
        $this->addSql('ALTER TABLE JeuConversion DROP CONSTRAINT FK_A66418444C1FF126');
        $this->addSql('ALTER TABLE Conversion DROP CONSTRAINT FK_F2CC24949F6D100F');
        $this->addSql('DROP SEQUENCE Conversion_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE JeuConversion_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE bdoh.pointjaugeage_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE pointjaugeage (id INT NOT NULL, bareme_id INT DEFAULT NULL, date TIMESTAMP(3) WITHOUT TIME ZONE NOT NULL, valeurparametreentree DOUBLE PRECISION NOT NULL, valeurparametresortie DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_a228ece75f49eaad ON pointjaugeage (bareme_id)');
        $this->addSql('COMMENT ON COLUMN pointjaugeage.date IS \'(DC2Type:datetimems)\'');
        $this->addSql('ALTER TABLE pointjaugeage ADD CONSTRAINT fk_a228ece75f49eaad FOREIGN KEY (bareme_id) REFERENCES bareme (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE Conversion');
        $this->addSql('DROP TABLE JeuConversion');
        $this->addSql('DROP INDEX IDX_DAC287494C1FF126');
        $this->addSql('ALTER TABLE Chronique DROP conversion_id');
        $this->addSql('ALTER TABLE Historique DROP CONSTRAINT FK_A2E2D63C408C274E');
        $this->addSql('ALTER TABLE Historique DROP CONSTRAINT FK_A2E2D63C26FB49FF');
        $this->addSql('DROP INDEX IDX_A2E2D63C408C274E');
        $this->addSql('DROP INDEX IDX_A2E2D63C26FB49FF');
        $this->addSql('ALTER TABLE Historique DROP chroniqueconvertie_id');
        $this->addSql('ALTER TABLE Historique DROP chroniquemere_id');
        $this->addSql('ALTER TABLE Historique DROP paramConversion');
    }
}
