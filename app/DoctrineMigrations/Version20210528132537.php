<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210528132537 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
        $this->addSql("INSERT INTO
    categorie (id, libelle, libelleen)
VALUES
       (nextval('categorie_id_seq'), 'Administration', 'Administration'),
       (nextval('categorie_id_seq'),'Autre', 'Other'),
       (nextval('categorie_id_seq'),'Bureau d''études', 'Engineering consultant'),
       (nextval('categorie_id_seq'),'Chercheur', 'Researcher'),
       (nextval('categorie_id_seq'),'Collectivité', 'Local authority'),
       (nextval('categorie_id_seq'),'Entreprise', 'Company'),
       (nextval('categorie_id_seq'),'Particulier', ' Private individual' )
ON CONFLICT (libelle)
DO UPDATE SET libelleen = EXCLUDED.libelleen;
");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('DELETE FROM categorie');
    }
}
