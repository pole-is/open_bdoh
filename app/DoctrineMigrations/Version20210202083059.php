<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210202083059 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE chronique_dataSet (dataset_id INT NOT NULL, chronique_id INT NOT NULL, PRIMARY KEY(dataset_id, chronique_id))');
        $this->addSql('CREATE INDEX IDX_3100014AD47C2D1B ON chronique_dataSet (dataset_id)');
        $this->addSql('CREATE INDEX IDX_3100014A93054D ON chronique_dataSet (chronique_id)');
        $this->addSql('ALTER TABLE chronique_dataSet ADD CONSTRAINT FK_3100014AD47C2D1B FOREIGN KEY (dataset_id) REFERENCES DataSet (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chronique_dataSet ADD CONSTRAINT FK_3100014A93054D FOREIGN KEY (chronique_id) REFERENCES Chronique (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE partenaire_observatoire DROP CONSTRAINT FK_ACB4456E98DE13AC');
        $this->addSql('ALTER TABLE partenaire_observatoire ADD CONSTRAINT FK_ACB4456E98DE13AC FOREIGN KEY (partenaire_id) REFERENCES Partenaire (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dataset DROP CONSTRAINT fk_40503eacef1c130d');
        $this->addSql('DROP INDEX idx_40503eacef1c130d');
        $this->addSql('ALTER TABLE dataset DROP chroniques_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE chronique_dataSet');
        $this->addSql('ALTER TABLE DataSet ADD chroniques_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE DataSet ADD CONSTRAINT fk_40503eacef1c130d FOREIGN KEY (chroniques_id) REFERENCES chronique (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_40503eacef1c130d ON DataSet (chroniques_id)');
        $this->addSql('ALTER TABLE partenaire_observatoire DROP CONSTRAINT fk_acb4456e98de13ac');
        $this->addSql('ALTER TABLE partenaire_observatoire ADD CONSTRAINT fk_acb4456e98de13ac FOREIGN KEY (partenaire_id) REFERENCES partenaire (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
