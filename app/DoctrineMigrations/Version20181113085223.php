<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181113085223 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql(
            <<<'SQL'
DROP FUNCTION IF EXISTS bdoh_export_interpolate_linear(integer, timestamp without time zone, timestamp without time zone, interval);
SQL
        );
        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_export_interpolate_linear(
	IN chronique_id integer,
	IN starting_date timestamp without time zone,
	IN ending_date timestamp without time zone,
	IN timestep interval
) RETURNS TABLE(
	start_date timestamp without time zone,
	end_date timestamp without time zone,
	start_valeur double precision,
	end_valeur double precision,
	start_qualite_id integer,
	end_qualite_id integer,
	step timestamp without time zone
) AS
$BODY$
	DECLARE
		cursorSteps CURSOR FOR
            SELECT * FROM generate_series(starting_date, ending_date, timestep) AS s(s) ORDER BY s.s ASC;
		cursorMesures CURSOR FOR
            SELECT * FROM bdoh_segments(chronique_id, starting_date, ending_date) AS s ORDER BY s.start_date ASC;
		dateStep TIMESTAMP;
		dateDebut TIMESTAMP;
		dateFin TIMESTAMP;
		valeurDebut DOUBLE PRECISION;
		valeurFin DOUBLE PRECISION;
		qualiteDebut INTEGER;
		qualiteFin INTEGER;
	BEGIN
		OPEN cursorSteps;
		OPEN cursorMesures;
		FETCH cursorSteps INTO dateStep;
		FETCH cursorMesures INTO dateDebut, dateFin, valeurDebut, valeurFin, qualiteDebut, qualiteFin;
		WHILE dateStep IS NOT NULL LOOP
			IF dateStep >= dateFin THEN
				FETCH cursorMesures INTO dateDebut, dateFin, valeurDebut, valeurFin, qualiteDebut, qualiteFin;
			ELSE
				RETURN QUERY SELECT dateDebut, dateFin, valeurDebut, valeurFin, qualiteDebut, qualiteFin, dateStep;
				FETCH cursorSteps INTO dateStep;
			END IF;
		END LOOP;
		CLOSE cursorSteps;
		CLOSE cursorMesures;
	END;
$BODY$
LANGUAGE plpgsql STABLE
COST 100
ROWS 100000;
SQL
        );
        $this->addSql(
            <<<'SQL'
ALTER FUNCTION bdoh_export_interpolate_linear(integer, timestamp without time zone, timestamp without time zone, interval)
    OWNER TO bdoh;
SQL
        );
        $this->addSql(
            <<<'SQL'
DROP FUNCTION IF EXISTS bdoh_segments(integer, timestamp without time zone, timestamp without time zone);
SQL
        );
        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_segments(
    IN chronique_id integer,
    IN starting_date timestamp without time zone,
    IN ending_date timestamp without time zone
) RETURNS TABLE(
    start_date timestamp without time zone,
    end_date timestamp without time zone,
    start_valeur double precision,
    end_valeur double precision,
    start_qualite_id integer,
    end_qualite_id integer
) AS
$BODY$
    DECLARE
        gap_id CONSTANT mesure.qualite_id%TYPE := bdoh_chronique_qualite_id(chronique_id, 100);
    BEGIN
        RETURN QUERY
            SELECT * FROM (
                SELECT LAG(date, 1, TIMESTAMP '-infinity') OVER w,
                    date,
                    LAG(valeur, 1, NULL) OVER w,
                    valeur,
                    LAG(qualite_id, 1, gap_id) OVER w,
                    COALESCE(qualite_id, gap_id)
                FROM bdoh_bounded_mesures(chronique_id, starting_date, ending_date)
                WINDOW w AS (
                    ORDER BY date ASC
                    ROWS BETWEEN 1 PRECEDING AND CURRENT ROW
                )
            ) m
            OFFSET 1;
    END;
$BODY$
LANGUAGE plpgsql STABLE
COST 100
ROWS 100000;
SQL
        );
        $this->addSql(
            <<<'SQL'
ALTER FUNCTION bdoh_segments(integer, timestamp without time zone, timestamp without time zone)
    OWNER TO bdoh;
SQL
        );

        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_bounded_mesures(
    IN chronique_id integer,
    IN begin_date timestamp without time zone,
    IN end_date timestamp without time zone
) RETURNS TABLE(
    date timestamp without time zone,
    valeur double precision,
    qualite_id integer
) AS
$BODY$
    DECLARE
        c_id ALIAS FOR chronique_id;
        lower_date mesure.date%TYPE;
        upper_date mesure.date%TYPE;
        gap_id CONSTANT mesure.qualite_id%TYPE := bdoh_chronique_qualite_id(chronique_id, 100);
    BEGIN
        SELECT MAX(m.date) INTO lower_date FROM mesure m WHERE m.chronique_id = c_id AND m.date <= begin_date;
        SELECT MIN(m.date) INTO upper_date FROM mesure m WHERE m.chronique_id = c_id AND m.date > end_date;

        IF lower_date IS NULL THEN
            lower_date := TIMESTAMP '-infinity';
            RETURN QUERY SELECT TIMESTAMP '-infinity', NULL::DOUBLE PRECISION, gap_id;
        END IF;

        IF upper_date IS NULL THEN
            upper_date := TIMESTAMP 'infinity';
        END IF;

        RETURN QUERY
            SELECT m.date, m.valeur, COALESCE(m.qualite_id, gap_id)
            FROM mesure m
            WHERE m.chronique_id = c_id
            AND m.date BETWEEN lower_date AND upper_date
            ORDER BY m.date ASC;

        IF upper_date = TIMESTAMP 'infinity' THEN
            RETURN QUERY SELECT TIMESTAMP 'infinity', NULL::DOUBLE PRECISION, gap_id;
        END IF;
    END;
$BODY$
LANGUAGE plpgsql STABLE
COST 100
ROWS 100000;
SQL
        );
        $this->addSql(
            <<<'SQL'
ALTER FUNCTION bdoh_bounded_mesures(integer, timestamp without time zone, timestamp without time zone)
    OWNER TO bdoh;
SQL
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql(
            <<<'SQL'
DROP FUNCTION IF EXISTS bdoh_export_interpolate_linear(integer, timestamp without time zone, timestamp without time zone, interval);
SQL
        );

        $this->addSql(
            <<<'SQL'
DROP FUNCTION IF EXISTS bdoh_segments(integer, timestamp without time zone, timestamp without time zone);
SQL
        );
        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_segments(
    IN chronique_id integer,
    IN start_date timestamp without time zone,
    IN end_date timestamp without time zone)
  RETURNS TABLE(start_date timestamp without time zone, end_date timestamp without time zone, start_valeur double precision, end_valeur double precision, start_qualite_id integer, end_qualite_id integer) AS
$BODY$
                SELECT * FROM (
                    SELECT LAG(date, 1, TIMESTAMP '-infinity') OVER w,
                           date,
                           LAG(valeur) OVER w,
                           valeur,
                           LAG(qualite_id, 1, bdoh_chronique_qualite_id(chronique_id , 100)) OVER w,
                           COALESCE(qualite_id, bdoh_chronique_qualite_id(chronique_id , 100))
                      FROM bdoh_bounded_mesures(chronique_id, start_date, end_date)
                    WINDOW w AS (
                        ORDER BY date ASC
                        ROWS BETWEEN 1 PRECEDING AND CURRENT ROW
                    )
                ) m
                OFFSET 1
            $BODY$
  LANGUAGE sql STABLE
  COST 100
  ROWS 1000;
SQL
        );
        $this->addSql(
            <<<'SQL'
ALTER FUNCTION bdoh_segments(integer, timestamp without time zone, timestamp without time zone)
  OWNER TO bdoh;
SQL
        );

        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_bounded_mesures(
    IN chronique_id integer,
    IN begin_date timestamp without time zone,
    IN end_date timestamp without time zone)
  RETURNS TABLE(date timestamp without time zone, valeur double precision, qualite_id integer) AS
$BODY$
                DECLARE
                    c_id ALIAS FOR chronique_id;
                    lower_date mesure.date%TYPE;
                    upper_date mesure.date%TYPE;
                    gap_id     CONSTANT mesure.qualite_id%TYPE := bdoh_chronique_qualite_id(chronique_id, 100);
                BEGIN
                    SELECT MAX(m.date) INTO lower_date FROM mesure m WHERE m.chronique_id = c_id AND m.date <= begin_date;
                    SELECT MIN(m.date) INTO upper_date FROM mesure m WHERE m.chronique_id = c_id AND m.date >= end_date;

                    IF lower_date IS NULL THEN
                        RETURN QUERY SELECT TIMESTAMP '-infinity', NULL::DOUBLE PRECISION, gap_id;
                        lower_date := begin_date;
                    END IF;

                    IF upper_date IS NULL THEN
                        RETURN QUERY SELECT TIMESTAMP 'infinity', NULL::DOUBLE PRECISION, gap_id;
                        lower_date := end_date;
                    END IF;

                    RETURN QUERY
                        SELECT m.date, m.valeur, COALESCE(m.qualite_id, gap_id)
                          FROM mesure m
                         WHERE m.chronique_id = c_id
                           AND m.date BETWEEN lower_date AND upper_date;
                END;
            $BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 6000;
SQL
        );
        $this->addSql(
            <<<'SQL'
ALTER FUNCTION bdoh_bounded_mesures(integer, timestamp without time zone, timestamp without time zone)
  OWNER TO bdoh;
SQL
        );
    }
}
