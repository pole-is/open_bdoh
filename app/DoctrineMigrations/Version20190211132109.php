<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190211132109 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE historique ADD familleparametres_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE historique ADD familleparente_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE historique ADD CONSTRAINT FK_A2E2D63CC4A3C869 FOREIGN KEY (familleparametres_id) REFERENCES FamilleParametres (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE historique ADD CONSTRAINT FK_A2E2D63CAFC0E49B FOREIGN KEY (familleparente_id) REFERENCES FamilleParametres (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_A2E2D63CC4A3C869 ON historique (familleparametres_id)');
        $this->addSql('CREATE INDEX IDX_A2E2D63CAFC0E49B ON historique (familleparente_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE Historique DROP CONSTRAINT FK_A2E2D63CC4A3C869');
        $this->addSql('ALTER TABLE Historique DROP CONSTRAINT FK_A2E2D63CAFC0E49B');
        $this->addSql('DROP INDEX IDX_A2E2D63CC4A3C869');
        $this->addSql('DROP INDEX IDX_A2E2D63CAFC0E49B');
        $this->addSql('ALTER TABLE Historique DROP familleparametres_id');
        $this->addSql('ALTER TABLE Historique DROP familleparente_id');
    }
}
