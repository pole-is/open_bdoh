<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210311095644 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE chroniques_dataset');
        $this->addSql('ALTER TABLE chronique ADD dataset_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chronique ADD CONSTRAINT FK_DAC28749D47C2D1B FOREIGN KEY (dataset_id) REFERENCES DataSet (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_DAC28749D47C2D1B ON chronique (dataset_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE chroniques_dataset (dataset_id INT NOT NULL, chronique_id INT NOT NULL, PRIMARY KEY(dataset_id, chronique_id))');
        $this->addSql('CREATE INDEX idx_2f0ee2de93054d ON chroniques_dataset (chronique_id)');
        $this->addSql('CREATE INDEX idx_2f0ee2ded47c2d1b ON chroniques_dataset (dataset_id)');
        $this->addSql('ALTER TABLE chroniques_dataset ADD CONSTRAINT fk_2f0ee2de93054d FOREIGN KEY (chronique_id) REFERENCES chronique (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chroniques_dataset ADD CONSTRAINT fk_2f0ee2ded47c2d1b FOREIGN KEY (dataset_id) REFERENCES dataset (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Chronique DROP CONSTRAINT FK_DAC28749D47C2D1B');
        $this->addSql('DROP INDEX IDX_DAC28749D47C2D1B');
        $this->addSql('ALTER TABLE Chronique DROP dataset_id');
    }
}
