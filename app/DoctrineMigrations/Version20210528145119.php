<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210528145119 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TEMPORARY TABLE optionechantillonnagesortie_temp (ech varchar(256), pdt varchar(256))');

        $this->addSql(
            <<<EOF
            INSERT INTO optionechantillonnagesortie_temp VALUES
                ('Cumul', '1 événement'),
                ('Cumul', '1 an'),
                ('Cumul', '1 mois'),
                ('Cumul', '1 jour'),
                ('Cumul', '6 heures'),
                ('Cumul', '1 heure'),
                ('Cumul', '30 minutes'),
                ('Cumul', '15 minutes'),
                ('Cumul', '6 minutes'),
                ('Cumul', '5 minutes'),
                ('Moyenne', '1 événement'),
                ('Moyenne', '1 an'),
                ('Moyenne', '1 mois'),
                ('Moyenne', '1 jour'),
                ('Moyenne', '6 heures'),
                ('Moyenne', '1 heure'),
                ('Moyenne', '30 minutes'),
                ('Interpolation linéaire', '1 événement'),
                ('Interpolation linéaire', '1 an'),
                ('Interpolation linéaire', '1 mois'),
                ('Interpolation linéaire', '1 jour'),
                ('Interpolation linéaire', '6 heures'),
                ('Interpolation linéaire', '1 heure')
EOF
        );

        $this->addSql(
            <<<EOF
            INSERT INTO optionechantillonnagesortie (id, echantillonnageentree_id, pasechantillonnage_id)
                SELECT
                    nextval('optionechantillonnagesortie_id_seq'),
                    e.id,
                    pe.id
                    FROM optionechantillonnagesortie_temp v
                        INNER JOIN echantillonnage e ON (v.ech = e.nom)
                        INNER JOIN pasechantillonnage pe ON (v.pdt = pe.libelle)
            ON CONFLICT (echantillonnageentree_id, pasechantillonnage_id)
                DO NOTHING
EOF
        );

        $this->addSql("SELECT setval('optionechantillonnagesortie_id_seq', MAX(id)) FROM optionechantillonnagesortie");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('DELETE FROM OptionEchantillonnageSortie');
    }
}
