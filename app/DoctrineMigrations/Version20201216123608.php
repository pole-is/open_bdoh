<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20201216123608 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE TypeFunding_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE TypeFunding (id INT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE partenaire ADD typeFundings_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE partenaire DROP typefunding');
        $this->addSql('ALTER TABLE partenaire ADD CONSTRAINT FK_7DA2A0A31C1A21A7 FOREIGN KEY (typeFundings_id) REFERENCES TypeFunding (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_7DA2A0A31C1A21A7 ON partenaire (typeFundings_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE Partenaire DROP CONSTRAINT FK_7DA2A0A31C1A21A7');
        $this->addSql('DROP SEQUENCE TypeFunding_id_seq CASCADE');
        $this->addSql('DROP TABLE TypeFunding');
        $this->addSql('DROP INDEX IDX_7DA2A0A31C1A21A7');
        $this->addSql('ALTER TABLE Partenaire ADD typefunding VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE Partenaire DROP typeFundings_id');
    }
}
