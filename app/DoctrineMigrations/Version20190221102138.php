<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190221102138 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("UPDATE categorie SET libelleen='Administration' WHERE id = '12'");
        $this->addSql("UPDATE categorie SET libelleen='Other' WHERE id = '14'");
        $this->addSql("UPDATE categorie SET libelleen='Engineering consultant' WHERE id = '9'");
        $this->addSql("UPDATE categorie SET libelleen='Researcher' WHERE id = '8'");
        $this->addSql("UPDATE categorie SET libelleen='Local authority' WHERE id = '10'");
        $this->addSql("UPDATE categorie SET libelleen='Company' WHERE id = '11'");
        $this->addSql("UPDATE categorie SET libelleen='Private individual' WHERE id = '13'");

        $this->addSql("UPDATE objectifrecherche SET libelleen='Agrosystem' WHERE id = '21'");
        $this->addSql("UPDATE objectifrecherche SET libelleen='Land use planning' WHERE id = '22'");
        $this->addSql("UPDATE objectifrecherche SET libelleen='Biodiversity' WHERE id = '23'");
        $this->addSql("UPDATE objectifrecherche SET libelleen='Biogeochemistry' WHERE id = '24'");
        $this->addSql("UPDATE objectifrecherche SET libelleen='Climate change' WHERE id = '25'");
        $this->addSql("UPDATE objectifrecherche SET libelleen='Ecology' WHERE id = '26'");
        $this->addSql("UPDATE objectifrecherche SET libelleen='Landscape ecology' WHERE id = '27'");
        $this->addSql("UPDATE objectifrecherche SET libelleen='Geochemistry' WHERE id = '28'");
        $this->addSql("UPDATE objectifrecherche SET libelleen='Geology' WHERE id = '29'");
        $this->addSql("UPDATE objectifrecherche SET libelleen='Geophysics' WHERE id = '30'");
        $this->addSql("UPDATE objectifrecherche SET libelleen='Water resource management' WHERE id = '31'");
        $this->addSql("UPDATE objectifrecherche SET libelleen='Hydraulics' WHERE id = '32'");
        $this->addSql("UPDATE objectifrecherche SET libelleen='Hydrogeology' WHERE id = '33'");
        $this->addSql("UPDATE objectifrecherche SET libelleen='Hydrology' WHERE id = '34'");
        $this->addSql("UPDATE objectifrecherche SET libelleen='Meteorology' WHERE id = '35'");
        $this->addSql("UPDATE objectifrecherche SET libelleen='Microbiology' WHERE id = '36'");
        $this->addSql("UPDATE objectifrecherche SET libelleen='Micropollutants' WHERE id = '37'");
        $this->addSql("UPDATE objectifrecherche SET libelleen='Pedology' WHERE id = '38'");
        $this->addSql("UPDATE objectifrecherche SET libelleen='Humanities' WHERE id = '39'");
        $this->addSql("UPDATE objectifrecherche SET libelleen='Remote sensing' WHERE id = '40'");

        $this->addSql("UPDATE typetravaux SET libelleen='Other' WHERE id = '8'");
        $this->addSql("UPDATE typetravaux SET libelleen='Education' WHERE id = '6'");
        $this->addSql("UPDATE typetravaux SET libelleen='Consulting / design project' WHERE id = '7'");
        $this->addSql("UPDATE typetravaux SET libelleen='Research' WHERE id = '5'");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
