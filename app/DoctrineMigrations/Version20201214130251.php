<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20201214130251 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE topic_category_dataSet (dataset_id INT NOT NULL, topiccategory_id INT NOT NULL, PRIMARY KEY(dataset_id, topiccategory_id))');
        $this->addSql('CREATE INDEX IDX_6FC9B40ED47C2D1B ON topic_category_dataSet (dataset_id)');
        $this->addSql('CREATE INDEX IDX_6FC9B40EB45EC3B0 ON topic_category_dataSet (topiccategory_id)');
        $this->addSql('ALTER TABLE topic_category_dataSet ADD CONSTRAINT FK_6FC9B40ED47C2D1B FOREIGN KEY (dataset_id) REFERENCES DataSet (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE topic_category_dataSet ADD CONSTRAINT FK_6FC9B40EB45EC3B0 FOREIGN KEY (topiccategory_id) REFERENCES TopicCategory (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE topiccat_dataset');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE topiccat_dataset (dataset_id INT NOT NULL, topiccategory_id INT NOT NULL, PRIMARY KEY(dataset_id, topiccategory_id))');
        $this->addSql('CREATE INDEX idx_40656d7fb45ec3b0 ON topiccat_dataset (topiccategory_id)');
        $this->addSql('CREATE INDEX idx_40656d7fd47c2d1b ON topiccat_dataset (dataset_id)');
        $this->addSql('ALTER TABLE topiccat_dataset ADD CONSTRAINT fk_40656d7fd47c2d1b FOREIGN KEY (dataset_id) REFERENCES dataset (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE topiccat_dataset ADD CONSTRAINT fk_40656d7fb45ec3b0 FOREIGN KEY (topiccategory_id) REFERENCES topiccategory (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE topic_category_dataSet');
    }
}
