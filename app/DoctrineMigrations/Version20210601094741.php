<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210601094741 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TEMPORARY TABLE newqualite (jeu VARCHAR(256), code VARCHAR(256), libelle VARCHAR(256), libelleEn VARCHAR(256), ordre INT, type VARCHAR(256), style json)');

        $this->addSql('ALTER TABLE qualite ADD CONSTRAINT unique_jeu_code UNIQUE (jeu_id, code)');

        $this->addSql(
            <<<EOF
            INSERT INTO newqualite VALUES
                ('VALIDE', 'v', 'Valide', 'Valid', 800, null, '{"color":[0,0,255],"thickness":1}'),
                ('VALIDE', 'a', 'Absente', 'Missing', 300, null, '{"color":[0,242,255],"thickness":1}'),
                ('VALIDE', 'l', 'Lacune', 'Gap', 100, 'gap', '{"color":[255,0,0],"thickness":5}'),
                ('VALIDE', 'i', 'Invalide', 'Invalid', 200, 'gap', '{"color":[255,0,0],"thickness":5}'),
                ('VALIDE', 'd', 'Douteuse', 'Uncertain', 400, null, '{"color":[0,255,43],"thickness":1}'),
                ('VALIDE', 'e', 'Estimée', 'Estimated', 500, null, '{"color":[247,105,10],"thickness":1}'),
                ('VALIDE', 'lq', 'Limite de quantification', 'Quantification limit', 600, null, '{"color":[255,0,242],"thickness":1}'),
                ('VALIDE', 'ld', 'Limite de détection', 'Detection limit', 700, null, '{"color":[255,0,242],"thickness":1}'),
                ('VALIDE', 'gap', 'Lacune technique', 'Technical gap', 100, 'gap', null),
                ('Hydro2', '9', 'Bonne', 'Good', null, null, null),
                ('Hydro2', '5', 'Estimée', 'Estimated', null, null, null),
                ('Hydro2', '8', 'Reconstituée bonne', 'Rebuilt good', null, null, null),
                ('Hydro2', 'I', 'Valeur inconnue faible', 'Unknown low value', null, null, null),
                ('Hydro2', 'S', 'Valeur inconnue forte', 'Unknown high value', null, null, null),
                ('Hydro2', 'gap', 'Lacune technique', 'Technical gap', null, null, null)
EOF
        );

        $this->addSql(
            <<<EOF
            INSERT INTO qualite (id, jeu_id, code, libelle, libelleEn, ordre, type, style)
                SELECT
                       nextval('qualite_id_seq'),
                       j.id,
                       nq.code,
                       nq.libelle,
                       nq.libelleEn,
                       nq.ordre,
                       nq.type,
                       nq.style
                FROM newqualite nq INNER JOIN jeuqualite j ON (nq.jeu = j.nom)
            ON CONFLICT ON CONSTRAINT unique_jeu_code
                DO UPDATE SET
                      libelle = excluded.libelle,
                      libelleEn = excluded.libelleEn,
                      ordre = excluded.ordre,
                      type = excluded.type,
                      style = excluded.style
EOF
        );

        $this->addSql('ALTER TABLE qualite DROP CONSTRAINT unique_jeu_code');

        $this->addSql("SELECT setval('qualite_id_seq', MAX(id)) FROM qualite");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('DELETE FROM qualite');
    }
}
