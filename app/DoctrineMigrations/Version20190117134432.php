<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190117134432 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql(
            <<<'SQL'
DROP FUNCTION bdoh_subsampled_instantaneous_series_measures(integer, timestamp without time zone, timestamp without time zone, double precision);
SQL
        );
        $this->addSql(
            <<<'SQL'
DROP FUNCTION bdoh_subsampled_instantaneous_all_series_measures(integer, timestamp without time zone[], timestamp without time zone[], double precision[]);
SQL
        );
        $this->addSql(
            <<<'SQL'
DROP FUNCTION bdoh_subsampled_instantaneous_all_series_lengths(integer, timestamp without time zone[], timestamp without time zone[], double precision[]);
SQL
        );
        $this->addSql(
            <<<'SQL'
DROP FUNCTION bdoh_subsampled_mean_series_measures(integer, timestamp without time zone, timestamp without time zone, double precision, character varying);
SQL
        );
        $this->addSql(
            <<<'SQL'
DROP FUNCTION bdoh_subsampled_mean_all_series_measures(integer, timestamp without time zone[], timestamp without time zone[], double precision[], character varying);
SQL
        );
        $this->addSql(
            <<<'SQL'
DROP FUNCTION bdoh_subsampled_mean_all_series_lengths(integer, timestamp without time zone[], timestamp without time zone[], double precision[], character varying);
SQL
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_subsampled_instantaneous_series_measures(
    IN chroniqueid integer,
    IN startdate timestamp without time zone,
    IN enddate timestamp without time zone,
    IN subinterval double precision)
  RETURNS TABLE(milliseconds double precision, value double precision) AS
$BODY$
BEGIN
	-- WITH clause yields measures with date in milliseconds, subinterval index (starting from zero) and value
	-- First subselect (alias "vmin") yields, for each subinterval, the date & value for the measure with smallest value
	-- Second subselect (alias "vmax") yields, for each subinterval, the date & value for the largest measure with greatest value
	-- Finally, we append the very first & last measures in our date interval

	RETURN QUERY
		WITH miv(millisecs, index, val) AS (
			SELECT 1000 * EXTRACT(EPOCH FROM date AT TIME ZONE 'UTC'),
			(EXTRACT(EPOCH FROM AGE(date, startDate))/subinterval)::INTEGER,
			valeur
			FROM mesure WHERE chronique_id = chroniqueId
			AND date >= startDate AND date <= endDate
			ORDER BY date
		)
		SELECT millisecs, val FROM (SELECT * FROM miv LIMIT 1) first_m -- very first measure
		UNION
		SELECT millisecs, val FROM -- row with min value per subinterval
		(
			SELECT ROW_NUMBER() OVER(PARTITION BY index ORDER BY value ASC) AS rank,
			millisecs,
			val
			FROM miv
		) vmin
		WHERE vmin.rank = 1
		UNION
		SELECT millisecs, val FROM -- row with max value per subinterval
		(
			SELECT ROW_NUMBER() OVER(PARTITION BY index ORDER BY value DESC) AS rank,
			millisecs,
			val
			FROM miv
		) vmax
		WHERE vmax.rank = 1
		UNION
		SELECT millisecs, val FROM (SELECT * FROM miv ORDER BY milliseconds DESC LIMIT 1) last_m -- very last measure
		ORDER BY millisecs
        ;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 1000;
SQL
        );
        $this->addSql(
            <<<'SQL'
ALTER FUNCTION bdoh_subsampled_instantaneous_series_measures(integer, timestamp without time zone, timestamp without time zone, double precision)
  OWNER TO bdoh;

SQL
        );
        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_subsampled_instantaneous_all_series_measures(
    IN chroniqueid integer,
    IN startdates timestamp without time zone[],
    IN enddates timestamp without time zone[],
    IN subintervals double precision[])
  RETURNS TABLE(milliseconds double precision, value double precision) AS
$BODY$
DECLARE
	nSeries BIGINT;
	i BIGINT;
BEGIN
	nSeries := ARRAY_LENGTH(startDates, 1);
	FOR i IN 1..nSeries LOOP
		RETURN QUERY SELECT * FROM bdoh_subsampled_instantaneous_series_measures(chroniqueId,
			startDates[i], endDates[i], subintervals[i]);
	END LOOP;

END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 1000;
SQL
        );
        $this->addSql(
            <<<'SQL'
ALTER FUNCTION bdoh_subsampled_instantaneous_all_series_measures(integer, timestamp without time zone[], timestamp without time zone[], double precision[])
  OWNER TO bdoh;
SQL
        );
        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_subsampled_instantaneous_all_series_lengths(
    IN chroniqueid integer,
    IN startdates timestamp without time zone[],
    IN enddates timestamp without time zone[],
    IN subintervals double precision[])
  RETURNS TABLE(measure_count bigint) AS
$BODY$
DECLARE
	nSeries BIGINT;
	i BIGINT;
BEGIN
	nSeries := ARRAY_LENGTH(startDates, 1);
	FOR i IN 1..nSeries LOOP
		RETURN QUERY SELECT COUNT(*) FROM bdoh_subsampled_instantaneous_series_measures(chroniqueId,
			startDates[i], endDates[i], subintervals[i]);
	END LOOP;

END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 1000;
SQL
        );
        $this->addSql(
            <<<'SQL'
ALTER FUNCTION bdoh_subsampled_instantaneous_all_series_lengths(integer, timestamp without time zone[], timestamp without time zone[], double precision[])
  OWNER TO bdoh;
SQL
        );
        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_subsampled_mean_series_measures(
    IN chroniqueid integer,
    IN startdate timestamp without time zone,
    IN enddate timestamp without time zone,
    IN subinterval double precision,
    IN direction character varying DEFAULT 'AHEAD'::character varying)
  RETURNS TABLE(milliseconds double precision, value double precision) AS
$BODY$
DECLARE
	directionDateFilter VARCHAR;
	shiftedDate VARCHAR;
	overallQuery VARCHAR;
	intervalFirstDate DOUBLE PRECISION;
	intervalLastDate DOUBLE PRECISION;
BEGIN
	SELECT 1000 * MIN(EXTRACT(EPOCH FROM date AT TIME ZONE 'UTC')) INTO intervalFirstDate
		FROM mesure WHERE chronique_id = chroniqueId AND date >= startDate;
	SELECT 1000 * MAX(EXTRACT(EPOCH FROM date AT TIME ZONE 'UTC')) INTO intervalLastDate
		FROM mesure WHERE chronique_id = chroniqueId AND date <= endDate;

	-- String replacement variables in ALL the stored procedure:
	-- $1 = chroniqueId
	-- $2 = startDate
	-- $3 = endDate
	-- $4 = subinterval
	-- $5 = intervalFirstDate
	-- $6 = intervalLastDate

	CASE UPPER(direction)
		WHEN 'AHEAD' THEN
			directionDateFilter := 'AND date < (SELECT MAX(date) FROM mesure WHERE chronique_id = $1)';
			shiftedDate := 'COALESCE(LEAD(date) OVER(obd),'
				       '(SELECT MIN(date) FROM mesure WHERE chronique_id = $1 AND date > $3)'
				       ')';
		ELSE
			directionDateFilter := 'AND date > (SELECT MIN(date) FROM mesure WHERE chronique_id = $1)';
			shiftedDate := 'COALESCE(LAG(date) OVER(obd),'
				       '(SELECT MAX(date) FROM mesure WHERE chronique_id = $1 AND date < $2)'
				       ')';
	END CASE;

        overallQuery := 'WITH dsiiv(millisecs, shifted_ms, meas_index, intvl_index, val) AS ('
			'	SELECT 1000 * EXTRACT(EPOCH FROM date AT TIME ZONE ''UTC''),'
			'	1000 * EXTRACT(EPOCH FROM ' || shiftedDate || ' AT TIME ZONE ''UTC''),'
			'	ROW_NUMBER() OVER(obd),'
			'	(EXTRACT(EPOCH FROM age(date, $2))/$4)::INTEGER,'
			'	valeur FROM mesure WHERE chronique_id = $1'
			'	AND date >= $2 AND date <= $3 ' || directionDateFilter ||
			'	WINDOW obd AS (ORDER BY date)'
			'	ORDER BY date'
			'), vmin(millisecs, shifted_ms, meas_index, rank, val) AS ('
			'	SELECT millisecs, shifted_ms, meas_index,'
			'	ROW_NUMBER() OVER(PARTITION BY intvl_index ORDER BY val ASC),'
			'	val FROM dsiiv'
			'), vmin_filtered(millisecs, shifted_ms, meas_index, val) AS ('
			'	SELECT millisecs, shifted_ms, meas_index, val'
			'	FROM vmin WHERE rank = 1'
			'), vmax(millisecs, shifted_ms, meas_index, rank, val) AS ('
			'	SELECT millisecs, shifted_ms, meas_index,'
			'	ROW_NUMBER() OVER(PARTITION BY intvl_index ORDER BY val DESC),'
			'	val FROM dsiiv'
			'), vmax_filtered(millisecs, shifted_ms, meas_index, val) AS ('
			'	SELECT millisecs, shifted_ms, meas_index, val'
			'	FROM vmax WHERE rank = 1'
			'), vfirst(millisecs, shifted_ms, meas_index, val) AS ('
			'	SELECT millisecs, shifted_ms, meas_index, val'
			'	FROM dsiiv WHERE millisecs = $5'
			'), vlast(millisecs, shifted_ms, meas_index, val) AS ('
			'	SELECT millisecs, shifted_ms, meas_index, val'
			'	FROM dsiiv WHERE millisecs = $6'
			')'
			'SELECT millisecs, val FROM('
			'	SELECT millisecs, val, meas_index FROM vfirst'
			'	UNION SELECT shifted_ms, val, meas_index FROM vfirst'
			'	UNION'
			'	SELECT millisecs, val, meas_index FROM vmin_filtered'
			'	UNION SELECT shifted_ms, val, meas_index FROM vmin_filtered'
			'	UNION'
			'	SELECT millisecs, val, meas_index FROM vmax_filtered'
			'	UNION SELECT shifted_ms, val, meas_index FROM vmax_filtered'
			'	UNION'
			'	SELECT millisecs, val, meas_index FROM vlast'
			'	UNION SELECT shifted_ms, val, meas_index FROM vlast'
			'	ORDER BY millisecs, meas_index'
			') x'
			;

	RETURN QUERY EXECUTE overallQuery USING
		chroniqueId, startDate, endDate, subinterval, intervalFirstDate, intervalLastDate;

END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 1000;
SQL
        );
        $this->addSql(
            <<<'SQL'
ALTER FUNCTION bdoh_subsampled_mean_series_measures(integer, timestamp without time zone, timestamp without time zone, double precision, character varying)
  OWNER TO bdoh;
SQL
        );
        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_subsampled_mean_all_series_measures(
    IN chroniqueid integer,
    IN startdates timestamp without time zone[],
    IN enddates timestamp without time zone[],
    IN subintervals double precision[],
    IN direction character varying DEFAULT 'AHEAD'::character varying)
  RETURNS TABLE(milliseconds double precision, value double precision) AS
$BODY$
DECLARE
	nSeries BIGINT;
	i BIGINT;
BEGIN
	nSeries := ARRAY_LENGTH(startDates, 1);
	FOR i IN 1..nSeries LOOP
		RETURN QUERY SELECT * FROM bdoh_subsampled_mean_series_measures(chroniqueId,
			startDates[i], endDates[i], subintervals[i], direction);
	END LOOP;

END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 1000;
SQL
        );
        $this->addSql(
            <<<'SQL'
ALTER FUNCTION bdoh_subsampled_mean_all_series_measures(integer, timestamp without time zone[], timestamp without time zone[], double precision[], character varying)
  OWNER TO bdoh;
SQL
        );
        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_subsampled_mean_all_series_lengths(
    IN chroniqueid integer,
    IN startdates timestamp without time zone[],
    IN enddates timestamp without time zone[],
    IN subintervals double precision[],
    IN direction character varying DEFAULT 'AHEAD'::character varying)
  RETURNS TABLE(measure_count bigint) AS
$BODY$
DECLARE
	nSeries BIGINT;
	i BIGINT;
BEGIN
	nSeries := ARRAY_LENGTH(startDates, 1);
	FOR i IN 1..nSeries LOOP
		RETURN QUERY SELECT COUNT(*) FROM bdoh_subsampled_mean_series_measures(chroniqueId,
			startDates[i], endDates[i], subintervals[i], direction);
	END LOOP;

END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 1000;
SQL
        );
        $this->addSql(
            <<<'SQL'
ALTER FUNCTION bdoh_subsampled_mean_all_series_lengths(integer, timestamp without time zone[], timestamp without time zone[], double precision[], character varying)
  OWNER TO bdoh;
SQL
        );
    }
}
