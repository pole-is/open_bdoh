<?php
declare(strict_types=1);

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;

/**
 * Class AbstractScriptMigration.
 */
abstract class AbstractScriptMigration extends AbstractMigration
{
    const SCRIPT_DIR = __DIR__ . '/../sql/';

    /**
     * @param string $sql
     */
    protected function addStatements($sql)
    {
        $defs = array_map('trim', preg_split('/(?=^(?:CREATE|DROP|SELECT|INSERT|UPDATE|DELETE|GRANT|REVOKE))/im', $sql));
        foreach ($defs as $def) {
            if (!$def) {
                continue;
            }
            $this->addSql($def);
        }
    }

    /**
     * @param string $sql
     */
    protected function removeFunctions($sql)
    {
        preg_match_all('/^CREATE(?:\s+OR\s+REPLACE)?\s+(FUNCTION|AGGREGATE)\s+(\w+\s*\([^)]+\))/im', $sql, $matches, PREG_SET_ORDER);
        foreach ($matches as list(, $type, $sig)) {
            $sig = preg_replace(
                ['/\s+DEFAULT\s+[^,)]+/i', '/ {2,}/m'],
                ['', ' '],
                strtr($sig, "\n\t", '  ')
            );
            $this->addSql("DROP $type IF EXISTS $sig CASCADE");
        }
    }

    /**
     * @param string $name
     *
     * @return string
     */
    protected function readFile($name)
    {
        $sql = file_get_contents(self::SCRIPT_DIR . $name);
        $sql = preg_replace('/^\s*--.*$/m', '', $sql);

        return $sql;
    }
}
