<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210126091246 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE dataset DROP CONSTRAINT fk_40503eac93054d');
        $this->addSql('DROP INDEX idx_40503eac93054d');
        $this->addSql('ALTER TABLE dataset RENAME COLUMN chronique_id TO chroniques_id');
        $this->addSql('ALTER TABLE dataset ADD CONSTRAINT FK_40503EACEF1C130D FOREIGN KEY (chroniques_id) REFERENCES Chronique (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_40503EACEF1C130D ON dataset (chroniques_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE DataSet DROP CONSTRAINT FK_40503EACEF1C130D');
        $this->addSql('DROP INDEX IDX_40503EACEF1C130D');
        $this->addSql('ALTER TABLE DataSet RENAME COLUMN chroniques_id TO chronique_id');
        $this->addSql('ALTER TABLE DataSet ADD CONSTRAINT fk_40503eac93054d FOREIGN KEY (chronique_id) REFERENCES chronique (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_40503eac93054d ON DataSet (chronique_id)');
    }
}
