<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 * @SuppressWarnings(PHPMD.ShortMethodName)
 */
class Version20180502170600 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_update_filling_rates(
    chro_id chronique.id%TYPE,
    startDate TIMESTAMP(3) DEFAULT '-infinity',
    endDate TIMESTAMP(3) DEFAULT 'infinity'
)
RETURNS INTEGER
VOLATILE STRICT
LANGUAGE 'plpgsql'
AS $$
DECLARE
    discontinue BOOLEAN;
    mesCount INTEGER;
    firstDate TIMESTAMP(3);
    lastDate TIMESTAMP(3);
    tauxCount INTEGER;
BEGIN
    -- Récupère des informations sur la chronique
    SELECT
            dtype = 'discontinue',
            nbmesures,
            datedebutmesures,
            datefinmesures
        INTO discontinue, mesCount, firstDate, lastDate
        FROM chronique
        WHERE id = chro_id;

    -- Nous ne voulons pas de taux de remplissage pour les chroniques discontinues
    IF discontinue OR mesCount = 0 THEN
        DELETE FROM tauxremplissage WHERE chronique_id = chro_id;
        RETURN 0;
    END IF;

    -- Supprime les taux qui ne correspondent à aucune mesure.
    DELETE FROM tauxremplissage WHERE chronique_id = chro_id AND (fin < firstDate OR debut > lastDate);

    IF startDate > '-infinity'::TIMESTAMP(3) THEN
        -- Calcule à partir de la mesure non-lacunaire précédente
        SELECT MAX(date)
            INTO firstDate
            FROM mesure
            WHERE chronique_id = chro_id
            AND qualite_id NOT IN (SELECT id FROM qualite WHERE ordre = 100 OR code = 'gap')
            AND date <= startDate;
        IF firstDate < startDate THEN
            startDate = firstDate;
        END IF;
    ELSE
        -- Ne calcule pas avant le début de la chronique
        IF firstDate > startDate THEN
            startDate = firstDate;
        END IF;
    END IF;

    IF endDate < 'infinity'::TIMESTAMP(3) THEN
        -- Calcule jusqu'à la mesure non-lacunaire suivante
        SELECT MIN(date)
            INTO lastDate
            FROM mesure
            WHERE chronique_id = chro_id
            AND qualite_id NOT IN (SELECT id FROM qualite WHERE ordre = 100 OR code = 'gap')
            AND date >= endDate;
        IF lastDate > endDate THEN
            endDate = lastDate;
        END IF;
    ELSE
        -- Ne calcule pas après la fin de la chronique
        IF lastDate < endDate THEN
            endDate = lastDate;
        END IF;
    END IF;

    RAISE INFO 'Chronicle #%: updating filling rate from % to %', chro_id, startDate, endDate;

    -- Recalcule les taux de remplissages inclus la période demandée
    INSERT INTO tauxremplissage(id, chronique_id, debut, fin, poids, taux) (
        WITH
            mesures(qualite_id, debut, fin) AS (
                SELECT
                    m.qualite_id,
                    m.date,
                    LEAD(m.date, 1, endDate) OVER (ORDER BY date ROWS BETWEEN CURRENT ROW AND 1 FOLLOWING)
                FROM mesure m
                WHERE m.chronique_id = chro_id
            ),
            valides(debut, fin) AS (
                SELECT
                    debut,
                    fin
                FROM mesures
                WHERE qualite_id NOT IN (SELECT id FROM qualite WHERE ordre = 100 OR code = 'gap')
            ),
            mois(debut, fin, duration) AS (
                SELECT
                    t,
                    t + '1 month',
                    EXTRACT('epoch' FROM ((t + '1 month') - t))
                FROM generate_series(DATE_TRUNC('month', startDate), endDate, '1 month') t
                WHERE t < endDate
            )
        SELECT
            nextval('tauxremplissage_id_seq'),
            chro_id,
            m.debut,
            m.fin,
            m.duration,
            SUM(
                CASE WHEN v.debut IS NOT NULL AND v.fin IS NOT NULL THEN
                    EXTRACT('epoch' FROM (
                        CASE WHEN v.fin < m.fin THEN v.fin ELSE m.fin END
                        -
                        CASE WHEN v.debut > m.debut THEN v.debut ELSE m.debut END
                    ))
                ELSE
                    0
                END
            ) / m.duration
        FROM
            mois m
            LEFT JOIN valides v ON ((m.debut, m.fin) OVERLAPS (v.debut, v.fin))
        GROUP BY m.debut, m.fin, m.duration
    );

    GET DIAGNOSTICS tauxCount := ROW_COUNT;
    RAISE INFO 'Chronicle #%: % filling rate record(s) created', chro_id, tauxCount;

    RETURN tauxCount;
END;
$$
SQL
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
    }
}
