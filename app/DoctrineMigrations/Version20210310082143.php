<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210310082143 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE partenaire_observatoire');
        $this->addSql('DROP TABLE dataset_observatoire');
        $this->addSql('ALTER TABLE dataset ADD observatoire_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE dataset ADD CONSTRAINT FK_40503EAC61ED1B0D FOREIGN KEY (observatoire_id) REFERENCES Observatoire (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_40503EAC61ED1B0D ON dataset (observatoire_id)');
        $this->addSql('DROP INDEX idx_dac287491ce2a12b');
        $this->addSql('ALTER TABLE chronique DROP listechroniques_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE partenaire_observatoire (observatoire_id INT NOT NULL, partenaire_id INT NOT NULL, PRIMARY KEY(observatoire_id, partenaire_id))');
        $this->addSql('CREATE INDEX idx_acb4456e61ed1b0d ON partenaire_observatoire (observatoire_id)');
        $this->addSql('CREATE INDEX idx_acb4456e98de13ac ON partenaire_observatoire (partenaire_id)');
        $this->addSql('CREATE TABLE dataset_observatoire (observatoire_id INT NOT NULL, dataset_id INT NOT NULL, PRIMARY KEY(observatoire_id, dataset_id))');
        $this->addSql('CREATE INDEX idx_4dae3192d47c2d1b ON dataset_observatoire (dataset_id)');
        $this->addSql('CREATE INDEX idx_4dae319261ed1b0d ON dataset_observatoire (observatoire_id)');
        $this->addSql('ALTER TABLE partenaire_observatoire ADD CONSTRAINT fk_acb4456e98de13ac FOREIGN KEY (partenaire_id) REFERENCES partenaire (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE partenaire_observatoire ADD CONSTRAINT fk_acb4456e61ed1b0d FOREIGN KEY (observatoire_id) REFERENCES observatoire (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dataset_observatoire ADD CONSTRAINT fk_4dae3192d47c2d1b FOREIGN KEY (dataset_id) REFERENCES dataset (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dataset_observatoire ADD CONSTRAINT fk_4dae319261ed1b0d FOREIGN KEY (observatoire_id) REFERENCES observatoire (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE DataSet DROP CONSTRAINT FK_40503EAC61ED1B0D');
        $this->addSql('DROP INDEX IDX_40503EAC61ED1B0D');
        $this->addSql('ALTER TABLE DataSet DROP observatoire_id');
        $this->addSql('ALTER TABLE Chronique ADD listechroniques_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE Chronique ALTER miseAJour TYPE TIMESTAMP(3) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE Chronique ALTER miseAJour DROP DEFAULT');
        $this->addSql('CREATE INDEX idx_dac287491ce2a12b ON Chronique (listechroniques_id)');
    }
}
