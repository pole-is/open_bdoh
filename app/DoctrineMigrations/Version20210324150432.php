<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210324150432 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE topic_category_dataset DROP CONSTRAINT FK_6FC9B40ED47C2D1B');
        $this->addSql('ALTER TABLE topic_category_dataset DROP CONSTRAINT FK_6FC9B40EB45EC3B0');
        $this->addSql('ALTER TABLE topic_category_dataset ADD CONSTRAINT FK_6FC9B40ED47C2D1B FOREIGN KEY (dataset_id) REFERENCES DataSet (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE topic_category_dataset ADD CONSTRAINT FK_6FC9B40EB45EC3B0 FOREIGN KEY (topiccategory_id) REFERENCES TopicCategory (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE criteria_geology_dataset DROP CONSTRAINT FK_5C567A1979262535');
        $this->addSql('ALTER TABLE criteria_geology_dataset DROP CONSTRAINT FK_5C567A19D47C2D1B');
        $this->addSql('ALTER TABLE criteria_geology_dataset ADD CONSTRAINT FK_5C567A1979262535 FOREIGN KEY (criteriageology_id) REFERENCES CriteriaGeology (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE criteria_geology_dataset ADD CONSTRAINT FK_5C567A19D47C2D1B FOREIGN KEY (dataset_id) REFERENCES DataSet (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE criteria_climat_dataset DROP CONSTRAINT FK_1F40E8D12929DC79');
        $this->addSql('ALTER TABLE criteria_climat_dataset DROP CONSTRAINT FK_1F40E8D1D47C2D1B');
        $this->addSql('ALTER TABLE criteria_climat_dataset ADD CONSTRAINT FK_1F40E8D12929DC79 FOREIGN KEY (criteriaclimat_id) REFERENCES CriteriaClimat (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE criteria_climat_dataset ADD CONSTRAINT FK_1F40E8D1D47C2D1B FOREIGN KEY (dataset_id) REFERENCES DataSet (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE topic_category_dataSet DROP CONSTRAINT fk_6fc9b40ed47c2d1b');
        $this->addSql('ALTER TABLE topic_category_dataSet DROP CONSTRAINT fk_6fc9b40eb45ec3b0');
        $this->addSql('ALTER TABLE topic_category_dataSet ADD CONSTRAINT fk_6fc9b40ed47c2d1b FOREIGN KEY (dataset_id) REFERENCES dataset (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE topic_category_dataSet ADD CONSTRAINT fk_6fc9b40eb45ec3b0 FOREIGN KEY (topiccategory_id) REFERENCES topiccategory (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE criteria_geology_dataSet DROP CONSTRAINT fk_5c567a19d47c2d1b');
        $this->addSql('ALTER TABLE criteria_geology_dataSet DROP CONSTRAINT fk_5c567a1979262535');
        $this->addSql('ALTER TABLE criteria_geology_dataSet ADD CONSTRAINT fk_5c567a19d47c2d1b FOREIGN KEY (dataset_id) REFERENCES dataset (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE criteria_geology_dataSet ADD CONSTRAINT fk_5c567a1979262535 FOREIGN KEY (criteriageology_id) REFERENCES criteriageology (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE criteria_climat_dataSet DROP CONSTRAINT fk_1f40e8d1d47c2d1b');
        $this->addSql('ALTER TABLE criteria_climat_dataSet DROP CONSTRAINT fk_1f40e8d12929dc79');
        $this->addSql('ALTER TABLE criteria_climat_dataSet ADD CONSTRAINT fk_1f40e8d1d47c2d1b FOREIGN KEY (dataset_id) REFERENCES dataset (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE criteria_climat_dataSet ADD CONSTRAINT fk_1f40e8d12929dc79 FOREIGN KEY (criteriaclimat_id) REFERENCES criteriaclimat (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
