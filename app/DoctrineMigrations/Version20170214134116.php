<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 * @SuppressWarnings(PHPMD.ShortMethodName)
 */
class Version20170214134116 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('CREATE SEQUENCE jms_jobs_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE jms_cron_jobs_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE jms_jobs (id BIGINT NOT NULL, state VARCHAR(15) NOT NULL, queue VARCHAR(50) NOT NULL, priority SMALLINT NOT NULL, createdAt TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, startedAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, checkedAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, workerName VARCHAR(50) DEFAULT NULL, executeAfter TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, closedAt TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, command VARCHAR(255) NOT NULL, args JSON NOT NULL, output TEXT DEFAULT NULL, errorOutput TEXT DEFAULT NULL, exitCode SMALLINT DEFAULT NULL, maxRuntime SMALLINT NOT NULL, maxRetries SMALLINT NOT NULL, stackTrace BYTEA DEFAULT NULL, runtime SMALLINT DEFAULT NULL, memoryUsage INT DEFAULT NULL, memoryUsageReal INT DEFAULT NULL, originalJob_id BIGINT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_704ADB9349C447F1 ON jms_jobs (originalJob_id)');
        $this->addSql('CREATE INDEX cmd_search_index ON jms_jobs (command)');
        $this->addSql('CREATE INDEX sorting_index ON jms_jobs (state, priority, id)');
        $this->addSql('COMMENT ON COLUMN jms_jobs.stackTrace IS \'(DC2Type:jms_job_safe_object)\'');
        $this->addSql('CREATE TABLE jms_job_dependencies (source_job_id BIGINT NOT NULL, dest_job_id BIGINT NOT NULL, PRIMARY KEY(source_job_id, dest_job_id))');
        $this->addSql('CREATE INDEX IDX_8DCFE92CBD1F6B4F ON jms_job_dependencies (source_job_id)');
        $this->addSql('CREATE INDEX IDX_8DCFE92C32CF8D4C ON jms_job_dependencies (dest_job_id)');
        $this->addSql('CREATE TABLE jms_cron_jobs (id INT NOT NULL, command VARCHAR(200) NOT NULL, lastRunAt TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_55F5ED428ECAEAD4 ON jms_cron_jobs (command)');
        $this->addSql('CREATE TABLE jms_job_related_entities (job_id BIGINT NOT NULL, related_class VARCHAR(150) NOT NULL, related_id VARCHAR(100) NOT NULL, PRIMARY KEY(job_id, related_class, related_id))');
        $this->addSql('CREATE INDEX IDX_E956F4E2BE04EA9 ON jms_job_related_entities (job_id)');
        $this->addSql('CREATE TABLE jms_job_statistics (job_id BIGINT NOT NULL, characteristic VARCHAR(30) NOT NULL, createdAt TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, charValue DOUBLE PRECISION NOT NULL, PRIMARY KEY(job_id, characteristic, createdAt))');
        $this->addSql('ALTER TABLE jms_jobs ADD CONSTRAINT FK_704ADB9349C447F1 FOREIGN KEY (originalJob_id) REFERENCES jms_jobs (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE jms_job_dependencies ADD CONSTRAINT FK_8DCFE92CBD1F6B4F FOREIGN KEY (source_job_id) REFERENCES jms_jobs (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE jms_job_dependencies ADD CONSTRAINT FK_8DCFE92C32CF8D4C FOREIGN KEY (dest_job_id) REFERENCES jms_jobs (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE jms_job_related_entities ADD CONSTRAINT FK_E956F4E2BE04EA9 FOREIGN KEY (job_id) REFERENCES jms_jobs (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE jms_jobs DROP CONSTRAINT FK_704ADB9349C447F1');
        $this->addSql('ALTER TABLE jms_job_dependencies DROP CONSTRAINT FK_8DCFE92CBD1F6B4F');
        $this->addSql('ALTER TABLE jms_job_dependencies DROP CONSTRAINT FK_8DCFE92C32CF8D4C');
        $this->addSql('ALTER TABLE jms_job_related_entities DROP CONSTRAINT FK_E956F4E2BE04EA9');
        $this->addSql('DROP SEQUENCE jms_jobs_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE jms_cron_jobs_id_seq CASCADE');
        $this->addSql('DROP TABLE jms_jobs');
        $this->addSql('DROP TABLE jms_job_dependencies');
        $this->addSql('DROP TABLE jms_cron_jobs');
        $this->addSql('DROP TABLE jms_job_related_entities');
        $this->addSql('DROP TABLE jms_job_statistics');
    }
}
