<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 * @SuppressWarnings(PHPMD.ShortMethodName)
 */
class Version20171114114139 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('ALTER TABLE transformation ADD sortie_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE transformation ADD CONSTRAINT FK_3FD6625DCC72D953 FOREIGN KEY (sortie_id) REFERENCES Chronique (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_3FD6625DCC72D953 ON transformation (sortie_id)');
        $this->addSql('UPDATE transformation t SET sortie_id = c.id FROM chronique c WHERE t.id IN (c.premiereentree_id, c.secondeentree_id)');
        $this->addSql(
            'DELETE FROM transformation t WHERE NOT EXISTS ' .
            '(SELECT * FROM chronique c WHERE t.id = c.premiereentree_id OR t.id = c.secondeentree_id)'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE Transformation DROP CONSTRAINT FK_3FD6625DCC72D953');
        $this->addSql('DROP INDEX IDX_3FD6625DCC72D953');
        $this->addSql('ALTER TABLE Transformation DROP sortie_id');
    }
}
