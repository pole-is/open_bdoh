<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210127124936 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE theia_categorie_dataset DROP CONSTRAINT fk_713fb989cb4778a9');
        $this->addSql('DROP SEQUENCE bdoh.theiacategorie_id_seq CASCADE');
        $this->addSql('DROP TABLE theiacategorie');
        $this->addSql('DROP TABLE theia_categorie_dataset');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE bdoh.theiacategorie_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE theiacategorie (id INT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE theia_categorie_dataset (chronique_id INT NOT NULL, theiacategorie_id INT NOT NULL, PRIMARY KEY(chronique_id, theiacategorie_id))');
        $this->addSql('CREATE INDEX idx_713fb989cb4778a9 ON theia_categorie_dataset (theiacategorie_id)');
        $this->addSql('CREATE INDEX idx_713fb98993054d ON theia_categorie_dataset (chronique_id)');
        $this->addSql('ALTER TABLE theia_categorie_dataset ADD CONSTRAINT fk_713fb989cb4778a9 FOREIGN KEY (theiacategorie_id) REFERENCES theiacategorie (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE theia_categorie_dataset ADD CONSTRAINT fk_713fb98993054d FOREIGN KEY (chronique_id) REFERENCES chronique (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
