<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210114132405 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE partenaire DROP CONSTRAINT fk_7da2a0a32b9b7b3c');
        $this->addSql('DROP SEQUENCE bdoh.contactorganisationrole_id_seq CASCADE');
        $this->addSql('DROP TABLE contactorganisationrole');
        $this->addSql('ALTER TABLE personnetheia DROP CONSTRAINT FK_3BA3D4E338898CF5');
        $this->addSql('ALTER TABLE personnetheia DROP CONSTRAINT FK_3BA3D4E3FB88E14F');
        $this->addSql('ALTER TABLE personnetheia ADD CONSTRAINT FK_3BA3D4E338898CF5 FOREIGN KEY (partenaires_id) REFERENCES Partenaire (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE personnetheia ADD CONSTRAINT FK_3BA3D4E3FB88E14F FOREIGN KEY (utilisateur_id) REFERENCES Utilisateur (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dataset DROP CONSTRAINT FK_40503EACD8B5F69');
        $this->addSql('ALTER TABLE dataset DROP conditionsutilisation');
        $this->addSql('ALTER TABLE dataset ADD CONSTRAINT FK_40503EACD8B5F69 FOREIGN KEY (dataconstraints_id) REFERENCES DataConstraint (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP INDEX idx_7da2a0a32b9b7b3c');
        $this->addSql('ALTER TABLE partenaire DROP roleorganisation_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE bdoh.contactorganisationrole_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE contactorganisationrole (id INT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('DROP INDEX UNIQ_90B54B32EC0C1EEE');
        $this->addSql('ALTER TABLE Partenaire ADD roleorganisation_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE Partenaire ADD CONSTRAINT fk_7da2a0a32b9b7b3c FOREIGN KEY (roleorganisation_id) REFERENCES contactorganisationrole (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_7da2a0a32b9b7b3c ON Partenaire (roleorganisation_id)');
        $this->addSql('ALTER TABLE DataSet DROP CONSTRAINT fk_40503eacd8b5f69');
        $this->addSql('ALTER TABLE DataSet ADD conditionsutilisation VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE DataSet ADD CONSTRAINT fk_40503eacd8b5f69 FOREIGN KEY (dataconstraints_id) REFERENCES dataconstraint (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE PersonneTheia DROP CONSTRAINT fk_3ba3d4e338898cf5');
        $this->addSql('ALTER TABLE PersonneTheia DROP CONSTRAINT fk_3ba3d4e3fb88e14f');
        $this->addSql('ALTER TABLE PersonneTheia ADD CONSTRAINT fk_3ba3d4e338898cf5 FOREIGN KEY (partenaires_id) REFERENCES partenaire (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE PersonneTheia ADD CONSTRAINT fk_3ba3d4e3fb88e14f FOREIGN KEY (utilisateur_id) REFERENCES utilisateur (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
