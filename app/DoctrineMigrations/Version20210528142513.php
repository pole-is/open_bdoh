<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210528142513 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
        $this->addSql("INSERT INTO
    pasechantillonnage (id , libelle, libelleen, unite, valeur, approxsecondes )
VALUES
       (nextval('pasechantillonnage_id_seq'),'5 minutes', '5 minutes', 'minute', 5, 300),
       (nextval('pasechantillonnage_id_seq'), '6 minutes', '6 minutes', 'minute', 6, 360),
       (nextval('pasechantillonnage_id_seq'), '15 minutes', '15 minutes', 'minute', 15, 900 ),
       (nextval('pasechantillonnage_id_seq'), '30 minutes', '30 minutes','minute', 30, 1800),
       (nextval('pasechantillonnage_id_seq'),'1 heure', '1 hour', 'hour', 1, 3600),
       (nextval('pasechantillonnage_id_seq'), '6 heures', '6 hours', 'hour', 6, 21600),
       (nextval('pasechantillonnage_id_seq'), '1 jour', '1 day','day', 1, 86400),
       (nextval('pasechantillonnage_id_seq'),'1 mois', '1 month' ,'month', 1, 2629800),
       (nextval('pasechantillonnage_id_seq'), '1 an', '1 year','year', 1, 31557600),
       (nextval('pasechantillonnage_id_seq'), '1 événement', '1 event','event', 1, 2000000000 )

ON CONFLICT (unite, valeur)
DO UPDATE SET libelleen = EXCLUDED.libelleen;
");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DELETE FROM pasechantillonnage');
    }
}
