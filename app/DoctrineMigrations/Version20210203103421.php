<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210203103421 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE liste_chroniques_dataSet (dataset_id INT NOT NULL, listeChroniques_id INT NOT NULL, PRIMARY KEY(dataset_id, listeChroniques_id))');
        $this->addSql('CREATE INDEX IDX_E501B452D47C2D1B ON liste_chroniques_dataSet (dataset_id)');
        $this->addSql('CREATE INDEX IDX_E501B452AF9CEADE ON liste_chroniques_dataSet (listeChroniques_id)');
        $this->addSql('ALTER TABLE liste_chroniques_dataSet ADD CONSTRAINT FK_E501B452D47C2D1B FOREIGN KEY (dataset_id) REFERENCES DataSet (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE liste_chroniques_dataSet ADD CONSTRAINT FK_E501B452AF9CEADE FOREIGN KEY (listeChroniques_id) REFERENCES ListeChroniques (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE chronique_dataset');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE chronique_dataset (dataset_id INT NOT NULL, chronique_id INT NOT NULL, PRIMARY KEY(dataset_id, chronique_id))');
        $this->addSql('CREATE INDEX idx_3100014ad47c2d1b ON chronique_dataset (dataset_id)');
        $this->addSql('CREATE INDEX idx_3100014a93054d ON chronique_dataset (chronique_id)');
        $this->addSql('ALTER TABLE chronique_dataset ADD CONSTRAINT fk_3100014a93054d FOREIGN KEY (chronique_id) REFERENCES chronique (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chronique_dataset ADD CONSTRAINT fk_3100014ad47c2d1b FOREIGN KEY (dataset_id) REFERENCES dataset (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE liste_chroniques_dataSet');
    }
}
