<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 * @SuppressWarnings(PHPMD.ShortMethodName)
 */
class Version20170811083748 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('ALTER TABLE utilisateur ADD roleAdmin VARCHAR(255) DEFAULT NULL');

        $this->addSql(
            "UPDATE utilisateur u
                SET roleAdmin = 'FCT'
                FROM role r
                WHERE u.id = r.utilisateur_id
                AND r.valeur = 'gestionnaire'
                AND r.dtype = 'observatoire'
                AND r.observatoire_id IS NULL"
        );

        $this->addSql(
            "DELETE FROM role
                WHERE valeur = 'gestionnaire'
                AND dtype = 'observatoire'
                AND observatoire_id IS NULL"
        );

        $this->addSql('DELETE FROM role WHERE valeur IS null');

        $this->addSql('ALTER TABLE role ALTER utilisateur_id SET NOT NULL');
        $this->addSql('ALTER TABLE role ALTER valeur SET NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE Role ALTER utilisateur_id DROP NOT NULL');
        $this->addSql('ALTER TABLE Role ALTER valeur DROP NOT NULL');

        $this->addSql(
            "INSERT INTO role(id, utilisateur_id, valeur, dtype)
                SELECT nextval('role_id_seq'), id, 'gestionnaire, 'observatoire'
                FROM utilisateur
                WHERE roleAdmin = 'FCT'"
        );

        $this->addSql('ALTER TABLE Utilisateur DROP roleAdmin');
    }
}
