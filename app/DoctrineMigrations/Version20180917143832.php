<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180917143832 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE observatoires_partenaires_scientifiques DROP CONSTRAINT FK_5C80B31D61ED1B0D');
        $this->addSql('ALTER TABLE observatoires_partenaires_scientifiques DROP CONSTRAINT FK_5C80B31D98DE13AC');
        $this->addSql('ALTER TABLE observatoires_partenaires_scientifiques ADD CONSTRAINT FK_5C80B31D61ED1B0D FOREIGN KEY (observatoire_id) REFERENCES Observatoire (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE observatoires_partenaires_scientifiques ADD CONSTRAINT FK_5C80B31D98DE13AC FOREIGN KEY (partenaire_id) REFERENCES Partenaire (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE observatoires_partenaires_institutionnels DROP CONSTRAINT FK_DDFC381E61ED1B0D');
        $this->addSql('ALTER TABLE observatoires_partenaires_institutionnels DROP CONSTRAINT FK_DDFC381E98DE13AC');
        $this->addSql('ALTER TABLE observatoires_partenaires_institutionnels ADD CONSTRAINT FK_DDFC381E61ED1B0D FOREIGN KEY (observatoire_id) REFERENCES Observatoire (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE observatoires_partenaires_institutionnels ADD CONSTRAINT FK_DDFC381E98DE13AC FOREIGN KEY (partenaire_id) REFERENCES Partenaire (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE doi DROP CONSTRAINT FK_5ED9529A61ED1B0D');
        $this->addSql('ALTER TABLE doi ADD CONSTRAINT FK_5ED9529A61ED1B0D FOREIGN KEY (observatoire_id) REFERENCES Observatoire (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE station DROP CONSTRAINT FK_5084C12D131A4F72');
        $this->addSql('ALTER TABLE station ADD CONSTRAINT FK_5084C12D131A4F72 FOREIGN KEY (commune_id) REFERENCES Commune (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chronique DROP CONSTRAINT FK_DAC28749AB9BB300');
        $this->addSql('ALTER TABLE chronique ADD CONSTRAINT FK_DAC28749AB9BB300 FOREIGN KEY (producteur_id) REFERENCES Partenaire (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pointjaugeage DROP CONSTRAINT FK_A228ECE75F49EAAD');
        $this->addSql('ALTER TABLE pointjaugeage ADD CONSTRAINT FK_A228ECE75F49EAAD FOREIGN KEY (bareme_id) REFERENCES Bareme (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bareme DROP CONSTRAINT FK_35A1B5D861ED1B0D');
        $this->addSql('ALTER TABLE bareme ADD CONSTRAINT FK_35A1B5D861ED1B0D FOREIGN KEY (observatoire_id) REFERENCES Observatoire (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bassin DROP CONSTRAINT fk_660811f3166baf05');
        $this->addSql('ALTER TABLE bassin DROP CONSTRAINT FK_660811F361ED1B0D');
        $this->addSql('ALTER TABLE bassin ADD CONSTRAINT FK_660811F390F720D2 FOREIGN KEY (stationexutoire_id) REFERENCES Station (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bassin ADD CONSTRAINT FK_660811F361ED1B0D FOREIGN KEY (observatoire_id) REFERENCES Observatoire (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER INDEX idx_660811f3166baf05 RENAME TO IDX_660811F390F720D2');
        $this->addSql('ALTER TABLE role DROP CONSTRAINT FK_F75B255461ED1B0D');
        $this->addSql('ALTER TABLE role DROP CONSTRAINT FK_F75B2554FB88E14F');
        $this->addSql('ALTER TABLE role DROP CONSTRAINT FK_F75B2554F6BD1646');
        $this->addSql('ALTER TABLE role ADD CONSTRAINT FK_F75B255461ED1B0D FOREIGN KEY (observatoire_id) REFERENCES Observatoire (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE role ADD CONSTRAINT FK_F75B2554FB88E14F FOREIGN KEY (utilisateur_id) REFERENCES Utilisateur (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE role ADD CONSTRAINT FK_F75B2554F6BD1646 FOREIGN KEY (site_id) REFERENCES SiteExperimental (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE besoin DROP CONSTRAINT FK_86B4ED27FB88E14F');
        $this->addSql('ALTER TABLE besoin DROP CONSTRAINT FK_86B4ED2761ED1B0D');
        $this->addSql('ALTER TABLE besoin ADD CONSTRAINT FK_86B4ED27FB88E14F FOREIGN KEY (utilisateur_id) REFERENCES Utilisateur (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE besoin ADD CONSTRAINT FK_86B4ED2761ED1B0D FOREIGN KEY (observatoire_id) REFERENCES Observatoire (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE besoins_parametres DROP CONSTRAINT FK_75A89B03FE6EED44');
        $this->addSql('ALTER TABLE besoins_parametres DROP CONSTRAINT FK_75A89B036358FF62');
        $this->addSql('ALTER TABLE besoins_parametres ADD CONSTRAINT FK_75A89B03FE6EED44 FOREIGN KEY (besoin_id) REFERENCES Besoin (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE besoins_parametres ADD CONSTRAINT FK_75A89B036358FF62 FOREIGN KEY (parametre_id) REFERENCES TypeParametre (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE besoins_stations DROP CONSTRAINT FK_59777037FE6EED44');
        $this->addSql('ALTER TABLE besoins_stations ADD CONSTRAINT FK_59777037FE6EED44 FOREIGN KEY (besoin_id) REFERENCES Besoin (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE historique DROP CONSTRAINT fk_a2e2d63cd9b0f6d6');
        $this->addSql('ALTER TABLE historique DROP CONSTRAINT fk_a2e2d63c6ecd41f5');
        $this->addSql('ALTER TABLE historique DROP CONSTRAINT fk_a2e2d63c5001bc01');
        $this->addSql('ALTER TABLE historique DROP CONSTRAINT FK_A2E2D63C60BB6FE6');
        $this->addSql('ALTER TABLE historique DROP CONSTRAINT FK_A2E2D63C61ED1B0D');
        $this->addSql('ALTER TABLE historique DROP CONSTRAINT FK_A2E2D63CFB88E14F');
        $this->addSql('ALTER TABLE historique DROP CONSTRAINT FK_A2E2D63C5F49EAAD');
        $this->addSql('ALTER TABLE historique DROP CONSTRAINT FK_A2E2D63C131A4F72');
        $this->addSql('ALTER TABLE historique DROP CONSTRAINT FK_A2E2D63CEC4A74AB');
        $this->addSql('ALTER TABLE historique DROP CONSTRAINT FK_A2E2D63C21BDB235');
        $this->addSql('ALTER TABLE historique DROP CONSTRAINT FK_A2E2D63C98DE13AC');
        $this->addSql('ALTER TABLE historique ADD courseau_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE historique ADD bassin_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE historique ADD CONSTRAINT FK_A2E2D63C32164871 FOREIGN KEY (courseau_id) REFERENCES CoursEau (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE historique ADD CONSTRAINT FK_A2E2D63CE30215E FOREIGN KEY (bassin_id) REFERENCES Bassin (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE historique ADD CONSTRAINT FK_A2E2D63CA977DE57 FOREIGN KEY (observatoirecible_id) REFERENCES Observatoire (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE historique ADD CONSTRAINT FK_A2E2D63C76FC9A0D FOREIGN KEY (typeparametre_id) REFERENCES TypeParametre (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE historique ADD CONSTRAINT FK_A2E2D63C5175C848 FOREIGN KEY (siteexperimental_id) REFERENCES SiteExperimental (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE historique ADD CONSTRAINT FK_A2E2D63C60BB6FE6 FOREIGN KEY (auteur_id) REFERENCES Utilisateur (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE historique ADD CONSTRAINT FK_A2E2D63C61ED1B0D FOREIGN KEY (observatoire_id) REFERENCES Observatoire (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE historique ADD CONSTRAINT FK_A2E2D63CFB88E14F FOREIGN KEY (utilisateur_id) REFERENCES Utilisateur (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE historique ADD CONSTRAINT FK_A2E2D63C5F49EAAD FOREIGN KEY (bareme_id) REFERENCES Bareme (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE historique ADD CONSTRAINT FK_A2E2D63C131A4F72 FOREIGN KEY (commune_id) REFERENCES Commune (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE historique ADD CONSTRAINT FK_A2E2D63CEC4A74AB FOREIGN KEY (unite_id) REFERENCES Unite (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE historique ADD CONSTRAINT FK_A2E2D63C21BDB235 FOREIGN KEY (station_id) REFERENCES Station (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE historique ADD CONSTRAINT FK_A2E2D63C98DE13AC FOREIGN KEY (partenaire_id) REFERENCES Partenaire (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_A2E2D63C32164871 ON historique (courseau_id)');
        $this->addSql('CREATE INDEX IDX_A2E2D63CE30215E ON historique (bassin_id)');
        $this->addSql('ALTER INDEX idx_a2e2d63c5001bc01 RENAME TO IDX_A2E2D63CA977DE57');
        $this->addSql('ALTER INDEX idx_a2e2d63c6ecd41f5 RENAME TO IDX_A2E2D63C76FC9A0D');
        $this->addSql('ALTER INDEX idx_a2e2d63cd9b0f6d6 RENAME TO IDX_A2E2D63C5175C848');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE Doi DROP CONSTRAINT fk_5ed9529a61ed1b0d');
        $this->addSql('ALTER TABLE Doi ADD CONSTRAINT fk_5ed9529a61ed1b0d FOREIGN KEY (observatoire_id) REFERENCES observatoire (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE observatoires_partenaires_scientifiques DROP CONSTRAINT fk_5c80b31d61ed1b0d');
        $this->addSql('ALTER TABLE observatoires_partenaires_scientifiques DROP CONSTRAINT fk_5c80b31d98de13ac');
        $this->addSql('ALTER TABLE observatoires_partenaires_scientifiques ADD CONSTRAINT fk_5c80b31d61ed1b0d FOREIGN KEY (observatoire_id) REFERENCES observatoire (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE observatoires_partenaires_scientifiques ADD CONSTRAINT fk_5c80b31d98de13ac FOREIGN KEY (partenaire_id) REFERENCES partenaire (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE observatoires_partenaires_institutionnels DROP CONSTRAINT fk_ddfc381e61ed1b0d');
        $this->addSql('ALTER TABLE observatoires_partenaires_institutionnels DROP CONSTRAINT fk_ddfc381e98de13ac');
        $this->addSql('ALTER TABLE observatoires_partenaires_institutionnels ADD CONSTRAINT fk_ddfc381e61ed1b0d FOREIGN KEY (observatoire_id) REFERENCES observatoire (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE observatoires_partenaires_institutionnels ADD CONSTRAINT fk_ddfc381e98de13ac FOREIGN KEY (partenaire_id) REFERENCES partenaire (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE PointJaugeage DROP CONSTRAINT fk_a228ece75f49eaad');
        $this->addSql('ALTER TABLE PointJaugeage ADD CONSTRAINT fk_a228ece75f49eaad FOREIGN KEY (bareme_id) REFERENCES bareme (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE besoins_parametres DROP CONSTRAINT fk_75a89b03fe6eed44');
        $this->addSql('ALTER TABLE besoins_parametres DROP CONSTRAINT fk_75a89b036358ff62');
        $this->addSql('ALTER TABLE besoins_parametres ADD CONSTRAINT fk_75a89b03fe6eed44 FOREIGN KEY (besoin_id) REFERENCES besoin (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE besoins_parametres ADD CONSTRAINT fk_75a89b036358ff62 FOREIGN KEY (parametre_id) REFERENCES typeparametre (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE besoins_stations DROP CONSTRAINT fk_59777037fe6eed44');
        $this->addSql('ALTER TABLE besoins_stations ADD CONSTRAINT fk_59777037fe6eed44 FOREIGN KEY (besoin_id) REFERENCES besoin (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Chronique DROP CONSTRAINT fk_dac28749ab9bb300');
        $this->addSql('ALTER TABLE Chronique ADD CONSTRAINT fk_dac28749ab9bb300 FOREIGN KEY (producteur_id) REFERENCES partenaire (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Bassin DROP CONSTRAINT FK_660811F390F720D2');
        $this->addSql('ALTER TABLE Bassin DROP CONSTRAINT fk_660811f361ed1b0d');
        $this->addSql('ALTER TABLE Bassin ADD CONSTRAINT fk_660811f3166baf05 FOREIGN KEY (stationexutoire_id) REFERENCES station (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Bassin ADD CONSTRAINT fk_660811f361ed1b0d FOREIGN KEY (observatoire_id) REFERENCES observatoire (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER INDEX idx_660811f390f720d2 RENAME TO idx_660811f3166baf05');
        $this->addSql('ALTER TABLE Role DROP CONSTRAINT fk_f75b255461ed1b0d');
        $this->addSql('ALTER TABLE Role DROP CONSTRAINT fk_f75b2554fb88e14f');
        $this->addSql('ALTER TABLE Role DROP CONSTRAINT fk_f75b2554f6bd1646');
        $this->addSql('ALTER TABLE Role ADD CONSTRAINT fk_f75b255461ed1b0d FOREIGN KEY (observatoire_id) REFERENCES observatoire (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Role ADD CONSTRAINT fk_f75b2554fb88e14f FOREIGN KEY (utilisateur_id) REFERENCES utilisateur (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Role ADD CONSTRAINT fk_f75b2554f6bd1646 FOREIGN KEY (site_id) REFERENCES siteexperimental (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Bareme DROP CONSTRAINT fk_35a1b5d861ed1b0d');
        $this->addSql('ALTER TABLE Bareme ADD CONSTRAINT fk_35a1b5d861ed1b0d FOREIGN KEY (observatoire_id) REFERENCES observatoire (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Station DROP CONSTRAINT fk_5084c12d131a4f72');
        $this->addSql('ALTER TABLE Station ADD CONSTRAINT fk_5084c12d131a4f72 FOREIGN KEY (commune_id) REFERENCES commune (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Besoin DROP CONSTRAINT fk_86b4ed27fb88e14f');
        $this->addSql('ALTER TABLE Besoin DROP CONSTRAINT fk_86b4ed2761ed1b0d');
        $this->addSql('ALTER TABLE Besoin ADD CONSTRAINT fk_86b4ed27fb88e14f FOREIGN KEY (utilisateur_id) REFERENCES utilisateur (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Besoin ADD CONSTRAINT fk_86b4ed2761ed1b0d FOREIGN KEY (observatoire_id) REFERENCES observatoire (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Historique DROP CONSTRAINT FK_A2E2D63C32164871');
        $this->addSql('ALTER TABLE Historique DROP CONSTRAINT FK_A2E2D63CE30215E');
        $this->addSql('ALTER TABLE Historique DROP CONSTRAINT FK_A2E2D63CA977DE57');
        $this->addSql('ALTER TABLE Historique DROP CONSTRAINT FK_A2E2D63C76FC9A0D');
        $this->addSql('ALTER TABLE Historique DROP CONSTRAINT FK_A2E2D63C5175C848');
        $this->addSql('ALTER TABLE Historique DROP CONSTRAINT fk_a2e2d63c60bb6fe6');
        $this->addSql('ALTER TABLE Historique DROP CONSTRAINT fk_a2e2d63c61ed1b0d');
        $this->addSql('ALTER TABLE Historique DROP CONSTRAINT fk_a2e2d63cfb88e14f');
        $this->addSql('ALTER TABLE Historique DROP CONSTRAINT fk_a2e2d63c5f49eaad');
        $this->addSql('ALTER TABLE Historique DROP CONSTRAINT fk_a2e2d63c131a4f72');
        $this->addSql('ALTER TABLE Historique DROP CONSTRAINT fk_a2e2d63cec4a74ab');
        $this->addSql('ALTER TABLE Historique DROP CONSTRAINT fk_a2e2d63c21bdb235');
        $this->addSql('ALTER TABLE Historique DROP CONSTRAINT fk_a2e2d63c98de13ac');
        $this->addSql('DROP INDEX IDX_A2E2D63C32164871');
        $this->addSql('DROP INDEX IDX_A2E2D63CE30215E');
        $this->addSql('ALTER TABLE Historique DROP courseau_id');
        $this->addSql('ALTER TABLE Historique DROP bassin_id');
        $this->addSql('ALTER TABLE Historique ADD CONSTRAINT fk_a2e2d63cd9b0f6d6 FOREIGN KEY (siteexperimental_id) REFERENCES siteexperimental (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Historique ADD CONSTRAINT fk_a2e2d63c6ecd41f5 FOREIGN KEY (typeparametre_id) REFERENCES typeparametre (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Historique ADD CONSTRAINT fk_a2e2d63c5001bc01 FOREIGN KEY (observatoirecible_id) REFERENCES observatoire (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Historique ADD CONSTRAINT fk_a2e2d63c60bb6fe6 FOREIGN KEY (auteur_id) REFERENCES utilisateur (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Historique ADD CONSTRAINT fk_a2e2d63c61ed1b0d FOREIGN KEY (observatoire_id) REFERENCES observatoire (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Historique ADD CONSTRAINT fk_a2e2d63cfb88e14f FOREIGN KEY (utilisateur_id) REFERENCES utilisateur (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Historique ADD CONSTRAINT fk_a2e2d63c5f49eaad FOREIGN KEY (bareme_id) REFERENCES bareme (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Historique ADD CONSTRAINT fk_a2e2d63c131a4f72 FOREIGN KEY (commune_id) REFERENCES commune (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Historique ADD CONSTRAINT fk_a2e2d63cec4a74ab FOREIGN KEY (unite_id) REFERENCES unite (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Historique ADD CONSTRAINT fk_a2e2d63c21bdb235 FOREIGN KEY (station_id) REFERENCES station (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Historique ADD CONSTRAINT fk_a2e2d63c98de13ac FOREIGN KEY (partenaire_id) REFERENCES partenaire (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER INDEX idx_a2e2d63ca977de57 RENAME TO idx_a2e2d63c5001bc01');
        $this->addSql('ALTER INDEX idx_a2e2d63c76fc9a0d RENAME TO idx_a2e2d63c6ecd41f5');
        $this->addSql('ALTER INDEX idx_a2e2d63c5175c848 RENAME TO idx_a2e2d63cd9b0f6d6');
    }
}
