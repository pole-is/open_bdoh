<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210219121324 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE liste_chroniques_dataset DROP CONSTRAINT fk_e501b452af9ceade');
        $this->addSql('ALTER TABLE chronique DROP CONSTRAINT fk_dac287491ce2a12b');
        $this->addSql('DROP SEQUENCE bdoh.listechroniques_id_seq CASCADE');
        $this->addSql('CREATE TABLE chroniques_dataSet (dataset_id INT NOT NULL DEFAULT NULL, chronique_id INT NOT NULL DEFAULT NULL, PRIMARY KEY(dataset_id, chronique_id))');
        $this->addSql('CREATE INDEX IDX_2F0EE2DED47C2D1B ON chroniques_dataSet (dataset_id)');
        $this->addSql('CREATE INDEX IDX_2F0EE2DE93054D ON chroniques_dataSet (chronique_id)');
        $this->addSql('ALTER TABLE chroniques_dataSet ADD CONSTRAINT FK_2F0EE2DED47C2D1B FOREIGN KEY (dataset_id) REFERENCES DataSet (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chroniques_dataSet ADD CONSTRAINT FK_2F0EE2DE93054D FOREIGN KEY (chronique_id) REFERENCES Chronique (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE liste_chroniques_dataset');
        $this->addSql('DROP TABLE listechroniques');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE bdoh.listechroniques_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE liste_chroniques_dataset (dataset_id INT NOT NULL, listechroniques_id INT NOT NULL, PRIMARY KEY(dataset_id, listechroniques_id))');
        $this->addSql('CREATE INDEX idx_e501b452d47c2d1b ON liste_chroniques_dataset (dataset_id)');
        $this->addSql('CREATE INDEX idx_e501b452af9ceade ON liste_chroniques_dataset (listechroniques_id)');
        $this->addSql('CREATE TABLE listechroniques (id INT NOT NULL, chronique_id INT DEFAULT NULL, nom VARCHAR(255) DEFAULT NULL, supprimer BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX uniq_8cf9ad6293054d ON listechroniques (chronique_id)');
        $this->addSql('ALTER TABLE liste_chroniques_dataset ADD CONSTRAINT fk_e501b452af9ceade FOREIGN KEY (listechroniques_id) REFERENCES listechroniques (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE liste_chroniques_dataset ADD CONSTRAINT fk_e501b452d47c2d1b FOREIGN KEY (dataset_id) REFERENCES dataset (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE listechroniques ADD CONSTRAINT fk_8cf9ad6293054d FOREIGN KEY (chronique_id) REFERENCES chronique (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE chroniques_dataSet');
        $this->addSql('ALTER TABLE Chronique ADD listechroniques_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE Chronique ALTER miseAJour TYPE TIMESTAMP(3) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE Chronique ALTER miseAJour DROP DEFAULT');
        $this->addSql('ALTER TABLE Chronique ADD CONSTRAINT fk_dac287491ce2a12b FOREIGN KEY (listechroniques_id) REFERENCES listechroniques (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_dac287491ce2a12b ON Chronique (listechroniques_id)');
    }
}
