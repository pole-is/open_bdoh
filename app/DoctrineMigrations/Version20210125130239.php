<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210125130239 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE dataset DROP CONSTRAINT FK_40503EACD8B5F69');
        $this->addSql('ALTER TABLE dataset ADD chronique_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE dataset ADD CONSTRAINT FK_40503EAC93054D FOREIGN KEY (chronique_id) REFERENCES Chronique (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dataset ADD CONSTRAINT FK_40503EACD8B5F69 FOREIGN KEY (dataconstraints_id) REFERENCES DataConstraint (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_40503EAC93054D ON dataset (chronique_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE DataSet DROP CONSTRAINT FK_40503EAC93054D');
        $this->addSql('ALTER TABLE DataSet DROP CONSTRAINT fk_40503eacd8b5f69');
        $this->addSql('DROP INDEX IDX_40503EAC93054D');
        $this->addSql('ALTER TABLE DataSet DROP chronique_id');
        $this->addSql('ALTER TABLE DataSet ADD CONSTRAINT fk_40503eacd8b5f69 FOREIGN KEY (dataconstraints_id) REFERENCES dataconstraint (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
