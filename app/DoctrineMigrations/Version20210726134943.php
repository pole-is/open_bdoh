<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210726134943 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE observatoire ALTER miseajourautomatique DROP DEFAULT');
        $this->addSql('ALTER TABLE observatoires_data_manager DROP CONSTRAINT fk_26c6793a98de13ac');
        $this->addSql('DROP INDEX idx_26c6793a98de13ac');
        $this->addSql('ALTER TABLE observatoires_data_manager DROP CONSTRAINT observatoires_data_manager_pkey');
        $this->addSql('ALTER TABLE observatoires_data_manager RENAME COLUMN partenaire_id TO personnetheia_id');
        $this->addSql('ALTER TABLE observatoires_data_manager ADD CONSTRAINT FK_26C6793A89274090 FOREIGN KEY (personnetheia_id) REFERENCES PersonneTheia (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_26C6793A89274090 ON observatoires_data_manager (personnetheia_id)');
        $this->addSql('ALTER TABLE observatoires_data_manager ADD PRIMARY KEY (observatoire_id, personnetheia_id)');
        $this->addSql('ALTER TABLE personnetheia DROP CONSTRAINT fk_3ba3d4e338898cf5');
        $this->addSql('ALTER TABLE personnetheia DROP CONSTRAINT FK_3BA3D4E3FB88E14F');
        $this->addSql('DROP INDEX idx_3ba3d4e338898cf5');
        $this->addSql('ALTER TABLE personnetheia RENAME COLUMN partenaires_id TO partenaire_id');
        $this->addSql('ALTER TABLE personnetheia ADD CONSTRAINT FK_3BA3D4E398DE13AC FOREIGN KEY (partenaire_id) REFERENCES Partenaire (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE personnetheia ADD CONSTRAINT FK_3BA3D4E3FB88E14F FOREIGN KEY (utilisateur_id) REFERENCES Utilisateur (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_3BA3D4E398DE13AC ON personnetheia (partenaire_id)');
        $this->addSql('ALTER TABLE dataset DROP CONSTRAINT FK_40503EACD8B5F69');
        $this->addSql('DROP INDEX idx_40503eacd8b5f69');
        $this->addSql('ALTER TABLE dataset RENAME COLUMN dataconstraints_id TO dataconstraint_id');
        $this->addSql('ALTER TABLE dataset ADD CONSTRAINT FK_40503EAC2FA57584 FOREIGN KEY (dataconstraint_id) REFERENCES DataConstraint (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_40503EAC2FA57584 ON dataset (dataconstraint_id)');
        $this->addSql('ALTER TABLE theiacategories ALTER nom SET NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE Observatoire ALTER miseAJourAutomatique SET DEFAULT \'false\'');
        $this->addSql('ALTER TABLE PersonneTheia DROP CONSTRAINT FK_3BA3D4E398DE13AC');
        $this->addSql('ALTER TABLE PersonneTheia DROP CONSTRAINT fk_3ba3d4e3fb88e14f');
        $this->addSql('DROP INDEX IDX_3BA3D4E398DE13AC');
        $this->addSql('ALTER TABLE PersonneTheia RENAME COLUMN partenaire_id TO partenaires_id');
        $this->addSql('ALTER TABLE PersonneTheia ADD CONSTRAINT fk_3ba3d4e338898cf5 FOREIGN KEY (partenaires_id) REFERENCES partenaire (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE PersonneTheia ADD CONSTRAINT fk_3ba3d4e3fb88e14f FOREIGN KEY (utilisateur_id) REFERENCES utilisateur (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_3ba3d4e338898cf5 ON PersonneTheia (partenaires_id)');
        $this->addSql('ALTER TABLE observatoires_data_manager DROP CONSTRAINT FK_26C6793A89274090');
        $this->addSql('DROP INDEX IDX_26C6793A89274090');
        $this->addSql('DROP INDEX observatoires_data_manager_pkey');
        $this->addSql('ALTER TABLE observatoires_data_manager RENAME COLUMN personnetheia_id TO partenaire_id');
        $this->addSql('ALTER TABLE observatoires_data_manager ADD CONSTRAINT fk_26c6793a98de13ac FOREIGN KEY (partenaire_id) REFERENCES personnetheia (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_26c6793a98de13ac ON observatoires_data_manager (partenaire_id)');
        $this->addSql('ALTER TABLE observatoires_data_manager ADD PRIMARY KEY (observatoire_id, partenaire_id)');
        $this->addSql('ALTER TABLE TheiaCategories ALTER nom DROP NOT NULL');
        $this->addSql('ALTER TABLE DataSet DROP CONSTRAINT fk_40503eacd8b5f69');
        $this->addSql('ALTER TABLE DataSet ADD CONSTRAINT fk_40503eacd8b5f69 FOREIGN KEY (dataconstraints_id) REFERENCES dataconstraint (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
