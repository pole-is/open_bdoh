<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210707132605 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        //Passage en migration des famille de paramètre
        $this->addSql('CREATE TEMP TABLE newfamilleparametres (nom VARCHAR(256), nomEn VARCHAR(256), prefix VARCHAR(256), nomfamilleparente VARCHAR(256))');
        $this->addSql(
            <<<EOF
            INSERT INTO newfamilleparametres VALUES
               ('Hydrologie', 'Hydrology', '001', null),
               ('Atmosphère - Météorologie', 'Atmosphere - Meteorology', '002', null),
               ('Chimie', 'Chemistry', '003',  null),
               ('Général', 'General', '1', 'Chimie')
EOF
        );

        // Insère/met à jour les familles de paramètres
        $this->addSql(
            <<<EOF
            INSERT INTO familleparametres (id, nom, nomEn, prefix, familleparente_id)
                SELECT nextval('familleparametres_id_seq'), nf.nom, nf.nomEn, nf.prefix, null
                FROM newfamilleparametres nf
            ON CONFLICT (nom)
                DO UPDATE SET nomEn = excluded.nomEn, prefix = excluded.prefix
EOF
        );

        // Met à jour les relations hiérarchiques
        $this->addSql(
            <<<EOF
            UPDATE familleparametres f set familleparente_id = pf.id
                FROM newfamilleparametres nf
                    INNER JOIN familleparametres pf ON (nf.nomfamilleparente = pf.nom)
                WHERE nf.nom = f.nom
EOF
        );

        $this->addSql("SELECT setval('familleparametres_id_seq', MAX(id)) FROM familleparametres");

        //Passage en migration des type de paramètre
        $this->addSql('CREATE TEMP TABLE newtypeparametre (nom VARCHAR(256), nomEn VARCHAR(256), code VARCHAR(256), famille VARCHAR(256))');
        $this->addSql(
            <<<EOF
            INSERT INTO newtypeparametre VALUES
                ('Hauteur d''eau', 'Water height', 'HT', 'Hydrologie'),
                ('Débit', 'Discharge', 'DEB', 'Hydrologie'),
                ('Température de l''eau', 'Water temperature', 'TEMPE', 'Hydrologie'),
                ('Température de l''air', 'Air temperature', 'HT', 'Atmosphère - Météorologie'),
                ('Niveau piézométrique', 'Groundwater level', 'PZ', 'Hydrologie'),
                ('Vitesse de l''eau', 'Water speed', 'VE', 'Hydrologie'),
                ('Vitesse du vent', 'Wind speed', 'VV', 'Atmosphère - Météorologie'),
                ('Direction du vent', 'Wind direction', 'DIRV', 'Atmosphère - Météorologie'),
                ('Précipitation', 'Precipitation', 'PRCP', 'Atmosphère - Météorologie'),
                ('Pression atmosphérique', 'Atmospheric pressure', 'PA', 'Atmosphère - Météorologie'),
                ('Volume d''eau', 'Water volume', 'VOLE', 'Hydrologie'),
                ('Humidité de l''air', 'Humidity', 'HUMA', 'Atmosphère - Météorologie'),
                ('pH', 'pH', 'PH', 'Général'),
                ('Conductivité', 'Conductivity', 'COND', 'Général')
EOF
        );

        $this->addSql(
            <<<EOF
            INSERT INTO typeparametre (id, nom, nomEn, code, familleparametres_id)
                SELECT nextval('typeparametre_id_seq'), ntp.nom, ntp.nomEn, ntp.code, f.id
                FROM newtypeparametre ntp
                    INNER JOIN familleparametres f ON (ntp.famille = f.nom)
            ON CONFLICT (nom)
                DO UPDATE SET nomEn = excluded.nomEn, code = excluded.code, familleparametres_id = excluded.familleparametres_id
EOF
        );

        $this->addSql("SELECT setval('typeparametre_id_seq', MAX(id)) FROM typeparametre");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DELETE FROM familleparametres');
        $this->addSql('DELETE FROM typeparametre');
    }
}
