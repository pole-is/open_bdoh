<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 * @SuppressWarnings(PHPMD.ShortMethodName)
 */
class Version20170412121937 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            <<<'SQL'
            CREATE FUNCTION bdoh_segments(
                chronique_id mesure.chronique_id%TYPE,
                start_date mesure.date%TYPE,
                end_date mesure.date%TYPE
            )
            RETURNS TABLE(
                start_date mesure.date%TYPE,
                end_date mesure.date%TYPE,
                start_valeur mesure.valeur%TYPE,
                end_valeur mesure.valeur%TYPE,
                start_qualite_id mesure.qualite_id%TYPE,
                end_qualite_id mesure.qualite_id%TYPE
            )
            STABLE
            LANGUAGE 'sql'
            AS $$
                 SELECT
                        lag(date) OVER w,
                        m.date,
                        lag(valeur) OVER w,
                        m.valeur,
                        lag(qualite_id) OVER w,
                        m.qualite_id
                    FROM mesure m
                    WHERE m.chronique_id = $1 AND m.date BETWEEN $2 AND $3
                    WINDOW w AS (
                        PARTITION BY chronique_id
                        ORDER BY date ASC
                        ROWS BETWEEN 1 PRECEDING AND CURRENT ROW
                    )
            $$
SQL
        );

        $this->addSql(
            <<<'SQL'
            CREATE FUNCTION bdoh_interpolate_linear(
                start_date mesure.date%TYPE,
                start_valeur mesure.valeur%TYPE,
                end_date mesure.date%TYPE,
                end_valeur mesure.valeur%TYPE,
                date mesure.date%TYPE
            )
            RETURNS mesure.valeur%TYPE
            IMMUTABLE
            LANGUAGE 'sql'
            AS $$
                 SELECT $2 + ($4 - $2) * EXTRACT(EPOCH FROM ($5 - $1)) / EXTRACT(EPOCH FROM ($3 - $1));
            $$
SQL
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('DROP FUNCTION bdoh_interpolate_linear(
            start_date mesure.date%TYPE,
            start_valeur mesure.valeur%TYPE,
            end_date mesure.date%TYPE,
            end_valeur mesure.valeur%TYPE,
            date mesure.date%TYPE
        )');

        $this->addSql('DROP FUNCTION bdoh_segments(
            chronique_id mesure.chronique_id%TYPE,
            start_date mesure.date%TYPE,
            end_date mesure.date%TYPE
        )');
    }
}
