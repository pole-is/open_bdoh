<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20201214115639 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
        $this->addSql('CREATE SEQUENCE TopicCategory_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE TopicCategory (id INT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE topicCat_DataSet (dataset_id INT NOT NULL, topiccategory_id INT NOT NULL, PRIMARY KEY(dataset_id, topiccategory_id))');
        $this->addSql('CREATE INDEX IDX_40656D7FD47C2D1B ON topicCat_DataSet (dataset_id)');
        $this->addSql('CREATE INDEX IDX_40656D7FB45EC3B0 ON topicCat_DataSet (topiccategory_id)');
        $this->addSql('ALTER TABLE topicCat_DataSet ADD CONSTRAINT FK_40656D7FD47C2D1B FOREIGN KEY (dataset_id) REFERENCES DataSet (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE topicCat_DataSet ADD CONSTRAINT FK_40656D7FB45EC3B0 FOREIGN KEY (topiccategory_id) REFERENCES TopicCategory (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dataset DROP topiccategory');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE topicCat_DataSet DROP CONSTRAINT FK_40656D7FB45EC3B0');
        $this->addSql('DROP SEQUENCE TopicCategory_id_seq CASCADE');
        $this->addSql('DROP TABLE TopicCategory');
        $this->addSql('DROP TABLE topicCat_DataSet');
        $this->addSql('COMMENT ON COLUMN jms_jobs.args IS NULL');
        $this->addSql('DROP INDEX UNIQ_90B54B32EC0C1EEE');
        $this->addSql('ALTER TABLE DataSet ADD topiccategory TEXT NOT NULL');
    }
}
