<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210319104109 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE theiacategories ADD milieu_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE theiacategories DROP milieu');
        $this->addSql('ALTER TABLE theiacategories ADD CONSTRAINT FK_CDEB484B735F057F FOREIGN KEY (milieu_id) REFERENCES Milieu (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_CDEB484B735F057F ON theiacategories (milieu_id)');
        $this->addSql('DROP INDEX uniq_dac287499f990a5e');
        $this->addSql('ALTER TABLE chronique ADD milieu_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE chronique DROP milieu');
        $this->addSql('ALTER TABLE chronique ADD CONSTRAINT FK_DAC28749735F057F FOREIGN KEY (milieu_id) REFERENCES Milieu (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_DAC28749735F057F ON chronique (milieu_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE TheiaCategories DROP CONSTRAINT FK_CDEB484B735F057F');
        $this->addSql('DROP INDEX IDX_CDEB484B735F057F');
        $this->addSql('ALTER TABLE TheiaCategories ADD milieu VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE TheiaCategories DROP milieu_id');
        $this->addSql('ALTER TABLE Chronique DROP CONSTRAINT FK_DAC28749735F057F');
        $this->addSql('DROP INDEX IDX_DAC28749735F057F');
        $this->addSql('ALTER TABLE Chronique ADD milieu VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE Chronique DROP milieu_id');
        $this->addSql('CREATE UNIQUE INDEX uniq_dac287499f990a5e ON Chronique (milieu)');
    }
}
