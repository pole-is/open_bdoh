<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210601144856 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        //Traduction dans Valide de Hydro 2 en Valide
        $this->addSql('CREATE TEMP TABLE newqualitejeuqualite (jeu_dest VARCHAR(256), code_dest VARCHAR(256), jeu_src VARCHAR(256), code_src VARCHAR(256))');
        $this->addSql(
            <<<EOF
            INSERT INTO newqualitejeuqualite VALUES
                ( 'Hydro2', '0',   'VALIDE', 'a'),
                ( 'Hydro2', '5',   'VALIDE', 'd'),
                ( 'Hydro2', '9',   'VALIDE', 'v'),
                ( 'Hydro2', 'gap', 'VALIDE', 'gap'),
                ( 'Hydro2', 'gap', 'VALIDE', 'l'),
                ( 'VALIDE', 'a',   'Hydro2', '0'),
                ( 'VALIDE', 'd',   'Hydro2', '5'),
                ( 'VALIDE', 'd',   'Hydro2', 'I'),
                ( 'VALIDE', 'd',   'Hydro2', 'S'),
                ( 'VALIDE', 'l',   'Hydro2', 'gap'),
                ( 'VALIDE', 'v',   'Hydro2', '8'),
                ( 'VALIDE', 'v',   'Hydro2', '9'),
                ( 'VALIDE', 'a',   'VALIDE', 'a'),
                ( 'VALIDE', 'd',   'VALIDE', 'd'),
                ( 'VALIDE', 'e',   'VALIDE', 'e'),
                ( 'VALIDE', 'i',   'VALIDE', 'i'),
                ( 'VALIDE', 'l',   'VALIDE', 'gap'),
                ( 'VALIDE', 'l',   'VALIDE', 'l'),
                ( 'VALIDE', 'ld',  'VALIDE', 'ld'),
                ( 'VALIDE', 'lq',  'VALIDE', 'lq'),
                ( 'VALIDE', 'v',   'VALIDE', 'v')
EOF
        );

        // Déduplique
        $this->addSql(
            <<<EOF
            WITH dupes AS (
                SELECT array_remove(array_agg(id), min(id)) AS to_delete
                FROM qualitejeuqualite
                GROUP BY jeu_id, traduction_id, qualite_id
                HAVING count(DISTINCT id) > 1
            )
            DELETE FROM qualitejeuqualite qjq USING dupes d WHERE qjq.id = ANY (d.to_delete)
EOF
        );

        $this->addSql('ALTER TABLE qualitejeuqualite ADD CONSTRAINT unique_triplet UNIQUE (traduction_id, jeu_id, qualite_id)');

        $this->addSql(
            <<<EOF
        INSERT INTO qualitejeuqualite (id, traduction_id, jeu_id, qualite_id)
            SELECT
                nextval('qualitejeuqualite_id_seq'),
                dest_q.id,
                dest_q.jeu_id,
                src_q.id
            FROM
                newqualitejeuqualite new
                    INNER JOIN jeuqualite dest_jeu ON (new.jeu_dest = dest_jeu.nom)
                    INNER JOIN qualite dest_q ON (new.code_dest = dest_q.code AND dest_q.jeu_id = dest_jeu.id)
                    INNER JOIN jeuqualite src_jeu ON (new.jeu_src = src_jeu.nom)
                    INNER JOIN qualite src_q ON (new.code_src = src_q.code AND src_q.jeu_id = src_jeu.id)
        ON CONFLICT (traduction_id, jeu_id, qualite_id)
            DO NOTHING
EOF
        );

        $this->addSql('ALTER TABLE qualitejeuqualite DROP CONSTRAINT unique_triplet');

        $this->addSql("SELECT setval('qualitejeuqualite_id_seq', MAX(id)) FROM qualitejeuqualite");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('DELETE FROM qualitejeuqualite');
    }
}
