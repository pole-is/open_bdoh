<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20201214152230 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE CriteriaClimat_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE CriteriaGeology_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE CriteriaClimat (id INT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE CriteriaGeology (id INT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE criteria_geology_dataSet (dataset_id INT NOT NULL, criteriageology_id INT NOT NULL, PRIMARY KEY(dataset_id, criteriageology_id))');
        $this->addSql('CREATE INDEX IDX_5C567A19D47C2D1B ON criteria_geology_dataSet (dataset_id)');
        $this->addSql('CREATE INDEX IDX_5C567A1979262535 ON criteria_geology_dataSet (criteriageology_id)');
        $this->addSql('CREATE TABLE criteria_climat_dataSet (dataset_id INT NOT NULL, criteriaclimat_id INT NOT NULL, PRIMARY KEY(dataset_id, criteriaclimat_id))');
        $this->addSql('CREATE INDEX IDX_1F40E8D1D47C2D1B ON criteria_climat_dataSet (dataset_id)');
        $this->addSql('CREATE INDEX IDX_1F40E8D12929DC79 ON criteria_climat_dataSet (criteriaclimat_id)');
        $this->addSql('ALTER TABLE criteria_geology_dataSet ADD CONSTRAINT FK_5C567A19D47C2D1B FOREIGN KEY (dataset_id) REFERENCES DataSet (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE criteria_geology_dataSet ADD CONSTRAINT FK_5C567A1979262535 FOREIGN KEY (criteriageology_id) REFERENCES CriteriaGeology (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE criteria_climat_dataSet ADD CONSTRAINT FK_1F40E8D1D47C2D1B FOREIGN KEY (dataset_id) REFERENCES DataSet (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE criteria_climat_dataSet ADD CONSTRAINT FK_1F40E8D12929DC79 FOREIGN KEY (criteriaclimat_id) REFERENCES CriteriaClimat (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dataset DROP portailsearchcriteriaclimat');
        $this->addSql('ALTER TABLE dataset DROP portailsearchcriteriageology');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE criteria_climat_dataSet DROP CONSTRAINT FK_1F40E8D12929DC79');
        $this->addSql('ALTER TABLE criteria_geology_dataSet DROP CONSTRAINT FK_5C567A1979262535');
        $this->addSql('DROP SEQUENCE CriteriaClimat_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE CriteriaGeology_id_seq CASCADE');
        $this->addSql('DROP TABLE CriteriaClimat');
        $this->addSql('DROP TABLE CriteriaGeology');
        $this->addSql('DROP TABLE criteria_geology_dataSet');
        $this->addSql('DROP TABLE criteria_climat_dataSet');
        $this->addSql('ALTER TABLE DataSet ADD portailsearchcriteriaclimat TEXT NOT NULL');
        $this->addSql('ALTER TABLE DataSet ADD portailsearchcriteriageology TEXT NOT NULL');
    }
}
