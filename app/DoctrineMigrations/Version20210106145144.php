<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210106145144 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE PersonneTheiaEtRolePersonneTheia_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE PersonneTheiaEtRolePersonneTheia (id INT NOT NULL, personnetheia_id INT DEFAULT NULL, rolepersonnetheia_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_76F8E79089274090 ON PersonneTheiaEtRolePersonneTheia (personnetheia_id)');
        $this->addSql('CREATE INDEX IDX_76F8E7903B2FBBD6 ON PersonneTheiaEtRolePersonneTheia (rolepersonnetheia_id)');
        $this->addSql('CREATE TABLE contact_theia_dataSet (dataset_id INT NOT NULL, contacttheia_id INT NOT NULL, PRIMARY KEY(dataset_id, contacttheia_id))');
        $this->addSql('CREATE INDEX IDX_A311B0E2D47C2D1B ON contact_theia_dataSet (dataset_id)');
        $this->addSql('CREATE INDEX IDX_A311B0E2CD8785EC ON contact_theia_dataSet (contacttheia_id)');
        $this->addSql('ALTER TABLE PersonneTheiaEtRolePersonneTheia ADD CONSTRAINT FK_76F8E79089274090 FOREIGN KEY (personnetheia_id) REFERENCES PersonneTheia (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE PersonneTheiaEtRolePersonneTheia ADD CONSTRAINT FK_76F8E7903B2FBBD6 FOREIGN KEY (rolepersonnetheia_id) REFERENCES RolePersonneTheia (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE contact_theia_dataSet ADD CONSTRAINT FK_A311B0E2D47C2D1B FOREIGN KEY (dataset_id) REFERENCES DataSet (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE contact_theia_dataSet ADD CONSTRAINT FK_A311B0E2CD8785EC FOREIGN KEY (contacttheia_id) REFERENCES PersonneTheiaEtRolePersonneTheia (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE contact_theia_dataSet DROP CONSTRAINT FK_A311B0E2CD8785EC');
        $this->addSql('DROP SEQUENCE PersonneTheiaEtRolePersonneTheia_id_seq CASCADE');
        $this->addSql('DROP TABLE PersonneTheiaEtRolePersonneTheia');
        $this->addSql('DROP TABLE contact_theia_dataSet');
    }
}
