<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Migrations\IrreversibleMigrationException;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210630075833 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        // Ces deux commandes sont exécutées immédiatement plutôt que dans le flot des migrations
        $conn = $this->connection->getWrappedConnection();
        $conn->exec('
            CREATE TEMPORARY TABLE newcommune(nom VARCHAR(255) NOT NULL, nomcomplet VARCHAR(255) NOT NULL, codePostal
    VARCHAR(5) NOT NULL, codeInsee VARCHAR(5) NOT NULL, CONSTRAINT unique_postal_insee UNIQUE (codeInsee, codePostal));
        ');
        $conn->pgsqlCopyFromFile('newcommune', 'compress.zlib://' . __DIR__ . '/files/communes-2020.csv.gz', ',', 'NULL', 'codeinsee,codepostal,nomcomplet,nom');

        // Supprime les communes non référencées par les autres tables
        $this->addSql(
            <<<SQL
            DELETE FROM commune WHERE id NOT IN (
                    SELECT commune_id FROM station WHERE commune_id IS NOT NULL
                UNION
                    SELECT commune_id FROM historique WHERE commune_id IS NOT NULL
            )
SQL
        );

        // Retrouve les codes postaux manquants à partir du code INSEE (risque mineur d'erreur)
        $this->addSql('UPDATE commune c SET codePostal = nc.codePostal FROM newcommune nc WHERE
            c.codePostal IS NULL AND c.codeInsee = nc.codeInsee');

        // Déduplique les communes
        // 1. crée une table temporaire avec les ids des doublons et un id que l'on conservera
        // 2. met à jour les references
        // 3. supprime les doublons
        $this->addSql(
            <<<SQL
            CREATE TEMPORARY TABLE commune_dupes AS
                SELECT min(id) AS winner, array_remove(array_agg(id), min(id)) AS losers
                FROM commune
                GROUP BY (codeInsee, codePostal)
                HAVING count(id) > 1
SQL
        );
        $this->addSql('UPDATE station s SET commune_id = d.winner FROM commune_dupes d WHERE s.commune_id = ANY (d.losers)');
        $this->addSql('UPDATE historique h SET commune_id = d.winner FROM commune_dupes d WHERE h.commune_id = ANY (d.losers)');
        $this->addSql('DELETE FROM commune c USING commune_dupes d WHERE c.id = ANY (d.losers)');

        // Réajuste le générateur d'id (optionel)
        $this->addSql("SELECT setval('commune_id_seq', max(id)) FROM commune");

        // Met à jour la table
        $this->addSql("ALTER TABLE commune ALTER COLUMN id SET DEFAULT nextval('commune_id_seq');");
        $this->addSql('ALTER TABLE commune ADD CONSTRAINT unique_codes UNIQUE (codePostal, codeInsee);');

        // Upsert les nouvelles communes
        $this->addSql('INSERT INTO commune(nom, codeInsee, codePostal)
            SELECT nc.nomComplet, nc.codeInsee, nc.codePostal FROM newcommune nc
            ON CONFLICT ON CONSTRAINT unique_codes DO UPDATE SET nom = excluded.nom');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        throw new IrreversibleMigrationException();
    }
}
