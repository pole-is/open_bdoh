<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210707125426 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        // Corrige les m^2 et m^3 des unités existantes
        $this->addSql("UPDATE unite SET libelle = REPLACE(libelle, 'm2', 'm²'), libelleEn = REPLACE(libelleEn, 'm2', 'm²')");
        $this->addSql("UPDATE unite SET libelle = REPLACE(libelle, 'm3', 'm³'), libelleEn = REPLACE(libelleEn, 'm3', 'm³')");

        $this->addSql("INSERT INTO
    unite (id, libelle, libelleEn)
VALUES
       (nextval('unite_id_seq'), 'm', 'm' ),
       (nextval('unite_id_seq'), 'mm', 'mm' ),
       (nextval('unite_id_seq'), 'cm', 'cm' ),
       (nextval('unite_id_seq'), 'L/s', 'L/s' ),
       (nextval('unite_id_seq'), 'm³/s', 'm³/s' ),
       (nextval('unite_id_seq'), 'L', 'L' ),
       (nextval('unite_id_seq'), '%', '%' ),
       (nextval('unite_id_seq'), 'g', 'g' ),
       (nextval('unite_id_seq'), 'kg', 'kg' ),
       (nextval('unite_id_seq'), 'T', 'T' ),
       (nextval('unite_id_seq'), 'm³', 'm³' ),
       (nextval('unite_id_seq'), 'degrés', 'degrees' ),
       (nextval('unite_id_seq'), '°C', '°C' ),
       (nextval('unite_id_seq'), 'm/s', 'm/s' ),
       (nextval('unite_id_seq'), 'km/h', 'km/h' ),
       (nextval('unite_id_seq'), 'mm/h', 'mm/h' ),
       (nextval('unite_id_seq'), 'L/m²', 'L/m²' ),
       (nextval('unite_id_seq'), 'hPa', 'hPa' ),
       (nextval('unite_id_seq'), 'mbar', 'mbar' ),
       (nextval('unite_id_seq'), 'µS/cm', 'µS/cm' )

       ON CONFLICT (libelle)
            DO UPDATE SET libelleEn = excluded.libelleEn;
");

        $this->addSql("SELECT setval('unite_id_seq', MAX(id)) FROM unite");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('DELETE FROM unite');
    }
}
