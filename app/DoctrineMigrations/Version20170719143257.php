<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 * @SuppressWarnings(PHPMD.ShortMethodName)
 */
class Version20170719143257 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_update_filling_rates(
    chro_id chronique.id%TYPE,
    startDate TIMESTAMP(3) DEFAULT '-infinity',
    endDate TIMESTAMP(3) DEFAULT 'infinity'
)
RETURNS INTEGER
VOLATILE STRICT
LANGUAGE 'plpgsql'
AS $$
DECLARE
    discontinue BOOLEAN;
    mesCount INTEGER;
    firstDate TIMESTAMP(3);
    lastDate TIMESTAMP(3);
    tauxCount INTEGER;
BEGIN
    -- Récupère des informations sur la chronique
    SELECT
            dtype = 'discontinue',
            nbmesures,
            datedebutmesures,
            datefinmesures
        INTO discontinue, mesCount, firstDate, lastDate
        FROM chronique
        WHERE id = chro_id;

    -- Nous ne voulons pas de taux de remplissage pour les chroniques discontinues
    IF discontinue OR mesCount = 0 THEN
        DELETE FROM tauxremplissage WHERE chronique_id = chro_id;
        RETURN 0;
    END IF;

    -- Limite la période de recalcul
    IF firstDate > startDate THEN
        startDate := firstDate;
    END IF;
    IF lastDate < endDate THEN
        endDate := lastDate;
    END IF;

    RAISE INFO 'Chronicle #%: updating filling rate from % to %', chro_id, startDate, endDate;

    -- Supprime les taux de remplissage en dehors des périodes de données, ou dans la période à recalculer.
    DELETE FROM tauxremplissage
        WHERE (NOT (debut, fin) OVERLAPS (firstDate, lastDate)
                OR (debut, fin) OVERLAPS (startDate, endDate))
        AND chronique_id = chro_id;

    -- Recalcule les taux de remplissages inclus la période demandée
    INSERT INTO tauxremplissage(id, chronique_id, debut, fin, poids, taux) (
        WITH
            mesures(qualite_id, debut, fin) AS (
                SELECT
                    m.qualite_id,
                    m.date,
                    LEAD(m.date, 1, endDate) OVER (ORDER BY date ROWS BETWEEN CURRENT ROW AND 1 FOLLOWING)
                FROM mesure m
                WHERE m.chronique_id = chro_id
            ),
            valides(debut, fin) AS (
                SELECT
                    debut,
                    fin
                FROM mesures
                WHERE qualite_id NOT IN (SELECT id FROM qualite WHERE ordre = 100)
            ),
            mois(debut, fin, duration) AS (
                SELECT
                    t,
                    t + '1 month',
                    EXTRACT('epoch' FROM ((t + '1 month') - t))
                FROM generate_series(DATE_TRUNC('month', startDate), endDate, '1 month') t
                WHERE t < endDate
            )
        SELECT
            nextval('tauxremplissage_id_seq'),
            chro_id,
            m.debut,
            m.fin,
            m.duration,
            SUM(EXTRACT('epoch' FROM (
                    CASE WHEN v.fin < m.fin THEN v.fin ELSE m.fin END
                    -
                    CASE WHEN v.debut > m.debut THEN v.debut ELSE m.debut END
            ))) / m.duration
        FROM
            mois m
            LEFT JOIN valides v ON ((m.debut, m.fin) OVERLAPS (v.debut, v.fin))
        GROUP BY m.debut, m.fin, m.duration
    );

    GET DIAGNOSTICS tauxCount := ROW_COUNT;
    RAISE INFO 'Chronicle #%: % filling rate record(s) created', chro_id, tauxCount;

    RETURN tauxCount;
END;
$$
SQL
        );

        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_update_measures_metadata(
    chro_id chronique.id%TYPE,
    OUT firstDate TIMESTAMP(3),
    OUT lastDate TIMESTAMP(3),
    OUT mesCount INTEGER
)
VOLATILE STRICT
LANGUAGE 'plpgsql'
AS $$
DECLARE
    discontinue BOOLEAN;
BEGIN
    -- Détermine le type de chronique
    SELECT
        dtype = 'discontinue'
        INTO discontinue
        FROM chronique
        WHERE id = chro_id;

    -- Récupère les bornes
    IF discontinue THEN
        SELECT
            MIN(debut), MAX(fin), COUNT(debut)
            INTO firstDate, lastDate, mesCount
            FROM plage
            WHERE chronique_id = chro_id;
    ELSE
        SELECT
            MIN(date), MAX(date), COUNT(date)
            INTO firstDate, lastDate, mesCount
            FROM mesure
            WHERE chronique_id = chro_id;
    END IF;

    -- Met à jour les métadonnées dans chronique
    UPDATE chronique
        SET nbmesures = mesCount, datedebutmesures = firstDate, datefinmesures = lastDate
        WHERE id = chro_id;

    RAISE INFO 'Chronicle #%: % measures from % to %', chro_id, mesCount, firstDate, lastDate;
END;
$$
SQL
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql(
            'DROP FUNCTION bdoh_update_measures_metadata(chronique.id%TYPE)'
        );
        $this->addSql(
            'DROP FUNCTION bdoh_update_filling_rates(chronique.id%TYPE, TIMESTAMP(3), TIMESTAMP(3))'
        );
    }
}
