<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190129154614 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE courseau_observatoires');
        $this->addSql('ALTER TABLE courseau ADD observatoire_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE courseau ADD CONSTRAINT FK_346196BA61ED1B0D FOREIGN KEY (observatoire_id) REFERENCES Observatoire (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_346196BA61ED1B0D ON courseau (observatoire_id)');

        // Tous les cours d'eaux existant à ce point vont dans l'observatoire YZERON
        $this->addSql('UPDATE courseau SET observatoire_id=10');

        $this->addSql('ALTER TABLE courseau ALTER observatoire_id DROP DEFAULT');
        $this->addSql('ALTER TABLE courseau ALTER observatoire_id SET NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE courseau_observatoires (courseau_id INT NOT NULL, observatoire_id INT NOT NULL, PRIMARY KEY(courseau_id, observatoire_id))');
        $this->addSql('CREATE INDEX idx_c05b50661ed1b0d ON courseau_observatoires (observatoire_id)');
        $this->addSql('CREATE INDEX idx_c05b50632164871 ON courseau_observatoires (courseau_id)');
        $this->addSql('ALTER TABLE courseau_observatoires ADD CONSTRAINT fk_c05b50632164871 FOREIGN KEY (courseau_id) REFERENCES courseau (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE courseau_observatoires ADD CONSTRAINT fk_c05b50661ed1b0d FOREIGN KEY (observatoire_id) REFERENCES observatoire (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE CoursEau DROP CONSTRAINT FK_346196BA61ED1B0D');
        $this->addSql('DROP INDEX IDX_346196BA61ED1B0D');
        $this->addSql('ALTER TABLE CoursEau DROP observatoire_id');

        // Tous les cours d'eaux existant à ce point retourne dans l'observatoire YZERON
        $this->addSql('INSERT INTO courseau_observatoires (courseau_id, observatoire_id) SELECT id, 10 FROM courseau ON CONFLICT DO NOTHING');
    }
}
