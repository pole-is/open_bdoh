<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210204085346 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE listechroniques ADD chronique_discontinue_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE listechroniques ADD chronique_convertie_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE listechroniques ADD chronique_calculee_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE listechroniques ADD CONSTRAINT FK_8CF9AD62F0ABD5D0 FOREIGN KEY (chronique_discontinue_id) REFERENCES Chronique (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE listechroniques ADD CONSTRAINT FK_8CF9AD629442DDA7 FOREIGN KEY (chronique_convertie_id) REFERENCES Chronique (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE listechroniques ADD CONSTRAINT FK_8CF9AD62BAED0790 FOREIGN KEY (chronique_calculee_id) REFERENCES Chronique (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_8CF9AD62F0ABD5D0 ON listechroniques (chronique_discontinue_id)');
        $this->addSql('CREATE INDEX IDX_8CF9AD629442DDA7 ON listechroniques (chronique_convertie_id)');
        $this->addSql('CREATE INDEX IDX_8CF9AD62BAED0790 ON listechroniques (chronique_calculee_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE ListeChroniques DROP CONSTRAINT FK_8CF9AD62F0ABD5D0');
        $this->addSql('ALTER TABLE ListeChroniques DROP CONSTRAINT FK_8CF9AD629442DDA7');
        $this->addSql('ALTER TABLE ListeChroniques DROP CONSTRAINT FK_8CF9AD62BAED0790');
        $this->addSql('DROP INDEX IDX_8CF9AD62F0ABD5D0');
        $this->addSql('DROP INDEX IDX_8CF9AD629442DDA7');
        $this->addSql('DROP INDEX IDX_8CF9AD62BAED0790');
        $this->addSql('ALTER TABLE ListeChroniques DROP chronique_discontinue_id');
        $this->addSql('ALTER TABLE ListeChroniques DROP chronique_convertie_id');
        $this->addSql('ALTER TABLE ListeChroniques DROP chronique_calculee_id');
    }
}
