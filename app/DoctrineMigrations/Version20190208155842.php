<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190208155842 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE FamilleParametres_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE FamilleParametres (id INT NOT NULL, nom VARCHAR(255) NOT NULL, nomEn VARCHAR(255) NOT NULL, familleParente_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_163312BC6C6E55B5 ON FamilleParametres (nom)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_163312BCE46E7608 ON FamilleParametres (nomEn)');
        $this->addSql('CREATE INDEX IDX_163312BCE09DE74B ON FamilleParametres (familleParente_id)');
        $this->addSql('ALTER TABLE FamilleParametres ADD CONSTRAINT FK_163312BCE09DE74B FOREIGN KEY (familleParente_id) REFERENCES FamilleParametres (id) ON DELETE RESTRICT NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE typeparametre ADD familleParametres_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE typeparametre ADD CONSTRAINT FK_7E773B8B77DD839C FOREIGN KEY (familleParametres_id) REFERENCES FamilleParametres (id) ON DELETE RESTRICT NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_7E773B8B77DD839C ON typeparametre (familleParametres_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE FamilleParametres DROP CONSTRAINT FK_163312BCE09DE74B');
        $this->addSql('ALTER TABLE TypeParametre DROP CONSTRAINT FK_7E773B8B77DD839C');
        $this->addSql('DROP SEQUENCE FamilleParametres_id_seq CASCADE');
        $this->addSql('DROP TABLE FamilleParametres');
        $this->addSql('DROP INDEX IDX_7E773B8B77DD839C');
        $this->addSql('ALTER TABLE TypeParametre DROP familleParametres_id');
    }
}
