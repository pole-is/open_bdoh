<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210114150144 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE personne_theia_et_role_personne_theia_observatoire (dataset_id INT NOT NULL, contacttheia_id INT NOT NULL, PRIMARY KEY(dataset_id, contacttheia_id))');
        $this->addSql('CREATE INDEX IDX_850435DFD47C2D1B ON personne_theia_et_role_personne_theia_observatoire (dataset_id)');
        $this->addSql('CREATE INDEX IDX_850435DFCD8785EC ON personne_theia_et_role_personne_theia_observatoire (contacttheia_id)');
        $this->addSql('CREATE TABLE personne_theia_et_role_personne_theia_theia_dataSet (dataset_id INT NOT NULL, contacttheia_id INT NOT NULL, PRIMARY KEY(dataset_id, contacttheia_id))');
        $this->addSql('CREATE INDEX IDX_EF59972BD47C2D1B ON personne_theia_et_role_personne_theia_theia_dataSet (dataset_id)');
        $this->addSql('CREATE INDEX IDX_EF59972BCD8785EC ON personne_theia_et_role_personne_theia_theia_dataSet (contacttheia_id)');
        $this->addSql('ALTER TABLE personne_theia_et_role_personne_theia_observatoire ADD CONSTRAINT FK_850435DFD47C2D1B FOREIGN KEY (dataset_id) REFERENCES Observatoire (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE personne_theia_et_role_personne_theia_observatoire ADD CONSTRAINT FK_850435DFCD8785EC FOREIGN KEY (contacttheia_id) REFERENCES PersonneTheiaEtRolePersonneTheia (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE personne_theia_et_role_personne_theia_theia_dataSet ADD CONSTRAINT FK_EF59972BD47C2D1B FOREIGN KEY (dataset_id) REFERENCES DataSet (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE personne_theia_et_role_personne_theia_theia_dataSet ADD CONSTRAINT FK_EF59972BCD8785EC FOREIGN KEY (contacttheia_id) REFERENCES PersonneTheiaEtRolePersonneTheia (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE contact_theia_dataset');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE contact_theia_dataset (dataset_id INT NOT NULL, contacttheia_id INT NOT NULL, PRIMARY KEY(dataset_id, contacttheia_id))');
        $this->addSql('CREATE INDEX idx_a311b0e2cd8785ec ON contact_theia_dataset (contacttheia_id)');
        $this->addSql('CREATE INDEX idx_a311b0e2d47c2d1b ON contact_theia_dataset (dataset_id)');
        $this->addSql('ALTER TABLE contact_theia_dataset ADD CONSTRAINT fk_a311b0e2d47c2d1b FOREIGN KEY (dataset_id) REFERENCES dataset (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE contact_theia_dataset ADD CONSTRAINT fk_a311b0e2cd8785ec FOREIGN KEY (contacttheia_id) REFERENCES personnetheiaetrolepersonnetheia (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE personne_theia_et_role_personne_theia_observatoire');
        $this->addSql('DROP TABLE personne_theia_et_role_personne_theia_theia_dataSet');
    }
}
