<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210204095959 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE listechroniques DROP CONSTRAINT fk_8cf9ad62baed0790');
        $this->addSql('ALTER TABLE listechroniques DROP CONSTRAINT fk_8cf9ad629442dda7');
        $this->addSql('ALTER TABLE listechroniques DROP CONSTRAINT fk_8cf9ad62f0abd5d0');
        $this->addSql('ALTER TABLE listechroniques DROP CONSTRAINT fk_8cf9ad6271fae7a4');
        $this->addSql('DROP INDEX idx_8cf9ad62baed0790');
        $this->addSql('DROP INDEX idx_8cf9ad629442dda7');
        $this->addSql('DROP INDEX idx_8cf9ad62f0abd5d0');
        $this->addSql('DROP INDEX idx_8cf9ad6271fae7a4');
        $this->addSql('ALTER TABLE listechroniques DROP chronique_continue_id');
        $this->addSql('ALTER TABLE listechroniques DROP chronique_discontinue_id');
        $this->addSql('ALTER TABLE listechroniques DROP chronique_convertie_id');
        $this->addSql('ALTER TABLE listechroniques DROP chronique_calculee_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE Chronique ALTER miseAJour TYPE TIMESTAMP(3) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE Chronique ALTER miseAJour DROP DEFAULT');
        $this->addSql('ALTER TABLE ListeChroniques ADD chronique_continue_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ListeChroniques ADD chronique_discontinue_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ListeChroniques ADD chronique_convertie_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ListeChroniques ADD chronique_calculee_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ListeChroniques ADD CONSTRAINT fk_8cf9ad62baed0790 FOREIGN KEY (chronique_calculee_id) REFERENCES chronique (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ListeChroniques ADD CONSTRAINT fk_8cf9ad629442dda7 FOREIGN KEY (chronique_convertie_id) REFERENCES chronique (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ListeChroniques ADD CONSTRAINT fk_8cf9ad62f0abd5d0 FOREIGN KEY (chronique_discontinue_id) REFERENCES chronique (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ListeChroniques ADD CONSTRAINT fk_8cf9ad6271fae7a4 FOREIGN KEY (chronique_continue_id) REFERENCES chronique (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_8cf9ad62baed0790 ON ListeChroniques (chronique_calculee_id)');
        $this->addSql('CREATE INDEX idx_8cf9ad629442dda7 ON ListeChroniques (chronique_convertie_id)');
        $this->addSql('CREATE INDEX idx_8cf9ad62f0abd5d0 ON ListeChroniques (chronique_discontinue_id)');
        $this->addSql('CREATE INDEX idx_8cf9ad6271fae7a4 ON ListeChroniques (chronique_continue_id)');
    }
}
