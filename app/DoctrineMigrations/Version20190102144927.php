<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190102144927 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_transformed_mesures(IN transfoid integer)
  RETURNS TABLE(date timestamp without time zone, valeur double precision[]) AS
$BODY$

DECLARE

    typeTransfoLimites VARCHAR;
    turnLimitsToGap    BOOLEAN;
    coeffLimitsAdd     DOUBLE PRECISION;
    coeffLimitsMult    DOUBLE PRECISION;

BEGIN

    typeTransfoLimites := valuelimittransformationtype FROM jeubareme jb
        JOIN transformation t ON t.jeubaremeactuel_id = jb.id WHERE t.id = transfoId;

    IF typeTransfoLimites = 'chronique.lq_ld.options.gap' THEN
        turnLimitsToGap := true;
        coeffLimitsAdd  := -9999;
        coeffLimitsMult := 0.0;
    ELSE
        turnLimitsToGap := false;
    END IF;

    IF typeTransfoLimites IS NULL OR typeTransfoLimites = 'chronique.lq_ld.options.true_value' THEN
        coeffLimitsAdd  := 0.0;
        coeffLimitsMult := 1.0;
    END IF;

    IF typeTransfoLimites = 'chronique.lq_ld.options.half_value' THEN
        coeffLimitsAdd  := 0.0;
        coeffLimitsMult := 0.5;
    END IF;

    IF typeTransfoLimites = 'chronique.lq_ld.options.placeholder' THEN
        coeffLimitsAdd  := valuelimitplaceholder FROM jeubareme jb
            JOIN transformation t ON t.jeubaremeactuel_id = jb.id WHERE t.id = transfoId;
        coeffLimitsMult := 0.0;
    END IF;

    RETURN QUERY
        SELECT
            m.date,
            CASE
                WHEN b.nom = 'lacune' THEN
                    ARRAY[-9999, 100]
                WHEN turnLimitsToGap AND q.ordre IN (600, 700) THEN
                    ARRAY[-9999, 200]
                WHEN b.nom = 'identite' THEN
                    CASE
                        WHEN q.ordre IN (600, 700) THEN
                            ARRAY[m.valeur * coeffLimitsMult + coeffLimitsAdd, 500]
                        ELSE
                            ARRAY[m.valeur, q.ordre]
                    END
                ELSE
                    bdoh_interp_for_bareme(m.valeur, q.ordre, b.valeurs, turnLimitsToGap, coeffLimitsAdd, coeffLimitsMult)
            END
        FROM
            transformation t,
            mesure m,
            qualite q,
            baremejeubareme bjb,
            bareme b
        WHERE
            t.id=transfoId AND
            m.chronique_id=t.entree_id AND
            q.id=m.qualite_id AND
            bjb.jeubareme_id=t.jeubaremeactuel_id AND
            m.date >= bjb.debutValidite AND
            (m.date < bjb.finValidite OR bjb.finValidite IS NULL) AND
            b.id=bjb.bareme_id AND
            b.nom <> 'manuel'
        ORDER BY m.date ASC;

END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 1000;
SQL
        );
        $this->addSql(
            <<<'SQL'
ALTER FUNCTION bdoh_transformed_mesures(integer)
  OWNER TO bdoh;
SQL
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_transformed_mesures(IN transfoid integer)
  RETURNS TABLE(date timestamp without time zone, valeur double precision[]) AS
$BODY$

DECLARE

    typeTransfoLimites VARCHAR;
    turnLimitsToGap    BOOLEAN;
    coeffLimitsAdd     DOUBLE PRECISION;
    coeffLimitsMult    DOUBLE PRECISION;

BEGIN

    typeTransfoLimites := valuelimittransformationtype FROM jeubareme jb
        JOIN transformation t ON t.jeubaremeactuel_id = jb.id WHERE t.id = transfoId;

    IF typeTransfoLimites = 'chronique.lq_ld.options.gap' THEN
        turnLimitsToGap := true;
        coeffLimitsAdd  := -9999;
        coeffLimitsMult := 0.0;
    ELSE
        turnLimitsToGap := false;
    END IF;

    IF typeTransfoLimites IS NULL OR typeTransfoLimites = 'chronique.lq_ld.options.true_value' THEN
        coeffLimitsAdd  := 0.0;
        coeffLimitsMult := 1.0;
    END IF;

    IF typeTransfoLimites = 'chronique.lq_ld.options.half_value' THEN
        coeffLimitsAdd  := 0.0;
        coeffLimitsMult := 0.5;
    END IF;

    IF typeTransfoLimites = 'chronique.lq_ld.options.placeholder' THEN
        coeffLimitsAdd  := valuelimitplaceholder FROM jeubareme jb
            JOIN transformation t ON t.jeubaremeactuel_id = jb.id WHERE t.id = transfoId;
        coeffLimitsMult := 0.0;
    END IF;

    RETURN QUERY
        SELECT
            m.date,
            CASE
                WHEN b.nom = 'lacune' THEN
                    ARRAY[-9999, 100]
                WHEN turnLimitsToGap AND q.ordre IN (600, 700) THEN
                    ARRAY[-9999, 200]
                WHEN b.nom = 'identite' THEN
                    CASE
                        WHEN q.ordre IN (600, 700) THEN
                            ARRAY[m.valeur * coeffLimitsMult + coeffLimitsAdd, q.ordre]
                        ELSE
                            ARRAY[m.valeur, q.ordre]
                    END
                ELSE
                    bdoh_interp_for_bareme(m.valeur, q.ordre, b.valeurs, turnLimitsToGap, coeffLimitsAdd, coeffLimitsMult)
            END
        FROM
            transformation t,
            mesure m,
            qualite q,
            baremejeubareme bjb,
            bareme b
        WHERE
            t.id=transfoId AND
            m.chronique_id=t.entree_id AND
            q.id=m.qualite_id AND
            bjb.jeubareme_id=t.jeubaremeactuel_id AND
            m.date >= bjb.debutValidite AND
            (m.date < bjb.finValidite OR bjb.finValidite IS NULL) AND
            b.id=bjb.bareme_id AND
            b.nom <> 'manuel'
        ORDER BY m.date ASC;

END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 1000;
SQL
        );
        $this->addSql(
            <<<'SQL'
ALTER FUNCTION bdoh_transformed_mesures(integer)
  OWNER TO bdoh;
SQL
        );
    }
}
