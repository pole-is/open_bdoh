<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190116124901 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_subsampled_instantaneous_series_measures_with_flag(
    IN chroniqueid integer,
    IN startdate timestamp without time zone,
    IN enddate timestamp without time zone,
    IN subinterval double precision)
  RETURNS TABLE(e double precision, v double precision, q integer, o integer, f text) AS
$BODY$
BEGIN
	-- WITH clause yields measures with date in milliseconds, subinterval index (starting from zero) and value
	-- First subselect (alias "vmin") yields, for each subinterval, the date & value for the measure with smallest value
	-- Second subselect (alias "vmax") yields, for each subinterval, the date & value for the largest measure with greatest value
	-- Finally, we append the very first & last measures in our date interval
	RETURN QUERY
		WITH mi(ems, idx, val, qid, ord, flag) AS (
			SELECT
				1000 * EXTRACT(EPOCH FROM m.date AT TIME ZONE 'UTC') AS ems,
				floor(EXTRACT(EPOCH FROM AGE(m.date, startdate))/subinterval)::INTEGER AS idx,
				m.valeur AS val,
				m.qualite_id AS qid,
				q.ordre AS ord,
				CASE q.ordre
					WHEN 100 THEN 'gap'
					WHEN 200 THEN 'invalid'
					ELSE 'valid_' || q.code
				END AS flag
			FROM mesure m INNER JOIN qualite q ON (m.qualite_id = q.id)
			WHERE chronique_id = chroniqueId AND date >= startDate AND date <= endDate
			ORDER BY m.date
		)
		SELECT ems, val, qid, ord, flag FROM (SELECT * FROM mi ORDER BY ems ASC LIMIT 1) first_m -- very first measure
		UNION
		SELECT ems, val, qid, ord, flag FROM ( -- row with min value per subinterval
			SELECT
				ROW_NUMBER() OVER (PARTITION BY idx ORDER BY val ASC) AS rank,
				ems,
				val,
				qid,
				ord,
				flag
			FROM mi
			WHERE val IS NOT NULL -- filtrage des mesures invalid ou gap inutiles
		) vmin
		WHERE vmin.rank = 1
		UNION
		SELECT ems, val, qid, ord, flag FROM ( -- row with max value per subinterval
			SELECT
				ROW_NUMBER() OVER (PARTITION BY idx ORDER BY val DESC) AS rank,
				ems,
				val,
				qid,
				ord,
				flag
			FROM mi
			WHERE val IS NOT NULL -- filtrage des mesures invalid ou gap inutiles
		) vmax
		WHERE vmax.rank = 1
		UNION
		SELECT ems, val, qid, ord, flag FROM (SELECT * FROM mi ORDER BY ems DESC LIMIT 1) last_m -- very last measure
		ORDER BY ems;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 1000;
SQL
        );
        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_subsampled_instantaneous_all_series_measures_with_flag(
    IN chroniqueid integer,
    IN startdates timestamp without time zone[],
    IN enddates timestamp without time zone[],
    IN subintervals double precision[])
  RETURNS TABLE(e double precision, v double precision, q integer, o integer, f text) AS
$BODY$
DECLARE
	nSeries BIGINT;
	i BIGINT;
BEGIN
	nSeries := ARRAY_LENGTH(startDates, 1);
	FOR i IN 1..nSeries LOOP
		RETURN QUERY
			SELECT *
			FROM bdoh_subsampled_instantaneous_series_measures_with_flag(
				chroniqueId,
				startDates[i],
				endDates[i],
				subintervals[i]
			);
	END LOOP;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 1000;
SQL
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql(
            <<<'SQL'
DROP FUNCTION bdoh_subsampled_instantaneous_all_series_measures_with_flag(integer, timestamp without time zone[], timestamp without time zone[], double precision[])
SQL
        );
        $this->addSql(
            <<<'SQL'
DROP FUNCTION bdoh_subsampled_instantaneous_series_measures_with_flag(integer, timestamp without time zone, timestamp without time zone, double precision);
SQL
        );
    }
}
