<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210120142721 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE data_manager_dataSet (dataset_id INT NOT NULL, partenaire_id INT NOT NULL, PRIMARY KEY(dataset_id, partenaire_id))');
        $this->addSql('CREATE INDEX IDX_4491DB31D47C2D1B ON data_manager_dataSet (dataset_id)');
        $this->addSql('CREATE INDEX IDX_4491DB3198DE13AC ON data_manager_dataSet (partenaire_id)');
        $this->addSql('CREATE TABLE principal_investigator_dataSet (dataset_id INT NOT NULL, partenaire_id INT NOT NULL, PRIMARY KEY(dataset_id, partenaire_id))');
        $this->addSql('CREATE INDEX IDX_D4483058D47C2D1B ON principal_investigator_dataSet (dataset_id)');
        $this->addSql('CREATE INDEX IDX_D448305898DE13AC ON principal_investigator_dataSet (partenaire_id)');
        $this->addSql('CREATE TABLE data_collector_dataSet (dataset_id INT NOT NULL, partenaire_id INT NOT NULL, PRIMARY KEY(dataset_id, partenaire_id))');
        $this->addSql('CREATE INDEX IDX_4158353CD47C2D1B ON data_collector_dataSet (dataset_id)');
        $this->addSql('CREATE INDEX IDX_4158353C98DE13AC ON data_collector_dataSet (partenaire_id)');
        $this->addSql('CREATE TABLE project_member_dataSet (dataset_id INT NOT NULL, partenaire_id INT NOT NULL, PRIMARY KEY(dataset_id, partenaire_id))');
        $this->addSql('CREATE INDEX IDX_D9AE8406D47C2D1B ON project_member_dataSet (dataset_id)');
        $this->addSql('CREATE INDEX IDX_D9AE840698DE13AC ON project_member_dataSet (partenaire_id)');
        $this->addSql('ALTER TABLE data_manager_dataSet ADD CONSTRAINT FK_4491DB31D47C2D1B FOREIGN KEY (dataset_id) REFERENCES DataSet (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE data_manager_dataSet ADD CONSTRAINT FK_4491DB3198DE13AC FOREIGN KEY (partenaire_id) REFERENCES PersonneTheia (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE principal_investigator_dataSet ADD CONSTRAINT FK_D4483058D47C2D1B FOREIGN KEY (dataset_id) REFERENCES DataSet (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE principal_investigator_dataSet ADD CONSTRAINT FK_D448305898DE13AC FOREIGN KEY (partenaire_id) REFERENCES PersonneTheia (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE data_collector_dataSet ADD CONSTRAINT FK_4158353CD47C2D1B FOREIGN KEY (dataset_id) REFERENCES DataSet (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE data_collector_dataSet ADD CONSTRAINT FK_4158353C98DE13AC FOREIGN KEY (partenaire_id) REFERENCES PersonneTheia (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE project_member_dataSet ADD CONSTRAINT FK_D9AE8406D47C2D1B FOREIGN KEY (dataset_id) REFERENCES DataSet (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE project_member_dataSet ADD CONSTRAINT FK_D9AE840698DE13AC FOREIGN KEY (partenaire_id) REFERENCES PersonneTheia (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE data_manager_dataSet');
        $this->addSql('DROP TABLE principal_investigator_dataSet');
        $this->addSql('DROP TABLE data_collector_dataSet');
        $this->addSql('DROP TABLE project_member_dataSet');
    }
}
