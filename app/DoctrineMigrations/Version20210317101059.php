<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210317101059 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE TheiaCategories_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE TheiaCategories (id INT NOT NULL, milieu VARCHAR(255) NOT NULL, uriOzcarTheia JSON NOT NULL, familleParametre_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_CDEB484B1323280F ON TheiaCategories (familleParametre_id)');
        $this->addSql('COMMENT ON COLUMN TheiaCategories.uriOzcarTheia IS \'(DC2Type:json_array)\'');
        $this->addSql('ALTER TABLE TheiaCategories ADD CONSTRAINT FK_CDEB484B1323280F FOREIGN KEY (familleParametre_id) REFERENCES FamilleParametres (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chronique ADD milieu VARCHAR(255) DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DAC287499F990A5E ON chronique (milieu)');
        $this->addSql('ALTER TABLE familleparametres DROP milieu');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE TheiaCategories_id_seq CASCADE');
        $this->addSql('DROP TABLE TheiaCategories');
        $this->addSql('ALTER TABLE FamilleParametres ADD milieu VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE Chronique DROP milieu');
    }
}
