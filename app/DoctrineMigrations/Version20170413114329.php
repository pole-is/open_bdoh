<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 * @SuppressWarnings(PHPMD.ShortMethodName)
 */
class Version20170413114329 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            <<<'SQL'
            CREATE OR REPLACE FUNCTION bdoh_chronique_qualite_id(
                chronique_id chronique.id%TYPE,
                qualite_ordre qualite.ordre%TYPE
            )
            RETURNS qualite.id%TYPE
            STABLE
            LANGUAGE 'sql'
            AS $$
                SELECT q.id
                FROM chronique c
                    INNER JOIN station s ON (c.station_id = s.id)
                    INNER JOIN stations_sites sts ON (s.id = sts.station_id)
                    INNER JOIN siteexperimental se ON (sts.site_id = se.id)
                    INNER JOIN observatoire o ON (se.observatoire_id = o.id)
                    INNER JOIN qualite q ON (o.jeu_id = q.jeu_id)
                WHERE q.ordre = $2
                    AND q.code != 'gap'
                    AND c.id = $1;
            $$;
SQL
        );

        $this->addSql(
            <<<'SQL'
            CREATE OR REPLACE FUNCTION bdoh_bounded_mesures(
                chronique_id mesure.chronique_id%TYPE,
                begin_date mesure.date%TYPE,
                end_date mesure.date%TYPE
            )
            RETURNS TABLE(
                date mesure.date%TYPE,
                valeur mesure.valeur%TYPE,
                qualite_id mesure.qualite_id%TYPE
            )
            STABLE
            ROWS 6000
            LANGUAGE 'plpgsql'
            AS $$
                DECLARE
                    c_id ALIAS FOR chronique_id;
                    lower_date mesure.date%TYPE;
                    upper_date mesure.date%TYPE;
                    gap_id     CONSTANT mesure.qualite_id%TYPE := bdoh_chronique_qualite_id(chronique_id, 100);
                BEGIN
                    SELECT MAX(m.date) INTO lower_date FROM mesure m WHERE m.chronique_id = c_id AND m.date <= begin_date;
                    SELECT MIN(m.date) INTO upper_date FROM mesure m WHERE m.chronique_id = c_id AND m.date >= end_date;

                    IF lower_date IS NULL THEN
                        RETURN QUERY SELECT TIMESTAMP '-infinity', NULL::DOUBLE PRECISION, gap_id;
                        lower_date := begin_date;
                    END IF;

                    IF upper_date IS NULL THEN
                        RETURN QUERY SELECT TIMESTAMP 'infinity', NULL::DOUBLE PRECISION, gap_id;
                        lower_date := end_date;
                    END IF;

                    RETURN QUERY
                        SELECT m.date, m.valeur, COALESCE(m.qualite_id, gap_id)
                          FROM mesure m
                         WHERE m.chronique_id = c_id
                           AND m.date BETWEEN lower_date AND upper_date;
                END;
            $$;
SQL
        );

        $this->addSql(
            'DROP FUNCTION bdoh_segments(mesure.chronique_id%TYPE, mesure.date%TYPE, mesure.date%TYPE)'
        );

        $this->addSql(
            <<<'SQL'
            CREATE FUNCTION bdoh_segments(
                chronique_id mesure.chronique_id%TYPE,
                start_date mesure.date%TYPE,
                end_date mesure.date%TYPE
            )
            RETURNS TABLE(
                start_date mesure.date%TYPE,
                end_date mesure.date%TYPE,
                start_valeur mesure.valeur%TYPE,
                end_valeur mesure.valeur%TYPE,
                start_qualite_id mesure.qualite_id%TYPE,
                end_qualite_id mesure.qualite_id%TYPE
            )
            STABLE
            LANGUAGE 'sql'
            AS $$
                SELECT * FROM (
                    SELECT LAG(date, 1, TIMESTAMP '-infinity') OVER w,
                           date,
                           LAG(valeur) OVER w,
                           valeur,
                           LAG(qualite_id, 1, bdoh_chronique_qualite_id(chronique_id , 100)) OVER w,
                           COALESCE(qualite_id, bdoh_chronique_qualite_id(chronique_id , 100))
                      FROM bdoh_bounded_mesures(chronique_id, start_date, end_date)
                    WINDOW w AS (
                        ORDER BY date ASC
                        ROWS BETWEEN 1 PRECEDING AND CURRENT ROW
                    )
                ) m
                OFFSET 1
            $$;
SQL
        );

        $this->addSql(
            'DROP FUNCTION bdoh_interpolate_linear(start_date mesure.date%TYPE, start_valeur mesure.valeur%TYPE, ' .
            'end_date mesure.date%TYPE, end_valeur mesure.valeur%TYPE, date mesure.date%TYPE);'
        );

        $this->addSql(
            <<<'SQL'
            CREATE FUNCTION bdoh_interpolate_linear(
                start_date mesure.date%TYPE,
                start_valeur mesure.valeur%TYPE,
                end_date mesure.date%TYPE,
                end_valeur mesure.valeur%TYPE,
                date mesure.date%TYPE
            )
            RETURNS mesure.valeur%TYPE
            IMMUTABLE
            LANGUAGE 'sql'
            AS $$
                 SELECT CASE
                            WHEN $5 = $1 THEN
                                $2
                            WHEN $5 = $3 THEN
                                $4
                            WHEN ISFINITE($1) AND ISFINITE($3) AND ISFINITE($5) THEN
                                $2 + ($4 - $2) * EXTRACT(EPOCH FROM ($5 - $1)) / EXTRACT(EPOCH FROM ($3 - $1))
                            ELSE
                                NULL
                        END;

            $$
SQL
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql(
            <<<'SQL'
            DROP FUNCTION bdoh_chronique_qualite_id(
                chronique_id chronique.id%TYPE,
                qualite_ordre qualite.ordre%TYPE
            );
SQL
        );

        $this->addSql(
            <<<'SQL'
            DROP FUNCTION bdoh_bounded_mesures(
                chronique_id mesure.chronique_id%TYPE,
                begin_date mesure.date%TYPE,
                end_date mesure.date%TYPE
            );
SQL
        );

        $this->addSql(
            <<<'SQL'
            DROP FUNCTION bdoh_segments(
                chronique_id mesure.chronique_id%TYPE,
                begin_date mesure.date%TYPE,
                end_date mesure.date%TYPE
            );
SQL
        );

        $this->addSql(
            <<<'SQL'
            DROP FUNCTION bdoh_interpolate_linear(
                start_date mesure.date%TYPE,
                start_valeur mesure.valeur%TYPE,
                end_date mesure.date%TYPE,
                end_valeur mesure.valeur%TYPE,
                date mesure.date%TYPE
            );
SQL
        );
    }
}
