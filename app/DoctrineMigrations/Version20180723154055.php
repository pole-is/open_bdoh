<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180723154055 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_insert_mesures_two_transformations(
    chroniquefilleid integer,
    jeuqualiteid integer,
    transfomainid integer,
    transfosecondid integer,
    delaymain double precision DEFAULT 0.0,
    delaysecond double precision DEFAULT 0.0,
    coefficient double precision DEFAULT 1.0)
  RETURNS integer AS
$BODY$

DECLARE

	cursorMain         CURSOR FOR SELECT date + delayMain * INTERVAL '1 MINUTE', valeur
                                   FROM bdoh_transformed_mesures(transfoMainId) ORDER BY date;
	cursorSecond       CURSOR FOR SELECT date + delaySecond * INTERVAL '1 MINUTE', valeur
                                   FROM bdoh_transformed_mesures(transfoSecondId) ORDER BY date;
	dateMain           TIMESTAMP;
	dateMainAvant      TIMESTAMP;
	dateSecond         TIMESTAMP;
	dateSecondAvant    TIMESTAMP;
	valeurMain         DOUBLE PRECISION[];
	valeurMainAvant    DOUBLE PRECISION[];
	valeurSecond       DOUBLE PRECISION[];
	valeurSecondAvant  DOUBLE PRECISION[];
	dateInsert         TIMESTAMP;
	valeurMainInsert   DOUBLE PRECISION[];
	valeurSecondInsert DOUBLE PRECISION[];
	yInsert            DOUBLE PRECISION;
	qualiteInsert      DOUBLE PRECISION;
	qualiteId          INTEGER;

BEGIN

	OPEN cursorMain;
	OPEN cursorSecond;

	dateMainAvant := NULL;
	valeurMainAvant := NULL;
	dateSecondAvant := NULL;
	valeurSecondAvant := NULL;
	FETCH cursorMain INTO dateMain, valeurMain;
	FETCH cursorSecond INTO dateSecond, valeurSecond;

	WHILE dateMain IS NOT NULL AND dateSecond IS NOT NULL LOOP

		IF dateMain < dateSecond THEN
			dateInsert := dateMain;
			valeurMainInsert := valeurMain;
			valeurSecondInsert := bdoh_interp_between_dates(dateInsert, dateSecondAvant, valeurSecondAvant, dateSecond, valeurSecond);

			dateMainAvant := dateMain;
			valeurMainAvant := valeurMain;
			FETCH cursorMain INTO dateMain, valeurMain;
			CONTINUE WHEN dateSecondAvant IS NULL;

		ELSIF dateSecond < dateMain THEN
			dateInsert := dateSecond;
			valeurMainInsert := bdoh_interp_between_dates(dateInsert, dateMainAvant, valeurMainAvant, dateMain, valeurMain);
			valeurSecondInsert := valeurSecond;

			dateSecondAvant := dateSecond;
			valeurSecondAvant := valeurSecond;
			FETCH cursorSecond INTO dateSecond, valeurSecond;
			CONTINUE WHEN dateMainAvant IS NULL;

		ELSE
			dateInsert := dateMain;
			valeurMainInsert := valeurMain;
			valeurSecondInsert := valeurSecond;

			dateMainAvant := dateMain;
			valeurMainAvant := valeurMain;
			FETCH cursorMain INTO dateMain, valeurMain;
			dateSecondAvant := dateSecond;
			valeurSecondAvant := valeurSecond;
			FETCH cursorSecond INTO dateSecond, valeurSecond;

		END IF;

		qualiteInsert := bdoh_cross_quality_orders(ROUND(valeurMainInsert[2])::INTEGER, ROUND(valeurSecondInsert[2])::INTEGER);
		IF qualiteInsert <= 200 THEN
			yInsert := -9999;
		ELSE
			yInsert := valeurMainInsert[1] * valeurSecondInsert[1] * coefficient;
		END IF;

		SELECT id INTO qualiteId
			FROM qualite
			WHERE ordre = ROUND(qualiteInsert)
			AND jeu_id = jeuQualiteId
			AND code <> 'gap';

		INSERT INTO mesure (id, qualite_id, chronique_id, date, valeur, estcalculee)
		VALUES(NEXTVAL('MESURE_ID_SEQ'), qualiteId, chroniqueFilleId, dateInsert, yInsert, true);

	END LOOP;

	CLOSE cursorMain;
	CLOSE cursorSecond;

	RETURN 0;

EXCEPTION

	WHEN OTHERS THEN
		RETURN 16;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
SQL
        );
        $this->addSql(
            <<<'SQL'
ALTER FUNCTION bdoh_insert_mesures_two_transformations(integer, integer, integer, integer, double precision, double precision, double precision)
  OWNER TO bdoh;
SQL
        );
        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_transformed_mesures(IN transfoid integer)
  RETURNS TABLE(date timestamp without time zone, valeur double precision[]) AS
$BODY$

DECLARE

	typeTransfoLimites VARCHAR;
	turnLimitsToGap    BOOLEAN;
	coeffLimitsAdd     DOUBLE PRECISION;
	coeffLimitsMult    DOUBLE PRECISION;

BEGIN

	typeTransfoLimites := valuelimittransformationtype FROM jeubareme jb
		JOIN transformation t ON t.jeubaremeactuel_id = jb.id WHERE t.id = transfoId;

	IF typeTransfoLimites = 'chronique.lq_ld.options.gap' THEN
		turnLimitsToGap := true;
		coeffLimitsAdd  := -9999;
		coeffLimitsMult := 0.0;
	ELSE
		turnLimitsToGap := false;
	END IF;

	IF typeTransfoLimites IS NULL OR typeTransfoLimites = 'chronique.lq_ld.options.true_value' THEN
		coeffLimitsAdd  := 0.0;
		coeffLimitsMult := 1.0;
	END IF;

	IF typeTransfoLimites = 'chronique.lq_ld.options.half_value' THEN
		coeffLimitsAdd  := 0.0;
		coeffLimitsMult := 0.5;
	END IF;

	IF typeTransfoLimites = 'chronique.lq_ld.options.placeholder' THEN
		coeffLimitsAdd  := valuelimitplaceholder FROM jeubareme jb
			JOIN transformation t ON t.jeubaremeactuel_id = jb.id WHERE t.id = transfoId;
		coeffLimitsMult := 0.0;
	END IF;

	RETURN QUERY
		SELECT
			m.date,
			CASE
				WHEN b.nom = 'lacune' THEN
					ARRAY[-9999, 100]
				WHEN turnLimitsToGap AND q.ordre IN (600, 700) THEN
					ARRAY[-9999, 200]
				WHEN b.nom = 'identite' THEN
					CASE
						WHEN q.ordre IN (600, 700) THEN
							ARRAY[m.valeur * coeffLimitsMult + coeffLimitsAdd, q.ordre]
						ELSE
							ARRAY[m.valeur, q.ordre]
					END
				ELSE
					bdoh_interp_for_bareme(m.valeur, q.ordre, b.valeurs, turnLimitsToGap, coeffLimitsAdd, coeffLimitsMult)
			END
		FROM
			transformation t,
			mesure m,
			qualite q,
			baremejeubareme bjb,
			bareme b
		WHERE
			t.id=transfoId AND
			m.chronique_id=t.entree_id AND
			q.id=m.qualite_id AND
			bjb.jeubareme_id=t.jeubaremeactuel_id AND
			m.date >= bjb.debutValidite AND
			(m.date < bjb.finValidite OR bjb.finValidite IS NULL) AND
			b.id=bjb.bareme_id AND
			b.nom <> 'manuel'
		ORDER BY m.date ASC;

END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 1000;
SQL
        );
        $this->addSql(
            <<<'SQL'
ALTER FUNCTION bdoh_transformed_mesures(integer)
  OWNER TO bdoh;
SQL
        );
        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_interp_for_bareme(
    entree double precision,
    qualiteentree integer,
    bareme double precision[],
    limitsasgaps boolean DEFAULT false,
    limitsaddcoeff double precision DEFAULT 0.0,
    limitsmultcoeff double precision DEFAULT 1.0)
  RETURNS double precision[] AS
$BODY$

DECLARE

	entree_adaptee  DOUBLE PRECISION;
	minBareme       DOUBLE PRECISION;
	maxBareme       DOUBLE PRECISION;
	longueur        INTEGER;
	indice          INTEGER;
	indiceMin       INTEGER;
	indiceMax       INTEGER;
	lignePrecedente DOUBLE PRECISION[];
	ligneSuivante   DOUBLE PRECISION[];
	valeur          DOUBLE PRECISION;
	qualite         DOUBLE PRECISION;

BEGIN

        IF qualiteEntree IN (600, 700) THEN
                IF limitsAsGaps THEN
                    RETURN ARRAY[-9999, 200];
                END IF;
                entree_adaptee := entree * limitsMultCoeff + limitsAddCoeff;
        ELSE
                entree_adaptee := entree;
        END IF;

	minBareme := bareme[1][1];
	longueur := array_length(bareme, 1);
	maxBareme := bareme[longueur][1];

	IF qualiteEntree <= 200 THEN
		RETURN ARRAY[-9999, qualiteEntree];
	END IF;

	IF entree_adaptee < minBareme OR entree_adaptee > maxBareme THEN
		RETURN ARRAY[-9999, 100];
	END IF;

	indiceMin := 1;
	indiceMax := longueur - 1;
	indice := longueur / 2;

	WHILE entree_adaptee < bareme[indice][1]
		OR entree_adaptee > bareme[indice+1][1]
	LOOP
		IF entree_adaptee < bareme[indice][1] THEN
			indice := indice-1;
		ELSE
			indice := indice+1;
		END IF;
	END LOOP;

	lignePrecedente := bareme[indice:indice][1:3];
	ligneSuivante := bareme[indice+1:indice+1][1:3];

	IF entree_adaptee = lignePrecedente[1][1] THEN
		qualite := bdoh_cross_quality_orders(ROUND(lignePrecedente[1][3])::INTEGER, qualiteEntree, limitsAsGaps);
		valeur := lignePrecedente[1][2];

	ELSIF entree_adaptee = ligneSuivante[1][1] THEN
		qualite := bdoh_cross_quality_orders(ROUND(ligneSuivante[1][3])::INTEGER, qualiteEntree, limitsAsGaps);
		valeur := ligneSuivante[1][2];

	ELSE
		qualite := bdoh_cross_quality_orders(ROUND(lignePrecedente[1][3])::INTEGER, ROUND(ligneSuivante[1][3])::INTEGER, limitsAsGaps);
		qualite := bdoh_cross_quality_orders(ROUND(qualite)::INTEGER, qualiteEntree, limitsAsGaps);
		valeur := ((ligneSuivante[1][1] - entree_adaptee) * lignePrecedente[1][2]
			+ (entree_adaptee - lignePrecedente[1][1]) * ligneSuivante[1][2])
			/ (ligneSuivante[1][1] - lignePrecedente[1][1]);

	END IF;

	IF qualite <= 200 THEN
		valeur := -9999;
	END IF;

	RETURN ARRAY[valeur, qualite];

END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100;
SQL
        );
        $this->addSql(
            <<<'SQL'
ALTER FUNCTION bdoh_interp_for_bareme(double precision, integer, double precision[], boolean, double precision, double precision)
  OWNER TO bdoh;
SQL
        );
        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_interp_between_dates(
    dateinterp timestamp without time zone,
    dateavant timestamp without time zone,
    valeuravant double precision[],
    dateapres timestamp without time zone,
    valeurapres double precision[])
  RETURNS double precision[] AS
$BODY$

DECLARE

	valeur        DOUBLE PRECISION;
	qualite       DOUBLE PRECISION;
	secondes      DOUBLE PRECISION;
	secondesAvant DOUBLE PRECISION;
	secondesApres DOUBLE PRECISION;

BEGIN

	IF dateAvant IS NULL OR dateApres IS NULL OR valeurAvant IS NULL OR valeurApres IS NULL THEN
		RETURN ARRAY[-9999, 100];
	END IF;

	IF valeurAvant[2] <= 200 THEN
		RETURN ARRAY[-9999, valeurAvant[2]];
	END IF;

	IF valeurApres[2] <= 200 THEN
		RETURN ARRAY[-9999, valeurApres[2]];
	END IF;

	secondes := EXTRACT(EPOCH FROM dateInterp AT TIME ZONE 'UTC');
	secondesAvant := EXTRACT(EPOCH FROM dateAvant AT TIME ZONE 'UTC');
	secondesApres := EXTRACT(EPOCH FROM dateApres AT TIME ZONE 'UTC');
	valeur := ((secondesApres - secondes) * valeurAvant[1]
		+ (secondes - secondesAvant) * valeurApres[1])
		/ (secondesApres - secondesAvant);
	qualite := bdoh_cross_quality_orders(ROUND(valeurAvant[2])::INTEGER, ROUND(valeurApres[2])::INTEGER);

	RETURN ARRAY[valeur, qualite];

END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100;
SQL
        );
        $this->addSql(
            <<<'SQL'
ALTER FUNCTION bdoh_interp_between_dates(timestamp without time zone, timestamp without time zone, double precision[], timestamp without time zone, double precision[])
  OWNER TO bdoh;
SQL
        );
        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_purge_mesures_child_chronique(chroniquefilleid integer)
  RETURNS void AS
$BODY$

BEGIN

	DELETE FROM mesure WHERE chronique_id = chroniqueFilleId AND estcalculee;

	DELETE FROM mesure WHERE id IN (
		SELECT DISTINCT m.id
		FROM
			mesure m,
			chronique c,
			transformation t,
			baremejeubareme bjb,
			bareme b
		WHERE
			m.chronique_id = c.id AND
			c.id = chroniqueFilleId AND
			(t.id = c.premiereentree_id OR t.id = c.secondeentree_id) AND
			bjb.jeubareme_id = t.jeubaremeactuel_id AND
			m.date >= bjb.debutvalidite AND
			(m.date < bjb.finvalidite OR bjb.finvalidite IS NULL) AND
			bjb.bareme_id = b.id AND
			b.nom <> 'manuel'
	);

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
SQL
        );
        $this->addSql(
            <<<'SQL'
ALTER FUNCTION bdoh_purge_mesures_child_chronique(integer)
  OWNER TO bdoh;
SQL
        );
        $this->addSql(
            <<<'SQL'
DROP FUNCTION IF EXISTS bdoh_insert_mesures_two_transformations(integer, integer, integer, integer, double precision);
SQL
        );
        $this->addSql(
            <<<'SQL'
DROP FUNCTION IF EXISTS bdoh_interp_for_bareme(double precision, integer, double precision[]);
SQL
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_insert_mesures_two_transformations(
    chroniquefilleid integer,
    jeuqualiteid integer,
    transfomainid integer,
    transfosecondid integer,
    delaymain double precision DEFAULT 0.0,
    delaysecond double precision DEFAULT 0.0,
    coefficient double precision DEFAULT 1.0)
  RETURNS integer AS
$BODY$

DECLARE
	cursorMain         CURSOR FOR SELECT date + delayMain * INTERVAL '1 MINUTE', valeur
                                   FROM bdoh_transformed_mesures(transfoMainId) ORDER BY date;
	cursorSecond       CURSOR FOR SELECT date + delaySecond * INTERVAL '1 MINUTE', valeur
                                   FROM bdoh_transformed_mesures(transfoSecondId) ORDER BY date;
	dateMain           TIMESTAMP;
	dateMainAvant      TIMESTAMP;
	dateSecond         TIMESTAMP;
	dateSecondAvant    TIMESTAMP;
	valeurMain         DOUBLE PRECISION[];
	valeurMainAvant    DOUBLE PRECISION[];
	valeurSecond       DOUBLE PRECISION[];
	valeurSecondAvant  DOUBLE PRECISION[];
	dateInsert         TIMESTAMP;
	valeurMainInsert   DOUBLE PRECISION[];
	valeurSecondInsert DOUBLE PRECISION[];
	yInsert            DOUBLE PRECISION;
	qualiteInsert      DOUBLE PRECISION;
	qualiteId          INTEGER;
BEGIN
	OPEN cursorMain;
	OPEN cursorSecond;

	dateMainAvant := NULL;
	valeurMainAvant := NULL;
	dateSecondAvant := NULL;
	valeurSecondAvant := NULL;
	FETCH cursorMain INTO dateMain, valeurMain;
	FETCH cursorSecond INTO dateSecond, valeurSecond;

	WHILE dateMain IS NOT NULL OR dateSecond IS NOT NULL LOOP

		IF dateMain < dateSecond OR dateSecond IS NULL THEN
			dateInsert := dateMain;
			valeurMainInsert := valeurMain;
			valeurSecondInsert := bdoh_interp_between_dates(dateInsert, dateSecondAvant, valeurSecondAvant, dateSecond, valeurSecond);

			dateMainAvant := dateMain;
			valeurMainAvant := valeurMain;
			FETCH cursorMain INTO dateMain, valeurMain;

		ELSIF dateSecond < dateMain OR dateMain IS NULL THEN
			dateInsert := dateSecond;
			valeurMainInsert := bdoh_interp_between_dates(dateInsert, dateMainAvant, valeurMainAvant, dateMain, valeurMain);
			valeurSecondInsert := valeurSecond;

			dateSecondAvant := dateSecond;
			valeurSecondAvant := valeurSecond;
			FETCH cursorSecond INTO dateSecond, valeurSecond;

		ELSE
			dateInsert := dateMain;
			valeurMainInsert := valeurMain;
			valeurSecondInsert := valeurSecond;

			dateMainAvant := dateMain;
			valeurMainAvant := valeurMain;
			FETCH cursorMain INTO dateMain, valeurMain;
			dateSecondAvant := dateSecond;
			valeurSecondAvant := valeurSecond;
			FETCH cursorSecond INTO dateSecond, valeurSecond;

		END IF ;

                qualiteInsert := bdoh_cross_quality_orders(ROUND(valeurMainInsert[2])::INTEGER, ROUND(valeurSecondInsert[2])::INTEGER);
		IF qualiteInsert <= 200 THEN
			yInsert := -9999;
		ELSE
			yInsert := valeurMainInsert[1] * valeurSecondInsert[1] * coefficient;
		END IF;

		SELECT id INTO qualiteId
                    FROM qualite
                    WHERE ordre = ROUND(qualiteInsert)
                    AND jeu_id = jeuQualiteId
                    AND code <> 'gap';

		INSERT INTO mesure (id, qualite_id, chronique_id, date, valeur, estcalculee)
		VALUES(NEXTVAL('MESURE_ID_SEQ'), qualiteId, chroniqueFilleId, dateInsert, yInsert, true);

	END LOOP;

	CLOSE cursorMain;
	CLOSE cursorSecond;

	RETURN 0;

EXCEPTION

	WHEN OTHERS THEN
		RETURN 16;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
SQL
        );
        $this->addSql(
            <<<'SQL'
ALTER FUNCTION bdoh_insert_mesures_two_transformations(integer, integer, integer, integer, double precision, double precision, double precision)
  OWNER TO bdoh;
SQL
        );
        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_transformed_mesures(IN transfoid integer)
  RETURNS TABLE(date timestamp without time zone, valeur double precision[]) AS
$BODY$
DECLARE
        typeTransfoLimites VARCHAR;
        turnLimitsToGap    BOOLEAN;
        coeffLimitsAdd     DOUBLE PRECISION;
        coeffLimitsMult    DOUBLE PRECISION;
BEGIN

        typeTransfoLimites := valuelimittransformationtype FROM jeubareme jb
            JOIN transformation t ON t.jeubaremeactuel_id = jb.id WHERE t.id = transfoId;

        IF typeTransfoLimites = 'chronique.lq_ld.options.gap' THEN
            turnLimitsToGap := true;
            coeffLimitsAdd  := -9999.0;
            coeffLimitsMult := 0.0;
        ELSE
            turnLimitsToGap := false;
        END IF;

        IF typeTransfoLimites IS NULL OR typeTransfoLimites = 'chronique.lq_ld.options.true_value' THEN
            coeffLimitsAdd  := 0.0;
            coeffLimitsMult := 1.0;
        END IF;

        IF typeTransfoLimites = 'chronique.lq_ld.options.half_value' THEN
            coeffLimitsAdd  := 0.0;
            coeffLimitsMult := 0.5;
        END IF;

        IF typeTransfoLimites = 'chronique.lq_ld.options.placeholder' THEN
            coeffLimitsAdd  := valuelimitplaceholder FROM jeubareme jb
                JOIN transformation t ON t.jeubaremeactuel_id = jb.id WHERE t.id = transfoId;
            coeffLimitsMult := 0.0;
        END IF;

	RETURN QUERY
		SELECT m.date,
		CASE
                    WHEN b.nom = 'lacune' THEN
                        ARRAY[-9999, 100]
                    WHEN turnLimitsToGap AND q.ordre IN (600, 700) THEN
                        ARRAY[-9999, 200]
                    WHEN b.nom = 'identite' THEN
                        CASE
                            WHEN q.ordre IN (600, 700) THEN
                                ARRAY[m.valeur * coeffLimitsMult + coeffLimitsAdd, q.ordre]
                            ELSE
                                ARRAY[m.valeur, q.ordre]
                        END
                    WHEN b.nom = 'manuel' THEN
                        NULL
                    ELSE
                        replace(bdoh_interp_for_bareme(m.valeur, q.ordre, b.valeurs, turnLimitsToGap, coeffLimitsAdd, coeffLimitsMult)::varchar,
                            '{NULL,NULL}', '{-9999,100}')::DOUBLE PRECISION[]
		END
		FROM transformation t
		LEFT JOIN mesure m ON m.chronique_id = t.entree_id
		LEFT JOIN qualite q ON m.qualite_id = q.id
		LEFT JOIN jeuBareme jb ON jb.id = t.jeuBaremeActuel_id
		LEFT JOIN baremeJeuBareme bjb ON bjb.jeuBareme_id = jb.id AND m.date >= bjb.debutValidite AND (m.date < bjb.finValidite OR bjb.finValidite IS NULL)
		LEFT JOIN bareme b ON bjb.bareme_id = b.id
		WHERE t.id = transfoId
		AND b.nom <> 'manuel'
		ORDER BY m.valeur DESC;

END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 1000;
SQL
        );
        $this->addSql(
            <<<'SQL'
ALTER FUNCTION bdoh_transformed_mesures(integer)
  OWNER TO bdoh;
SQL
        );
        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_interp_for_bareme(
    entree double precision,
    qualiteentree integer,
    bareme double precision[],
    limitsasgaps boolean DEFAULT false,
    limitsaddcoeff double precision DEFAULT 0.0,
    limitsmultcoeff double precision DEFAULT 1.0)
  RETURNS double precision[] AS
$BODY$

DECLARE

        entree_adaptee DOUBLE PRECISION;
	minBareme       DOUBLE PRECISION;
	maxBareme       DOUBLE PRECISION;
	longueur        INTEGER;
	indice          INTEGER;
	indiceMin       INTEGER;
	indiceMax       INTEGER;
	lignePrecedente DOUBLE PRECISION[];
	ligneSuivante   DOUBLE PRECISION[];
        valeur          DOUBLE PRECISION;
	qualite         DOUBLE PRECISION;
BEGIN

        IF qualiteEntree IN (600, 700) THEN
                IF limitsAsGaps THEN
                    RETURN ARRAY[-9999.0, 200.0];
                END IF;
                entree_adaptee := entree * limitsMultCoeff + limitsAddCoeff;
        ELSE
                entree_adaptee := entree;
        END IF;

	minBareme := bareme[1][1];
	longueur := array_length(bareme, 1);
	maxBareme := bareme[longueur][1];

	IF qualiteEntree <= 200 THEN
		RETURN ARRAY[-9999.0, qualiteEntree];
	END IF;
	IF entree_adaptee < minBareme OR entree_adaptee > maxBareme
	THEN
		RETURN ARRAY[-9999.0, 100.0];
	END IF;

	indiceMin := 1;
	indiceMax := longueur - 1;
	indice := longueur / 2;

	WHILE entree_adaptee < bareme[indice][1]
		OR entree_adaptee > bareme[indice+1][1]
	LOOP
		IF entree_adaptee < bareme[indice][1]
		THEN indice := indice-1;
		ELSE indice := indice+1;
		END IF;
	END LOOP;

	lignePrecedente := bareme[indice:indice][1:3];
	ligneSuivante := bareme[indice+1:indice+1][1:3];
        qualite := bdoh_cross_quality_orders(ROUND(lignePrecedente[1][3])::INTEGER, ROUND(ligneSuivante[1][3])::INTEGER, limitsAsGaps);
        qualite := bdoh_cross_quality_orders(ROUND(qualite)::INTEGER, qualiteEntree, limitsAsGaps);

        valeur := ((ligneSuivante[1][1] - entree_adaptee) * lignePrecedente[1][2]
                + (entree_adaptee - lignePrecedente[1][1]) * ligneSuivante[1][2])
                / (ligneSuivante[1][1] - lignePrecedente[1][1]);

	RETURN ARRAY[valeur, qualite];

END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100;
SQL
        );
        $this->addSql(
            <<<'SQL'
ALTER FUNCTION bdoh_interp_for_bareme(double precision, integer, double precision[], boolean, double precision, double precision)
  OWNER TO bdoh;
SQL
        );
        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_interp_between_dates(
    dateinterp timestamp without time zone,
    dateavant timestamp without time zone,
    valeuravant double precision[],
    dateapres timestamp without time zone,
    valeurapres double precision[])
  RETURNS double precision[] AS
$BODY$ -- TODO voir type renvoyé par la fonction

DECLARE

	valeur        DOUBLE PRECISION;
	qualite       DOUBLE PRECISION;
	secondes      DOUBLE PRECISION;
	secondesAvant DOUBLE PRECISION;
	secondesApres DOUBLE PRECISION;

BEGIN

	IF dateAvant IS NULL OR dateApres IS NULL OR valeurAvant IS NULL OR valeurApres IS NULL THEN
		RETURN ARRAY[-9999, 100];
	END IF;

	IF valeurAvant[2] <= 200 THEN
		RETURN ARRAY[-9999, valeurAvant[2]];
	END IF;

	IF valeurApres[2] <= 200 THEN
		RETURN valeurAvant;
	END IF;

	secondes := EXTRACT(EPOCH FROM dateInterp AT TIME ZONE 'UTC');
	secondesAvant := EXTRACT(EPOCH FROM dateAvant AT TIME ZONE 'UTC');
	secondesApres := EXTRACT(EPOCH FROM dateApres AT TIME ZONE 'UTC');
	valeur := ((secondesApres - secondes) * valeurAvant[1]
		+ (secondes - secondesAvant) * valeurApres[1])
		/ (secondesApres - secondesAvant);
	qualite := bdoh_cross_quality_orders(ROUND(valeurAvant[2])::INTEGER, ROUND(valeurApres[2])::INTEGER);
	RETURN ARRAY[valeur, qualite];

END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100;
SQL
        );
        $this->addSql(
            <<<'SQL'
ALTER FUNCTION bdoh_interp_between_dates(timestamp without time zone, timestamp without time zone, double precision[], timestamp without time zone, double precision[])
  OWNER TO bdoh;
SQL
        );
        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_purge_mesures_child_chronique(chroniquefilleid integer)
  RETURNS void AS
$BODY$

BEGIN

        DELETE FROM mesure WHERE chronique_id = chroniqueFilleId AND estcalculee;

	DELETE FROM mesure WHERE id IN (
		SELECT m.id
		FROM mesure m
		LEFT JOIN chronique c on c.id = m.chronique_id
		LEFT JOIN transformation t ON t.id = c.premiereentree_id OR t.id = c.secondeentree_id
		LEFT JOIN jeuBareme jb ON jb.id = t.jeuBaremeActuel_id
		LEFT JOIN baremeJeuBareme bjb ON bjb.jeuBareme_id = jb.id AND m.date >= bjb.debutValidite AND (m.date < bjb.finValidite OR bjb.finValidite IS NULL)
		LEFT JOIN bareme b ON bjb.bareme_id = b.id
		WHERE m.chronique_id = chroniqueFilleId
		AND b.nom <> 'manuel'
	);

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
SQL
        );
        $this->addSql(
            <<<'SQL'
ALTER FUNCTION bdoh_purge_mesures_child_chronique(integer)
  OWNER TO bdoh;
SQL
        );
        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_insert_mesures_two_transformations(
    chroniquefilleid integer,
    jeuqualiteid integer,
    transfomainid integer,
    transfosecondid integer,
    coefficient double precision DEFAULT 1.0)
  RETURNS integer AS
$BODY$

DECLARE

	cursorMain         CURSOR FOR SELECT date, valeur FROM bdoh_transformed_mesures(transfoMainId) ORDER BY date;
	cursorSecond       CURSOR FOR SELECT date, valeur FROM bdoh_transformed_mesures(transfoSecondId) ORDER BY date;
	dateMain           TIMESTAMP;
	dateMainAvant      TIMESTAMP;
	dateSecond         TIMESTAMP;
	dateSecondAvant    TIMESTAMP;
	valeurMain         DOUBLE PRECISION[];
	valeurMainAvant    DOUBLE PRECISION[];
	valeurSecond       DOUBLE PRECISION[];
	valeurSecondAvant  DOUBLE PRECISION[];
	dateInsert         TIMESTAMP;
	valeurMainInsert   DOUBLE PRECISION[];
	valeurSecondInsert DOUBLE PRECISION[];
	yInsert            DOUBLE PRECISION;
	qualiteInsert      DOUBLE PRECISION;
	qualiteId          INTEGER;
BEGIN

	OPEN cursorMain;
	OPEN cursorSecond;

	dateMainAvant := NULL;
	valeurMainAvant := NULL;
	dateSecondAvant := NULL;
	valeurSecondAvant := NULL;
	FETCH cursorMain INTO dateMain, valeurMain;
	FETCH cursorSecond INTO dateSecond, valeurSecond;

	WHILE dateMain IS NOT NULL OR dateSecond IS NOT NULL LOOP

		IF dateMain < dateSecond OR dateSecond IS NULL THEN
			dateInsert := dateMain;
			valeurMainInsert := valeurMain;
			valeurSecondInsert := bdoh_interp_between_dates(dateInsert, dateSecondAvant, valeurSecondAvant, dateSecond, valeurSecond);

			dateMainAvant := dateMain;
			valeurMainAvant := valeurMain;
			FETCH cursorMain INTO dateMain, valeurMain;

		ELSIF dateSecond < dateMain OR dateMain IS NULL THEN
			dateInsert := dateSecond;
			valeurMainInsert := bdoh_interp_between_dates(dateInsert, dateMainAvant, valeurMainAvant, dateMain, valeurMain);
			valeurSecondInsert := valeurSecond;

			dateSecondAvant := dateSecond;
			valeurSecondAvant := valeurSecond;
			FETCH cursorSecond INTO dateSecond, valeurSecond;

		ELSE
			dateInsert := dateMain;
			valeurMainInsert := valeurMain;
			valeurSecondInsert := valeurSecond;

			dateMainAvant := dateMain;
			valeurMainAvant := valeurMain;
			FETCH cursorMain INTO dateMain, valeurMain;
			dateSecondAvant := dateSecond;
			valeurSecondAvant := valeurSecond;
			FETCH cursorSecond INTO dateSecond, valeurSecond;

		END IF ;

		qualiteInsert := valeurMainInsert[2];
		IF valeurSecondInsert[2] < qualiteInsert THEN
			qualiteInsert := valeurSecondInsert[2];
		END IF;
		IF qualiteInsert < 2 THEN
			yInsert := -9999;
		ELSE
			yInsert := valeurMainInsert[1] * valeurSecondInsert[1] * coefficient;
		END IF;

		qualiteId = (SELECT id FROM qualite WHERE ordre = ROUND(qualiteInsert) AND jeu_id = jeuQualiteId AND code <> 'gap');

		INSERT INTO mesure (id, qualite_id, chronique_id, date, valeur, estcalculee)
		VALUES(NEXTVAL('MESURE_ID_SEQ'), qualiteId, chroniqueFilleId, dateInsert, yInsert, true);

	END LOOP;

	CLOSE cursorMain;
	CLOSE cursorSecond;

	RETURN 0;

EXCEPTION

	WHEN OTHERS THEN
		RETURN 16;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
SQL
        );
        $this->addSql(
            <<<'SQL'
ALTER FUNCTION bdoh_insert_mesures_two_transformations(integer, integer, integer, integer, double precision)
  OWNER TO bdoh;
SQL
        );
        $this->addSql(
            <<<'SQL'
CREATE OR REPLACE FUNCTION bdoh_interp_for_bareme(
    entree double precision,
    qualiteentree integer,
    bareme double precision[])
  RETURNS double precision[] AS
$BODY$

DECLARE

	minBareme       DOUBLE PRECISION;
	maxBareme       DOUBLE PRECISION;
	longueur        INTEGER;
	indice          INTEGER;
	indiceMin       INTEGER;
	indiceMax       INTEGER;
	lignePrecedente DOUBLE PRECISION[];
	ligneSuivante   DOUBLE PRECISION[];
	qualite         DOUBLE PRECISION;
BEGIN

	minBareme := bareme[1][1];
	longueur := array_length(bareme, 1);
	maxBareme := bareme[longueur][1];

	IF qualiteEntree < 2 THEN
		RETURN ARRAY[-9999.0, qualiteEntree];
	END IF;
	IF entree < minBareme OR entree > maxBareme
	THEN
		RETURN ARRAY[-9999.0, 0.0];
	END IF;

	indiceMin := 1;
	indiceMax := longueur - 1;
	indice := longueur / 2;

	WHILE entree < bareme[indice][1]
		OR entree > bareme[indice+1][1]
	LOOP
		IF entree < bareme[indice][1]
		THEN indice := indice-1;
		ELSE indice :=indice+1;
		END IF;
	END LOOP;

	lignePrecedente := bareme[indice:indice][1:3];
	ligneSuivante := bareme[indice+1:indice+1][1:3];
	qualite := lignePrecedente[1][3];
	IF ligneSuivante[1][3] < qualite THEN
		qualite := ligneSuivante[1][3];
	END IF;
	IF qualiteEntree < qualite THEN
		qualite := qualiteEntree;
	END IF;

	RETURN ARRAY[
		((ligneSuivante[1][1] - entree) * lignePrecedente[1][2]
			+ (entree - lignePrecedente[1][1]) * ligneSuivante[1][2]
			) / (ligneSuivante[1][1] - lignePrecedente[1][1]),
		qualite
		];

END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100;
SQL
        );
        $this->addSql(
            <<<'SQL'
ALTER FUNCTION bdoh_interp_for_bareme(double precision, integer, double precision[])
  OWNER TO bdoh;
SQL
        );
    }
}
