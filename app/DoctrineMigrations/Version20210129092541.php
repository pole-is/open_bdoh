<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210129092541 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE partenaire_observatoire (observatoire_id INT NOT NULL, partenaire_id INT NOT NULL, PRIMARY KEY(observatoire_id, partenaire_id))');
        $this->addSql('CREATE INDEX IDX_ACB4456E61ED1B0D ON partenaire_observatoire (observatoire_id)');
        $this->addSql('CREATE INDEX IDX_ACB4456E98DE13AC ON partenaire_observatoire (partenaire_id)');
        $this->addSql('ALTER TABLE partenaire_observatoire ADD CONSTRAINT FK_ACB4456E61ED1B0D FOREIGN KEY (observatoire_id) REFERENCES Observatoire (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE partenaire_observatoire ADD CONSTRAINT FK_ACB4456E98DE13AC FOREIGN KEY (partenaire_id) REFERENCES Partenaire (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE partenaire DROP CONSTRAINT fk_7da2a0a361ed1b0d');
        $this->addSql('DROP INDEX idx_7da2a0a361ed1b0d');
        $this->addSql('ALTER TABLE partenaire DROP observatoire_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE partenaire_observatoire');
        $this->addSql('DROP INDEX UNIQ_90B54B32EC0C1EEE');
        $this->addSql('ALTER TABLE Partenaire ADD observatoire_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE Partenaire ADD CONSTRAINT fk_7da2a0a361ed1b0d FOREIGN KEY (observatoire_id) REFERENCES observatoire (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_7da2a0a361ed1b0d ON Partenaire (observatoire_id)');
    }
}
