<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210120125300 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE personnetheiaetrolepersonnetheia DROP CONSTRAINT fk_76f8e7903b2fbbd6');
        $this->addSql('ALTER TABLE personne_theia_et_role_personne_theia_observatoire DROP CONSTRAINT fk_850435dfcd8785ec');
        $this->addSql('ALTER TABLE personne_theia_et_role_personne_theia_theia_dataset DROP CONSTRAINT fk_ef59972bcd8785ec');
        $this->addSql('DROP SEQUENCE bdoh.personnetheiaetrolepersonnetheia_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE bdoh.rolepersonnetheia_id_seq CASCADE');
        $this->addSql('CREATE TABLE observatoires_data_manager (observatoire_id INT NOT NULL, partenaire_id INT NOT NULL, PRIMARY KEY(observatoire_id, partenaire_id))');
        $this->addSql('CREATE INDEX IDX_26C6793A61ED1B0D ON observatoires_data_manager (observatoire_id)');
        $this->addSql('CREATE INDEX IDX_26C6793A98DE13AC ON observatoires_data_manager (partenaire_id)');
        $this->addSql('ALTER TABLE observatoires_data_manager ADD CONSTRAINT FK_26C6793A61ED1B0D FOREIGN KEY (observatoire_id) REFERENCES Observatoire (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE observatoires_data_manager ADD CONSTRAINT FK_26C6793A98DE13AC FOREIGN KEY (partenaire_id) REFERENCES PersonneTheia (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE rolepersonnetheia');
        $this->addSql('DROP TABLE personnetheiaetrolepersonnetheia');
        $this->addSql('DROP TABLE personne_theia_et_role_personne_theia_observatoire');
        $this->addSql('DROP TABLE personne_theia_et_role_personne_theia_theia_dataset');
        $this->addSql('ALTER TABLE observatoire ADD projectLeader_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE observatoire ADD CONSTRAINT FK_90B54B32E26A02CC FOREIGN KEY (projectLeader_id) REFERENCES PersonneTheia (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_90B54B32EC0C1EEE ON observatoire (theiaCode)');
        $this->addSql('CREATE INDEX IDX_90B54B32E26A02CC ON observatoire (projectLeader_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE bdoh.personnetheiaetrolepersonnetheia_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE bdoh.rolepersonnetheia_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE rolepersonnetheia (id INT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE personnetheiaetrolepersonnetheia (id INT NOT NULL, personnetheia_id INT DEFAULT NULL, rolepersonnetheia_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_76f8e7903b2fbbd6 ON personnetheiaetrolepersonnetheia (rolepersonnetheia_id)');
        $this->addSql('CREATE INDEX idx_76f8e79089274090 ON personnetheiaetrolepersonnetheia (personnetheia_id)');
        $this->addSql('CREATE TABLE personne_theia_et_role_personne_theia_observatoire (dataset_id INT NOT NULL, contacttheia_id INT NOT NULL, PRIMARY KEY(dataset_id, contacttheia_id))');
        $this->addSql('CREATE INDEX idx_850435dfcd8785ec ON personne_theia_et_role_personne_theia_observatoire (contacttheia_id)');
        $this->addSql('CREATE INDEX idx_850435dfd47c2d1b ON personne_theia_et_role_personne_theia_observatoire (dataset_id)');
        $this->addSql('CREATE TABLE personne_theia_et_role_personne_theia_theia_dataset (dataset_id INT NOT NULL, contacttheia_id INT NOT NULL, PRIMARY KEY(dataset_id, contacttheia_id))');
        $this->addSql('CREATE INDEX idx_ef59972bcd8785ec ON personne_theia_et_role_personne_theia_theia_dataset (contacttheia_id)');
        $this->addSql('CREATE INDEX idx_ef59972bd47c2d1b ON personne_theia_et_role_personne_theia_theia_dataset (dataset_id)');
        $this->addSql('ALTER TABLE personnetheiaetrolepersonnetheia ADD CONSTRAINT fk_76f8e7903b2fbbd6 FOREIGN KEY (rolepersonnetheia_id) REFERENCES rolepersonnetheia (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE personnetheiaetrolepersonnetheia ADD CONSTRAINT fk_76f8e79089274090 FOREIGN KEY (personnetheia_id) REFERENCES personnetheia (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE personne_theia_et_role_personne_theia_observatoire ADD CONSTRAINT fk_850435dfcd8785ec FOREIGN KEY (contacttheia_id) REFERENCES personnetheiaetrolepersonnetheia (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE personne_theia_et_role_personne_theia_observatoire ADD CONSTRAINT fk_850435dfd47c2d1b FOREIGN KEY (dataset_id) REFERENCES observatoire (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE personne_theia_et_role_personne_theia_theia_dataset ADD CONSTRAINT fk_ef59972bcd8785ec FOREIGN KEY (contacttheia_id) REFERENCES personnetheiaetrolepersonnetheia (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE personne_theia_et_role_personne_theia_theia_dataset ADD CONSTRAINT fk_ef59972bd47c2d1b FOREIGN KEY (dataset_id) REFERENCES dataset (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE observatoires_data_manager');
        $this->addSql('ALTER TABLE Observatoire DROP CONSTRAINT FK_90B54B32E26A02CC');
        $this->addSql('DROP INDEX UNIQ_90B54B32EC0C1EEE');
        $this->addSql('DROP INDEX IDX_90B54B32E26A02CC');
        $this->addSql('ALTER TABLE Observatoire DROP projectLeader_id');
    }
}
