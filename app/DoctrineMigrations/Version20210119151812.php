<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210119151812 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DELETE FROM Partenaire');
        $this->addSql('DROP TABLE observatoires_partenaires_institutionnels');
        $this->addSql('DROP TABLE observatoires_partenaires_scientifiques');
        $this->addSql('ALTER TABLE partenaire ADD observatoire_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE partenaire ALTER estfinanceur DROP DEFAULT');
        $this->addSql('ALTER TABLE partenaire ADD CONSTRAINT FK_7DA2A0A361ED1B0D FOREIGN KEY (observatoire_id) REFERENCES Observatoire (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_7DA2A0A361ED1B0D ON partenaire (observatoire_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE observatoires_partenaires_institutionnels (observatoire_id INT NOT NULL, partenaire_id INT NOT NULL, PRIMARY KEY(observatoire_id, partenaire_id))');
        $this->addSql('CREATE INDEX idx_ddfc381e98de13ac ON observatoires_partenaires_institutionnels (partenaire_id)');
        $this->addSql('CREATE INDEX idx_ddfc381e61ed1b0d ON observatoires_partenaires_institutionnels (observatoire_id)');
        $this->addSql('CREATE TABLE observatoires_partenaires_scientifiques (observatoire_id INT NOT NULL, partenaire_id INT NOT NULL, PRIMARY KEY(observatoire_id, partenaire_id))');
        $this->addSql('CREATE INDEX idx_5c80b31d61ed1b0d ON observatoires_partenaires_scientifiques (observatoire_id)');
        $this->addSql('CREATE INDEX idx_5c80b31d98de13ac ON observatoires_partenaires_scientifiques (partenaire_id)');
        $this->addSql('ALTER TABLE observatoires_partenaires_institutionnels ADD CONSTRAINT fk_ddfc381e98de13ac FOREIGN KEY (partenaire_id) REFERENCES partenaire (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE observatoires_partenaires_institutionnels ADD CONSTRAINT fk_ddfc381e61ed1b0d FOREIGN KEY (observatoire_id) REFERENCES observatoire (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE observatoires_partenaires_scientifiques ADD CONSTRAINT fk_5c80b31d98de13ac FOREIGN KEY (partenaire_id) REFERENCES partenaire (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE observatoires_partenaires_scientifiques ADD CONSTRAINT fk_5c80b31d61ed1b0d FOREIGN KEY (observatoire_id) REFERENCES observatoire (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE Partenaire DROP CONSTRAINT FK_7DA2A0A361ED1B0D');
        $this->addSql('DROP INDEX IDX_7DA2A0A361ED1B0D');
        $this->addSql('ALTER TABLE Partenaire DROP observatoire_id');
        $this->addSql('ALTER TABLE Partenaire ALTER estFinanceur SET DEFAULT \'false\'');
    }
}
