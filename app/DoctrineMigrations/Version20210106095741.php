<?php

/*
 * Base de Données des Observatoires en Hydrologie
 * Copyright (C) 2012-2019 IRSTEA
 * Copyright (C) 2020-2021 INRAE
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Irstea\Bdoh\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210106095741 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE PersonneTheia_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE PersonneTheia (id INT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE utilisateur_personnetheia (utilisateur_id INT NOT NULL, personnetheia_id INT NOT NULL, PRIMARY KEY(utilisateur_id, personnetheia_id))');
        $this->addSql('CREATE INDEX IDX_6DC1E190FB88E14F ON utilisateur_personnetheia (utilisateur_id)');
        $this->addSql('CREATE INDEX IDX_6DC1E19089274090 ON utilisateur_personnetheia (personnetheia_id)');
        $this->addSql('ALTER TABLE utilisateur_personnetheia ADD CONSTRAINT FK_6DC1E190FB88E14F FOREIGN KEY (utilisateur_id) REFERENCES Utilisateur (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE utilisateur_personnetheia ADD CONSTRAINT FK_6DC1E19089274090 FOREIGN KEY (personnetheia_id) REFERENCES PersonneTheia (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE utilisateur_personnetheia DROP CONSTRAINT FK_6DC1E19089274090');
        $this->addSql('DROP SEQUENCE PersonneTheia_id_seq CASCADE');
        $this->addSql('DROP TABLE PersonneTheia');
        $this->addSql('DROP TABLE utilisateur_personnetheia');
        $this->addSql('DROP INDEX UNIQ_90B54B32EC0C1EEE');
    }
}
