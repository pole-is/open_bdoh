CREATE OR REPLACE FUNCTION bdoh_interp_to_cumulative(
        idChronique      INTEGER,
        beginDate        TIMESTAMP WITHOUT TIME ZONE,
        endDate          TIMESTAMP WITHOUT TIME ZONE,
        samplingStepId   INTEGER,
        startAt          VARCHAR DEFAULT 'round-hour'
)
RETURNS TABLE(boundary_start TIMESTAMP WITHOUT TIME ZONE, boundary_end TIMESTAMP WITHOUT TIME ZONE,
        valeur DOUBLE PRECISION, qualite VARCHAR) AS $$
DECLARE
        inputSampling VARCHAR;
BEGIN

        SELECT COALESCE(LOWER(e.code), 'instantaneous') INTO inputSampling
            FROM echantillonnage e
            LEFT JOIN chronique c ON c.echantillonnage_id = e.id
            WHERE c.id = idChronique;

        IF inputSampling IN('instantaneous', 'mean') THEN
            RETURN QUERY SELECT * FROM
                bdoh_interp_to_mean_or_cumul(idChronique, beginDate, endDate, samplingStepId, false, startAt);
        ELSIF inputSampling = 'cumulative' THEN
            RETURN QUERY SELECT * FROM
                bdoh_interp_cumul_to_cumul(idChronique, beginDate, endDate, samplingStepId, startAt);
        ELSE
            RETURN;
        END IF;

END;
$$ LANGUAGE 'plpgsql' VOLATILE;
