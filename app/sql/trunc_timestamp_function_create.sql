-- Function: trunc_timestamp(timestamp without time zone, integer)

-- This line is only useful if the language is not already active on DB :
--CREATE LANGUAGE plpgsql;

-- DROP FUNCTION trunc_timestamp(timestamp without time zone, integer);

CREATE OR REPLACE FUNCTION trunc_timestamp(my_timestamp timestamp without time zone, my_precision integer)
  RETURNS timestamp without time zone AS
$BODY$
DECLARE
	day_beginning TIMESTAMP;
	gap_seconds   INTEGER;
	precision_day INTEGER;
	precision_sec INTEGER;
BEGIN
	IF     my_precision = 1     THEN RETURN DATE_TRUNC('second', my_timestamp);
	ELSEIF my_precision = 60    THEN RETURN DATE_TRUNC('minute', my_timestamp);
	ELSEIF my_precision = 3600  THEN RETURN DATE_TRUNC('hour',   my_timestamp);
	ELSEIF my_precision = 86400 THEN RETURN DATE_TRUNC('day',    my_timestamp);
	ELSE
		precision_day := my_precision / 86400;
		precision_sec := my_precision % 86400;
		day_beginning := DATE_TRUNC('day', my_timestamp);
		gap_seconds   := DATE_PART('epoch', (my_timestamp - day_beginning))::INTEGER;
		RETURN my_timestamp - (gap_seconds % precision_sec) * '1 second'::INTERVAL
		                    - precision_day * '1 day'::INTERVAL;
	END IF;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
--ALTER FUNCTION trunc_timestamp(timestamp without time zone, integer) OWNER TO postgres;
