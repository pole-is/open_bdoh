CREATE OR REPLACE FUNCTION bdoh_compute_chronique(
  	chroniqueFilleId INTEGER
	)
	RETURNS INTEGER AS $$

DECLARE
	transfoMainId   INTEGER;
	transfoSecondId INTEGER;
	jeuQualiteId    INTEGER;
        delayMain       DOUBLE PRECISION;
        delaySecond     DOUBLE PRECISION;
BEGIN

        jeuQualiteId := bdoh_jeu_qualite_id(chroniqueFilleId);

	PERFORM bdoh_purge_mesures_child_chronique(chroniqueFilleId);

	transfoMainId := (SELECT premiereentree_id FROM chronique WHERE id = chroniqueFilleId);
	transfoSecondId := (SELECT secondeentree_id FROM chronique WHERE id = chroniqueFilleId);

	IF transfoSecondId IS NULL THEN
		RETURN bdoh_insert_mesures_one_transformation(chroniqueFilleId, jeuQualiteId, transfoMainId);

	ELSE
                delayMain := (SELECT j.delaipropagation FROM jeubareme j WHERE j.id =
                        (SELECT t.jeubaremeactuel_id FROM transformation t WHERE t.id = transfoMainId));
                IF delayMain IS NULL THEN
                        delayMain := 0;
                END IF;
                delaySecond := (SELECT j.delaipropagation FROM jeubareme j WHERE j.id =
                        (SELECT t.jeubaremeactuel_id FROM transformation t WHERE t.id = transfoSecondId));
                IF delaySecond IS NULL THEN
                        delaySecond := 0;
                END IF;

		RETURN bdoh_insert_mesures_two_transformations(chroniqueFilleId, jeuQualiteId, transfoMainId, transfoSecondId,
			delayMain, delaySecond, (SELECT facteurmultiplicatif FROM chronique WHERE id = chroniqueFilleId));

	END IF;

EXCEPTION

	WHEN OTHERS THEN
		RETURN 32;

END;
$$ LANGUAGE 'plpgsql' VOLATILE;