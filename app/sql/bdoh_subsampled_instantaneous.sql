-- Stored procedures used to extract subsets of measures from a chronicle.
-- Measures are to be arranged into "series", each with a given start date and an end date.
-- We subsample measures in each series as follow: the span of the series is divided in time subintervals
-- of a given small span, and in each, we keep the min & max values with the corresponding dates. We also
-- keep the very first & very last measures of the series.

-- Gives the measures for a subsampled series
CREATE OR REPLACE FUNCTION bdoh_subsampled_instantaneous_series_measures(
	chroniqueId INTEGER,
	startDate TIMESTAMP,
	endDate TIMESTAMP,
	subinterval DOUBLE PRECISION
)
RETURNS TABLE(milliseconds DOUBLE PRECISION, value DOUBLE PRECISION)
AS $FUNC1$
BEGIN
	-- WITH clause yields measures with date in milliseconds, subinterval index (starting from zero) and value
	-- First subselect (alias "vmin") yields, for each subinterval, the date & value for the measure with smallest value
	-- Second subselect (alias "vmax") yields, for each subinterval, the date & value for the largest measure with greatest value
	-- Finally, we append the very first & last measures in our date interval

	RETURN QUERY
		WITH miv(millisecs, index, val) AS (
			SELECT 1000 * EXTRACT(EPOCH FROM date AT TIME ZONE 'UTC'),
			(EXTRACT(EPOCH FROM AGE(date, startDate))/subinterval)::INTEGER,
			valeur
			FROM mesure WHERE chronique_id = chroniqueId
			AND date >= startDate AND date <= endDate
			ORDER BY date
		)
		SELECT millisecs, val FROM (SELECT * FROM miv LIMIT 1) first_m -- very first measure
		UNION
		SELECT millisecs, val FROM -- row with min value per subinterval
		(
			SELECT ROW_NUMBER() OVER(PARTITION BY index ORDER BY val ASC) AS rank,
			millisecs,
			val
			FROM miv
		) vmin
		WHERE vmin.rank = 1
		UNION
		SELECT millisecs, val FROM -- row with max value per subinterval
		(
			SELECT ROW_NUMBER() OVER(PARTITION BY index ORDER BY val DESC) AS rank,
			millisecs,
			val
			FROM miv
		) vmax
		WHERE vmax.rank = 1
		UNION
		SELECT millisecs, val FROM (SELECT * FROM miv ORDER BY millisecs DESC LIMIT 1) last_m -- very last measure
		ORDER BY millisecs
        ;
END;
$FUNC1$ LANGUAGE 'plpgsql' STABLE;

-- Gives the measures for a set of subsampled series
-- NOTE: we assume that startDates, endDates, subintervals are equally sized and do not proceed to any checking
CREATE OR REPLACE FUNCTION bdoh_subsampled_instantaneous_all_series_measures(
	chroniqueId INTEGER,
	startDates TIMESTAMP[],
	endDates TIMESTAMP[],
	subintervals DOUBLE PRECISION[]
)
RETURNS TABLE(milliseconds DOUBLE PRECISION, value DOUBLE PRECISION)
AS $FUNC2$
DECLARE
	nSeries BIGINT;
	i BIGINT;
BEGIN
	nSeries := ARRAY_LENGTH(startDates, 1);
	FOR i IN 1..nSeries LOOP
		RETURN QUERY SELECT * FROM bdoh_subsampled_instantaneous_series_measures(chroniqueId,
			startDates[i], endDates[i], subintervals[i]);
	END LOOP;

END;
$FUNC2$ LANGUAGE 'plpgsql' STABLE;

-- Gives the measure counts for a set of subsampled series
-- NOTE: we assume that startDates, endDates, subintervals are equally sized and do not proceed to any checking
CREATE OR REPLACE FUNCTION bdoh_subsampled_instantaneous_all_series_lengths(
	chroniqueId INTEGER,
	startDates TIMESTAMP[],
	endDates TIMESTAMP[],
	subintervals DOUBLE PRECISION[]
)
RETURNS TABLE(measure_count BIGINT)
AS $FUNC3$
DECLARE
	nSeries BIGINT;
	i BIGINT;
BEGIN
	nSeries := ARRAY_LENGTH(startDates, 1);
	FOR i IN 1..nSeries LOOP
		RETURN QUERY SELECT COUNT(*) FROM bdoh_subsampled_instantaneous_series_measures(chroniqueId,
			startDates[i], endDates[i], subintervals[i]);
	END LOOP;

END;
$FUNC3$ LANGUAGE 'plpgsql' STABLE;