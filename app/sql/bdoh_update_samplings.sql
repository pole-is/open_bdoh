UPDATE echantillonnage SET hasdirection = false, ordresortie = 4, code = 'instantaneous', nom = 'Interpolation linéaire',
    nomEn = 'Linear interpolation' WHERE (nom LIKE 'Instantan%' OR nom = 'Interpolation linéaire');
UPDATE echantillonnage SET hasdirection = true, ordresortie = 8, code = 'mean', nomEn = 'Mean' WHERE nom = 'Moyenne';
UPDATE echantillonnage SET hasdirection = true, ordresortie = 12, code = 'cumulative', nomEn = 'Accumulation' WHERE nom = 'Cumul';
