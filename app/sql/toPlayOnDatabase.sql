-- "Majusculisation" de tous les codes
update station set code = upper(code);
update chronique set code = upper(code);
update siteexperimental set slug = upper(slug);
update observatoire set slug = upper(slug);

-- Déclaration à la main des colonnes géométriques
delete from geometry_columns;

insert into geometry_columns
(
  f_table_catalog,
  f_table_schema,
  f_table_name,
  f_geometry_column,
  coord_dimension,
  srid,
  "type"
)
values
(
'', 'bdoh', 'station', 'point', 2, 2154, 'GEOMETRY' --'POINT'
);

insert into geometry_columns
(
  f_table_catalog,
  f_table_schema,
  f_table_name,
  f_geometry_column,
  coord_dimension,
  srid,
  "type"
)
values
(
'', 'bdoh', 'bassin', 'perimetre', 2, 2154, 'GEOMETRY' --'POLYGON'
);

insert into geometry_columns
(
  f_table_catalog,
  f_table_schema,
  f_table_name,
  f_geometry_column,
  coord_dimension,
  srid,
  "type"
)
values
(
'', 'bdoh', 'courseau', 'trace', 2, 2154, 'GEOMETRY' --'LINESTRING'
);