CREATE OR REPLACE FUNCTION bdoh_interp_between_dates(
	dateInterp TIMESTAMP,
	dateAvant TIMESTAMP,
	valeurAvant DOUBLE PRECISION[],
	dateApres TIMESTAMP,
	valeurApres DOUBLE PRECISION[]
	)
	RETURNS DOUBLE PRECISION[] AS $$ -- TODO voir type renvoyé par la fonction

DECLARE

	valeur        DOUBLE PRECISION;
	qualite       DOUBLE PRECISION;
	secondes      DOUBLE PRECISION;
	secondesAvant DOUBLE PRECISION;
	secondesApres DOUBLE PRECISION;

BEGIN

	IF dateAvant IS NULL OR dateApres IS NULL OR valeurAvant IS NULL OR valeurApres IS NULL THEN
		RETURN ARRAY[-9999, 100];
	END IF;

	IF valeurAvant[2] < 300 THEN
		RETURN ARRAY[-9999, valeurAvant[2]];
	END IF;

	IF valeurApres[2] < 300 THEN
		RETURN valeurAvant;
	END IF;

	secondes := EXTRACT(EPOCH FROM dateInterp AT TIME ZONE 'UTC');
	secondesAvant := EXTRACT(EPOCH FROM dateAvant AT TIME ZONE 'UTC');
	secondesApres := EXTRACT(EPOCH FROM dateApres AT TIME ZONE 'UTC');
	valeur := ((secondesApres - secondes) * valeurAvant[1]
		+ (secondes - secondesAvant) * valeurApres[1])
		/ (secondesApres - secondesAvant);
	qualite := bdoh_cross_quality_orders(ROUND(valeurAvant[2])::INTEGER, ROUND(valeurApres[2])::INTEGER);
	RETURN ARRAY[valeur, qualite];

END;
$$ LANGUAGE 'plpgsql' STABLE;


--SELECT interp_between_dates(
--	TIMESTAMP '2002-01-01 12:00:00',
--	TIMESTAMP '2002-01-01 00:00:00',
--	ARRAY[2.0, 2],
--	TIMESTAMP '2002-01-02 00:00:00',
--	ARRAY[5.0, 4]);