CREATE OR REPLACE FUNCTION bdoh_insert_mesures_one_transformation(
  	chroniqueFilleId INTEGER,
  	jeuQualiteId     INTEGER,
	transfoId        INTEGER
	)
	RETURNS INTEGER AS $$

DECLARE
	curseur       CURSOR FOR SELECT date, valeur FROM bdoh_transformed_mesures(transfoId) ORDER BY date;
	dateInsert    TIMESTAMP;
	valeurQualite DOUBLE PRECISION[];
	qualiteId     INTEGER;
BEGIN
	OPEN curseur;

	FETCH curseur INTO dateInsert, valeurQualite;

	WHILE dateInsert IS NOT NULL LOOP

		qualiteId = (SELECT id FROM qualite WHERE ordre = ROUND(valeurQualite[2]) AND jeu_id = jeuQualiteId AND code <> 'gap');

		INSERT INTO mesure (id, qualite_id, chronique_id, date, valeur, estcalculee)
		VALUES(NEXTVAL('MESURE_ID_SEQ'), qualiteId, chroniqueFilleId, dateInsert, valeurQualite[1], true);

		FETCH curseur INTO dateInsert, valeurQualite;

	END LOOP;

	CLOSE curseur;

	RETURN 0;

EXCEPTION

	WHEN OTHERS THEN
		RETURN 16;

END;
$$ LANGUAGE 'plpgsql' VOLATILE;