CREATE OR REPLACE FUNCTION bdoh_insert_mesures_two_transformations(
  	chroniqueFilleId INTEGER,
  	jeuQualiteId     INTEGER,
	transfoMainId    INTEGER,
  	transfoSecondId  INTEGER,
        delayMain        DOUBLE PRECISION DEFAULT 0.0,
        delaySecond      DOUBLE PRECISION DEFAULT 0.0,
  	coefficient      DOUBLE PRECISION DEFAULT 1.0
	)
	RETURNS INTEGER AS $$

DECLARE
	cursorMain         CURSOR FOR SELECT date + delayMain * INTERVAL '1 MINUTE', valeur
                                   FROM bdoh_transformed_mesures(transfoMainId) ORDER BY date;
	cursorSecond       CURSOR FOR SELECT date + delaySecond * INTERVAL '1 MINUTE', valeur
                                   FROM bdoh_transformed_mesures(transfoSecondId) ORDER BY date;
	dateMain           TIMESTAMP;
	dateMainAvant      TIMESTAMP;
	dateSecond         TIMESTAMP;
	dateSecondAvant    TIMESTAMP;
	valeurMain         DOUBLE PRECISION[];
	valeurMainAvant    DOUBLE PRECISION[];
	valeurSecond       DOUBLE PRECISION[];
	valeurSecondAvant  DOUBLE PRECISION[];
	dateInsert         TIMESTAMP;
	valeurMainInsert   DOUBLE PRECISION[];
	valeurSecondInsert DOUBLE PRECISION[];
	yInsert            DOUBLE PRECISION;
	qualiteInsert      DOUBLE PRECISION;
	qualiteId          INTEGER;
BEGIN
	OPEN cursorMain;
	OPEN cursorSecond;

	dateMainAvant := NULL;
	valeurMainAvant := NULL;
	dateSecondAvant := NULL;
	valeurSecondAvant := NULL;
	FETCH cursorMain INTO dateMain, valeurMain;
	FETCH cursorSecond INTO dateSecond, valeurSecond;

	WHILE dateMain IS NOT NULL OR dateSecond IS NOT NULL LOOP

		IF dateMain < dateSecond OR dateSecond IS NULL THEN
			dateInsert := dateMain;
			valeurMainInsert := valeurMain;
			valeurSecondInsert := bdoh_interp_between_dates(dateInsert, dateSecondAvant, valeurSecondAvant, dateSecond, valeurSecond);

			dateMainAvant := dateMain;
			valeurMainAvant := valeurMain;
			FETCH cursorMain INTO dateMain, valeurMain;

		ELSIF dateSecond < dateMain OR dateMain IS NULL THEN
			dateInsert := dateSecond;
			valeurMainInsert := bdoh_interp_between_dates(dateInsert, dateMainAvant, valeurMainAvant, dateMain, valeurMain);
			valeurSecondInsert := valeurSecond;

			dateSecondAvant := dateSecond;
			valeurSecondAvant := valeurSecond;
			FETCH cursorSecond INTO dateSecond, valeurSecond;

		ELSE
			dateInsert := dateMain;
			valeurMainInsert := valeurMain;
			valeurSecondInsert := valeurSecond;

			dateMainAvant := dateMain;
			valeurMainAvant := valeurMain;
			FETCH cursorMain INTO dateMain, valeurMain;
			dateSecondAvant := dateSecond;
			valeurSecondAvant := valeurSecond;
			FETCH cursorSecond INTO dateSecond, valeurSecond;

		END IF ;

                qualiteInsert := bdoh_cross_quality_orders(ROUND(valeurMainInsert[2])::INTEGER, ROUND(valeurSecondInsert[2])::INTEGER);
		IF qualiteInsert < 300 THEN
			yInsert := -9999;
		ELSE
			yInsert := valeurMainInsert[1] * valeurSecondInsert[1] * coefficient;
		END IF;

		SELECT id INTO qualiteId
                    FROM qualite
                    WHERE ordre = ROUND(qualiteInsert)
                    AND jeu_id = jeuQualiteId
                    AND code <> 'gap';

		INSERT INTO mesure (id, qualite_id, chronique_id, date, valeur, estcalculee)
		VALUES(NEXTVAL('MESURE_ID_SEQ'), qualiteId, chroniqueFilleId, dateInsert, yInsert, true);

	END LOOP;

	CLOSE cursorMain;
	CLOSE cursorSecond;

	RETURN 0;

EXCEPTION

	WHEN OTHERS THEN
		RETURN 16;

END;
$$ LANGUAGE 'plpgsql' VOLATILE;