CREATE OR REPLACE FUNCTION bdoh_interp_to_mean_or_cumul(
        idChronique      INTEGER,
        beginDate        TIMESTAMP WITHOUT TIME ZONE,
        endDate          TIMESTAMP WITHOUT TIME ZONE,
        samplingStepId   INTEGER,
        isMean           BOOLEAN,
        startAt          VARCHAR DEFAULT 'round-hour'
)
RETURNS TABLE(boundary_start TIMESTAMP WITHOUT TIME ZONE, boundary_end TIMESTAMP WITHOUT TIME ZONE,
        value DOUBLE PRECISION, quality VARCHAR) AS $$
DECLARE
        jeuQualiteId      INTEGER;
        inputSamplingCode VARCHAR;
        integralFunction  VARCHAR;

        stepValue         INTEGER;
        stepUnit          VARCHAR;
        isEvent           BOOLEAN;
        stepInterval      INTERVAL;

        chroniqueMinDate  TIMESTAMP WITHOUT TIME ZONE;
        chroniqueMaxDate  TIMESTAMP WITHOUT TIME ZONE;
        maxDate           TIMESTAMP WITHOUT TIME ZONE;

        firstBoundary     TIMESTAMP WITHOUT TIME ZONE;
        secondBoundary    TIMESTAMP WITHOUT TIME ZONE;

        integratedValue   DOUBLE PRECISION;
        qualityOrder      INTEGER;
        qualityCode       VARCHAR;
BEGIN

        -- Min & max time series dates
        SELECT MIN(m.date), MAX(m.date) INTO chroniqueMinDate, chroniqueMaxDate
            FROM mesure m WHERE m.chronique_id = idChronique;

        -- If there are less than 2 measures, then stop
        IF chroniqueMinDate IS NULL OR chroniqueMaxDate = chroniqueMinDate THEN
            RETURN;
        END IF;

        -- Get time series' sampling &
        -- determine which store procedure should be used to integrate in each time interval
        SELECT COALESCE(LOWER(e.code), 'instantaneous') INTO inputSamplingCode
            FROM echantillonnage e
            JOIN chronique c ON c.echantillonnage_id = e.id
            WHERE c.id = idChronique;

        IF inputSamplingCode = 'instantaneous' THEN
            integralFunction := 'bdoh_integrate_instant_interval';
        ELSIF inputSamplingCode = 'mean' THEN
            integralFunction := 'bdoh_integrate_mean_interval';
        ELSE
            RETURN;
        END IF;

        -- Get quality set ID from time series
        jeuQualiteId := bdoh_jeu_qualite_id(idChronique);

        -- Get output time step value & unit
        SELECT p.valeur, p.unite INTO stepValue, stepUnit
            FROM pasEchantillonnage p WHERE p.id = samplingStepId;

        isEvent := (LOWER(stepUnit) = 'event');
        IF isEvent THEN
            -- Process one event: only one interval defined by beginDate and endDate

            -- If necessary, move beginDate & endDate inside the time series' range
            IF beginDate < chroniqueMinDate THEN
                firstBoundary := chroniqueMinDate;
            ELSE
                firstBoundary := beginDate;
            END IF;
            IF endDate > chroniqueMaxDate THEN
                maxDate := chroniqueMaxDate;
            ELSE
                maxDate := endDate;
            END IF;

            -- If we start from an existing measure, then set firstBoundary properly.
            -- In that case, also set maxDate to the date of an existing measure.
            IF LOWER(startAt) = 'existing-measure' THEN
                SELECT MIN(m.date), MAX(m.date) INTO firstBoundary, maxDate
                    FROM mesure m WHERE m.chronique_id = idChronique
                    AND m.date >= firstBoundary AND m.date <= maxDate;
            END IF;

            -- Now compute interval length
            stepInterval := maxDate - firstBoundary;

        ELSE
            -- Process 'normal' case: N intervals of a given span

            -- Interval length is given by step value & step unit
            stepInterval := stepValue || ' ' || stepUnit;

            -- firstBoundary should not precede the time series' 1st date
            firstBoundary := beginDate;
            WHILE firstBoundary < chroniqueMinDate LOOP
                firstBoundary := firstBoundary + stepInterval;
            END LOOP;

            -- If we start from an existing measure, then set firstBoundary properly
            IF LOWER(startAt) = 'existing-measure' THEN
                SELECT MIN(m.date) INTO firstBoundary
                    FROM mesure m WHERE m.chronique_id = idChronique
                    AND m.date >= firstBoundary;
            END IF;

            -- Max date is end date
            maxDate := endDate;

        END IF;

        -- We never know: if maxDate is no later than firstBoundary, then exit now
        IF maxDate <= firstBoundary THEN
            RETURN;
        END IF;

        secondBoundary := firstBoundary + stepInterval;
        WHILE secondBoundary <= maxDate LOOP

            EXECUTE 'SELECT vq.val, vq.quality_order, q.code '
                    'FROM ' || integralFunction || '($1, $2, $3) vq '
                    'JOIN qualite q ON q.jeu_id = $4 AND q.ordre = vq.quality_order AND q.code <> ''gap'''
            USING idChronique, firstBoundary, secondBoundary, jeuQualiteId
            INTO integratedValue, qualityOrder, qualityCode;

            IF isMean AND qualityOrder > 200 THEN
                integratedValue := integratedValue / EXTRACT(EPOCH FROM (secondBoundary - firstBoundary));
            END IF;

            RETURN QUERY SELECT firstBoundary, secondBoundary, ROUND(integratedValue::NUMERIC, 3)::DOUBLE PRECISION, qualityCode;

            firstBoundary := secondBoundary;
            secondBoundary := firstBoundary + stepInterval;

        END LOOP;

END;
$$ LANGUAGE 'plpgsql' VOLATILE;
