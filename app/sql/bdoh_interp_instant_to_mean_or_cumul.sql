CREATE OR REPLACE FUNCTION bdoh_interp_instant_to_mean_or_cumul(
        idChronique      INTEGER,
        beginDate        TIMESTAMP WITHOUT TIME ZONE,
        endDate          TIMESTAMP WITHOUT TIME ZONE,
        samplingStepId   INTEGER,
        --jeuQualiteId     INTEGER,
        isMean           BOOLEAN,
        measureDirection VARCHAR
)
RETURNS TABLE(date TIMESTAMP WITHOUT TIME ZONE, valeur DOUBLE PRECISION, quality VARCHAR) AS $$
DECLARE
        jeuQualiteId      INTEGER;

        stepValue         INTEGER;
        stepUnit          VARCHAR;
        isEvent           BOOLEAN;
        stepInterval      INTERVAL;

        chroniqueMinDate  TIMESTAMP WITHOUT TIME ZONE;
        chroniqueMaxDate  TIMESTAMP WITHOUT TIME ZONE;
        maxDate           TIMESTAMP WITHOUT TIME ZONE;

        firstBoundary     TIMESTAMP WITHOUT TIME ZONE;
        secondBoundary    TIMESTAMP WITHOUT TIME ZONE;
        printFirst        BOOLEAN;

        integratedValue   DOUBLE PRECISION;
        qualityOrder      INTEGER;
        qualityCode       VARCHAR;
BEGIN

        -- Get quality set ID from time series
        jeuQualiteId := bdoh_jeu_qualite_id(idChronique);

        -- Get output time step value & unit
        SELECT p.valeur, p.unite INTO stepValue, stepUnit
            FROM pasEchantillonnage p WHERE p.id = samplingStepId;

        -- Min & max time series dates
        SELECT MIN(m.date), MAX(m.date) INTO chroniqueMinDate, chroniqueMaxDate
            FROM mesure m WHERE m.chronique_id = idChronique;

        isEvent := (LOWER(stepUnit) = 'event');
        IF isEvent THEN
            -- Process one event: only one interval defined by beginDate and endDate

            -- If necessary, move beginDate & endDate inside the time series' range
            IF beginDate < chroniqueMinDate THEN
                firstBoundary := chroniqueMinDate;
            ELSE
                firstBoundary := beginDate;
            END IF;
            IF endDate > chroniqueMaxDate THEN
                maxDate := chroniqueMaxDate;
            ELSE
                maxDate := endDate;
            END IF;

            -- Now compute interval length
            stepInterval := maxDate - firstBoundary;

        ELSE
            -- Process 'normal' case: N intervals of a given span

            -- Interval length is given by step value & step unit
            stepInterval := stepValue || ' ' || stepUnit;

            -- firstBoundary should not precede the time series' 1st date
            firstBoundary := beginDate;
            WHILE firstBoundary < chroniqueMinDate LOOP
                firstBoundary := firstBoundary + stepInterval;
            END LOOP;

            -- Max date is end date
            maxDate := endDate;

        END IF;

        -- Tell whether the output date should be the 1st boundary
        printFirst := (LOWER(measureDirection) = 'ahead' OR isEvent);

        secondBoundary := firstBoundary + stepInterval;
        WHILE secondBoundary <= maxDate LOOP

            SELECT vq.val, vq.quality_order, q.code INTO integratedValue, qualityOrder, qualityCode
                FROM bdoh_integrate_instant_interval(idChronique, firstBoundary, secondBoundary) vq
                LEFT JOIN qualite q ON q.jeu_id = jeuQualiteId AND q.ordre = vq.quality_order AND q.code <> 'gap';

            IF isMean AND qualityOrder > 1 THEN
                integratedValue := integratedValue / EXTRACT(EPOCH FROM (secondBoundary - firstBoundary));
            END IF;

            RETURN QUERY SELECT
                CASE WHEN printFirst THEN firstBoundary ELSE secondBoundary END,
                integratedValue, qualityCode;

            firstBoundary := secondBoundary;
            secondBoundary := firstBoundary + stepInterval;

        END LOOP;

        -- If working on an event, repeat output row but with end date instead of start date
        -- HINT: see code just above: firstBoundary now contains the 'correct' value for the second boundary
        IF isEvent THEN RETURN QUERY SELECT firstBoundary, integratedValue, qualityCode; END IF;

END;
$$ LANGUAGE 'plpgsql' VOLATILE;
