CREATE OR REPLACE FUNCTION copy_from_csv(table_name text, table_fieds text, file_path text, delimeter text DEFAULT ';'::text)
  RETURNS void AS
$BODY$

declare statement text;
begin

statement := 'COPY ' || table_name || ' (' || table_fieds || ') ' || 'FROM ''' || file_path || ''' WITH ';
statement := statement || 'DELIMITER ''' || delimeter || '''  CSV';

execute statement;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;

revoke all on function copy_from_csv(text, text, text, text) from bdoh;
grant execute on function copy_from_csv(text, text, text, text) to bdoh;