CREATE OR REPLACE FUNCTION bdoh_purge_mesures_child_chronique(
	chroniqueFilleId INTEGER
	)
	RETURNS VOID AS $$

BEGIN

        -- Suppression de toutes les "mesures" calculees de la chronique
        DELETE FROM mesure WHERE chronique_id = chroniqueFilleId AND estcalculee;

        -- Suppression des mesures entrees manuellement et se retrouvant desormais dans la plage d'un bareme
	DELETE FROM mesure WHERE id IN (
		SELECT m.id
		FROM mesure m
		LEFT JOIN chronique c on c.id = m.chronique_id
		LEFT JOIN transformation t ON t.id = c.premiereentree_id OR t.id = c.secondeentree_id
		LEFT JOIN jeuBareme jb ON jb.id = t.jeuBaremeActuel_id
		LEFT JOIN baremeJeuBareme bjb ON bjb.jeuBareme_id = jb.id AND m.date >= bjb.debutValidite AND (m.date < bjb.finValidite OR bjb.finValidite IS NULL)
		LEFT JOIN bareme b ON bjb.bareme_id = b.id
		WHERE m.chronique_id = chroniqueFilleId
		AND b.nom <> 'manuel'
	);

END;
$$ LANGUAGE 'plpgsql' VOLATILE;