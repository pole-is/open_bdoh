CREATE OR REPLACE FUNCTION bdoh_interp_for_bareme(
	entree          DOUBLE PRECISION,
	qualiteEntree   INTEGER,
	bareme          DOUBLE PRECISION[],
        limitsAsGaps    BOOLEAN DEFAULT false,
        limitsAddCoeff  DOUBLE PRECISION DEFAULT 0.0,
        limitsMultCoeff DOUBLE PRECISION DEFAULT 1.0
        )
	RETURNS DOUBLE PRECISION[] AS $$

DECLARE

        entree_adaptee DOUBLE PRECISION;
	minBareme       DOUBLE PRECISION;
	maxBareme       DOUBLE PRECISION;
	longueur        INTEGER;
	indice          INTEGER;
	indiceMin       INTEGER;
	indiceMax       INTEGER;
	lignePrecedente DOUBLE PRECISION[];
	ligneSuivante   DOUBLE PRECISION[];
        valeur          DOUBLE PRECISION;
	qualite         DOUBLE PRECISION;
BEGIN

        IF qualiteEntree IN (600, 700) THEN
                IF limitsAsGaps THEN
                    RETURN ARRAY[-9999.0, 200.0];
                END IF;
                entree_adaptee := entree * limitsMultCoeff + limitsAddCoeff;
        ELSE
                entree_adaptee := entree;
        END IF;

	minBareme := bareme[1][1];
	longueur := array_length(bareme, 1);
	maxBareme := bareme[longueur][1];

	IF qualiteEntree < 300 THEN
		RETURN ARRAY[-9999.0, qualiteEntree];
	END IF;
	IF entree_adaptee < minBareme OR entree_adaptee > maxBareme
	THEN
		RETURN ARRAY[-9999.0, 100.0];
	END IF;

	indiceMin := 1;
	indiceMax := longueur - 1;
	indice := longueur / 2;

	WHILE entree_adaptee < bareme[indice][1]
		OR entree_adaptee > bareme[indice+1][1]
	LOOP
		IF entree_adaptee < bareme[indice][1]
		THEN indice := indice-1;
		ELSE indice := indice+1;
		END IF;
	END LOOP;

	lignePrecedente := bareme[indice:indice][1:3];
	ligneSuivante := bareme[indice+1:indice+1][1:3];
        qualite := bdoh_cross_quality_orders(ROUND(lignePrecedente[1][3])::INTEGER, ROUND(ligneSuivante[1][3])::INTEGER, limitsAsGaps);
        qualite := bdoh_cross_quality_orders(ROUND(qualite)::INTEGER, qualiteEntree, limitsAsGaps);

        valeur := ((ligneSuivante[1][1] - entree_adaptee) * lignePrecedente[1][2]
                + (entree_adaptee - lignePrecedente[1][1]) * ligneSuivante[1][2])
                / (ligneSuivante[1][1] - lignePrecedente[1][1]);

	RETURN ARRAY[valeur, qualite];

END;
$$ LANGUAGE 'plpgsql' STABLE;