CREATE OR REPLACE FUNCTION bdoh_select_values_for_time_step(
	idChronique INTEGER,
        beginDate   TIMESTAMP WITHOUT TIME ZONE,
        endDate     TIMESTAMP WITHOUT TIME ZONE
	)
	RETURNS TABLE(timest DOUBLE PRECISION, val DOUBLE PRECISION, qualOrder INTEGER, qual VARCHAR) AS $$

BEGIN

	RETURN QUERY EXECUTE
		'SELECT EXTRACT(EPOCH FROM m.date AT TIME ZONE ''UTC'') as tst, m.valeur, q.ordre, q.code '
		'FROM mesure m '
                'INNER JOIN qualite q '
                '    ON m.qualite_id = q.id '
		'WHERE m.chronique_id = $1 '
		'AND m.date > $2 '
                'AND m.date < $3 '
                'UNION '
                'SELECT EXTRACT(EPOCH FROM m.date AT TIME ZONE ''UTC'') as tst, m.valeur, q.ordre, q.code '
		'FROM mesure m '
                'INNER JOIN qualite q '
                '    ON m.qualite_id = q.id '
		'WHERE m.chronique_id = $1 '
		'AND m.date = (SELECT MAX(date) FROM mesure '
                '              WHERE chronique_id = $1 '
                '              AND date <= $2) '
                'UNION '
                'SELECT EXTRACT(EPOCH FROM m.date AT TIME ZONE ''UTC'') as tst, m.valeur, q.ordre, q.code '
		'FROM mesure m '
                'INNER JOIN qualite q '
                '    ON m.qualite_id = q.id '
		'WHERE m.chronique_id = $1 '
		'AND m.date = (SELECT MIN(date) FROM mesure '
                '              WHERE chronique_id = $1 '
                '              AND date >= $3) '
		'ORDER BY tst ASC '
                USING idChronique, beginDate, endDate;

END;
$$ LANGUAGE 'plpgsql' STABLE;