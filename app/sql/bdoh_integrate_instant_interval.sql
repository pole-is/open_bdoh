CREATE OR REPLACE FUNCTION bdoh_integrate_instant_interval(
        idChronique      INTEGER,
        beginDate        TIMESTAMP WITHOUT TIME ZONE,
        endDate          TIMESTAMP WITHOUT TIME ZONE
)
RETURNS TABLE(val DOUBLE PRECISION, quality_order INTEGER) AS $$
DECLARE
        minQualityOrder INTEGER;
        integratedValue DOUBLE PRECISION;
        nMeasures       INTEGER;

        beginSeconds    DOUBLE PRECISION;
        endSeconds      DOUBLE PRECISION;
BEGIN

------- Remember: 'gap' order = 100, 'invalid' order = 200

        SELECT bdoh_min_quality_order(qualOrder), COUNT(*) INTO minQualityOrder, nMeasures
            FROM bdoh_select_values_for_time_step(idChronique, beginDate, endDate);

        IF minQualityOrder <= 200 OR nMeasures < 2 THEN
            -- There was at least one gap or invalid measure, or there were not enough measures
            integratedValue := -9999;
            IF nMeasures < 2 THEN minQualityOrder := 100; END IF;
        ELSE
            -- We may compute an integrated value...
            beginSeconds := EXTRACT(EPOCH FROM beginDate AT TIME ZONE 'UTC');
            endSeconds := EXTRACT(EPOCH FROM endDate AT TIME ZONE 'UTC');

            EXECUTE
            'WITH date_value(seconds, value) AS ('
            '    SELECT'
            '    CASE'
            '        WHEN timest < $1 THEN $1'
            '        WHEN timest > $2 THEN $2'
            '        ELSE timest'
            '    END,'
            '    CASE'
            '        WHEN timest < $1 THEN'
            '            ((LEAD(timest) OVER() - $1) * val'
            '            + ($1 - timest) * LEAD(val) OVER()'
            '            ) / (LEAD(timest) OVER() - timest)'
            '        WHEN timest > $2 THEN'
            '            ((timest - $2) * LAG(val) OVER ()'
            '            + ($2 - LAG(timest) OVER()) * val'
            '            ) / (timest - LAG(timest) OVER())'
            '        ELSE val'
            '    END'
            '    FROM bdoh_select_values_for_time_step($3, $4, $5)'
            '), '
            'trapezium(surface) AS ('
            '    SELECT COALESCE((LEAD(seconds) OVER() - seconds) * (LEAD(value) OVER() + value) / 2, 0)'
            '    FROM date_value'
            ')'
            ' SELECT SUM(t.surface) FROM trapezium t;'
            USING beginSeconds, endSeconds, idChronique, beginDate, endDate
            INTO integratedValue;

        END IF;

        RETURN QUERY SELECT integratedValue, minQualityOrder;

END;
$$ LANGUAGE 'plpgsql' VOLATILE;