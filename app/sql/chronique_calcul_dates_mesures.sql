CREATE OR REPLACE FUNCTION bdoh_chronique_calcul_dates_mesures (idChronique INTEGER, dtype VARCHAR)
RETURNS INTEGER AS $$

DECLARE
    firstDate   TIMESTAMP;
    lastDate    TIMESTAMP;
    nbMes       INTEGER;

BEGIN
    IF dtype = 'discontinue' THEN
        SELECT INTO firstDate, lastDate, nbMes MIN(p.debut), MAX(p.fin), COUNT(p.chronique_id)
        FROM plage p
        WHERE p.chronique_id = idChronique
        GROUP BY p.chronique_id;
    ELSE
        SELECT INTO firstDate, lastDate, nbMes MIN(m.date), MAX(m.date), COUNT(m.chronique_id)
        FROM mesure m
        WHERE m.chronique_id = idChronique
        GROUP BY m.chronique_id;
    END IF;


    IF firstDate IS NOT NULL AND lastDate IS NOT NULL AND nbMes IS NOT NULL THEN
        UPDATE chronique SET (dateDebutMesures, dateFinMesures, nbMesures) = (firstDate, lastDate, nbMes)
        WHERE id = idChronique;

        RETURN 0;
    ELSE
        UPDATE chronique SET (dateDebutMesures, dateFinMesures, nbMesures) = (NULL, NULL, NULL)
        WHERE id = idChronique;

        RETURN 1;
    END IF;

END;
$$ LANGUAGE 'plpgsql' VOLATILE;
-- exemple request to update all chronicles
SELECT c.id, bdoh_chronique_calcul_dates_mesures(c.id, c.dtype) FROM chronique c ORDER BY c.id;
