CREATE OR REPLACE FUNCTION bdoh_cross_quality_orders(
        ordre1                INTEGER,
        ordre2                INTEGER,
        limitsAsGaps          BOOLEAN
)
RETURNS INTEGER AS $$
DECLARE
        resultingQualityOrder INTEGER;
BEGIN
-- Ordres : I = 200, E = 500, Ld = 600, Lq = 700, V >= 800
-- Si limitsAsGaps = true, alors ordre1 ou ordre2 = Lq ou Ld force le resultat a etre I
-- Sinon :
-- {Lq|Ld}.V = E
-- Plus petit ordre pour les autres croisements
        IF limitsAsGaps AND (ordre1 IN (600, 700) OR ordre2 IN (600, 700)) THEN
            resultingQualityOrder = 200;
        ELSIF (ordre1 IN (600, 700) AND ordre2 >= 800) OR (ordre2 IN (600, 700) AND ordre1 >= 800) THEN
            resultingQualityOrder = 500;
        ELSIF ordre1 < ordre2 THEN
            resultingQualityOrder = ordre1;
        ELSE
            resultingQualityOrder = ordre2;
        END IF;

        RETURN resultingQualityOrder;

END;
$$ LANGUAGE 'plpgsql' STABLE STRICT;

--------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION bdoh_cross_quality_orders(
        ordre1 INTEGER,
        ordre2 INTEGER
)
RETURNS INTEGER AS $$
BEGIN
        RETURN bdoh_cross_quality_orders(ordre1, ordre2, false);
END;
$$ LANGUAGE 'plpgsql' STABLE STRICT;

--------------------------------------------------------------------------------

-- CREATE OR REPLACE FUNCTION bdoh_final_quality_order(
--         ordre INTEGER
-- )
-- RETURNS INTEGER AS $$
-- BEGIN
--         RETURN
--             CASE WHEN ordre IN (600, 700) THEN 500
--             ELSE ordre
--             END;
-- END;
-- $$ LANGUAGE 'plpgsql' IMMUTABLE;


--------------------------------------------------------------------------------

DROP AGGREGATE IF EXISTS bdoh_min_quality_order(INTEGER);
CREATE AGGREGATE bdoh_min_quality_order(INTEGER) (
        SFUNC     = 'bdoh_cross_quality_orders',
--        FINALFUNC = 'bdoh_final_quality_order',
        STYPE     = INTEGER
);
DROP AGGREGATE IF EXISTS bdoh_min_quality_order(INTEGER, BOOLEAN);
CREATE AGGREGATE bdoh_min_quality_order(INTEGER, BOOLEAN) (
        SFUNC     = 'bdoh_cross_quality_orders',
--        FINALFUNC = 'bdoh_final_quality_order',
        STYPE     = INTEGER
);

--------------------------------------------------------------------------------

DROP FUNCTION IF EXISTS bdoh_final_quality_order(INTEGER);
