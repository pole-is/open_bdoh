CREATE FUNCTION fill_technical_baremes() RETURNS VOID AS $$
BEGIN

    IF (SELECT nom FROM bareme WHERE nom = 'identite') IS NULL THEN
        INSERT INTO bareme (id, nom, valeurs, dateCreation) VALUES
        (NEXTVAL('BAREME_ID_SEQ'), 'identite', '{{-1000000000,-1000000000,4},{1000000000,1000000000,4}}', '1900-01-01');
    END IF;

    IF (SELECT nom FROM bareme WHERE nom = 'lacune') IS NULL THEN
        INSERT INTO bareme (id, nom, valeurs, dateCreation) VALUES
        (NEXTVAL('BAREME_ID_SEQ'), 'lacune', '{{-1000000000,-9999,0},{1000000000,-9999,0}}', '1900-01-01');
    END IF;

    IF (SELECT nom FROM bareme WHERE nom = 'manuel') IS NULL THEN
        INSERT INTO bareme (id, nom, valeurs, dateCreation) VALUES
        (NEXTVAL('BAREME_ID_SEQ'), 'manuel', '{{-1000000000,-1000000000,4},{1000000000,1000000000,4}}', '1900-01-01');
    END IF;

    RETURN;
END;
$$ LANGUAGE 'plpgsql'
VOLATILE;

SELECT fill_technical_baremes();

DROP FUNCTION fill_technical_baremes();