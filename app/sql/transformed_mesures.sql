CREATE OR REPLACE FUNCTION bdoh_transformed_mesures(
	transfoId    INTEGER
	)
	RETURNS TABLE(date TIMESTAMP, valeur DOUBLE PRECISION[]) AS $$
DECLARE
        typeTransfoLimites VARCHAR;
        turnLimitsToGap    BOOLEAN;
        coeffLimitsAdd     DOUBLE PRECISION;
        coeffLimitsMult    DOUBLE PRECISION;
BEGIN

        typeTransfoLimites := valuelimittransformationtype FROM jeubareme jb
            JOIN transformation t ON t.jeubaremeactuel_id = jb.id WHERE t.id = transfoId;

        IF typeTransfoLimites = 'chronique.lq_ld.options.gap' THEN
        -- Policy = turn value limits to gaps
            turnLimitsToGap := true;
            coeffLimitsAdd  := -9999.0;
            coeffLimitsMult := 0.0;
        ELSE
            turnLimitsToGap := false;
        END IF;

        IF typeTransfoLimites IS NULL OR typeTransfoLimites = 'chronique.lq_ld.options.true_value' THEN
        -- Policy = use value limits as "true" values
            coeffLimitsAdd  := 0.0;
            coeffLimitsMult := 1.0;
        END IF;

        IF typeTransfoLimites = 'chronique.lq_ld.options.half_value' THEN
        -- Policy = use the half of value limits as "true" values
            coeffLimitsAdd  := 0.0;
            coeffLimitsMult := 0.5;
        END IF;

        IF typeTransfoLimites = 'chronique.lq_ld.options.placeholder' THEN
        -- Policy = replace value limits by the time series' placeholder
            coeffLimitsAdd  := valuelimitplaceholder FROM jeubareme jb
                JOIN transformation t ON t.jeubaremeactuel_id = jb.id WHERE t.id = transfoId;
            coeffLimitsMult := 0.0;
        END IF;

	RETURN QUERY
		SELECT m.date,
		CASE
                    WHEN b.nom = 'lacune' THEN
                        ARRAY[-9999, 100]
                    WHEN turnLimitsToGap AND q.ordre IN (600, 700) THEN
                        ARRAY[-9999, 200]
                    WHEN b.nom = 'identite' THEN
                        CASE
                            WHEN q.ordre IN (600, 700) THEN
                                ARRAY[m.valeur * coeffLimitsMult + coeffLimitsAdd, q.ordre]
                            ELSE
                                ARRAY[m.valeur, q.ordre]
                        END
                    WHEN b.nom = 'manuel' THEN
                        NULL
                    ELSE
                        replace(bdoh_interp_for_bareme(m.valeur, q.ordre, b.valeurs, turnLimitsToGap, coeffLimitsAdd, coeffLimitsMult)::varchar,
                            '{NULL,NULL}', '{-9999,100}')::DOUBLE PRECISION[]
		END
		FROM transformation t
		LEFT JOIN mesure m ON m.chronique_id = t.entree_id
		LEFT JOIN qualite q ON m.qualite_id = q.id
		LEFT JOIN jeuBareme jb ON jb.id = t.jeuBaremeActuel_id
		LEFT JOIN baremeJeuBareme bjb ON bjb.jeuBareme_id = jb.id AND m.date >= bjb.debutValidite AND (m.date < bjb.finValidite OR bjb.finValidite IS NULL)
		LEFT JOIN bareme b ON bjb.bareme_id = b.id
		WHERE t.id = transfoId
		AND b.nom <> 'manuel'
		ORDER BY m.valeur DESC;

END;
$$ LANGUAGE 'plpgsql' STABLE;