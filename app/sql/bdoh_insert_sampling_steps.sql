CREATE OR REPLACE FUNCTION add_sampling_step(val INTEGER, unit VARCHAR, approxSeconds INTEGER, label VARCHAR, labelEn VARCHAR)
RETURNS VOID AS
$$
BEGIN
	IF NOT EXISTS (SELECT * FROM pasEchantillonnage WHERE unite = unit AND valeur = val) THEN
		INSERT INTO pasEchantillonnage
		VALUES (NEXTVAL('pasechantillonnage_id_seq'), unit, val, approxSeconds, label, labelEn);
        ELSE
                UPDATE pasEchantillonnage SET approxSecondes = approxSeconds, libelle = label, libelleEn = labelEn
                WHERE unite = unit AND valeur = val;
	END IF;
END;
$$ LANGUAGE 'plpgsql' VOLATILE;

----------------------------------------------------------------

CREATE OR REPLACE FUNCTION add_sampling_option(sampling_id INTEGER, val INTEGER, unit VARCHAR)
RETURNS VOID AS
$$
DECLARE
	sampling_step_id INTEGER;
BEGIN
	sampling_step_id := id FROM pasEchantillonnage WHERE valeur = val AND unite = unit;
	IF sampling_step_id IS NOT NULL
            AND NOT EXISTS (SELECT * FROM optionEchantillonnageSortie
            WHERE echantillonnageEntree_id = sampling_id AND pasEchantillonnage_id = sampling_step_id)
        THEN
		INSERT INTO optionEchantillonnageSortie
		VALUES (NEXTVAL('optionechantillonnagesortie_id_seq'), sampling_id, sampling_step_id);
	END IF;
END;
$$ LANGUAGE 'plpgsql' VOLATILE;

----------------------------------------------------------------

CREATE OR REPLACE FUNCTION insert_sampling_steps()
RETURNS VOID AS
$$
DECLARE
	id_instant INTEGER;
	id_mean INTEGER;
	id_cumul INTEGER;
BEGIN
	id_instant := id FROM echantillonnage WHERE nom ILIKE 'instantan%';
	id_mean := id FROM echantillonnage WHERE nom ILIKE 'moyenne';
	id_cumul := id FROM echantillonnage WHERE nom ILIKE 'cumul';

	-- Add time steps, i.e. value-unit pairs.
	-- You can "add" a time step one or several times with "add_sampling_step".
	-- It will be actually added only if not present in table "pasEchantillonnage".
	PERFORM add_sampling_step(5, 'minute', 300, '5 minutes', '5 minutes');
	PERFORM add_sampling_step(6, 'minute', 360, '6 minutes', '6 minutes');
	PERFORM add_sampling_step(15, 'minute', 900, '15 minutes', '15 minutes');
	PERFORM add_sampling_step(30, 'minute', 1800, '30 minutes', '30 minutes');
	PERFORM add_sampling_step(1, 'hour', 3600, '1 heure', '1 hour');
	PERFORM add_sampling_step(6, 'hour', 21600, '6 heures', '6 hours');
	PERFORM add_sampling_step(1, 'day', 86400, '1 jour', '1 day');
	PERFORM add_sampling_step(1, 'month', 2629800, '1 mois', '1 month');
	PERFORM add_sampling_step(1, 'year', 31557600, '1 an', '1 year');
	PERFORM add_sampling_step(1, 'event', 2000000000, '1 événement', '1 event');

	-- Now, add "output sampling options", i.e. time steps associated with input sampling types (output is mean or cumulative).
	-- Again, a multiple "insertion" with "add_sampling_option" will actually occur once only.
	PERFORM add_sampling_option(id_instant, 1, 'hour');
	PERFORM add_sampling_option(id_instant, 6, 'hour');
	PERFORM add_sampling_option(id_instant, 1, 'day');
	PERFORM add_sampling_option(id_instant, 1, 'month');
	PERFORM add_sampling_option(id_instant, 1, 'year');
	PERFORM add_sampling_option(id_instant, 1, 'event');

	PERFORM add_sampling_option(id_mean, 30, 'minute');
	PERFORM add_sampling_option(id_mean, 1, 'hour');
	PERFORM add_sampling_option(id_mean, 6, 'hour');
	PERFORM add_sampling_option(id_mean, 1, 'day');
	PERFORM add_sampling_option(id_mean, 1, 'month');
	PERFORM add_sampling_option(id_mean, 1, 'year');
	PERFORM add_sampling_option(id_mean, 1, 'event');

	PERFORM add_sampling_option(id_cumul, 5, 'minute');
	PERFORM add_sampling_option(id_cumul, 6, 'minute');
	PERFORM add_sampling_option(id_cumul, 15, 'minute');
	PERFORM add_sampling_option(id_cumul, 30, 'minute');
	PERFORM add_sampling_option(id_cumul, 1, 'hour');
	PERFORM add_sampling_option(id_cumul, 6, 'hour');
	PERFORM add_sampling_option(id_cumul, 1, 'day');
	PERFORM add_sampling_option(id_cumul, 1, 'month');
	PERFORM add_sampling_option(id_cumul, 1, 'year');
	PERFORM add_sampling_option(id_cumul, 1, 'event');

END;
$$ LANGUAGE 'plpgsql' VOLATILE;

----------------------------------------------------------------

SELECT insert_sampling_steps();

DROP FUNCTION add_sampling_step(INTEGER, VARCHAR, INTEGER, VARCHAR, VARCHAR);
DROP FUNCTION add_sampling_option(INTEGER, INTEGER, VARCHAR);
DROP FUNCTION insert_sampling_steps();

/*
SELECT e.nom, s.valeur, s.unite FROM optionEchantillonnageSortie eps
JOIN Echantillonnage e ON e.id = eps.echantillonnageentree_id
JOIN pasEchantillonnage s ON s.id = eps.pasEchantillonnage_id;
*/