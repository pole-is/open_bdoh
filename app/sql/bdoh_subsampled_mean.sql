-- Stored procedures used to extract subsets of measures from a chronicle.
-- Measures are to be arranged into "series", each with a given start date and an end date.
-- We subsample measures in each series as follow: the span of the series is divided in time subintervals
-- of a given small span, and in each, we keep the min & max values with the corresponding dates. We also
-- keep the very first & very last measures of the series.

-- Gives the measures for a subsampled series
CREATE OR REPLACE FUNCTION bdoh_subsampled_mean_series_measures(
	chroniqueId INTEGER,
	startDate TIMESTAMP,
	endDate TIMESTAMP,
	subinterval DOUBLE PRECISION,
	direction VARCHAR DEFAULT 'AHEAD'
)
RETURNS TABLE(milliseconds DOUBLE PRECISION, value DOUBLE PRECISION)
AS $$
DECLARE
	directionDateFilter VARCHAR;
	shiftedDate VARCHAR;
	overallQuery VARCHAR;
	intervalFirstDate DOUBLE PRECISION;
	intervalLastDate DOUBLE PRECISION;
BEGIN
	SELECT 1000 * MIN(EXTRACT(EPOCH FROM date AT TIME ZONE 'UTC')) INTO intervalFirstDate
		FROM mesure WHERE chronique_id = chroniqueId AND date >= startDate;
	SELECT 1000 * MAX(EXTRACT(EPOCH FROM date AT TIME ZONE 'UTC')) INTO intervalLastDate
		FROM mesure WHERE chronique_id = chroniqueId AND date <= endDate;

	-- String replacement variables in ALL the stored procedure:
	-- $1 = chroniqueId
	-- $2 = startDate
	-- $3 = endDate
	-- $4 = subinterval
	-- $5 = intervalFirstDate
	-- $6 = intervalLastDate

	CASE UPPER(direction)
		WHEN 'AHEAD' THEN
			directionDateFilter := 'AND date < (SELECT MAX(date) FROM mesure WHERE chronique_id = $1)';
			shiftedDate := 'COALESCE(LEAD(date) OVER(obd),'
				       '(SELECT MIN(date) FROM mesure WHERE chronique_id = $1 AND date > $3)'
				       ')';
		ELSE
			directionDateFilter := 'AND date > (SELECT MIN(date) FROM mesure WHERE chronique_id = $1)';
			shiftedDate := 'COALESCE(LAG(date) OVER(obd),'
				       '(SELECT MAX(date) FROM mesure WHERE chronique_id = $1 AND date < $2)'
				       ')';
	END CASE;

        -- Overall query
        overallQuery := 'WITH dsiiv(millisecs, shifted_ms, meas_index, intvl_index, val) AS ('
			'	SELECT 1000 * EXTRACT(EPOCH FROM date AT TIME ZONE ''UTC''),'
			'	1000 * EXTRACT(EPOCH FROM ' || shiftedDate || ' AT TIME ZONE ''UTC''),'
			'	ROW_NUMBER() OVER(obd),'
			'	(EXTRACT(EPOCH FROM age(date, $2))/$4)::INTEGER,'
			'	valeur FROM mesure WHERE chronique_id = $1'
			'	AND date >= $2 AND date <= $3 ' || directionDateFilter ||
			'	WINDOW obd AS (ORDER BY date)'
			'	ORDER BY date'
			'), vmin(millisecs, shifted_ms, meas_index, rank, val) AS ('
			'	SELECT millisecs, shifted_ms, meas_index,'
			'	ROW_NUMBER() OVER(PARTITION BY intvl_index ORDER BY val ASC),'
			'	val FROM dsiiv'
			'), vmin_filtered(millisecs, shifted_ms, meas_index, val) AS ('
			'	SELECT millisecs, shifted_ms, meas_index, val'
			'	FROM vmin WHERE rank = 1'
			'), vmax(millisecs, shifted_ms, meas_index, rank, val) AS ('
			'	SELECT millisecs, shifted_ms, meas_index,'
			'	ROW_NUMBER() OVER(PARTITION BY intvl_index ORDER BY val DESC),'
			'	val FROM dsiiv'
			'), vmax_filtered(millisecs, shifted_ms, meas_index, val) AS ('
			'	SELECT millisecs, shifted_ms, meas_index, val'
			'	FROM vmax WHERE rank = 1'
			'), vfirst(millisecs, shifted_ms, meas_index, val) AS ('
			'	SELECT millisecs, shifted_ms, meas_index, val'
			'	FROM dsiiv WHERE millisecs = $5'
			'), vlast(millisecs, shifted_ms, meas_index, val) AS ('
			'	SELECT millisecs, shifted_ms, meas_index, val'
			'	FROM dsiiv WHERE millisecs = $6'
			')'
			'SELECT millisecs, val FROM('
			'	SELECT millisecs, val, meas_index FROM vfirst'
			'	UNION SELECT shifted_ms, val, meas_index FROM vfirst'
			'	UNION'
			'	SELECT millisecs, val, meas_index FROM vmin_filtered'
			'	UNION SELECT shifted_ms, val, meas_index FROM vmin_filtered'
			'	UNION'
			'	SELECT millisecs, val, meas_index FROM vmax_filtered'
			'	UNION SELECT shifted_ms, val, meas_index FROM vmax_filtered'
			'	UNION'
			'	SELECT millisecs, val, meas_index FROM vlast'
			'	UNION SELECT shifted_ms, val, meas_index FROM vlast'
			'	ORDER BY millisecs, meas_index'
			') x'
			;

	RETURN QUERY EXECUTE overallQuery USING
		chroniqueId, startDate, endDate, subinterval, intervalFirstDate, intervalLastDate;

END;
$$ LANGUAGE 'plpgsql' STABLE;

-- Gives the measures for a set of subsampled series
-- NOTE: we assume that startDates, endDates, subintervals are equally sized and do not proceed to any checking
CREATE OR REPLACE FUNCTION bdoh_subsampled_mean_all_series_measures(
	chroniqueId INTEGER,
	startDates TIMESTAMP[],
	endDates TIMESTAMP[],
	subintervals DOUBLE PRECISION[],
	direction VARCHAR DEFAULT 'AHEAD'
)
RETURNS TABLE(milliseconds DOUBLE PRECISION, value DOUBLE PRECISION)
AS $$
DECLARE
	nSeries BIGINT;
	i BIGINT;
BEGIN
	nSeries := ARRAY_LENGTH(startDates, 1);
	FOR i IN 1..nSeries LOOP
		RETURN QUERY SELECT * FROM bdoh_subsampled_mean_series_measures(chroniqueId,
			startDates[i], endDates[i], subintervals[i], direction);
	END LOOP;

END;
$$ LANGUAGE 'plpgsql' STABLE;

-- Gives the measure counts for a set of subsampled series
-- NOTE: we assume that startDates, endDates, subintervals are equally sized and do not proceed to any checking
CREATE OR REPLACE FUNCTION bdoh_subsampled_mean_all_series_lengths(
	chroniqueId INTEGER,
	startDates TIMESTAMP[],
	endDates TIMESTAMP[],
	subintervals DOUBLE PRECISION[],
	direction VARCHAR DEFAULT 'AHEAD'
)
RETURNS TABLE(measure_count BIGINT)
AS $$
DECLARE
	nSeries BIGINT;
	i BIGINT;
BEGIN
	nSeries := ARRAY_LENGTH(startDates, 1);
	FOR i IN 1..nSeries LOOP
		RETURN QUERY SELECT COUNT(*) FROM bdoh_subsampled_mean_series_measures(chroniqueId,
			startDates[i], endDates[i], subintervals[i], direction);
	END LOOP;

END;
$$ LANGUAGE 'plpgsql' STABLE;
