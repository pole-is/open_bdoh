CREATE OR REPLACE FUNCTION bdoh_interp_cumul_to_cumul(
        idChronique      INTEGER,
        beginDate        TIMESTAMP WITHOUT TIME ZONE,
        endDate          TIMESTAMP WITHOUT TIME ZONE,
        samplingStepId   INTEGER,
        startAt          VARCHAR DEFAULT 'round-hour'
)
RETURNS TABLE(boundary_start TIMESTAMP WITHOUT TIME ZONE, boundary_end TIMESTAMP WITHOUT TIME ZONE,
        value DOUBLE PRECISION, quality VARCHAR) AS $$
DECLARE
        jeuQualiteId     INTEGER;

        stepValue        INTEGER;
        stepUnit         VARCHAR;
        isEvent          BOOLEAN;
        stepInterval     INTERVAL;

        chroniqueMinDate TIMESTAMP WITHOUT TIME ZONE;
        measureDirection VARCHAR;

        firstBoundary    TIMESTAMP WITHOUT TIME ZONE;
        secondBoundary   TIMESTAMP WITHOUT TIME ZONE;

        qualityDateWhere VARCHAR;
        minQualityOrder  INTEGER;
        accumDateWhere   VARCHAR;
        accumulatedValue DOUBLE PRECISION;
BEGIN

        -- Get quality set ID from time series
        jeuQualiteId := bdoh_jeu_qualite_id(idChronique);

        -- Get output time step value & unit
        SELECT p.valeur, p.unite INTO stepValue, stepUnit
            FROM pasEchantillonnage p WHERE p.id = samplingStepId;
        isEvent := (LOWER(stepUnit) = 'event');

        -- Set very first boundary (date at which the interpolation begins)
        IF startAt = 'existing-measure' THEN
            -- Start from an existing measure

            -- Min & max time series dates
            SELECT MIN(m.date) INTO chroniqueMinDate
                FROM mesure m
                WHERE m.chronique_id = idChronique
                AND m.date >= beginDate;

            -- At least one measure is required!
            IF chroniqueMinDate IS NULL THEN
                RETURN;
            END IF;

            firstBoundary := chroniqueMinDate;
        ELSE
            -- Start from a round hour, i.e. firstDate here
            firstBoundary := beginDate;
        END IF;

        -- We never know: if endDate is no later than firstBoundary, then exit now
        IF endDate <= firstBoundary THEN
            RETURN;
        END IF;

        -- Compute adequate time step
        IF isEvent THEN
            -- Single event required: set time step accordingly
            stepInterval := endDate - firstBoundary;
        ELSE
            -- 'Normal' time step required: compute it from stepValue and stepUnit
            stepInterval := stepValue || ' ' || stepUnit;
        END IF;

        -- Retrieve the 'measure direction' for our time series
        SELECT COALESCE(c.directionMesure, 'backward') INTO measureDirection
            FROM chronique c WHERE c.id = idChronique;

        -- Set query parts used below, depending on 'measure direction'
        IF measureDirection IN ('backward', 'backwards') THEN
            qualityDateWhere := 'AND m.date > $2 AND m.date <= ('
                                '    SELECT MIN(date) FROM mesure'
                                '    WHERE chronique_id = $1'
                                '    AND date >= $3'
                                ')';
            accumDateWhere := 'AND m.date > $2 AND m.date <= $3';
        ELSE
            qualityDateWhere := 'AND m.date >= ('
                                '    SELECT MAX(date) FROM mesure'
                                '    WHERE chronique_id = $1'
                                '    AND date <= $2'
                                ') AND date < $3';
            accumDateWhere := 'AND m.date >= $2 AND m.date < $3';
        END IF;

        -- Compute!
        secondBoundary := firstBoundary + stepInterval;
        WHILE secondBoundary <= endDate LOOP

            -- Find minimum quality order that applies to current histogram bin
            EXECUTE 'SELECT bdoh_min_quality_order(q.ordre) FROM qualite q '
                    'JOIN mesure m ON m.qualite_id = q.id '
                    'WHERE q.jeu_id = $4 AND m.chronique_id = $1 '
                    || qualityDateWhere
            USING idChronique, firstBoundary, secondBoundary, jeuQualiteId
            INTO minQualityOrder;

            -- Get accumulated value
            IF minQualityOrder > 200 THEN
                -- Quality is high enough to accumulate in our bin
                EXECUTE 'SELECT COALESCE(SUM(m.valeur), 0) FROM mesure m '
                        'WHERE m.chronique_id = $1 '
                        || accumDateWhere
                USING idChronique, firstBoundary, secondBoundary
                INTO accumulatedValue;

            ELSE
                -- Quality is too poor, affect gap value
                accumulatedValue := -9999;
            END IF;

            RETURN QUERY SELECT firstBoundary, secondBoundary, accumulatedValue, q.code
                FROM qualite q
                WHERE q.jeu_id = jeuQualiteId
                AND q.ordre = minQualityOrder
                AND q.code <> 'gap';

            firstBoundary := secondBoundary;
            secondBoundary := firstBoundary + stepInterval;

        END LOOP;

END;
$$ LANGUAGE 'plpgsql' VOLATILE;
