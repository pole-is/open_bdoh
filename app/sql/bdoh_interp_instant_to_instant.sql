DROP FUNCTION IF EXISTS bdoh_interp_instant_to_instant(INTEGER, TIMESTAMP, TIMESTAMP, DOUBLE PRECISION);

CREATE OR REPLACE FUNCTION bdoh_interp_instant_to_instant(
        idChronique   INTEGER,
        beginDate     TIMESTAMP,
        endDate       TIMESTAMP,
        stepInMinutes DOUBLE PRECISION
)
RETURNS TABLE(date TIMESTAMP, valeur DOUBLE PRECISION, code_qualite VARCHAR) AS $$
DECLARE

        jeuQualiteId   INTEGER;

        stepInSeconds  DOUBLE PRECISION;
        measuresCursor CURSOR FOR SELECT timest, val, qualOrder FROM bdoh_select_values_for_time_step(idChronique, beginDate, endDate);

        endSeconds     DOUBLE PRECISION;
        minSeconds     DOUBLE PRECISION;
        maxSeconds     DOUBLE PRECISION;

        secondsInterp  DOUBLE PRECISION;
        valueInterp    DOUBLE PRECISION;

        secondsBefore  DOUBLE PRECISION;
        valueBefore    DOUBLE PRECISION;
        qOrderBefore   INTEGER;

        secondsAfter   DOUBLE PRECISION;
        valueAfter     DOUBLE PRECISION;
        qOrderAfter    INTEGER;

        qOrderInterp   INTEGER;
        qCodeInterp    VARCHAR;
BEGIN

        -- Get quality set ID from time series
        jeuQualiteId := bdoh_jeu_qualite_id(idChronique);

        -- Interpolation time step should range from 1 minute to 1 day (1440 minutes)
        IF stepInMinutes < 1 THEN
            stepInSeconds := 1;
        ELSIF stepInMinutes > 1440 THEN
            stepInSeconds := 1440;
        ELSE
            stepInSeconds := stepInMinutes;
        END IF;
        -- Now, convert time step to seconds
        stepInSeconds := stepInSeconds * 60;

        -- Set 1st interpolation date (in seconds) & date not to exceed (endDate in seconds)
	secondsInterp := EXTRACT(EPOCH FROM beginDate AT TIME ZONE 'UTC');
	endSeconds    := EXTRACT(EPOCH FROM endDate AT TIME ZONE 'UTC');

        -- Find min date in seconds for time series
        minSeconds := EXTRACT(EPOCH FROM MIN(m.date) AT TIME ZONE 'UTC')
            FROM mesure m WHERE m.chronique_id = idChronique;
        -- Now, make sure that secondsInterp is not before minSeconds
        -- by incrementing it enough times by stepInSeconds
        WHILE secondsInterp < minSeconds LOOP
            secondsInterp := secondsInterp + stepInSeconds;
        END LOOP;

        -- Find max date in seconds for time series
        maxSeconds := EXTRACT(EPOCH FROM MAX(m.date) AT TIME ZONE 'UTC')
            FROM mesure m WHERE m.chronique_id = idChronique;
        -- Now set maxSeconds to the minimum value between itself and endSeconds
        IF endSeconds < maxSeconds THEN
            maxSeconds := endSeconds;
        END IF;

        -- Open cursor & fetch a 1st row
        OPEN measuresCursor;
        FETCH measuresCursor INTO secondsAfter, valueAfter, qOrderAfter;

        -- Loop over all dates between beginDate & max. possible date (endDate or max. date in time series)
        WHILE secondsInterp <= maxSeconds LOOP

            WHILE secondsAfter < secondsInterp LOOP

                secondsBefore := secondsAfter;
                valueBefore := valueAfter;
                qOrderBefore := qOrderAfter;

                FETCH measuresCursor INTO secondsAfter, valueAfter, qOrderAfter;

            END LOOP;

            IF secondsAfter = secondsInterp THEN
                -- Strict equality between secondsAfter and secondsInterp
                -- That will seldom happen, but still, it can happen!
                qOrderInterp := qOrderAfter;

                IF qOrderInterp > 200 THEN
                    valueInterp := valueAfter;
                ELSE
                    valueInterp := -9999;
                END IF;

            ELSE
                -- Quite more likely: we must interpolate between secondsBefore and secondsAfter
                -- Value is linearly interpolated, or set to -9999. Quality is the worse of the two boundaries.
                qOrderInterp := bdoh_cross_quality_orders(qOrderBefore, qOrderAfter);

                IF qOrderInterp > 200 THEN
                    valueInterp := ((secondsAfter - secondsInterp) * valueBefore
                                   + (secondsInterp - secondsBefore) * valueAfter)
                                   / (secondsAfter - secondsBefore);
                ELSE
                    valueInterp := -9999;
                END IF;

            END IF;
            
            SELECT q.code INTO qCodeInterp
                FROM qualite q
                WHERE q.jeu_id = jeuQualiteId
                AND q.ordre = qOrderInterp
                AND q.code <> 'gap';

            RETURN QUERY SELECT TO_TIMESTAMP(secondsInterp) AT TIME ZONE 'UTC', ROUND(valueInterp::NUMERIC, 3)::DOUBLE PRECISION, qCodeInterp;

            secondsInterp := secondsInterp + stepInSeconds;

        END LOOP;

        CLOSE measuresCursor;

END;
$$ LANGUAGE 'plpgsql' VOLATILE;