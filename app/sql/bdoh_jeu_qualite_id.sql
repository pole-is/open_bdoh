CREATE OR REPLACE FUNCTION bdoh_jeu_qualite_id(
        idChronique INTEGER
)
RETURNS INTEGER AS $$
DECLARE
        jeuQualiteId INTEGER;
BEGIN
        SELECT o.jeu_id INTO jeuQualiteId
            FROM observatoire o
            LEFT JOIN siteExperimental si ON si.observatoire_id = o.id
            LEFT JOIN stations_sites ss ON ss.site_id = si.id
            LEFT JOIN chronique c ON c.station_id = ss.station_id
            WHERE c.id = idChronique
            LIMIT 1;

        RETURN jeuQualiteId;

END;
$$ LANGUAGE 'plpgsql' VOLATILE;
