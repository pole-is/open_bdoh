-- Modification de l'ordre des qualités Draix
update qualite set ordre = 0 where code = '0' and jeu_id in (select id from jeuqualite where nom like 'Draix');
update qualite set ordre = 1 where code = '5' and jeu_id in (select id from jeuqualite where nom like 'Draix');
update qualite set ordre = 2 where code = '4' and jeu_id in (select id from jeuqualite where nom like 'Draix');
update qualite set ordre = 3 where code = '1' and jeu_id in (select id from jeuqualite where nom like 'Draix');
update qualite set ordre = 4 where code = '3' and jeu_id in (select id from jeuqualite where nom like 'Draix');
update qualite set ordre = 5 where code = '2' and jeu_id in (select id from jeuqualite where nom like 'Draix');