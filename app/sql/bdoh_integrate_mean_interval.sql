CREATE OR REPLACE FUNCTION bdoh_integrate_mean_interval(
        idChronique      INTEGER,
        beginDate        TIMESTAMP WITHOUT TIME ZONE,
        endDate          TIMESTAMP WITHOUT TIME ZONE
)
RETURNS TABLE(val DOUBLE PRECISION, quality_order INTEGER) AS $$
DECLARE
        measureDirection VARCHAR;

        minQualityOrder  INTEGER;
        integratedValue  DOUBLE PRECISION;
        qualWhereClause  VARCHAR;

        dateBefore       TIMESTAMP WITHOUT TIME ZONE;
        dateAfter        TIMESTAMP WITHOUT TIME ZONE;

        sec1             VARCHAR;
        sec2             VARCHAR;
        whereSec         VARCHAR;

        beginSeconds     DOUBLE PRECISION;
        endSeconds       DOUBLE PRECISION;
BEGIN

        -- Measure direction. If not found for time series, take default value 'ahead'
        SELECT c.directionMesure INTO measureDirection
            FROM chronique c WHERE c.id = idChronique;
        IF measureDirection IS NULL THEN measureDirection := 'ahead'; END IF;

        -- Last date before or at beginDate, & 1st date after or at endDate
        SELECT MAX(m.date) INTO dateBefore
            FROM mesure m WHERE m.chronique_id = idChronique
            AND m.date <= beginDate;

        SELECT MIN(m.date) INTO dateAfter
            FROM mesure m WHERE chronique_id = idChronique
            AND m.date >= endDate;

        -- Depending on whether measures look 'ahead' or 'backward', find worst quality for our date interval
        IF LOWER(measureDirection) = 'ahead' THEN
            qualWhereClause := 'm.date >= (SELECT MAX(date) FROM mesure WHERE chronique_id = $1 AND date <= $2) AND m.date < $3';
        ELSE
            qualWhereClause := 'm.date > $2 AND m.date <= (SELECT MIN(date) FROM mesure WHERE chronique_id = $1 AND date >= $3)';
        END IF;
        EXECUTE 'SELECT bdoh_min_quality_order(q.ordre) FROM qualite q '
                'LEFT JOIN mesure m ON m.qualite_id = q.id '
                'WHERE m.chronique_id = $1 '
                'AND ' || qualWhereClause
        USING idChronique, beginDate, endDate
        INTO minQualityOrder;

        -- Now, it is time to perform an integral
        IF dateBefore IS NULL OR dateAfter IS NULL THEN
            -- Integration cannot be achieved because we lack measures
            minQualityOrder := 100;
        END IF;
        IF minQualityOrder <= 200 THEN
            -- There was at least one gap or invalid measure
            integratedValue := -9999;
        ELSE
            -- We may compute an integrated value...
            beginSeconds := EXTRACT(EPOCH FROM beginDate AT TIME ZONE 'UTC');
            endSeconds := EXTRACT(EPOCH FROM endDate AT TIME ZONE 'UTC');

            IF LOWER(measureDirection) = 'ahead' THEN
                sec1 := 'seconds';
                sec2 := 'LEAD(seconds) OVER()';
                whereSec := 'sec_1 < $2';
            ELSE
                sec1 := 'LAG(seconds) OVER()';
                sec2 := 'seconds';
                whereSec := 'sec_2 > $1';
            END IF;

            EXECUTE
            'WITH sv(seconds, value) AS ('
            '    SELECT'
            '    CASE'
            '        WHEN x.seconds < $1 THEN $1'
            '        WHEN x.seconds > $2 THEN $2'
            '        ELSE x.seconds'
            '    END,'
            '    x.valeur'
            '    FROM ('
            '        SELECT EXTRACT(EPOCH FROM date AT TIME ZONE ''UTC'') AS seconds, valeur'
            '        FROM mesure'
            '        WHERE chronique_id = $3 AND date >= $4 and date <= $5'
            '        ORDER BY date'
            '    ) x'
            '), '
            'ssv(sec_1, sec_2, value) AS ('
            '    SELECT ' || sec1 || ', ' || sec2 || ', value FROM sv'
            ') '
            'SELECT SUM((sec_2 - sec_1) * value) FROM ssv WHERE ' || whereSec
            USING beginSeconds, endSeconds, idChronique, dateBefore, dateAfter
            INTO integratedValue;

        END IF;

        RETURN QUERY SELECT integratedValue, minQualityOrder;

END;
$$ LANGUAGE 'plpgsql' VOLATILE;