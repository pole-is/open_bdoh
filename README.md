OpenBDOH - Base de Données des Observatoires Hydrologiques
======================================================

Ce projet a pour vocation d'ouvrir l'application BDOH aux acteurs extérieurs à INRAE, instituts de recherche ou bureaux d'étude, pouvant en bénéficier dans le cadre de leurs activités liées à l'hydrologie.

## Installation

Consulter [le guide d'installation](doc/guide_installation_OpenBDOH.md).
